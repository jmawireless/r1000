<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC-JMA">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
<text x="162.56" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="217.17" y="20.32" size="2.54" layer="94" font="fixed">RGANDHI</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC-JMA" prefix="FRAME">
<gates>
<gate name="G$1" symbol="A4L-LOC-JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR">
<packages>
<package name="QFP50P1400X1400X120-80N">
<smd name="1" x="-6.7" y="4.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="2" x="-6.7" y="4.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="3" x="-6.7" y="3.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="4" x="-6.7" y="3.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="5" x="-6.7" y="2.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="6" x="-6.7" y="2.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="7" x="-6.7" y="1.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="8" x="-6.7" y="1.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="9" x="-6.7" y="0.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="10" x="-6.7" y="0.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="11" x="-6.7" y="-0.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="12" x="-6.7" y="-0.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="13" x="-6.7" y="-1.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="14" x="-6.7" y="-1.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="15" x="-6.7" y="-2.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="16" x="-6.7" y="-2.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="17" x="-6.7" y="-3.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="18" x="-6.7" y="-3.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="19" x="-6.7" y="-4.25" dx="1.5" dy="0.3" layer="1"/>
<smd name="20" x="-6.7" y="-4.75" dx="1.5" dy="0.3" layer="1"/>
<smd name="21" x="-4.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="-4.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="-3.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="-3.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="25" x="-2.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="26" x="-2.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="27" x="-1.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="28" x="-1.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="29" x="-0.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="30" x="-0.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="31" x="0.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="32" x="0.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="33" x="1.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="34" x="1.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="35" x="2.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="36" x="2.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="37" x="3.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="38" x="3.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="39" x="4.25" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="40" x="4.75" y="-6.7" dx="1.5" dy="0.3" layer="1" rot="R90"/>
<smd name="41" x="6.7" y="-4.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="42" x="6.7" y="-4.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="43" x="6.7" y="-3.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="44" x="6.7" y="-3.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="45" x="6.7" y="-2.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="46" x="6.7" y="-2.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="47" x="6.7" y="-1.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="48" x="6.7" y="-1.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="49" x="6.7" y="-0.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="50" x="6.7" y="-0.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="51" x="6.7" y="0.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="52" x="6.7" y="0.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="53" x="6.7" y="1.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="54" x="6.7" y="1.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="55" x="6.7" y="2.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="56" x="6.7" y="2.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="57" x="6.7" y="3.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="58" x="6.7" y="3.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="59" x="6.7" y="4.25" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="60" x="6.7" y="4.75" dx="1.5" dy="0.3" layer="1" rot="R180"/>
<smd name="61" x="4.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="62" x="4.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="63" x="3.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="64" x="3.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="65" x="2.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="66" x="2.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="67" x="1.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="68" x="1.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="69" x="0.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="70" x="0.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="71" x="-0.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="72" x="-0.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="73" x="-1.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="74" x="-1.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="75" x="-2.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="76" x="-2.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="77" x="-3.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="78" x="-3.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="79" x="-4.25" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<smd name="80" x="-4.75" y="6.7" dx="1.5" dy="0.3" layer="1" rot="R270"/>
<wire x1="-4.5" y1="5.5" x2="5.5" y2="5.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="5.5" x2="5.5" y2="-5.5" width="0.127" layer="21"/>
<wire x1="5.5" y1="-5.5" x2="-5.5" y2="-5.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-5.5" x2="-5.5" y2="4.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="4.5" x2="-4.5" y2="5.5" width="0.127" layer="21"/>
<rectangle x1="-7" y1="5.5" x2="-6" y2="6.5" layer="21"/>
<wire x1="-4.5" y1="5.5" x2="5.5" y2="5.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="5.5" x2="5.5" y2="-5.5" width="0.127" layer="51"/>
<wire x1="5.5" y1="-5.5" x2="-5.5" y2="-5.5" width="0.127" layer="51"/>
<wire x1="-5.5" y1="-5.5" x2="-5.5" y2="4.5" width="0.127" layer="51"/>
<wire x1="-5.5" y1="4.5" x2="-4.5" y2="5.5" width="0.127" layer="51"/>
<rectangle x1="-7" y1="5.5" x2="-6" y2="6.5" layer="51"/>
<wire x1="-7.5" y1="7.5" x2="7.5" y2="7.5" width="0.127" layer="39"/>
<wire x1="7.5" y1="7.5" x2="7.5" y2="-7.5" width="0.127" layer="39"/>
<wire x1="7.5" y1="-7.5" x2="-7.5" y2="-7.5" width="0.127" layer="39"/>
<wire x1="-7.5" y1="-7.5" x2="-7.5" y2="7.5" width="0.127" layer="39"/>
<text x="-7.366" y="7.874" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-7.366" y="8.89" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="RESC1005X40N">
<wire x1="-0.48" y1="-0.94" x2="-0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.94" x2="0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.94" x2="0.48" y2="-0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.94" x2="-0.48" y2="-0.94" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="RES2013X38N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC1005X55N">
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="CAPC1608X55N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="HC49_US_SMD">
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-5.715" y1="1.397" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="5.715" y1="1.397" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TSW-105-XX-X-S">
<wire x1="-6.479" y1="1.155" x2="6.479" y2="1.155" width="0.2032" layer="21"/>
<wire x1="6.479" y1="1.155" x2="6.479" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="6.479" y1="-1.155" x2="-6.479" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-6.479" y1="-1.155" x2="-6.479" y2="1.155" width="0.2032" layer="21"/>
<pad name="1" x="5.08" y="0" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="2" x="2.54" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-2.54" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="5" x="-5.08" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-6.985" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="9.525" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.43" y1="-0.35" x2="-4.73" y2="0.35" layer="51"/>
<rectangle x1="-2.89" y1="-0.35" x2="-2.19" y2="0.35" layer="51"/>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
<rectangle x1="2.19" y1="-0.35" x2="2.89" y2="0.35" layer="51"/>
<rectangle x1="4.73" y1="-0.35" x2="5.43" y2="0.35" layer="51"/>
<wire x1="5.08" y1="1.5875" x2="6.985" y2="1.5875" width="0.127" layer="21"/>
<wire x1="6.985" y1="1.5875" x2="6.985" y2="0" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.5875" x2="6.985" y2="1.5875" width="0.127" layer="51"/>
<wire x1="6.985" y1="1.5875" x2="6.985" y2="0" width="0.127" layer="51"/>
<wire x1="-6.479" y1="1.155" x2="6.479" y2="1.155" width="0.2032" layer="51"/>
<wire x1="6.479" y1="1.155" x2="6.479" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="6.479" y1="-1.155" x2="-6.479" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-6.479" y1="-1.155" x2="-6.479" y2="1.155" width="0.2032" layer="51"/>
</package>
<package name="QFN65P700X700X80-33N">
<smd name="1" x="-3.35" y="2.275" dx="1" dy="0.35" layer="1"/>
<smd name="2" x="-3.35" y="1.625" dx="1" dy="0.35" layer="1"/>
<smd name="3" x="-3.35" y="0.975" dx="1" dy="0.35" layer="1"/>
<smd name="4" x="-3.35" y="0.325" dx="1" dy="0.35" layer="1"/>
<smd name="5" x="-3.35" y="-0.325" dx="1" dy="0.35" layer="1"/>
<smd name="6" x="-3.35" y="-0.975" dx="1" dy="0.35" layer="1"/>
<smd name="7" x="-3.35" y="-1.625" dx="1" dy="0.35" layer="1"/>
<smd name="8" x="-3.35" y="-2.275" dx="1" dy="0.35" layer="1"/>
<smd name="9" x="-2.275" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="10" x="-1.625" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="11" x="-0.975" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="12" x="-0.325" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="13" x="0.325" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="14" x="0.975" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="15" x="1.625" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="16" x="2.275" y="-3.35" dx="1" dy="0.35" layer="1" rot="R90"/>
<smd name="17" x="3.35" y="-2.275" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="18" x="3.35" y="-1.625" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="19" x="3.35" y="-0.975" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="20" x="3.35" y="-0.325" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="21" x="3.35" y="0.325" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="22" x="3.35" y="0.975" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="23" x="3.35" y="1.625" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="24" x="3.35" y="2.275" dx="1" dy="0.35" layer="1" rot="R180"/>
<smd name="25" x="2.275" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="26" x="1.625" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="27" x="0.975" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="28" x="0.325" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="29" x="-0.325" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="30" x="-0.975" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="31" x="-1.625" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="32" x="-2.275" y="3.35" dx="1" dy="0.35" layer="1" rot="R270"/>
<smd name="33" x="0" y="0" dx="4.85" dy="4.85" layer="1"/>
<wire x1="-2.8275" y1="4.1275" x2="-4.1275" y2="2.652" width="0.127" layer="51"/>
<wire x1="-4.1275" y1="2.652" x2="-4.1275" y2="-4.1275" width="0.127" layer="51"/>
<wire x1="-4.1275" y1="-4.1275" x2="4.1275" y2="-4.1275" width="0.127" layer="51"/>
<wire x1="4.1275" y1="-4.1275" x2="4.1275" y2="4.1275" width="0.127" layer="51"/>
<wire x1="4.1275" y1="4.1275" x2="-2.8275" y2="4.1275" width="0.127" layer="51"/>
<wire x1="-2.8275" y1="4.1275" x2="-4.1275" y2="2.652" width="0.127" layer="21"/>
<wire x1="-4.1275" y1="2.652" x2="-4.1275" y2="-4.1275" width="0.127" layer="21"/>
<wire x1="-4.1275" y1="-4.1275" x2="4.1275" y2="-4.1275" width="0.127" layer="21"/>
<wire x1="4.1275" y1="-4.1275" x2="4.1275" y2="4.1275" width="0.127" layer="21"/>
<wire x1="4.1275" y1="4.1275" x2="-2.8275" y2="4.1275" width="0.127" layer="21"/>
<text x="-3.9" y="4.55" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3.9" y="5.85" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<rectangle x1="-2" y1="2.8" x2="-1.9" y2="3.9" layer="41"/>
<rectangle x1="1.9" y1="2.8" x2="2" y2="3.9" layer="41"/>
<rectangle x1="-0.7" y1="-3.9" x2="-0.6" y2="-2.8" layer="41"/>
</package>
<package name="ONSEMI_LM317">
<smd name="1" x="-2.54" y="-10.213" dx="1.016" dy="3.504" layer="1"/>
<smd name="2" x="0" y="0" dx="10.49" dy="8.38" layer="1"/>
<smd name="3" x="2.54" y="-10.213" dx="1.016" dy="3.504" layer="1"/>
<wire x1="-6" y1="5" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-4" y2="-6" width="0.127" layer="21"/>
<wire x1="-4" y1="-6" x2="-4" y2="-13" width="0.127" layer="21"/>
<wire x1="-4" y1="-13" x2="-1" y2="-13" width="0.127" layer="21"/>
<wire x1="-1" y1="-13" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1" y1="-6" x2="1" y2="-6" width="0.127" layer="21"/>
<wire x1="1" y1="-6" x2="1" y2="-13" width="0.127" layer="21"/>
<wire x1="1" y1="-13" x2="4" y2="-13" width="0.127" layer="21"/>
<wire x1="4" y1="-13" x2="4" y2="-6" width="0.127" layer="21"/>
<wire x1="4" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="5" width="0.127" layer="21"/>
<wire x1="6" y1="5" x2="-6" y2="5" width="0.127" layer="21"/>
<wire x1="-6" y1="5" x2="-6" y2="-6" width="0.127" layer="51"/>
<wire x1="-6" y1="-6" x2="-4" y2="-6" width="0.127" layer="51"/>
<wire x1="-4" y1="-6" x2="-4" y2="-13" width="0.127" layer="51"/>
<wire x1="-4" y1="-13" x2="-1" y2="-13" width="0.127" layer="51"/>
<wire x1="-1" y1="-13" x2="-1" y2="-6" width="0.127" layer="51"/>
<wire x1="-1" y1="-6" x2="1" y2="-6" width="0.127" layer="51"/>
<wire x1="1" y1="-6" x2="1" y2="-13" width="0.127" layer="51"/>
<wire x1="1" y1="-13" x2="4" y2="-13" width="0.127" layer="51"/>
<wire x1="4" y1="-13" x2="4" y2="-6" width="0.127" layer="51"/>
<wire x1="4" y1="-6" x2="6" y2="-6" width="0.127" layer="51"/>
<wire x1="6" y1="-6" x2="6" y2="5" width="0.127" layer="51"/>
<wire x1="6" y1="5" x2="-6" y2="5" width="0.127" layer="51"/>
<text x="-6" y="6" size="1.27" layer="25">&gt;NAME</text>
<text x="-6" y="8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="AISG_C091-C006-804-2">
<pad name="1" x="3.5" y="0" drill="1.1"/>
<pad name="3" x="-3.5" y="0" drill="1.1"/>
<pad name="4" x="2.475" y="2.475" drill="1.1"/>
<pad name="5" x="-2.475" y="2.475" drill="1.1"/>
<pad name="6" x="2.475" y="-2.475" drill="1.1"/>
<pad name="7" x="-2.475" y="-2.475" drill="1.1"/>
<circle x="0" y="0" radius="9.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.95" width="0.127" layer="21"/>
<pad name="PAD" x="-4.25" y="7.3612" drill="1.2"/>
<wire x1="-1" y1="-5" x2="1" y2="-5" width="0.127" layer="21" curve="-180"/>
<wire x1="-1" y1="-5" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="1" y1="-5" x2="1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-6" x2="1.5" y2="-6" width="0.127" layer="21"/>
<text x="5.08" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="6.1" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="7.62" y="-7.62" size="1.4224" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.4224" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-1.27" y="-8.255" size="1.016" layer="25" font="vector" ratio="15">MALE</text>
<pad name="2" x="0" y="3.5" drill="1.1"/>
<pad name="8" x="0" y="-0.7" drill="1.1"/>
<text x="0.6" y="5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.42" y="5" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="-0.42" y="0.5" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.6" y="0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-2" y="7" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
<text x="-3" y="7" size="1.4224" layer="21" font="vector" ratio="15">P</text>
</package>
<package name="AISG_C091-G006-804-2">
<pad name="3" x="3.5" y="0" drill="1.1"/>
<pad name="1" x="-3.5" y="0" drill="1.1"/>
<pad name="5" x="2.475" y="2.475" drill="1.1"/>
<pad name="4" x="-2.475" y="2.475" drill="1.1"/>
<pad name="7" x="2.475" y="-2.475" drill="1.1"/>
<pad name="6" x="-2.475" y="-2.475" drill="1.1"/>
<circle x="0" y="0" radius="9.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.95" width="0.127" layer="21"/>
<pad name="PAD" x="-4.25" y="7.3612" drill="1.2"/>
<wire x1="1" y1="-5.16" x2="-1" y2="-5.16" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="-5.16" x2="1" y2="-4.16" width="0.127" layer="21"/>
<wire x1="-1" y1="-5.16" x2="-1" y2="-4.16" width="0.127" layer="21"/>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="5" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="6" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="7.62" y="-7.62" size="1.4224" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.4224" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-2.54" y="-8.255" size="1.016" layer="25" font="vector" ratio="15">FEMALE</text>
<pad name="2" x="0" y="3.5" drill="1.1"/>
<pad name="8" x="0" y="-0.7" drill="1.1"/>
<text x="-0.5" y="5" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="0.5" y="5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.5" y="0.5" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.5" y="0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-3" y="7" size="1.4224" layer="21" font="vector" ratio="15">P</text>
<text x="-2" y="7" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
</package>
<package name="ONSEMI_1SMC5.0AT3G">
<smd name="1" x="-3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<smd name="2" x="3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<wire x1="-4" y1="2.5" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="3" x2="-4" y2="2" width="0.127" layer="51"/>
<wire x1="-4" y1="2" x2="-5" y2="2" width="0.127" layer="51"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.127" layer="51"/>
<wire x1="-5" y1="-2" x2="-4" y2="-2" width="0.127" layer="51"/>
<wire x1="-4" y1="-2" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="2" width="0.127" layer="51"/>
<wire x1="4" y1="2" x2="5" y2="2" width="0.127" layer="51"/>
<wire x1="5" y1="2" x2="5" y2="-2" width="0.127" layer="51"/>
<wire x1="5" y1="-2" x2="4" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-2" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="21"/>
<text x="-5.08" y="3.81" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="0" y="3.81" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
</package>
<package name="2035-XX-SM">
<smd name="1" x="-2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="51"/>
<text x="-3" y="4" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="6" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-4" y1="3" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3.5" y2="3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3" x2="-3.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3.5" y2="3" width="0.127" layer="51"/>
<wire x1="-3.5" y1="3" x2="-3.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="-4" y2="3" width="0.127" layer="51"/>
</package>
<package name="TI_TCA9406_DCU">
<smd name="1" x="-0.75" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-0.25" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.25" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0.75" y="-1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.75" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.25" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-0.25" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="1.55" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<wire x1="-1.25" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.25" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="1.25" y2="-1.5" width="0.127" layer="51"/>
<rectangle x1="-2.5" y1="-1.5" x2="-2" y2="-1" layer="51"/>
<rectangle x1="-2.5" y1="-1.5" x2="-2" y2="-1" layer="21"/>
<text x="-1.5" y="2.5" size="1.016" layer="21" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.5" y="3.75" size="1.016" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TSW-104-XX-X-S">
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="21"/>
<pad name="1" x="3.81" y="0" drill="1" diameter="1.5" shape="square" rot="R180"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="3" x="-1.27" y="0" drill="1" diameter="1.5" rot="R180"/>
<pad name="4" x="-3.81" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-5.715" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="8.255" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.16" y1="-0.35" x2="-3.46" y2="0.35" layer="51"/>
<rectangle x1="-1.62" y1="-0.35" x2="-0.92" y2="0.35" layer="51"/>
<rectangle x1="0.92" y1="-0.35" x2="1.62" y2="0.35" layer="51"/>
<rectangle x1="3.46" y1="-0.35" x2="4.16" y2="0.35" layer="51"/>
<wire x1="5.715" y1="0.9525" x2="5.715" y2="-0.9525" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.5875" x2="5.715" y2="1.5875" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.5875" x2="5.715" y2="0" width="0.127" layer="51"/>
<wire x1="-5.209" y1="1.155" x2="5.209" y2="1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="1.155" x2="5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="5.209" y1="-1.155" x2="-5.209" y2="-1.155" width="0.2032" layer="51"/>
<wire x1="-5.209" y1="-1.155" x2="-5.209" y2="1.155" width="0.2032" layer="51"/>
</package>
<package name="LEDC1005X60N">
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="51"/>
</package>
<package name="LEDC1608X75N/80N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.825" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.825" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.65" x2="0.675" y2="-1.65" width="0.127" layer="21"/>
</package>
<package name="SOIC127P600X173-8N">
<smd name="1" x="-2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="2" x="-2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="3" x="-2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="4" x="-2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="5" x="2.7" y="-1.905" dx="1.6" dy="0.6" layer="1"/>
<smd name="6" x="2.7" y="-0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="7" x="2.7" y="0.635" dx="1.6" dy="0.6" layer="1"/>
<smd name="8" x="2.7" y="1.905" dx="1.6" dy="0.6" layer="1"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="3" x2="-2.5" y2="3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="51"/>
<wire x1="2.5" y1="3" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-3" x2="2.5" y2="-3" width="0.127" layer="51"/>
<wire x1="2.5" y1="-3" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-2.5" y1="3.5" x2="-2" y2="4" layer="51"/>
<rectangle x1="-2.5" y1="3.5" x2="-2" y2="4" layer="21"/>
<text x="-1.27" y="3.81" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.27" y="5.08" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="-3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.127" layer="51"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="51"/>
</package>
<package name="NXP_SOD128">
<smd name="1" x="-2.2" y="0" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.2" y="0" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-3.5" y1="-2.5" x2="3.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-2.5" x2="3.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="2.5" x2="-3.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2.5" x2="-3.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="2.5" x2="-4" y2="-2.5" width="0.127" layer="21"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" font="fixed" ratio="15">&gt;NAME</text>
<text x="-3.81" y="5.08" size="1.27" layer="27" font="fixed" ratio="15">&gt;VALUE</text>
</package>
<package name="DFN6X5MM">
<smd name="1" x="-1.905" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-0.635" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.635" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="1.905" y="-2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="1.905" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.635" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-0.635" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-1.905" y="2.8" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="0" y="0" dx="2.4" dy="4.1" layer="1" rot="R90"/>
<wire x1="-2.52" y1="1.5" x2="-2.52" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.52" y1="1.5" x2="2.52" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.25" x2="-2.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.25" x2="-2.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-1.5" x2="-2.75" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.5" x2="-2.75" y2="-1.25" width="0.127" layer="21"/>
<text x="-3.5" y="-1" size="0.8128" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="-4.5" y="-1" size="0.8128" layer="27" font="vector" ratio="15" rot="R90">&gt;VALUE</text>
</package>
<package name="TDFN50PC300X200X75-9N">
<smd name="1" x="-0.75" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-0.25" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="0.25" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="0.75" y="-1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="0.75" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.25" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-0.25" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="1.5" dx="0.75" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="0" y="0" dx="1.36" dy="1.46" layer="1" rot="R90"/>
<wire x1="-1.25" y1="1.5" x2="-1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.25" y1="1.5" x2="1.25" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.75" y1="-1.25" x2="-1.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.25" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.75" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.75" y1="-1.5" x2="-1.75" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1.5" x2="-1.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.25" y1="1.5" x2="1.25" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.25" x2="-1.75" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.25" x2="-1.75" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.25" width="0.127" layer="51"/>
<text x="-1.5" y="-1" size="0.8128" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="-2.5" y="-1" size="0.8128" layer="27" font="vector" ratio="15" rot="R90">&gt;VALUE</text>
</package>
<package name="RES5750X230N">
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<text x="4" y="-2" size="0.635" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-3" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="51"/>
</package>
<package name="DIODE1005X60N">
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.5" dy="0.7" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.635" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.52" y1="-0.93" x2="-0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.52" y1="0.93" x2="0.52" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="0.93" x2="0.52" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.52" y1="-0.93" x2="-0.52" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-1.15" x2="0.5" y2="-1.15" width="0.127" layer="51"/>
</package>
<package name="SOT-23">
<smd name="P$1" x="0" y="-1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="P$2" x="-0.95" y="1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<smd name="P$3" x="0.95" y="1" dx="0.9" dy="0.8" layer="1" rot="R90"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="2" y2="1" width="0.127" layer="21"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="21"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.127" layer="51"/>
<wire x1="-2" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="1" x2="2" y2="-1" width="0.127" layer="51"/>
<wire x1="2" y1="-1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="-0.25" y1="1" x2="0.25" y2="1" width="0.127" layer="51"/>
<text x="-2" y="2" size="1.27" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-2" y="4" size="1.27" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="DO-214AC_450X280">
<wire x1="-2.02" y1="-3.18" x2="-2.02" y2="3.18" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.18" x2="2.02" y2="3.18" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.18" x2="2.02" y2="-3.18" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.18" x2="-2.02" y2="-3.18" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.5" dy="3.6" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.5" dy="3.6" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.18" x2="-2.02" y2="3.18" width="0.127" layer="51"/>
<wire x1="-2.02" y1="3.18" x2="2.02" y2="3.18" width="0.127" layer="51"/>
<wire x1="2.02" y1="3.18" x2="2.02" y2="-3.18" width="0.127" layer="51"/>
<wire x1="2.02" y1="-3.18" x2="-2.02" y2="-3.18" width="0.127" layer="51" style="shortdash"/>
<wire x1="-2" y1="-3.4" x2="2" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.4" x2="2" y2="-3.4" width="0.127" layer="51"/>
</package>
<package name="TP_1.5MM">
<pad name="1" x="0" y="0" drill="1" diameter="1.5" rot="R180"/>
<text x="-1.905" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.35" y1="-0.35" x2="0.35" y2="0.35" layer="51"/>
</package>
<package name="CAPC2013X145N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2.111" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.1625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="PANASONIC_EEE1CA101WP">
<smd name="1" x="0" y="2.5" dx="3.2" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="0" y="-2.5" dx="3.2" dy="1.6" layer="1" rot="R90"/>
<wire x1="-1.27" y1="3.81" x2="-3.58" y2="3.81" width="0.127" layer="21"/>
<wire x1="-3.58" y1="3.81" x2="-3.58" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.58" y1="-3.81" x2="-1.27" y2="-3.81" width="0.127" layer="21"/>
<wire x1="1.27" y1="3.81" x2="3.58" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.58" y1="3.81" x2="3.58" y2="-3.81" width="0.127" layer="21"/>
<wire x1="3.58" y1="-3.81" x2="1.27" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="-3.58" y2="3.81" width="0.127" layer="51"/>
<wire x1="-3.58" y1="3.81" x2="-3.58" y2="-3.81" width="0.127" layer="51"/>
<wire x1="-3.58" y1="-3.81" x2="-1.27" y2="-3.81" width="0.127" layer="51"/>
<wire x1="1.27" y1="3.81" x2="3.58" y2="3.81" width="0.127" layer="51"/>
<wire x1="3.58" y1="3.81" x2="3.58" y2="-3.81" width="0.127" layer="51"/>
<wire x1="3.58" y1="-3.81" x2="1.27" y2="-3.81" width="0.127" layer="51"/>
<text x="1.27" y="3.81" size="1.27" layer="51" font="vector" ratio="15">+</text>
<text x="-2.54" y="3.81" size="1.27" layer="21" font="vector" ratio="15">+</text>
<text x="-3" y="5" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-3" y="6" size="1.016" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="TDK_LGJ12565_TS_XXX_M_XXX_H">
<wire x1="6.425" y1="7.22" x2="6.425" y2="-7.22" width="0.127" layer="21"/>
<wire x1="6.425" y1="-7.22" x2="-6.425" y2="-7.22" width="0.127" layer="21"/>
<wire x1="-6.425" y1="-7.22" x2="-6.425" y2="7.22" width="0.127" layer="21"/>
<wire x1="-6.425" y1="7.22" x2="6.425" y2="7.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="5.55" dx="2.6" dy="3.2" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-5.55" dx="2.6" dy="3.2" layer="1" roundness="25" rot="R270"/>
<text x="-7" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-7" y="-7" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="6.425" y1="7.22" x2="6.425" y2="-7.22" width="0.127" layer="51"/>
<wire x1="6.425" y1="-7.22" x2="-6.425" y2="-7.22" width="0.127" layer="51"/>
<wire x1="-6.425" y1="-7.22" x2="-6.425" y2="7.22" width="0.127" layer="51"/>
<wire x1="-6.425" y1="7.22" x2="6.425" y2="7.22" width="0.127" layer="51"/>
</package>
<package name="IND445X406X180">
<wire x1="2.425" y1="2.97" x2="2.425" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.425" y1="-3.22" x2="-2.425" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.425" y1="-3.22" x2="-2.425" y2="2.97" width="0.127" layer="21"/>
<wire x1="-2.425" y1="2.97" x2="2.425" y2="2.97" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.85" dx="1.5" dy="2.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.85" dx="1.5" dy="2.24" layer="1" roundness="25" rot="R270"/>
<text x="4" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-3" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2.425" y1="2.97" x2="2.425" y2="-3.22" width="0.127" layer="51"/>
<wire x1="2.425" y1="-3.22" x2="-2.425" y2="-3.22" width="0.127" layer="51"/>
<wire x1="-2.425" y1="-3.22" x2="-2.425" y2="2.97" width="0.127" layer="51"/>
<wire x1="-2.425" y1="2.97" x2="2.425" y2="2.97" width="0.127" layer="51"/>
</package>
<package name="DO-214AC_475X290">
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.25" dx="1.7" dy="2.5" layer="1" roundness="25" rot="R270"/>
<text x="3" y="-2" size="0.635" layer="21" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.25" y="-2" size="0.635" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.02" y1="-3.43" x2="-2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="-2.02" y1="3.43" x2="2.02" y2="3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="3.43" x2="2.02" y2="-3.43" width="0.127" layer="21"/>
<wire x1="2.02" y1="-3.43" x2="-2.02" y2="-3.43" width="0.127" layer="21" style="shortdash"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
<wire x1="-2" y1="-3.65" x2="2" y2="-3.65" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="MICROCHIP_PIC18F87J11">
<pin name="1" x="-5.08" y="-10.16" visible="pad" length="middle"/>
<text x="27.94" y="-2.54" size="1.778" layer="94">Microchip_PIC18F8XJ11</text>
<pin name="2" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-15.24" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-17.78" visible="pad" length="middle"/>
<pin name="5" x="-5.08" y="-20.32" visible="pad" length="middle"/>
<pin name="6" x="-5.08" y="-22.86" visible="pad" length="middle"/>
<pin name="7" x="-5.08" y="-25.4" visible="pad" length="middle"/>
<pin name="8" x="-5.08" y="-27.94" visible="pad" length="middle"/>
<pin name="9" x="-5.08" y="-30.48" visible="pad" length="middle"/>
<pin name="10" x="-5.08" y="-33.02" visible="pad" length="middle"/>
<pin name="11" x="-5.08" y="-35.56" visible="pad" length="middle"/>
<pin name="12" x="-5.08" y="-38.1" visible="pad" length="middle"/>
<pin name="13" x="-5.08" y="-40.64" visible="pad" length="middle"/>
<pin name="14" x="-5.08" y="-43.18" visible="pad" length="middle"/>
<pin name="15" x="-5.08" y="-45.72" visible="pad" length="middle"/>
<pin name="16" x="-5.08" y="-48.26" visible="pad" length="middle"/>
<pin name="17" x="-5.08" y="-50.8" visible="pad" length="middle"/>
<pin name="18" x="-5.08" y="-53.34" visible="pad" length="middle"/>
<pin name="19" x="-5.08" y="-55.88" visible="pad" length="middle"/>
<pin name="20" x="-5.08" y="-58.42" visible="pad" length="middle"/>
<pin name="21" x="-5.08" y="-60.96" visible="pad" length="middle"/>
<pin name="22" x="-5.08" y="-63.5" visible="pad" length="middle"/>
<pin name="23" x="-5.08" y="-66.04" visible="pad" length="middle"/>
<pin name="24" x="-5.08" y="-68.58" visible="pad" length="middle"/>
<pin name="25" x="-5.08" y="-71.12" visible="pad" length="middle"/>
<pin name="26" x="-5.08" y="-73.66" visible="pad" length="middle"/>
<pin name="27" x="-5.08" y="-76.2" visible="pad" length="middle"/>
<pin name="28" x="-5.08" y="-78.74" visible="pad" length="middle"/>
<pin name="29" x="-5.08" y="-81.28" visible="pad" length="middle"/>
<pin name="30" x="-5.08" y="-83.82" visible="pad" length="middle"/>
<pin name="31" x="-5.08" y="-86.36" visible="pad" length="middle"/>
<pin name="32" x="-5.08" y="-88.9" visible="pad" length="middle"/>
<pin name="33" x="-5.08" y="-91.44" visible="pad" length="middle"/>
<pin name="34" x="-5.08" y="-93.98" visible="pad" length="middle"/>
<pin name="35" x="-5.08" y="-96.52" visible="pad" length="middle"/>
<pin name="36" x="-5.08" y="-99.06" visible="pad" length="middle"/>
<pin name="37" x="-5.08" y="-101.6" visible="pad" length="middle"/>
<pin name="38" x="-5.08" y="-104.14" visible="pad" length="middle"/>
<pin name="39" x="-5.08" y="-106.68" visible="pad" length="middle"/>
<pin name="40" x="-5.08" y="-109.22" visible="pad" length="middle"/>
<pin name="41" x="81.28" y="-109.22" visible="pad" length="middle" rot="R180"/>
<pin name="42" x="81.28" y="-106.68" visible="pad" length="middle" rot="R180"/>
<pin name="43" x="81.28" y="-104.14" visible="pad" length="middle" rot="R180"/>
<pin name="44" x="81.28" y="-101.6" visible="pad" length="middle" rot="R180"/>
<pin name="45" x="81.28" y="-99.06" visible="pad" length="middle" rot="R180"/>
<pin name="46" x="81.28" y="-96.52" visible="pad" length="middle" rot="R180"/>
<pin name="47" x="81.28" y="-93.98" visible="pad" length="middle" rot="R180"/>
<pin name="48" x="81.28" y="-91.44" visible="pad" length="middle" rot="R180"/>
<pin name="49" x="81.28" y="-88.9" visible="pad" length="middle" rot="R180"/>
<pin name="50" x="81.28" y="-86.36" visible="pad" length="middle" rot="R180"/>
<pin name="51" x="81.28" y="-83.82" visible="pad" length="middle" rot="R180"/>
<pin name="52" x="81.28" y="-81.28" visible="pad" length="middle" rot="R180"/>
<pin name="53" x="81.28" y="-78.74" visible="pad" length="middle" rot="R180"/>
<pin name="54" x="81.28" y="-76.2" visible="pad" length="middle" rot="R180"/>
<pin name="55" x="81.28" y="-73.66" visible="pad" length="middle" rot="R180"/>
<pin name="56" x="81.28" y="-71.12" visible="pad" length="middle" rot="R180"/>
<pin name="57" x="81.28" y="-68.58" visible="pad" length="middle" rot="R180"/>
<pin name="58" x="81.28" y="-66.04" visible="pad" length="middle" rot="R180"/>
<pin name="59" x="81.28" y="-63.5" visible="pad" length="middle" rot="R180"/>
<pin name="60" x="81.28" y="-60.96" visible="pad" length="middle" rot="R180"/>
<pin name="61" x="81.28" y="-58.42" visible="pad" length="middle" rot="R180"/>
<pin name="62" x="81.28" y="-55.88" visible="pad" length="middle" rot="R180"/>
<pin name="63" x="81.28" y="-53.34" visible="pad" length="middle" rot="R180"/>
<pin name="64" x="81.28" y="-50.8" visible="pad" length="middle" rot="R180"/>
<pin name="65" x="81.28" y="-48.26" visible="pad" length="middle" rot="R180"/>
<pin name="66" x="81.28" y="-45.72" visible="pad" length="middle" rot="R180"/>
<pin name="67" x="81.28" y="-43.18" visible="pad" length="middle" rot="R180"/>
<pin name="68" x="81.28" y="-40.64" visible="pad" length="middle" rot="R180"/>
<pin name="69" x="81.28" y="-38.1" visible="pad" length="middle" rot="R180"/>
<pin name="70" x="81.28" y="-35.56" visible="pad" length="middle" rot="R180"/>
<pin name="71" x="81.28" y="-33.02" visible="pad" length="middle" rot="R180"/>
<pin name="72" x="81.28" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="73" x="81.28" y="-27.94" visible="pad" length="middle" rot="R180"/>
<pin name="74" x="81.28" y="-25.4" visible="pad" length="middle" rot="R180"/>
<pin name="75" x="81.28" y="-22.86" visible="pad" length="middle" rot="R180"/>
<pin name="76" x="81.28" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="77" x="81.28" y="-17.78" visible="pad" length="middle" rot="R180"/>
<pin name="78" x="81.28" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="79" x="81.28" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="80" x="81.28" y="-10.16" visible="pad" length="middle" rot="R180"/>
<text x="2.54" y="-10.16" size="1.778" layer="94">RH2/A18/PMD7</text>
<text x="2.54" y="-12.7" size="1.778" layer="94">RH3/A19/PMD6</text>
<text x="2.54" y="-15.24" size="1.778" layer="94">RE1/AD9/PMWR/P2C</text>
<text x="2.54" y="-17.78" size="1.778" layer="94">RE0/AD8/PMRD/P2D</text>
<text x="2.54" y="-20.32" size="1.778" layer="94">RG0/PMA8/ECCP3/P3A</text>
<text x="2.54" y="-22.86" size="1.778" layer="94">RG1/PMA7/TX2/CK2</text>
<text x="2.54" y="-25.4" size="1.778" layer="94">PG2/PMA6/RX2/DT2</text>
<text x="2.54" y="-27.94" size="1.778" layer="94">RG3/PMCS1/CCP4/P3D</text>
<text x="2.54" y="-30.48" size="1.778" layer="94">!MCLR</text>
<text x="2.54" y="-33.02" size="1.778" layer="94">RG4/PMCS2/CCP5/P1D</text>
<text x="2.54" y="-35.56" size="1.778" layer="94">VSS</text>
<text x="2.54" y="-38.1" size="1.778" layer="94">VDDCORE/VCAP</text>
<text x="2.54" y="-40.64" size="1.778" layer="94">RF7/PMD0/!SS1</text>
<text x="2.54" y="-43.18" size="1.778" layer="94">RF6/PMD1/AN11/C1INA</text>
<text x="2.54" y="-45.72" size="1.778" layer="94">PF5/PMD2/AN10/C1INB/CVREF</text>
<text x="2.54" y="-48.26" size="1.778" layer="94">RF4/AN9/C2INA</text>
<text x="2.54" y="-50.8" size="1.778" layer="94">RF3/AN8/C2INB</text>
<text x="2.54" y="-53.34" size="1.778" layer="94">RF2/PMA5/AN7/C1OUT</text>
<text x="2.54" y="-55.88" size="1.778" layer="94">RH7/PMWR/AN15/P1B</text>
<text x="2.54" y="-58.42" size="1.778" layer="94">RH6/PMRD/AN14/P1C/C1NC</text>
<text x="2.54" y="-60.96" size="1.778" layer="94">RH5/PMBE/AN13/P3B/C2IND</text>
<text x="2.54" y="-63.5" size="1.778" layer="94">RH4/PMD3/AN12/P3C/C2INC</text>
<text x="2.54" y="-66.04" size="1.778" layer="94">RF1/AN6/C2OUT</text>
<text x="2.54" y="-68.58" size="1.778" layer="94">ENVREG</text>
<text x="2.54" y="-71.12" size="1.778" layer="94">AVDD</text>
<text x="2.54" y="-73.66" size="1.778" layer="94">AVSS</text>
<text x="2.54" y="-76.2" size="1.778" layer="94">RA3/AN3/VREF+</text>
<text x="2.54" y="-78.74" size="1.778" layer="94">RA2/AN2/VREF-</text>
<text x="2.54" y="-81.28" size="1.778" layer="94">RA1/AN1</text>
<text x="2.54" y="-83.82" size="1.778" layer="94">RA0/AN0</text>
<text x="2.54" y="-86.36" size="1.778" layer="94">VSS</text>
<text x="2.54" y="-88.9" size="1.778" layer="94">VDD</text>
<text x="2.54" y="-91.44" size="1.778" layer="94">RA5/PMD4/AN4</text>
<text x="2.54" y="-93.98" size="1.778" layer="94">RA4/PMD5/T0CKI</text>
<text x="2.54" y="-96.52" size="1.778" layer="94">RC1/T1OSI/ECCP2/P2A</text>
<text x="2.54" y="-99.06" size="1.778" layer="94">RC0/T1OSO/T13CKI</text>
<text x="2.54" y="-101.6" size="1.778" layer="94">RC6/TX1/CK1</text>
<text x="2.54" y="-104.14" size="1.778" layer="94">RC7/RX1/DT1</text>
<text x="2.54" y="-106.68" size="1.778" layer="94">RJ4/BA0</text>
<text x="2.54" y="-109.22" size="1.778" layer="94">RJ5/!CE</text>
<text x="73.66" y="-109.22" size="1.778" layer="94" align="bottom-right">RJ6/!LB</text>
<text x="73.66" y="-106.68" size="1.778" layer="94" align="bottom-right">RJ7/!UB</text>
<text x="73.66" y="-104.14" size="1.778" layer="94" align="bottom-right">RC2/ECCP1/P1A</text>
<text x="73.66" y="-101.6" size="1.778" layer="94" align="bottom-right">RC3/SCK1/SCL1</text>
<text x="73.66" y="-99.06" size="1.778" layer="94" align="bottom-right">RC4/SDI1/SDA1</text>
<text x="73.66" y="-96.52" size="1.778" layer="94" align="bottom-right">RC5/SDO1</text>
<text x="73.66" y="-93.98" size="1.778" layer="94" align="bottom-right">RB7/KBI3/PGD</text>
<text x="73.66" y="-91.44" size="1.778" layer="94" align="bottom-right">VDD</text>
<text x="73.66" y="-88.9" size="1.778" layer="94" align="bottom-right">OSC1/CLKI/RA7</text>
<text x="73.66" y="-86.36" size="1.778" layer="94" align="bottom-right">OSC2/CLKO/RA6</text>
<text x="73.66" y="-83.82" size="1.778" layer="94" align="bottom-right">VSS</text>
<text x="73.66" y="-81.28" size="1.778" layer="94" align="bottom-right">RB6/KBI2/PGC</text>
<text x="73.66" y="-76.2" size="1.778" layer="94" align="bottom-right">RB4/KBI0/PMA1</text>
<text x="73.66" y="-73.66" size="1.778" layer="94" align="bottom-right">RB3/INT3/PMA2/ECCP2/P2A</text>
<text x="73.66" y="-71.12" size="1.778" layer="94" align="bottom-right">RB2/INT2/PMA3</text>
<text x="73.66" y="-68.58" size="1.778" layer="94" align="bottom-right">RB1/INT1/PMA4</text>
<text x="73.66" y="-66.04" size="1.778" layer="94" align="bottom-right">RB0/INT0/FLT0</text>
<text x="73.66" y="-63.5" size="1.778" layer="94" align="bottom-right">RJ3/!WRH</text>
<text x="73.66" y="-60.96" size="1.778" layer="94" align="bottom-right">RJ2/!WRL</text>
<text x="73.66" y="-78.74" size="1.778" layer="94" align="bottom-right">RB5/KBI1/PMA0</text>
<text x="73.66" y="-58.42" size="1.778" layer="94" align="bottom-right">RJ1/!OE</text>
<text x="73.66" y="-55.88" size="1.778" layer="94" align="bottom-right">RJ0/ALE</text>
<text x="73.66" y="-53.34" size="1.778" layer="94" align="bottom-right">RD7/AD7/PMD7/SS2</text>
<text x="73.66" y="-50.8" size="1.778" layer="94" align="bottom-right">RD6/AD6/PMD6/SCK2/SCL2</text>
<text x="73.66" y="-48.26" size="1.778" layer="94" align="bottom-right">RD5/AD5/PMD5/SDI2/SDA2</text>
<text x="73.66" y="-45.72" size="1.778" layer="94" align="bottom-right">RD4/AD4/PMD4/SDO2</text>
<text x="73.66" y="-43.18" size="1.778" layer="94" align="bottom-right">RD3/AD3/PMD3</text>
<text x="73.66" y="-40.64" size="1.778" layer="94" align="bottom-right">RD2/AD2/PMD2</text>
<text x="73.66" y="-38.1" size="1.778" layer="94" align="bottom-right">RD1/AD1/PMD1</text>
<text x="73.66" y="-35.56" size="1.778" layer="94" align="bottom-right">VSS</text>
<text x="73.66" y="-33.02" size="1.778" layer="94" align="bottom-right">VDD</text>
<text x="73.66" y="-30.48" size="1.778" layer="94" align="bottom-right">RD0/AD0/PMD0</text>
<text x="73.66" y="-27.94" size="1.778" layer="94" align="bottom-right">RE7/AD15/PMA9/ECCP2/P2A</text>
<text x="73.66" y="-25.4" size="1.778" layer="94" align="bottom-right">RE6/AD14/PMA10/P1B</text>
<text x="73.66" y="-22.86" size="1.778" layer="94" align="bottom-right">RE5/AD13/PMA11/P1C</text>
<text x="73.66" y="-20.32" size="1.778" layer="94" align="bottom-right">RE4/AD12/PMA12/P3B</text>
<text x="73.66" y="-17.78" size="1.778" layer="94" align="bottom-right">RE3/AD11/PMA13/P3C/REFO</text>
<text x="73.66" y="-15.24" size="1.778" layer="94" align="bottom-right">RE2/AD10/PMBE/P2B</text>
<text x="73.66" y="-12.7" size="1.778" layer="94" align="bottom-right">RH0/A16</text>
<text x="73.66" y="-10.16" size="1.778" layer="94" align="bottom-right">RH1/A17</text>
<wire x1="5.08" y1="0" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-114.3" width="0.254" layer="94"/>
<wire x1="0" y1="-114.3" x2="76.2" y2="-114.3" width="0.254" layer="94"/>
<wire x1="76.2" y1="-114.3" x2="76.2" y2="0" width="0.254" layer="94"/>
<wire x1="76.2" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="RESISTER">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-1.27" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAPACITOR">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CRYSTAL_2PIN">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="NC">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="AMIS_30624">
<pin name="1" x="-5.08" y="-7.62" visible="pad" length="middle"/>
<pin name="2" x="-5.08" y="-10.16" visible="pad" length="middle"/>
<pin name="3" x="-5.08" y="-12.7" visible="pad" length="middle"/>
<pin name="4" x="-5.08" y="-15.24" visible="pad" length="middle"/>
<pin name="5" x="-5.08" y="-17.78" visible="pad" length="middle"/>
<pin name="6" x="-5.08" y="-20.32" visible="pad" length="middle"/>
<pin name="7" x="-5.08" y="-22.86" visible="pad" length="middle"/>
<pin name="8" x="-5.08" y="-25.4" visible="pad" length="middle"/>
<pin name="9" x="-5.08" y="-27.94" visible="pad" length="middle"/>
<pin name="10" x="-5.08" y="-30.48" visible="pad" length="middle"/>
<pin name="11" x="-5.08" y="-33.02" visible="pad" length="middle"/>
<pin name="12" x="-5.08" y="-35.56" visible="pad" length="middle"/>
<pin name="13" x="-5.08" y="-38.1" visible="pad" length="middle"/>
<pin name="14" x="-5.08" y="-40.64" visible="pad" length="middle"/>
<pin name="15" x="-5.08" y="-43.18" visible="pad" length="middle"/>
<pin name="16" x="-5.08" y="-45.72" visible="pad" length="middle"/>
<pin name="17" x="30.48" y="-45.72" visible="pad" length="middle" rot="R180"/>
<pin name="18" x="30.48" y="-43.18" visible="pad" length="middle" rot="R180"/>
<pin name="19" x="30.48" y="-40.64" visible="pad" length="middle" rot="R180"/>
<pin name="20" x="30.48" y="-38.1" visible="pad" length="middle" rot="R180"/>
<pin name="21" x="30.48" y="-35.56" visible="pad" length="middle" rot="R180"/>
<pin name="22" x="30.48" y="-33.02" visible="pad" length="middle" rot="R180"/>
<pin name="23" x="30.48" y="-30.48" visible="pad" length="middle" rot="R180"/>
<pin name="24" x="30.48" y="-27.94" visible="pad" length="middle" rot="R180"/>
<pin name="25" x="30.48" y="-25.4" visible="pad" length="middle" rot="R180"/>
<pin name="26" x="30.48" y="-22.86" visible="pad" length="middle" rot="R180"/>
<pin name="27" x="30.48" y="-20.32" visible="pad" length="middle" rot="R180"/>
<pin name="28" x="30.48" y="-17.78" visible="pad" length="middle" rot="R180"/>
<pin name="29" x="30.48" y="-15.24" visible="pad" length="middle" rot="R180"/>
<pin name="30" x="30.48" y="-12.7" visible="pad" length="middle" rot="R180"/>
<pin name="31" x="30.48" y="-10.16" visible="pad" length="middle" rot="R180"/>
<pin name="32" x="30.48" y="-7.62" visible="pad" length="middle" rot="R180"/>
<text x="1.27" y="-7.62" size="1.778" layer="94">XP</text>
<text x="1.27" y="-10.16" size="1.778" layer="94">XP</text>
<text x="1.27" y="-12.7" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-15.24" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-17.78" size="1.778" layer="94">VBB</text>
<text x="1.27" y="-20.32" size="1.778" layer="94">SWI</text>
<text x="1.27" y="-22.86" size="1.778" layer="94">NC</text>
<text x="1.27" y="-25.4" size="1.778" layer="94">SDA</text>
<text x="1.27" y="-27.94" size="1.778" layer="94">SCK</text>
<text x="1.27" y="-30.48" size="1.778" layer="94">VDD</text>
<text x="1.27" y="-33.02" size="1.778" layer="94">GND</text>
<text x="1.27" y="-35.56" size="1.778" layer="94">TST1</text>
<text x="1.27" y="-38.1" size="1.778" layer="94">TST2</text>
<text x="1.27" y="-40.64" size="1.778" layer="94">GND</text>
<text x="1.27" y="-43.18" size="1.778" layer="94">HW</text>
<text x="1.27" y="-45.72" size="1.778" layer="94">NC</text>
<text x="24.13" y="-45.72" size="1.778" layer="94" align="bottom-right">CPN</text>
<text x="24.13" y="-43.18" size="1.778" layer="94" align="bottom-right">CPP</text>
<text x="24.13" y="-40.64" size="1.778" layer="94" align="bottom-right">VCP</text>
<text x="24.13" y="-38.1" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-35.56" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-33.02" size="1.778" layer="94" align="bottom-right">VBB</text>
<text x="24.13" y="-30.48" size="1.778" layer="94" align="bottom-right">YN</text>
<text x="24.13" y="-27.94" size="1.778" layer="94" align="bottom-right">YN</text>
<text x="24.13" y="-25.4" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-22.86" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-20.32" size="1.778" layer="94" align="bottom-right">YP</text>
<text x="24.13" y="-17.78" size="1.778" layer="94" align="bottom-right">YP</text>
<text x="24.13" y="-15.24" size="1.778" layer="94" align="bottom-right">XN</text>
<text x="24.13" y="-12.7" size="1.778" layer="94" align="bottom-right">XN</text>
<text x="24.13" y="-10.16" size="1.778" layer="94" align="bottom-right">GND</text>
<text x="24.13" y="-7.62" size="1.778" layer="94" align="bottom-right">GND</text>
<wire x1="0" y1="-2.54" x2="0" y2="-48.26" width="0.254" layer="94"/>
<wire x1="0" y1="-48.26" x2="25.4" y2="-48.26" width="0.254" layer="94"/>
<wire x1="25.4" y1="-48.26" x2="25.4" y2="0" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="3.81" y="-2.54" size="1.778" layer="94">DC MOTOR DR</text>
<text x="0" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="33" x="30.48" y="-5.08" visible="pad" length="middle" rot="R180"/>
<text x="24.13" y="-5.08" size="1.778" layer="94" align="bottom-right">PAD</text>
</symbol>
<symbol name="ONSEMI_LM317">
<text x="6.604" y="0" size="1.778" layer="94" ratio="15" rot="R180" align="center-left">VOUT</text>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="2" x="10.16" y="0" visible="pad" length="short" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-6.604" y="0" size="1.778" layer="94" ratio="15" align="center-left">VIN</text>
<text x="-2.032" y="-3.81" size="1.778" layer="94" ratio="15" align="center-left">ADJ</text>
<text x="-3.81" y="3.556" size="1.778" layer="94" ratio="15" align="center-left">LM317</text>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" rot="R90"/>
<pin name="3" x="-10.16" y="0" visible="pad" length="short"/>
<text x="-7.62" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="VALUE_LABEL">
<text x="0" y="0" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ZENER">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="GDT">
<polygon width="0.254" layer="94">
<vertex x="-0.635" y="1.27"/>
<vertex x="0.635" y="1.27"/>
<vertex x="0.635" y="0.635"/>
<vertex x="-0.635" y="0.635"/>
</polygon>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94" curve="90"/>
</symbol>
<symbol name="TI_TCA9406">
<pin name="1" x="-12.7" y="7.62" visible="pad" length="middle"/>
<pin name="2" x="-12.7" y="2.54" visible="pad" length="middle"/>
<pin name="3" x="-12.7" y="-2.54" visible="pad" length="middle"/>
<pin name="4" x="-12.7" y="-7.62" visible="pad" length="middle"/>
<pin name="5" x="12.7" y="-7.62" visible="pad" length="middle" rot="R180"/>
<pin name="6" x="12.7" y="-2.54" visible="pad" length="middle" rot="R180"/>
<pin name="7" x="12.7" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="8" x="12.7" y="7.62" visible="pad" length="middle" rot="R180"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-6.35" y="7.62" size="1.27" layer="94" ratio="15">SDA_B</text>
<text x="-6.35" y="2.54" size="1.27" layer="94" ratio="15">GND</text>
<text x="-6.35" y="-2.54" size="1.27" layer="94" ratio="15">VCCA</text>
<text x="-6.35" y="-7.62" size="1.27" layer="94" ratio="15">SDA_A</text>
<text x="6.35" y="-7.62" size="1.27" layer="94" ratio="15" align="bottom-right">SCL_A</text>
<text x="6.35" y="-2.54" size="1.27" layer="94" ratio="15" align="bottom-right">OE</text>
<text x="6.35" y="2.54" size="1.27" layer="94" ratio="15" align="bottom-right">VCCB</text>
<text x="6.35" y="7.62" size="1.27" layer="94" ratio="15" align="bottom-right">SCL_B</text>
<text x="-7.62" y="15.24" size="1.27" layer="95" ratio="15">&gt;NAME</text>
<text x="-7.62" y="17.78" size="1.27" layer="95" ratio="15">&gt;VALUE</text>
<text x="-3.81" y="10.16" size="1.27" layer="94" ratio="15">TCA9406</text>
</symbol>
<symbol name="LED">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.143" y2="2.413" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="-0.508" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.143" y1="2.413" x2="0.889" y2="4.445" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.127" y2="4.318" width="0.254" layer="94"/>
<wire x1="0.889" y1="4.445" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.778" x2="1.524" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="0.762" y2="3.683" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.81" x2="1.397" y2="3.048" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
</symbol>
<symbol name="RS485">
<text x="1.27" y="-7.62" size="1.778" layer="94" ratio="15">R0</text>
<text x="1.27" y="-12.7" size="1.778" layer="94" ratio="15">!RE</text>
<text x="1.27" y="-17.78" size="1.778" layer="94" ratio="15">DE</text>
<text x="1.27" y="-22.86" size="1.778" layer="94" ratio="15">DI</text>
<text x="19.05" y="-22.86" size="1.778" layer="94" ratio="15" align="bottom-right">GND</text>
<text x="19.05" y="-17.78" size="1.778" layer="94" ratio="15" align="bottom-right">A(D0/RI)</text>
<text x="19.05" y="-12.7" size="1.778" layer="94" ratio="15" align="bottom-right">B(!D0!/!RI!)</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" ratio="15" align="bottom-right">VCC</text>
<wire x1="0" y1="-2.54" x2="0" y2="-25.4" width="0.254" layer="94"/>
<wire x1="0" y1="-25.4" x2="20.32" y2="-25.4" width="0.254" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="-7.62" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="-12.7" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="-17.78" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-22.86" visible="pad" length="short"/>
<pin name="5" x="22.86" y="-22.86" visible="pad" length="short" rot="R180"/>
<pin name="6" x="22.86" y="-17.78" visible="pad" length="short" rot="R180"/>
<pin name="7" x="22.86" y="-12.7" visible="pad" length="short" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" visible="pad" length="short" rot="R180"/>
<text x="6.35" y="-2.54" size="1.778" layer="94" ratio="15">RS485</text>
<text x="0" y="2.54" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="0" y="-27.94" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
</symbol>
<symbol name="SCHOTTKY_DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.302" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="2" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="1" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-3.302" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="3.302" x2="0.508" y2="3.302" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.302" x2="0.508" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.302" x2="2.032" y2="-3.302" width="0.254" layer="94"/>
<wire x1="2.032" y1="-3.302" x2="2.032" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="MICROCHIP_24/5XC256">
<text x="3.81" y="-10.16" size="1.778" layer="94">!CS</text>
<text x="3.81" y="-15.24" size="1.778" layer="94">SO</text>
<text x="3.81" y="-20.32" size="1.778" layer="94">!WP</text>
<text x="3.81" y="-25.4" size="1.778" layer="94">VSS</text>
<text x="22.86" y="-25.4" size="1.778" layer="94" align="bottom-right">SI</text>
<text x="22.86" y="-20.32" size="1.778" layer="94" align="bottom-right">SCK</text>
<text x="22.86" y="-15.24" size="1.778" layer="94" align="bottom-right">!HOLD</text>
<text x="22.86" y="-10.16" size="1.778" layer="94" align="bottom-right">VCC</text>
<wire x1="0" y1="-2.54" x2="0" y2="-27.94" width="0.254" layer="94"/>
<wire x1="0" y1="-27.94" x2="25.4" y2="-27.94" width="0.254" layer="94"/>
<wire x1="25.4" y1="-27.94" x2="25.4" y2="0" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<text x="2.54" y="-2.54" size="1.27" layer="94">DFN,PDIP/SOIC, TSSOP</text>
<text x="2.54" y="5.08" size="1.778" layer="95" ratio="15">&gt;NAME</text>
<text x="2.54" y="2.54" size="1.778" layer="96" ratio="15">&gt;VALUE</text>
<pin name="P$1" x="-2.54" y="-10.16" visible="pad" length="short"/>
<pin name="P$2" x="-2.54" y="-15.24" visible="pad" length="short"/>
<pin name="P$3" x="-2.54" y="-20.32" visible="pad" length="short"/>
<pin name="P$4" x="-2.54" y="-25.4" visible="pad" length="short"/>
<pin name="P$5" x="27.94" y="-25.4" visible="pad" length="short" rot="R180"/>
<pin name="P$6" x="27.94" y="-20.32" visible="pad" length="short" rot="R180"/>
<pin name="P$7" x="27.94" y="-15.24" visible="pad" length="short" rot="R180"/>
<pin name="P$8" x="27.94" y="-10.16" visible="pad" length="short" rot="R180"/>
<pin name="P$9" x="27.94" y="-5.08" visible="pad" length="short" rot="R180"/>
<text x="22.86" y="-5.08" size="1.778" layer="94" align="bottom-right">PAD</text>
</symbol>
<symbol name="VARISTOR_ESD">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.15875" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.16001875" layer="94"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="INDUCTOR">
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-2.54" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
</symbol>
<symbol name="PNP_TRANSISTOR">
<wire x1="-2.086" y1="-1.678" x2="-1.578" y2="-2.594" width="0.1524" layer="94"/>
<wire x1="-1.578" y1="-2.594" x2="-0.516" y2="-1.478" width="0.1524" layer="94"/>
<wire x1="-0.516" y1="-1.478" x2="-2.086" y2="-1.678" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.808" y2="-2.124" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.778" x2="-1.524" y2="-2.413" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.413" x2="-0.762" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.651" x2="-1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.778" x2="-1.524" y2="-2.159" width="0.254" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="-1.143" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="-1.905" x2="-1.524" y2="-1.905" width="0.254" layer="94"/>
<text x="10.16" y="-7.62" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="10.16" y="-5.08" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-2.54" x2="0.254" y2="2.54" layer="94" rot="R180"/>
<pin name="B" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="E" x="-2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="C" x="-2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="TP">
<wire x1="0.762" y1="1.778" x2="0" y2="0.254" width="0.254" layer="94"/>
<wire x1="0" y1="0.254" x2="-0.762" y2="1.778" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.778" x2="0.762" y2="1.778" width="0.254" layer="94" curve="-180"/>
<text x="-1.27" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<pin name="PP" x="0" y="0" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="CAPACITOR_ELECTRO">
<wire x1="-0.635" y1="-1.27" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.889" y1="1.27" x2="0.381" y2="0" width="0.254" layer="94" curve="50"/>
<wire x1="0.381" y1="0" x2="0.889" y2="-1.27" width="0.254" layer="94" curve="50"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.381" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-5.08" y="2.54" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.032" y="0.254" size="1.27" layer="94" ratio="15">+</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MICROCHIP_PIC18F8XJ11" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="MICROCHIP_PIC18F87J11" x="0" y="0"/>
</gates>
<devices>
<device name="TQFP(PT)/QFP" package="QFP50P1400X1400X120-80N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="34" pad="34"/>
<connect gate="G$1" pin="35" pad="35"/>
<connect gate="G$1" pin="36" pad="36"/>
<connect gate="G$1" pin="37" pad="37"/>
<connect gate="G$1" pin="38" pad="38"/>
<connect gate="G$1" pin="39" pad="39"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="40" pad="40"/>
<connect gate="G$1" pin="41" pad="41"/>
<connect gate="G$1" pin="42" pad="42"/>
<connect gate="G$1" pin="43" pad="43"/>
<connect gate="G$1" pin="44" pad="44"/>
<connect gate="G$1" pin="45" pad="45"/>
<connect gate="G$1" pin="46" pad="46"/>
<connect gate="G$1" pin="47" pad="47"/>
<connect gate="G$1" pin="48" pad="48"/>
<connect gate="G$1" pin="49" pad="49"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="50" pad="50"/>
<connect gate="G$1" pin="51" pad="51"/>
<connect gate="G$1" pin="52" pad="52"/>
<connect gate="G$1" pin="53" pad="53"/>
<connect gate="G$1" pin="54" pad="54"/>
<connect gate="G$1" pin="55" pad="55"/>
<connect gate="G$1" pin="56" pad="56"/>
<connect gate="G$1" pin="57" pad="57"/>
<connect gate="G$1" pin="58" pad="58"/>
<connect gate="G$1" pin="59" pad="59"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="60" pad="60"/>
<connect gate="G$1" pin="61" pad="61"/>
<connect gate="G$1" pin="62" pad="62"/>
<connect gate="G$1" pin="63" pad="63"/>
<connect gate="G$1" pin="64" pad="64"/>
<connect gate="G$1" pin="65" pad="65"/>
<connect gate="G$1" pin="66" pad="66"/>
<connect gate="G$1" pin="67" pad="67"/>
<connect gate="G$1" pin="68" pad="68"/>
<connect gate="G$1" pin="69" pad="69"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="70" pad="70"/>
<connect gate="G$1" pin="71" pad="71"/>
<connect gate="G$1" pin="72" pad="72"/>
<connect gate="G$1" pin="73" pad="73"/>
<connect gate="G$1" pin="74" pad="74"/>
<connect gate="G$1" pin="75" pad="75"/>
<connect gate="G$1" pin="76" pad="76"/>
<connect gate="G$1" pin="77" pad="77"/>
<connect gate="G$1" pin="78" pad="78"/>
<connect gate="G$1" pin="79" pad="79"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="80" pad="80"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES_SMD_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_SMD_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2013X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ABLS2-10.000MHZ-D4Y-T" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL_2PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HC49_US_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC" prefix="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TSW-105-XX-X-S" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-5" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="G$1" symbol="VALUE_LABEL" x="5.08" y="2.54"/>
</gates>
<devices>
<device name="VERTICAL" package="TSW-105-XX-X-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AMIS_30642" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="AMIS_30624" x="12.7" y="-22.86"/>
</gates>
<devices>
<device name="" package="QFN65P700X700X80-33N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="16" pad="16"/>
<connect gate="G$1" pin="17" pad="17"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="19" pad="19"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="20" pad="20"/>
<connect gate="G$1" pin="21" pad="21"/>
<connect gate="G$1" pin="22" pad="22"/>
<connect gate="G$1" pin="23" pad="23"/>
<connect gate="G$1" pin="24" pad="24"/>
<connect gate="G$1" pin="25" pad="25"/>
<connect gate="G$1" pin="26" pad="26"/>
<connect gate="G$1" pin="27" pad="27"/>
<connect gate="G$1" pin="28" pad="28"/>
<connect gate="G$1" pin="29" pad="29"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="30" pad="30"/>
<connect gate="G$1" pin="31" pad="31"/>
<connect gate="G$1" pin="32" pad="32"/>
<connect gate="G$1" pin="33" pad="33"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ONSEMI_LM317" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="ONSEMI_LM317" x="0" y="0"/>
</gates>
<devices>
<device name="D2PAK-3_D2T-SUFFIX_CASE936" package="ONSEMI_LM317">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AISG_" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-PAD" symbol="PIN" x="5.08" y="-20.32"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-5" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-6" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="-7" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="G$1" symbol="VALUE_LABEL" x="2.54" y="2.54"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-8" symbol="PIN" x="5.08" y="-17.78"/>
</gates>
<devices>
<device name="MALE" package="AISG_C091-C006-804-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-PAD" pin="1" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE" package="AISG_C091-G006-804-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-PAD" pin="1" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_ONSEMI_1SMC5.0AT3G" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="ONSEMI_1SMC5.0AT3G">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GDT_2035-XX-SM" prefix="F" uservalue="yes">
<gates>
<gate name="G$1" symbol="GDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2035-XX-SM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_TCA9406" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="TI_TCA9406" x="0" y="0"/>
</gates>
<devices>
<device name="8-VFSOP" package="TI_TCA9406_DCU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TSW-104-XX-X-S" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="G$1" symbol="VALUE_LABEL" x="2.54" y="2.54"/>
</gates>
<devices>
<device name="VERTICAL" package="TSW-104-XX-X-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_SMD_" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="LEDC1005X60N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="LEDC1608X75N/80N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TI_DS3695AX" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="RS485" x="10.16" y="-12.7"/>
</gates>
<devices>
<device name="" package="SOIC127P600X173-8N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NXP_PMEG3030EP" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHOTTKY_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NXP_SOD128">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICROHIP_24/5XC256" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="MICROCHIP_24/5XC256" x="12.7" y="-12.7"/>
</gates>
<devices>
<device name="NOT_AVAILABLE" package="TDFN50PC300X200X75-9N">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="8DFN" package="DFN6X5MM">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PANASONIC_EZJ-S2VB223" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="VARISTOR_ESD" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVX_VC15MA0340KBA" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="VARISTOR_ESD" x="0" y="0"/>
</gates>
<devices>
<device name="2220" package="RES5750X230N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PANASONIC_DB2G42600L1" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIODE1005X60N">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMMBT2907ALT1G" prefix="U" uservalue="yes">
<gates>
<gate name="G$1" symbol="PNP_TRANSISTOR" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="G$1" pin="B" pad="P$1"/>
<connect gate="G$1" pin="C" pad="P$3"/>
<connect gate="G$1" pin="E" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COMCHIP_CGRA400X-G" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_450X280">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP_" prefix="TP">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="1.5MM" package="TP_1.5MM">
<connects>
<connect gate="G$1" pin="PP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PANASONIC_EEE1CA101WP" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR_ELECTRO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PANASONIC_EEE1CA101WP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TDK_LGJ12565_TS_XXX_M_XXX_H" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TDK_LGJ12565_TS_XXX_M_XXX_H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOURNS_SRP4020TA-100M" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="IND445X406X180">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COMCHIP_CDBA240LL-HF" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="SCHOTTKY_DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO-214AC_475X290">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+09V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+9V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+9V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+09V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="AGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AGND" prefix="AGND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VR1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U3" library="JMA_LBR" deviceset="MICROCHIP_PIC18F8XJ11" device="TQFP(PT)/QFP" value="PIC18F87J11"/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="R4" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1M"/>
<part name="C18" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C23" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="C14" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C10" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C17" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C15" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="SUPPLY6" library="supply2" deviceset="GND" device=""/>
<part name="+3V37" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V34" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V33" library="supply1" deviceset="+3V3" device=""/>
<part name="C19" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="C5" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="22pF"/>
<part name="C4" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="22pF"/>
<part name="X1" library="JMA_LBR" deviceset="ABLS2-10.000MHZ-D4Y-T" device="" value="ABLS2-10.000MHZ-D4Y-T"/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="J1" library="JMA_LBR" deviceset="TSW-105-XX-X-S" device="VERTICAL" value="TSW-105-05-G-S"/>
<part name="+3V36" library="supply1" deviceset="+3V3" device=""/>
<part name="SUPPLY9" library="supply2" deviceset="GND" device=""/>
<part name="U8" library="JMA_LBR" deviceset="AMIS_30642" device="" value="AMIS 30624"/>
<part name="C45" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="C48" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="220nF"/>
<part name="SUPPLY39" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY40" library="supply2" deviceset="+9V" device=""/>
<part name="C56" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="C36" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C35" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C49" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="SUPPLY37" library="supply2" deviceset="GND" device=""/>
<part name="C47" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C26" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C30" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C52" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C53" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="SUPPLY46" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY47" library="supply2" deviceset="GND" device=""/>
<part name="NC5" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC4" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C57" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="10uF"/>
<part name="SUPPLY44" library="supply2" deviceset="GND" device=""/>
<part name="NC6" library="JMA_LBR" deviceset="NC" device=""/>
<part name="C43" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="R44" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY36" library="supply2" deviceset="GND" device=""/>
<part name="R37" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="C34" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="3.3uF"/>
<part name="SUPPLY34" library="supply2" deviceset="GND" device=""/>
<part name="R38" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R47" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R35" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0 (DNP)"/>
<part name="R46" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0 (DNP)"/>
<part name="SUPPLY31" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY32" library="supply2" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" deviceset="A4L-LOC-JMA" device=""/>
<part name="FRAME3" library="frames" deviceset="A4L-LOC-JMA" device=""/>
<part name="C11" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C12" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="C8" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="4.7uF"/>
<part name="C1" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="4.7uF"/>
<part name="C6" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="C7" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R2" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="240"/>
<part name="R1" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="392"/>
<part name="SUPPLY27" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY24" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY30" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY22" library="supply2" deviceset="+9V" device=""/>
<part name="+3V39" library="supply1" deviceset="+3V3" device=""/>
<part name="J100" library="JMA_LBR" deviceset="AISG_" device="FEMALE" value="AISG_C091-G006-804-2"/>
<part name="J101" library="JMA_LBR" deviceset="AISG_" device="MALE" value="AISG G091-G006-804-2(M)"/>
<part name="D8" library="JMA_LBR" deviceset="ZENER_ONSEMI_1SMC5.0AT3G" device="" value="1SMC5.0AT3G"/>
<part name="F3" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="F1" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="F2" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="F4" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="C54" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="C55" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="C41" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="4.7uF"/>
<part name="C51" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="4.7uF"/>
<part name="C46" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="C50" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R41" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="240, 1%"/>
<part name="R40" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="706, 1%"/>
<part name="SUPPLY26" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY23" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY29" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY21" library="supply2" deviceset="+9V" device=""/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="FRAME2" library="frames" deviceset="A4L-LOC-JMA" device=""/>
<part name="U7" library="JMA_LBR" deviceset="TI_TCA9406" device="8-VFSOP" value="TCA94068"/>
<part name="C40" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C31" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="SUPPLY35" library="supply2" deviceset="GND" device=""/>
<part name="R42" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R43" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0(DNP)"/>
<part name="SUPPLY33" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY38" library="supply2" deviceset="GND" device=""/>
<part name="R39" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="R14" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="R34" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="R15" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="4.7K"/>
<part name="+3V311" library="supply1" deviceset="+3V3" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="C24" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R18" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R20" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0 (DNP)"/>
<part name="R23" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="J4" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL" value="TSW-104-05-G-S"/>
<part name="D2" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005CGCK"/>
<part name="D4" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005QBC/D"/>
<part name="D1" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005SYCK"/>
<part name="R12" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="330"/>
<part name="R13" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="523"/>
<part name="R11" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1K"/>
<part name="SUPPLY48" library="supply2" deviceset="GND" device=""/>
<part name="U5" library="JMA_LBR" deviceset="TI_DS3695AX" device="" value="DS3695AX"/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="SUPPLY45" library="supply2" deviceset="GND" device=""/>
<part name="R32" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R24" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="D12" library="JMA_LBR" deviceset="NXP_PMEG3030EP" device="" value="PMEG3030EP"/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="ANG" library="JMA_LBR" deviceset="TP_" device="1.5MM"/>
<part name="AGND" library="JMA_LBR" deviceset="TP_" device="1.5MM"/>
<part name="U9" library="JMA_LBR" deviceset="ONSEMI_LM317" device="D2PAK-3_D2T-SUFFIX_CASE936" value="LM317"/>
<part name="U1" library="JMA_LBR" deviceset="ONSEMI_LM317" device="D2PAK-3_D2T-SUFFIX_CASE936" value="LM317"/>
<part name="C33" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="SUPPLY43" library="supply2" deviceset="GND" device=""/>
<part name="R26" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="100K"/>
<part name="C25" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="+3V31" library="supply1" deviceset="+3V3" device=""/>
<part name="R22" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="U2" library="JMA_LBR" deviceset="MICROHIP_24/5XC256" device="8DFN" value="24FC256MNY"/>
<part name="+3V310" library="supply1" deviceset="+3V3" device=""/>
<part name="C9" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="SUPPLY28" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="R5" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0(DNP)"/>
<part name="R6" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="+3V38" library="supply1" deviceset="+3V3" device=""/>
<part name="R8" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="15K"/>
<part name="SUPPLY3" library="supply2" deviceset="GND" device=""/>
<part name="GND" library="JMA_LBR" deviceset="TP_" device="1.5MM"/>
<part name="+3V32" library="supply1" deviceset="+3V3" device=""/>
<part name="TX" library="JMA_LBR" deviceset="TP_" device="1.5MM"/>
<part name="RX" library="JMA_LBR" deviceset="TP_" device="1.5MM"/>
<part name="V+" library="JMA_LBR" deviceset="TP_" device="1.5MM"/>
<part name="AGND2" library="supply1" deviceset="AGND" device=""/>
<part name="AGND3" library="supply1" deviceset="AGND" device=""/>
<part name="AGND4" library="supply1" deviceset="AGND" device=""/>
<part name="AGND5" library="supply1" deviceset="AGND" device=""/>
<part name="AGND6" library="supply1" deviceset="AGND" device=""/>
<part name="R31" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10"/>
<part name="R29" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10"/>
<part name="C27" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C28" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="R30" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="(DNP)"/>
<part name="R28" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10"/>
<part name="C22" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C29" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R27" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="(DNP)"/>
<part name="SUPPLY41" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY42" library="supply2" deviceset="GND" device=""/>
<part name="R7" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="2K"/>
<part name="R10" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="2K"/>
<part name="R9" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="2K"/>
<part name="D5" library="JMA_LBR" deviceset="LED_SMD_" device="0402" value="APHHS1005SURCK"/>
<part name="R16" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="470"/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="J2" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL" value="TSW-104-05-G-S"/>
<part name="J3" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL" value="TSW-104-05-G-S"/>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="D10" library="JMA_LBR" deviceset="PANASONIC_EZJ-S2VB223" device="0805" value="EZJ-S2VB223"/>
<part name="D11" library="JMA_LBR" deviceset="PANASONIC_EZJ-S2VB223" device="0805" value="EZJ-S2VB223"/>
<part name="SUPPLY50" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY51" library="supply2" deviceset="GND" device=""/>
<part name="D3" library="JMA_LBR" deviceset="AVX_VC15MA0340KBA" device="2220" value="VC15MA0340KBA"/>
<part name="SUPPLY52" library="supply2" deviceset="GND" device=""/>
<part name="AGND7" library="supply1" deviceset="AGND" device=""/>
<part name="C16" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="4.7uF (DNP)"/>
<part name="C13" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="C39" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="C38" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="R19" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="240, 1%"/>
<part name="R21" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="1.49K, 1%"/>
<part name="SUPPLY8" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" deviceset="GND" device=""/>
<part name="U6" library="JMA_LBR" deviceset="ONSEMI_LM317" device="D2PAK-3_D2T-SUFFIX_CASE936" value="LM317"/>
<part name="NC1" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC2" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC3" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC7" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC8" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC9" library="JMA_LBR" deviceset="NC" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="+9V" device=""/>
<part name="D9" library="JMA_LBR" deviceset="PANASONIC_DB2G42600L1" device="" value="DB2G42600L1(DNP)"/>
<part name="R33" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="49.9K"/>
<part name="U4" library="JMA_LBR" deviceset="SMMBT2907ALT1G" device="" value="SMMBT2907ALT1G"/>
<part name="C32" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="D7" library="JMA_LBR" deviceset="COMCHIP_CGRA400X-G" device="" value="CGRA4001-G"/>
<part name="R25" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="10"/>
<part name="C2" library="JMA_LBR" deviceset="CAP_SMD_" device="0603" value="1uF"/>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="C21" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C44" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C3" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="0.1uF"/>
<part name="C37" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="C42" library="JMA_LBR" deviceset="PANASONIC_EEE1CA101WP" device="" value="100uF"/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="VREF" library="JMA_LBR" deviceset="TP_" device="1.5MM"/>
<part name="L5" library="JMA_LBR" deviceset="BOURNS_SRP4020TA-100M" device="" value="10uH"/>
<part name="L2" library="JMA_LBR" deviceset="BOURNS_SRP4020TA-100M" device="" value="10uH"/>
<part name="L6" library="JMA_LBR" deviceset="BOURNS_SRP4020TA-100M" device="" value="10uH"/>
<part name="L1" library="JMA_LBR" deviceset="BOURNS_SRP4020TA-100M" device="" value="10uH"/>
<part name="R45" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="R3" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="L4" library="JMA_LBR" deviceset="BOURNS_SRP4020TA-100M" device="" value="10uH"/>
<part name="R36" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="C20" library="JMA_LBR" deviceset="CAP_SMD_" device="0805" value="10uF"/>
<part name="L3" library="JMA_LBR" deviceset="TDK_LGJ12565_TS_XXX_M_XXX_H" device="" value="47uH"/>
<part name="R17" library="JMA_LBR" deviceset="RES_SMD_" device="0603" value="0"/>
<part name="D6" library="JMA_LBR" deviceset="COMCHIP_CDBA240LL-HF" device="" value="CDBA240LL-HF"/>
<part name="J5" library="JMA_LBR" deviceset="TSW-104-XX-X-S" device="VERTICAL" value="TSW-104-05-G-S"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="239.014" y="154.94" size="1.27" layer="97" rot="R270">0603</text>
<text x="246.634" y="154.94" size="1.27" layer="97" rot="R270">0603</text>
<wire x1="195.58" y1="172.72" x2="248.92" y2="172.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="248.92" y1="172.72" x2="251.46" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="170.18" x2="251.46" y2="142.24" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="142.24" x2="248.92" y2="139.7" width="0.1524" layer="97" style="shortdash"/>
<wire x1="248.92" y1="139.7" x2="195.58" y2="139.7" width="0.1524" layer="97" style="shortdash"/>
<wire x1="195.58" y1="139.7" x2="193.04" y2="142.24" width="0.1524" layer="97" style="shortdash"/>
<wire x1="193.04" y1="170.18" x2="195.58" y2="172.72" width="0.1524" layer="97" style="shortdash"/>
<wire x1="193.04" y1="142.24" x2="193.04" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="195.58" y1="137.16" x2="248.92" y2="137.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="248.92" y1="137.16" x2="251.46" y2="134.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="134.62" x2="251.46" y2="104.14" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="104.14" x2="248.92" y2="101.6" width="0.1524" layer="97" style="shortdash"/>
<wire x1="248.92" y1="101.6" x2="195.58" y2="101.6" width="0.1524" layer="97" style="shortdash"/>
<wire x1="195.58" y1="101.6" x2="193.04" y2="104.14" width="0.1524" layer="97" style="shortdash"/>
<wire x1="193.04" y1="134.62" x2="195.58" y2="137.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="193.04" y1="104.14" x2="193.04" y2="134.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="195.58" y1="99.06" x2="248.92" y2="99.06" width="0.1524" layer="97" style="shortdash"/>
<wire x1="248.92" y1="99.06" x2="251.46" y2="96.52" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="96.52" x2="251.46" y2="71.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="71.12" x2="248.92" y2="68.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="248.92" y1="68.58" x2="195.58" y2="68.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="195.58" y1="68.58" x2="193.04" y2="71.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="193.04" y1="96.52" x2="195.58" y2="99.06" width="0.1524" layer="97" style="shortdash"/>
<wire x1="193.04" y1="71.12" x2="193.04" y2="96.52" width="0.1524" layer="97" style="shortdash"/>
<text x="162.56" y="99.06" size="1.27" layer="97">RD5 &amp; RD6 are used 
as i2c to drive into 
stepper motor controller</text>
<wire x1="7.62" y1="167.64" x2="10.16" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="170.18" x2="185.42" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="185.42" y1="170.18" x2="187.96" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="187.96" y1="167.64" x2="187.96" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="187.96" y1="30.48" x2="185.42" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="185.42" y1="27.94" x2="10.16" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="27.94" x2="7.62" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="30.48" x2="7.62" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<text x="195.58" y="170.18" size="1.27" layer="97">DECOUPLING CAPACITOR</text>
<text x="195.58" y="134.62" size="1.27" layer="97">10MHZ EXTERNAL CRYSTAL</text>
<text x="10.16" y="167.64" size="1.27" layer="97">PIC18 MICROCONTROLLER SECTION</text>
<text x="195.58" y="96.52" size="1.27" layer="97">PROGRAMMING HEADER</text>
<text x="20.32" y="68.58" size="1.27" layer="97" rot="R90">External Potentiometer</text>
<text x="20.32" y="50.8" size="1.27" layer="97" rot="R90">Debug UART</text>
<wire x1="66.04" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="93"/>
<text x="213.36" y="78.994" size="1.27" layer="97">0603</text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="71.12" y="157.48" smashed="yes">
<attribute name="NAME" x="71.12" y="160.02" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="71.12" y="162.56" size="1.27" layer="96" ratio="15"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="60.96" y="38.1" smashed="yes">
<attribute name="VALUE" x="59.055" y="34.925" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="157.48" y="35.56" smashed="yes">
<attribute name="VALUE" x="155.575" y="32.385" size="1.27" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="223.52" y="129.54" smashed="yes">
<attribute name="NAME" x="218.44" y="130.175" size="1.27" layer="95"/>
<attribute name="VALUE" x="227.33" y="130.175" size="1.27" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="200.66" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="200.025" y="154.305" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="200.025" y="159.385" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C23" gate="G$1" x="246.38" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="245.745" y="153.67" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="245.745" y="159.385" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C14" gate="G$1" x="208.28" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="207.645" y="153.67" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="207.645" y="159.385" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="215.9" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="215.265" y="153.67" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="215.265" y="159.385" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C17" gate="G$1" x="223.52" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="222.885" y="153.67" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="222.885" y="159.385" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="231.14" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="230.505" y="153.67" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="230.505" y="159.385" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="200.66" y="144.78" smashed="yes">
<attribute name="VALUE" x="198.755" y="141.605" size="1.27" layer="96"/>
</instance>
<instance part="+3V37" gate="G$1" x="246.38" y="170.18" smashed="yes">
<attribute name="VALUE" x="243.84" y="170.815" size="1.27" layer="96"/>
</instance>
<instance part="+3V34" gate="G$1" x="160.02" y="157.48" smashed="yes">
<attribute name="VALUE" x="160.02" y="157.48" size="1.27" layer="96"/>
</instance>
<instance part="+3V33" gate="G$1" x="58.42" y="157.48" smashed="yes">
<attribute name="VALUE" x="58.42" y="157.48" size="1.27" layer="96"/>
</instance>
<instance part="C19" gate="G$1" x="238.76" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="238.125" y="153.67" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="238.125" y="159.385" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="210.82" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="210.185" y="113.03" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="210.185" y="118.11" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C4" gate="G$1" x="236.22" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="235.585" y="113.03" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="235.585" y="118.11" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="X1" gate="G$1" x="223.52" y="124.46" smashed="yes">
<attribute name="NAME" x="213.36" y="125.095" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="212.344" y="120.65" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="210.82" y="106.68" smashed="yes">
<attribute name="VALUE" x="208.915" y="103.505" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="GND" x="236.22" y="106.68" smashed="yes">
<attribute name="VALUE" x="234.315" y="103.505" size="1.27" layer="96"/>
</instance>
<instance part="J1" gate="-1" x="236.22" y="88.9" smashed="yes">
<attribute name="NAME" x="232.918" y="89.408" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-2" x="236.22" y="86.36" smashed="yes">
<attribute name="NAME" x="232.918" y="86.868" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-3" x="236.22" y="83.82" smashed="yes">
<attribute name="NAME" x="232.918" y="84.328" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-4" x="236.22" y="81.28" smashed="yes">
<attribute name="NAME" x="232.918" y="81.788" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J1" gate="-5" x="236.22" y="78.74" smashed="yes">
<attribute name="NAME" x="232.918" y="79.248" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="+3V36" gate="G$1" x="226.06" y="93.98" smashed="yes">
<attribute name="VALUE" x="226.06" y="93.98" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="GND" x="228.6" y="73.66" smashed="yes">
<attribute name="VALUE" x="226.695" y="70.485" size="1.27" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="C24" gate="G$1" x="48.26" y="121.92" smashed="yes">
<attribute name="NAME" x="43.815" y="122.555" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="49.53" y="122.555" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R18" gate="G$1" x="48.26" y="93.98" smashed="yes">
<attribute name="NAME" x="41.91" y="94.615" size="1.27" layer="95"/>
<attribute name="VALUE" x="52.07" y="94.488" size="1.27" layer="96"/>
</instance>
<instance part="R20" gate="G$1" x="48.26" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="54.61" y="90.805" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="45.72" y="90.932" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R23" gate="G$1" x="48.26" y="116.84" smashed="yes" rot="R180">
<attribute name="NAME" x="43.18" y="117.7036" size="1.27" layer="95"/>
<attribute name="VALUE" x="51.435" y="117.983" size="1.27" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="241.3" y="73.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="241.3" y="76.2" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="48.26" y="66.04" smashed="yes">
<attribute name="VALUE" x="46.355" y="62.865" size="1.27" layer="96"/>
</instance>
<instance part="ANG" gate="G$1" x="33.02" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="24.13" y="73.66" size="1.778" layer="95"/>
</instance>
<instance part="AGND" gate="G$1" x="33.02" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="24.13" y="71.12" size="1.778" layer="95"/>
</instance>
<instance part="R26" gate="G$1" x="27.94" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="27.0764" y="129.54" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.797" y="137.795" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C25" gate="G$1" x="33.02" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="32.385" y="114.935" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="32.385" y="120.65" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="33.02" y="109.22" smashed="yes">
<attribute name="VALUE" x="31.115" y="106.045" size="1.27" layer="96"/>
</instance>
<instance part="+3V31" gate="G$1" x="27.94" y="147.32" smashed="yes">
<attribute name="VALUE" x="27.94" y="147.32" size="1.27" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="43.18" y="127" smashed="yes">
<attribute name="NAME" x="48.26" y="126.1364" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="40.005" y="125.857" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY3" gate="GND" x="40.64" y="45.72" smashed="yes">
<attribute name="VALUE" x="38.735" y="42.545" size="1.27" layer="96"/>
</instance>
<instance part="GND" gate="G$1" x="38.1" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="29.21" y="53.34" size="1.778" layer="95"/>
</instance>
<instance part="+3V32" gate="G$1" x="43.18" y="63.5" smashed="yes">
<attribute name="VALUE" x="43.18" y="63.5" size="1.27" layer="96"/>
</instance>
<instance part="TX" gate="G$1" x="38.1" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="29.21" y="55.88" size="1.778" layer="95"/>
</instance>
<instance part="RX" gate="G$1" x="38.1" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="29.21" y="50.8" size="1.778" layer="95"/>
</instance>
<instance part="V+" gate="G$1" x="38.1" y="58.42" smashed="yes" rot="R90">
<attribute name="NAME" x="29.21" y="58.42" size="1.778" layer="95"/>
</instance>
<instance part="D5" gate="G$1" x="167.64" y="137.16" smashed="yes">
<attribute name="NAME" x="162.56" y="138.43" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="162.56" y="142.24" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R16" gate="G$1" x="172.72" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="171.8564" y="132.08" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="171.577" y="122.555" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY13" gate="GND" x="172.72" y="119.38" smashed="yes">
<attribute name="VALUE" x="170.815" y="116.205" size="1.27" layer="96"/>
</instance>
<instance part="D9" gate="G$1" x="48.26" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="50.038" y="109.22" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="39.624" y="107.442" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C2" gate="G$1" x="218.44" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="222.25" y="78.105" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="216.535" y="78.105" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="210.82" y="73.66" smashed="yes">
<attribute name="VALUE" x="208.915" y="70.485" size="1.27" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="35.56" y="81.28" smashed="yes">
<attribute name="VALUE" x="33.02" y="81.28" size="1.27" layer="96"/>
</instance>
<instance part="VREF" gate="G$1" x="33.02" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="24.13" y="76.2" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="11"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="66.04" y1="121.92" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
<wire x1="60.96" y1="121.92" x2="60.96" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="31"/>
<wire x1="60.96" y1="83.82" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<wire x1="60.96" y1="91.44" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<wire x1="60.96" y1="71.12" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="66.04" y1="71.12" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<junction x="60.96" y="71.12"/>
<pinref part="U3" gate="G$1" pin="26"/>
<wire x1="66.04" y1="83.82" x2="60.96" y2="83.82" width="0.1524" layer="91"/>
<junction x="60.96" y="83.82"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="53.34" y1="91.44" x2="60.96" y2="91.44" width="0.1524" layer="91"/>
<junction x="60.96" y="91.44"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="60.96" y1="121.92" x2="53.34" y2="121.92" width="0.1524" layer="91"/>
<junction x="60.96" y="121.92"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="70"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<wire x1="152.4" y1="121.92" x2="157.48" y2="121.92" width="0.1524" layer="91"/>
<wire x1="157.48" y1="121.92" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="51"/>
<wire x1="157.48" y1="73.66" x2="157.48" y2="38.1" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<junction x="157.48" y="73.66"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="200.66" y1="152.4" x2="200.66" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="200.66" y1="149.86" x2="208.28" y2="149.86" width="0.1524" layer="91"/>
<wire x1="208.28" y1="149.86" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<wire x1="215.9" y1="149.86" x2="223.52" y2="149.86" width="0.1524" layer="91"/>
<wire x1="223.52" y1="149.86" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<wire x1="231.14" y1="149.86" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
<wire x1="238.76" y1="149.86" x2="246.38" y2="149.86" width="0.1524" layer="91"/>
<wire x1="246.38" y1="149.86" x2="246.38" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="208.28" y1="152.4" x2="208.28" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="215.9" y1="152.4" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="223.52" y1="152.4" x2="223.52" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="231.14" y1="152.4" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
<wire x1="238.76" y1="152.4" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="200.66" y1="149.86" x2="200.66" y2="147.32" width="0.1524" layer="91"/>
<junction x="200.66" y="149.86"/>
<junction x="208.28" y="149.86"/>
<junction x="215.9" y="149.86"/>
<junction x="223.52" y="149.86"/>
<junction x="231.14" y="149.86"/>
<junction x="238.76" y="149.86"/>
<pinref part="C19" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="210.82" y1="111.76" x2="210.82" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="236.22" y1="111.76" x2="236.22" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="-4" pin="1"/>
<wire x1="231.14" y1="81.28" x2="228.6" y2="81.28" width="0.1524" layer="91"/>
<wire x1="228.6" y1="81.28" x2="228.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<wire x1="48.26" y1="68.58" x2="48.26" y2="71.12" width="0.1524" layer="91"/>
<wire x1="48.26" y1="71.12" x2="33.02" y2="71.12" width="0.1524" layer="91"/>
<pinref part="AGND" gate="G$1" pin="PP"/>
</segment>
<segment>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="33.02" y1="111.76" x2="33.02" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
<wire x1="40.64" y1="48.26" x2="40.64" y2="50.8" width="0.1524" layer="91"/>
<wire x1="40.64" y1="50.8" x2="38.1" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND" gate="G$1" pin="PP"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="172.72" y1="124.46" x2="172.72" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="213.36" y1="78.74" x2="210.82" y2="78.74" width="0.1524" layer="91"/>
<wire x1="210.82" y1="78.74" x2="210.82" y2="76.2" width="0.1524" layer="91"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="200.66" y1="162.56" x2="200.66" y2="165.1" width="0.1524" layer="91"/>
<wire x1="200.66" y1="165.1" x2="208.28" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="208.28" y1="165.1" x2="215.9" y2="165.1" width="0.1524" layer="91"/>
<wire x1="215.9" y1="165.1" x2="223.52" y2="165.1" width="0.1524" layer="91"/>
<wire x1="223.52" y1="165.1" x2="231.14" y2="165.1" width="0.1524" layer="91"/>
<wire x1="231.14" y1="165.1" x2="238.76" y2="165.1" width="0.1524" layer="91"/>
<wire x1="238.76" y1="165.1" x2="246.38" y2="165.1" width="0.1524" layer="91"/>
<wire x1="246.38" y1="165.1" x2="246.38" y2="167.64" width="0.1524" layer="91"/>
<wire x1="208.28" y1="162.56" x2="208.28" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="215.9" y1="162.56" x2="215.9" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="223.52" y1="162.56" x2="223.52" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="231.14" y1="162.56" x2="231.14" y2="165.1" width="0.1524" layer="91"/>
<wire x1="238.76" y1="162.56" x2="238.76" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="246.38" y1="162.56" x2="246.38" y2="165.1" width="0.1524" layer="91"/>
<junction x="208.28" y="165.1"/>
<junction x="215.9" y="165.1"/>
<junction x="223.52" y="165.1"/>
<junction x="231.14" y="165.1"/>
<junction x="238.76" y="165.1"/>
<junction x="246.38" y="165.1"/>
<pinref part="+3V37" gate="G$1" pin="+3V3"/>
<pinref part="C19" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
<pinref part="U3" gate="G$1" pin="25"/>
<wire x1="58.42" y1="93.98" x2="58.42" y2="111.76" width="0.1524" layer="91"/>
<wire x1="58.42" y1="111.76" x2="58.42" y2="154.94" width="0.1524" layer="91"/>
<wire x1="66.04" y1="86.36" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="58.42" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="32"/>
<wire x1="66.04" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="58.42" y1="68.58" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<junction x="58.42" y="86.36"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="53.34" y1="93.98" x2="58.42" y2="93.98" width="0.1524" layer="91"/>
<junction x="58.42" y="93.98"/>
<pinref part="D9" gate="G$1" pin="+"/>
<wire x1="53.34" y1="111.76" x2="58.42" y2="111.76" width="0.1524" layer="91"/>
<junction x="58.42" y="111.76"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="71"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="152.4" y1="124.46" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="160.02" y1="124.46" x2="160.02" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="48"/>
<wire x1="152.4" y1="66.04" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<wire x1="160.02" y1="66.04" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<junction x="160.02" y="124.46"/>
</segment>
<segment>
<pinref part="J1" gate="-5" pin="1"/>
<wire x1="231.14" y1="78.74" x2="226.06" y2="78.74" width="0.1524" layer="91"/>
<wire x1="226.06" y1="78.74" x2="226.06" y2="91.44" width="0.1524" layer="91"/>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="223.52" y1="78.74" x2="226.06" y2="78.74" width="0.1524" layer="91"/>
<junction x="226.06" y="78.74"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
<wire x1="27.94" y1="139.7" x2="27.94" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
<wire x1="43.18" y1="60.96" x2="43.18" y2="58.42" width="0.1524" layer="91"/>
<pinref part="V+" gate="G$1" pin="PP"/>
<wire x1="43.18" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="VREF" gate="G$1" pin="PP"/>
<wire x1="33.02" y1="76.2" x2="35.56" y2="76.2" width="0.1524" layer="91"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="35.56" y1="76.2" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="OSC1" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="210.82" y1="127" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<wire x1="218.44" y1="124.46" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<wire x1="210.82" y1="127" x2="208.28" y2="127" width="0.1524" layer="91"/>
<junction x="210.82" y="127"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="218.44" y1="129.54" x2="210.82" y2="129.54" width="0.1524" layer="91"/>
<wire x1="210.82" y1="129.54" x2="210.82" y2="127" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="210.82" y1="121.92" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<junction x="210.82" y="124.46"/>
<label x="208.28" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="49"/>
<wire x1="152.4" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<label x="167.64" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="OSC2" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="236.22" y1="127" x2="236.22" y2="124.46" width="0.1524" layer="91"/>
<wire x1="228.6" y1="124.46" x2="236.22" y2="124.46" width="0.1524" layer="91"/>
<wire x1="236.22" y1="127" x2="238.76" y2="127" width="0.1524" layer="91"/>
<junction x="236.22" y="127"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="228.6" y1="129.54" x2="236.22" y2="129.54" width="0.1524" layer="91"/>
<wire x1="236.22" y1="129.54" x2="236.22" y2="127" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="236.22" y1="121.92" x2="236.22" y2="124.46" width="0.1524" layer="91"/>
<junction x="236.22" y="124.46"/>
<label x="238.76" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="50"/>
<wire x1="152.4" y1="71.12" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<label x="167.64" y="71.12" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="!MCLR" class="0">
<segment>
<pinref part="J1" gate="-1" pin="1"/>
<wire x1="231.14" y1="88.9" x2="218.44" y2="88.9" width="0.1524" layer="91"/>
<label x="218.44" y="88.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="22.86" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="38.1" y1="127" x2="33.02" y2="127" width="0.1524" layer="91"/>
<wire x1="33.02" y1="127" x2="27.94" y2="127" width="0.1524" layer="91"/>
<wire x1="27.94" y1="127" x2="22.86" y2="127" width="0.1524" layer="91"/>
<wire x1="27.94" y1="129.54" x2="27.94" y2="127" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="33.02" y1="124.46" x2="33.02" y2="127" width="0.1524" layer="91"/>
<junction x="27.94" y="127"/>
<junction x="33.02" y="127"/>
<pinref part="R22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RB7" class="0">
<segment>
<pinref part="J1" gate="-3" pin="1"/>
<wire x1="231.14" y1="83.82" x2="218.44" y2="83.82" width="0.1524" layer="91"/>
<label x="218.44" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="47"/>
<wire x1="152.4" y1="63.5" x2="167.64" y2="63.5" width="0.1524" layer="91"/>
<label x="167.64" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RB6" class="0">
<segment>
<pinref part="J1" gate="-2" pin="1"/>
<wire x1="231.14" y1="86.36" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<label x="208.28" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="52"/>
<wire x1="152.4" y1="76.2" x2="167.64" y2="76.2" width="0.1524" layer="91"/>
<label x="167.64" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RD5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="65"/>
<wire x1="152.4" y1="109.22" x2="167.64" y2="109.22" width="0.1524" layer="91"/>
<label x="167.64" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RD6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="64"/>
<wire x1="152.4" y1="106.68" x2="167.64" y2="106.68" width="0.1524" layer="91"/>
<label x="167.64" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="43.18" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91"/>
<wire x1="40.64" y1="93.98" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="40.64" y1="91.44" x2="43.18" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="24"/>
<wire x1="66.04" y1="88.9" x2="40.64" y2="88.9" width="0.1524" layer="91"/>
<wire x1="40.64" y1="88.9" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<junction x="40.64" y="91.44"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="43.18" y1="121.92" x2="40.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="40.64" y1="121.92" x2="40.64" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="40.64" y1="116.84" x2="43.18" y2="116.84" width="0.1524" layer="91"/>
<wire x1="40.64" y1="116.84" x2="40.64" y2="111.76" width="0.1524" layer="91"/>
<junction x="40.64" y="116.84"/>
<pinref part="D9" gate="G$1" pin="-"/>
<wire x1="43.18" y1="111.76" x2="40.64" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="53.34" y1="116.84" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="12"/>
<wire x1="55.88" y1="116.84" x2="55.88" y2="119.38" width="0.1524" layer="91"/>
<wire x1="55.88" y1="119.38" x2="66.04" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RG2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="7"/>
<wire x1="66.04" y1="132.08" x2="45.72" y2="132.08" width="0.1524" layer="91"/>
<label x="45.72" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RG0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="5"/>
<wire x1="66.04" y1="137.16" x2="45.72" y2="137.16" width="0.1524" layer="91"/>
<label x="45.72" y="137.16" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RG1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="6"/>
<wire x1="66.04" y1="134.62" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<label x="45.72" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="30"/>
<wire x1="66.04" y1="73.66" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<pinref part="ANG" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="9"/>
<wire x1="66.04" y1="127" x2="48.26" y2="127" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="RC5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="46"/>
<wire x1="152.4" y1="60.96" x2="167.64" y2="60.96" width="0.1524" layer="91"/>
<label x="167.64" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RC4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="45"/>
<wire x1="152.4" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<label x="167.64" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RC3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="44"/>
<wire x1="152.4" y1="55.88" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
<label x="167.64" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="RA3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="27"/>
<wire x1="66.04" y1="81.28" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
<label x="48.26" y="81.28" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="37"/>
<wire x1="66.04" y1="55.88" x2="38.1" y2="55.88" width="0.1524" layer="91"/>
<pinref part="RX" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="38"/>
<wire x1="66.04" y1="53.34" x2="38.1" y2="53.34" width="0.1524" layer="91"/>
<pinref part="TX" gate="G$1" pin="PP"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="76"/>
<wire x1="152.4" y1="137.16" x2="162.56" y2="137.16" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="-"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="172.72" y1="137.16" x2="172.72" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RH5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="21"/>
<wire x1="66.04" y1="96.52" x2="38.1" y2="96.52" width="0.1524" layer="91"/>
<label x="38.1" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RH4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="22"/>
<wire x1="66.04" y1="93.98" x2="63.5" y2="93.98" width="0.1524" layer="91"/>
<wire x1="63.5" y1="93.98" x2="63.5" y2="99.06" width="0.1524" layer="91"/>
<wire x1="63.5" y1="99.06" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
<label x="38.1" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RJ5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="40"/>
<wire x1="66.04" y1="48.26" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<label x="58.42" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="217.551" y="85.725" size="1.27" layer="97" rot="R90">0805</text>
<text x="237.871" y="85.725" size="1.27" layer="97" rot="R90">0805</text>
<text x="175.768" y="90.17" size="1.27" layer="97" rot="R270">0603</text>
<text x="171.831" y="86.36" size="1.27" layer="97" rot="R90">0805</text>
<wire x1="139.7" y1="114.3" x2="139.7" y2="68.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="139.7" y1="68.58" x2="142.24" y2="66.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="142.24" y1="66.04" x2="251.46" y2="66.04" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="66.04" x2="254" y2="68.58" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="68.58" x2="254" y2="114.3" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="114.3" x2="251.46" y2="116.84" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="116.84" x2="142.24" y2="116.84" width="0.1524" layer="97" style="shortdash"/>
<wire x1="142.24" y1="116.84" x2="139.7" y2="114.3" width="0.1524" layer="97" style="shortdash"/>
<text x="142.24" y="114.3" size="1.27" layer="97">9V to 3.3V - VOLTAGE REGULATOR</text>
<text x="22.86" y="88.9" size="1.27" layer="97" align="bottom-right">A</text>
<text x="22.86" y="99.06" size="1.27" layer="97" align="bottom-right">B</text>
<text x="22.86" y="78.74" size="1.27" layer="97" align="bottom-right">DC RETURN</text>
<text x="22.86" y="83.82" size="1.27" layer="97" align="bottom-right">10-30V</text>
<text x="22.86" y="109.22" size="1.27" layer="97" align="bottom-right">+12V</text>
<text x="22.86" y="93.98" size="1.27" layer="97" align="bottom-right">GND</text>
<text x="22.86" y="68.58" size="1.27" layer="97" align="bottom-right">PAD</text>
<text x="22.86" y="38.1" size="1.27" layer="97" align="bottom-right">A</text>
<text x="22.86" y="48.26" size="1.27" layer="97" align="bottom-right">B</text>
<text x="22.86" y="27.94" size="1.27" layer="97" align="bottom-right">DC RETURN</text>
<text x="22.86" y="33.02" size="1.27" layer="97" align="bottom-right">10-30V</text>
<text x="22.86" y="58.42" size="1.27" layer="97" align="bottom-right">+12V</text>
<text x="22.86" y="43.18" size="1.27" layer="97" align="bottom-right">GND</text>
<text x="22.86" y="17.78" size="1.27" layer="97" align="bottom-right">PAD</text>
<text x="239.014" y="142.24" size="1.27" layer="97" rot="R270">0805</text>
<text x="218.694" y="142.24" size="1.27" layer="97" rot="R270">0805</text>
<text x="178.054" y="142.24" size="1.27" layer="97" rot="R270">0603</text>
<text x="172.974" y="142.24" size="1.27" layer="97" rot="R270">0805</text>
<wire x1="139.7" y1="167.64" x2="139.7" y2="121.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="139.7" y1="121.92" x2="142.24" y2="119.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="142.24" y1="119.38" x2="251.46" y2="119.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="119.38" x2="254" y2="121.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="121.92" x2="254" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="167.64" x2="251.46" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="170.18" x2="142.24" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="142.24" y1="170.18" x2="139.7" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<text x="142.24" y="167.64" size="1.27" layer="97">9V to 5V - VOLTAGE REGULATOR</text>
<wire x1="7.62" y1="170.18" x2="134.62" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="170.18" x2="137.16" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="137.16" y1="167.64" x2="137.16" y2="121.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="137.16" y1="121.92" x2="134.62" y2="119.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="119.38" x2="7.62" y2="119.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="119.38" x2="5.08" y2="121.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="5.08" y1="121.92" x2="5.08" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="5.08" y1="167.64" x2="7.62" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<text x="7.62" y="167.64" size="1.27" layer="97">10-30Vin to 9V - VOLTAGE REGULATOR</text>
<wire x1="5.08" y1="114.3" x2="5.08" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="5.08" y1="10.16" x2="7.62" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="7.62" x2="134.62" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="7.62" x2="137.16" y2="10.16" width="0.1524" layer="97" style="shortdash"/>
<wire x1="137.16" y1="10.16" x2="137.16" y2="114.3" width="0.1524" layer="97" style="shortdash"/>
<wire x1="137.16" y1="114.3" x2="134.62" y2="116.84" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="116.84" x2="7.62" y2="116.84" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="116.84" x2="5.08" y2="114.3" width="0.1524" layer="97" style="shortdash"/>
<text x="7.62" y="114.3" size="1.27" layer="97">AISG CONNECTOR INTERFACE + SURGE PROTECTION</text>
<wire x1="142.24" y1="63.5" x2="139.7" y2="60.96" width="0.1524" layer="97" style="shortdash"/>
<wire x1="139.7" y1="60.96" x2="139.7" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="139.7" y1="27.94" x2="142.24" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<wire x1="142.24" y1="25.4" x2="251.46" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="25.4" x2="254" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="27.94" x2="254" y2="60.96" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="60.96" x2="251.46" y2="63.5" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="63.5" x2="142.24" y2="63.5" width="0.1524" layer="97" style="shortdash"/>
<text x="142.24" y="60.96" size="1.27" layer="97">EXTERNAL i2c MEMORY</text>
<text x="109.982" y="145.796" size="1.27" layer="97" rot="R270">0805</text>
<text x="33.274" y="144.78" size="1.27" layer="97" rot="R270">0805</text>
<text x="54.864" y="141.986" size="1.27" layer="97" rot="R90">0805</text>
<text x="22.86" y="104.14" size="1.27" layer="97" align="bottom-right">-48V</text>
<text x="22.86" y="73.66" size="1.27" layer="97" align="bottom-right">NC</text>
<text x="22.86" y="22.86" size="1.27" layer="97" align="bottom-right">NC</text>
<text x="22.86" y="53.34" size="1.27" layer="97" align="bottom-right">-48V</text>
<text x="157.734" y="142.24" size="1.27" layer="97" rot="R270">0805</text>
<text x="155.448" y="90.17" size="1.27" layer="97" rot="R270">0805</text>
<text x="125.222" y="145.796" size="1.27" layer="97" rot="R270">0805</text>
<text x="49.784" y="141.986" size="1.27" layer="97" rot="R90">0805</text>
</plain>
<instances>
<instance part="C11" gate="G$1" x="175.26" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="174.625" y="86.995" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="174.625" y="93.345" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C12" gate="G$1" x="170.18" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="169.545" y="86.995" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="169.545" y="93.345" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C8" gate="G$1" x="215.9" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="215.265" y="86.36" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="215.265" y="93.345" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C1" gate="G$1" x="236.22" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="235.585" y="86.36" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="235.585" y="93.345" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="154.94" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="154.305" y="86.995" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="154.305" y="93.345" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="210.82" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="210.185" y="86.995" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="210.185" y="93.345" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="205.74" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="201.295" y="84.6836" size="1.27" layer="95"/>
<attribute name="VALUE" x="201.295" y="83.058" size="1.27" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="185.42" y="76.2" smashed="yes">
<attribute name="NAME" x="184.15" y="77.6986" size="1.27" layer="95"/>
<attribute name="VALUE" x="184.15" y="72.898" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY27" gate="GND" x="177.8" y="71.12" smashed="yes">
<attribute name="VALUE" x="175.895" y="67.945" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY24" gate="GND" x="154.94" y="78.74" smashed="yes">
<attribute name="VALUE" x="153.035" y="75.565" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY30" gate="GND" x="210.82" y="73.66" smashed="yes">
<attribute name="VALUE" x="208.915" y="70.485" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY22" gate="G$1" x="152.4" y="104.14" smashed="yes">
<attribute name="VALUE" x="150.495" y="107.315" size="1.27" layer="96"/>
</instance>
<instance part="+3V39" gate="G$1" x="238.76" y="106.68" smashed="yes">
<attribute name="VALUE" x="238.76" y="106.68" size="1.27" layer="96"/>
</instance>
<instance part="J100" gate="-1" x="27.94" y="58.42" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.242" y="58.928" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-PAD" x="27.94" y="17.78" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.242" y="18.288" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-3" x="27.94" y="48.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.242" y="48.768" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-4" x="27.94" y="43.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.242" y="43.688" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-5" x="27.94" y="38.1" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.242" y="38.608" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-6" x="27.94" y="33.02" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.242" y="33.528" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-7" x="27.94" y="27.94" smashed="yes" rot="MR0">
<attribute name="NAME" x="31.242" y="28.448" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J101" gate="-1" x="27.94" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="31.242" y="108.712" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-PAD" x="27.94" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="31.242" y="68.072" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-3" x="27.94" y="99.06" smashed="yes" rot="R180">
<attribute name="NAME" x="31.242" y="98.552" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-4" x="27.94" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="31.242" y="93.472" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-5" x="27.94" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="31.242" y="88.392" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-6" x="27.94" y="83.82" smashed="yes" rot="R180">
<attribute name="NAME" x="31.242" y="83.312" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-7" x="27.94" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="31.242" y="78.232" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="D8" gate="G$1" x="58.42" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="60.96" y="56.515" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="55.88" y="55.88" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="F3" gate="G$1" x="66.04" y="60.96" smashed="yes">
<attribute name="NAME" x="67.945" y="55.245" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="63.5" y="55.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F1" gate="G$1" x="73.66" y="60.96" smashed="yes">
<attribute name="NAME" x="76.2" y="55.88" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="71.12" y="55.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F2" gate="G$1" x="81.28" y="60.96" smashed="yes">
<attribute name="NAME" x="83.185" y="55.245" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="78.74" y="55.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F4" gate="G$1" x="88.9" y="60.96" smashed="yes">
<attribute name="NAME" x="90.805" y="55.88" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="86.36" y="55.88" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C54" gate="G$1" x="177.8" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="177.165" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="177.165" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C55" gate="G$1" x="172.72" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="172.085" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="172.085" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C41" gate="G$1" x="238.76" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="238.125" y="140.335" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="238.125" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C51" gate="G$1" x="218.44" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="217.805" y="140.335" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="217.805" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C46" gate="G$1" x="157.48" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="156.845" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="156.845" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C50" gate="G$1" x="213.36" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="212.725" y="140.335" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="212.725" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R41" gate="G$1" x="208.28" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="206.7814" y="135.89" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="210.947" y="133.985" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R40" gate="G$1" x="187.96" y="129.54" smashed="yes">
<attribute name="NAME" x="182.245" y="130.4036" size="1.27" layer="95"/>
<attribute name="VALUE" x="185.42" y="126.238" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY26" gate="GND" x="180.34" y="124.46" smashed="yes">
<attribute name="VALUE" x="178.435" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY23" gate="GND" x="157.48" y="132.08" smashed="yes">
<attribute name="VALUE" x="155.575" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY29" gate="GND" x="213.36" y="132.08" smashed="yes">
<attribute name="VALUE" x="211.455" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY21" gate="G$1" x="154.94" y="157.48" smashed="yes">
<attribute name="VALUE" x="153.035" y="160.655" size="1.27" layer="96"/>
</instance>
<instance part="P+1" gate="1" x="241.3" y="160.02" smashed="yes">
<attribute name="VALUE" x="241.3" y="160.02" size="1.27" layer="96"/>
</instance>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="J101" gate="G$1" x="12.7" y="78.74" smashed="yes" rot="R90">
<attribute name="VALUE" x="12.7" y="83.82" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="J100" gate="G$1" x="12.7" y="33.02" smashed="yes" rot="R90">
<attribute name="VALUE" x="12.7" y="40.64" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U9" gate="G$1" x="195.58" y="152.4"/>
<instance part="U1" gate="G$1" x="193.04" y="99.06"/>
<instance part="U2" gate="G$1" x="185.42" y="55.88"/>
<instance part="+3V310" gate="G$1" x="238.76" y="58.42" smashed="yes" rot="MR0">
<attribute name="VALUE" x="238.76" y="58.42" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C9" gate="G$1" x="231.14" y="53.34" smashed="yes" rot="MR0">
<attribute name="NAME" x="226.695" y="52.705" size="1.27" layer="95" ratio="10" rot="MR180"/>
<attribute name="VALUE" x="233.045" y="52.705" size="1.27" layer="96" ratio="10" rot="MR180"/>
</instance>
<instance part="SUPPLY28" gate="GND" x="223.52" y="48.26" smashed="yes" rot="MR0">
<attribute name="VALUE" x="220.345" y="47.625" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="SUPPLY25" gate="GND" x="175.26" y="30.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="172.085" y="32.385" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="R5" gate="G$1" x="167.64" y="35.56" smashed="yes" rot="R180">
<attribute name="NAME" x="161.925" y="36.4236" size="1.27" layer="95"/>
<attribute name="VALUE" x="160.655" y="33.782" size="1.27" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="167.64" y="53.34" smashed="yes">
<attribute name="NAME" x="162.433" y="54.2036" size="1.27" layer="95"/>
<attribute name="VALUE" x="170.561" y="53.848" size="1.27" layer="96"/>
</instance>
<instance part="+3V38" gate="G$1" x="157.48" y="58.42" smashed="yes" rot="MR0">
<attribute name="VALUE" x="157.48" y="58.42" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R8" gate="G$1" x="167.64" y="50.8" smashed="yes">
<attribute name="NAME" x="162.433" y="51.6636" size="1.27" layer="95"/>
<attribute name="VALUE" x="170.561" y="51.308" size="1.27" layer="96"/>
</instance>
<instance part="AGND2" gate="VR1" x="58.42" y="48.26" smashed="yes">
<attribute name="VALUE" x="55.88" y="43.18" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND3" gate="VR1" x="66.04" y="48.26" smashed="yes">
<attribute name="VALUE" x="63.5" y="43.18" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND4" gate="VR1" x="73.66" y="48.26" smashed="yes">
<attribute name="VALUE" x="71.12" y="43.18" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND5" gate="VR1" x="81.28" y="48.26" smashed="yes">
<attribute name="VALUE" x="78.74" y="43.18" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND6" gate="VR1" x="88.9" y="48.26" smashed="yes">
<attribute name="VALUE" x="86.36" y="43.18" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R31" gate="G$1" x="101.6" y="99.06" smashed="yes">
<attribute name="NAME" x="96.393" y="99.9236" size="1.27" layer="95"/>
<attribute name="VALUE" x="104.521" y="99.568" size="1.27" layer="96"/>
</instance>
<instance part="R29" gate="G$1" x="101.6" y="88.9" smashed="yes">
<attribute name="NAME" x="96.393" y="89.7636" size="1.27" layer="95"/>
<attribute name="VALUE" x="104.521" y="89.408" size="1.27" layer="96"/>
</instance>
<instance part="C27" gate="G$1" x="109.22" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="109.855" y="78.105" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="109.855" y="71.755" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="C28" gate="G$1" x="114.3" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="114.935" y="78.105" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="114.935" y="71.755" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="109.22" y="60.96" smashed="yes">
<attribute name="VALUE" x="107.315" y="57.785" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY20" gate="GND" x="114.3" y="60.96" smashed="yes">
<attribute name="VALUE" x="112.395" y="57.785" size="1.27" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="101.6" y="93.98" smashed="yes">
<attribute name="NAME" x="96.393" y="94.8436" size="1.27" layer="95"/>
<attribute name="VALUE" x="104.521" y="94.488" size="1.27" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="167.64" y="43.18" smashed="yes">
<attribute name="NAME" x="162.433" y="44.0436" size="1.27" layer="95"/>
<attribute name="VALUE" x="170.561" y="43.688" size="1.27" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="220.98" y="38.1" smashed="yes">
<attribute name="NAME" x="215.773" y="38.9636" size="1.27" layer="95"/>
<attribute name="VALUE" x="223.901" y="38.608" size="1.27" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="220.98" y="33.02" smashed="yes">
<attribute name="NAME" x="215.773" y="33.8836" size="1.27" layer="95"/>
<attribute name="VALUE" x="223.901" y="33.528" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY49" gate="GND" x="43.18" y="12.7" smashed="yes">
<attribute name="VALUE" x="41.275" y="9.525" size="1.27" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="96.52" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="98.298" y="54.356" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="93.98" y="55.88" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D11" gate="G$1" x="104.14" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="106.172" y="54.102" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="101.6" y="55.88" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY50" gate="GND" x="96.52" y="48.26" smashed="yes">
<attribute name="VALUE" x="94.615" y="45.085" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY51" gate="GND" x="104.14" y="48.26" smashed="yes">
<attribute name="VALUE" x="102.235" y="45.085" size="1.27" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="63.5" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="66.802" y="91.44" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="55.88" y="96.52" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY52" gate="GND" x="76.2" y="91.44" smashed="yes">
<attribute name="VALUE" x="79.375" y="92.075" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND7" gate="VR1" x="38.1" y="12.7" smashed="yes">
<attribute name="VALUE" x="33.02" y="12.7" size="1.27" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="53.34" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="52.705" y="142.24" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="51.181" y="144.907" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C13" gate="G$1" x="33.02" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="32.385" y="142.24" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="32.385" y="149.225" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C39" gate="G$1" x="109.22" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="108.585" y="142.875" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="108.585" y="149.225" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C38" gate="G$1" x="104.14" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="103.505" y="142.875" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="103.505" y="148.59" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R19" gate="G$1" x="83.82" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="82.3214" y="146.05" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="86.487" y="144.145" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R21" gate="G$1" x="63.5" y="132.08" smashed="yes">
<attribute name="NAME" x="57.785" y="132.9436" size="1.27" layer="95"/>
<attribute name="VALUE" x="60.96" y="128.778" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="58.42" y="124.46" smashed="yes">
<attribute name="VALUE" x="56.515" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY11" gate="GND" x="25.4" y="134.62" smashed="yes">
<attribute name="VALUE" x="23.495" y="131.445" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="GND" x="129.54" y="124.46" smashed="yes">
<attribute name="VALUE" x="127.635" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="U6" gate="G$1" x="71.12" y="154.94"/>
<instance part="J101" gate="-2" x="27.94" y="104.14" rot="R180"/>
<instance part="J101" gate="-8" x="27.94" y="73.66" rot="R180"/>
<instance part="J100" gate="-2" x="27.94" y="53.34" rot="R180"/>
<instance part="J100" gate="-8" x="27.94" y="22.86" rot="R180"/>
<instance part="NC1" gate="G$1" x="35.56" y="104.14"/>
<instance part="NC2" gate="G$1" x="35.56" y="93.98"/>
<instance part="NC3" gate="G$1" x="35.56" y="73.66"/>
<instance part="NC7" gate="G$1" x="35.56" y="53.34"/>
<instance part="NC8" gate="G$1" x="35.56" y="43.18"/>
<instance part="NC9" gate="G$1" x="35.56" y="22.86"/>
<instance part="SUPPLY14" gate="G$1" x="132.08" y="160.02" smashed="yes">
<attribute name="VALUE" x="130.175" y="163.195" size="1.27" layer="96"/>
</instance>
<instance part="R33" gate="G$1" x="91.44" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="92.71" y="138.2014" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="94.615" y="142.367" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="U4" gate="G$1" x="91.44" y="132.08" smashed="yes" rot="MR90">
<attribute name="NAME" x="88.392" y="134.62" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="99.06" y="127" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C32" gate="G$1" x="106.68" y="134.62" smashed="yes">
<attribute name="NAME" x="111.125" y="133.985" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="105.41" y="133.985" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="D7" gate="G$1" x="93.98" y="147.32" smashed="yes" rot="R180">
<attribute name="NAME" x="98.044" y="146.05" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="89.662" y="150.876" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C21" gate="G$1" x="58.42" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="57.785" y="142.875" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="57.785" y="148.59" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C44" gate="G$1" x="182.88" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="182.245" y="140.335" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="182.245" y="146.05" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="180.34" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="179.705" y="86.995" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="179.705" y="92.71" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C37" gate="G$1" x="124.46" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="123.825" y="142.875" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="123.825" y="149.225" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C42" gate="G$1" x="129.54" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="129.54" y="142.24" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="129.032" y="148.59" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L5" gate="G$1" x="228.6" y="152.4" smashed="yes">
<attribute name="NAME" x="223.52" y="153.67" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="226.06" y="149.86" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="L2" gate="G$1" x="162.56" y="99.06"/>
<instance part="L6" gate="G$1" x="165.1" y="152.4"/>
<instance part="L1" gate="G$1" x="226.06" y="99.06" smashed="yes">
<attribute name="NAME" x="220.98" y="100.33" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="226.06" y="96.52" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R45" gate="G$1" x="228.6" y="157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="222.25" y="158.9786" size="1.27" layer="95"/>
<attribute name="VALUE" x="232.41" y="159.258" size="1.27" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="226.06" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="219.71" y="105.6386" size="1.27" layer="95"/>
<attribute name="VALUE" x="229.87" y="105.918" size="1.27" layer="96"/>
</instance>
<instance part="L4" gate="G$1" x="116.84" y="154.94"/>
<instance part="R36" gate="G$1" x="116.84" y="160.02" smashed="yes" rot="R180">
<attribute name="NAME" x="110.49" y="161.5186" size="1.27" layer="95"/>
<attribute name="VALUE" x="120.65" y="161.798" size="1.27" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="48.26" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="47.625" y="142.24" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="47.625" y="149.225" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="L3" gate="G$1" x="40.64" y="157.48"/>
<instance part="R17" gate="G$1" x="40.64" y="162.56" smashed="yes" rot="R180">
<attribute name="NAME" x="34.29" y="164.0586" size="1.27" layer="95"/>
<attribute name="VALUE" x="44.45" y="164.338" size="1.27" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="25.4" y="154.94" smashed="yes">
<attribute name="NAME" x="17.78" y="156.21" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="15.24" y="149.86" size="1.27" layer="96" ratio="10"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="180.34" y1="76.2" x2="177.8" y2="76.2" width="0.1524" layer="91"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
<wire x1="177.8" y1="76.2" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="154.94" y1="86.36" x2="154.94" y2="83.82" width="0.1524" layer="91"/>
<wire x1="154.94" y1="83.82" x2="170.18" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="170.18" y1="83.82" x2="175.26" y2="83.82" width="0.1524" layer="91"/>
<wire x1="175.26" y1="83.82" x2="175.26" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="170.18" y1="86.36" x2="170.18" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
<wire x1="154.94" y1="83.82" x2="154.94" y2="81.28" width="0.1524" layer="91"/>
<junction x="154.94" y="83.82"/>
<junction x="170.18" y="83.82"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="175.26" y1="83.82" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<wire x1="180.34" y1="83.82" x2="180.34" y2="86.36" width="0.1524" layer="91"/>
<junction x="175.26" y="83.82"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="215.9" y1="86.36" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="236.22" y1="83.82" x2="215.9" y2="83.82" width="0.1524" layer="91"/>
<wire x1="215.9" y1="83.82" x2="210.82" y2="83.82" width="0.1524" layer="91"/>
<wire x1="210.82" y1="83.82" x2="210.82" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
<wire x1="210.82" y1="83.82" x2="210.82" y2="76.2" width="0.1524" layer="91"/>
<junction x="210.82" y="83.82"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="236.22" y1="86.36" x2="236.22" y2="83.82" width="0.1524" layer="91"/>
<junction x="215.9" y="83.82"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="182.88" y1="129.54" x2="180.34" y2="129.54" width="0.1524" layer="91"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<wire x1="180.34" y1="129.54" x2="180.34" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="157.48" y1="139.7" x2="157.48" y2="137.16" width="0.1524" layer="91"/>
<wire x1="157.48" y1="137.16" x2="172.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="172.72" y1="137.16" x2="177.8" y2="137.16" width="0.1524" layer="91"/>
<wire x1="177.8" y1="137.16" x2="177.8" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="2"/>
<wire x1="172.72" y1="139.7" x2="172.72" y2="137.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<wire x1="157.48" y1="137.16" x2="157.48" y2="134.62" width="0.1524" layer="91"/>
<junction x="157.48" y="137.16"/>
<junction x="172.72" y="137.16"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="177.8" y1="137.16" x2="182.88" y2="137.16" width="0.1524" layer="91"/>
<wire x1="182.88" y1="137.16" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
<junction x="177.8" y="137.16"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="238.76" y1="139.7" x2="238.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="238.76" y1="137.16" x2="218.44" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="218.44" y1="137.16" x2="213.36" y2="137.16" width="0.1524" layer="91"/>
<wire x1="213.36" y1="137.16" x2="213.36" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
<wire x1="213.36" y1="137.16" x2="213.36" y2="134.62" width="0.1524" layer="91"/>
<junction x="213.36" y="137.16"/>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="218.44" y1="139.7" x2="218.44" y2="137.16" width="0.1524" layer="91"/>
<junction x="218.44" y="137.16"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="226.06" y1="53.34" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<wire x1="223.52" y1="53.34" x2="223.52" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<pinref part="U2" gate="G$1" pin="P$9"/>
<wire x1="213.36" y1="50.8" x2="218.44" y2="50.8" width="0.1524" layer="91"/>
<wire x1="218.44" y1="50.8" x2="218.44" y2="53.34" width="0.1524" layer="91"/>
<wire x1="218.44" y1="53.34" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<junction x="223.52" y="53.34"/>
</segment>
<segment>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<pinref part="U2" gate="G$1" pin="P$4"/>
<wire x1="177.8" y1="30.48" x2="182.88" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="109.22" y1="68.58" x2="109.22" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="1"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="114.3" y1="68.58" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J101" gate="-7" pin="1"/>
<wire x1="33.02" y1="78.74" x2="43.18" y2="78.74" width="0.1524" layer="91"/>
<wire x1="43.18" y1="78.74" x2="43.18" y2="27.94" width="0.1524" layer="91"/>
<pinref part="J100" gate="-7" pin="1"/>
<wire x1="43.18" y1="27.94" x2="33.02" y2="27.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="27.94" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<junction x="43.18" y="27.94"/>
</segment>
<segment>
<pinref part="D10" gate="G$1" pin="1"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
<wire x1="96.52" y1="53.34" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="1"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
<wire x1="104.14" y1="53.34" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="1"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
<wire x1="71.12" y1="93.98" x2="76.2" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="58.42" y1="132.08" x2="58.42" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="25.4" y1="139.7" x2="33.02" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="33.02" y1="139.7" x2="48.26" y2="139.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="139.7" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
<wire x1="53.34" y1="139.7" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="33.02" y1="142.24" x2="33.02" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="25.4" y1="139.7" x2="25.4" y2="137.16" width="0.1524" layer="91"/>
<junction x="33.02" y="139.7"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="53.34" y1="139.7" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="139.7" x2="58.42" y2="142.24" width="0.1524" layer="91"/>
<junction x="53.34" y="139.7"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="48.26" y1="142.24" x2="48.26" y2="139.7" width="0.1524" layer="91"/>
<junction x="48.26" y="139.7"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="109.22" y1="142.24" x2="109.22" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="104.14" y1="139.7" x2="104.14" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="104.14" y1="139.7" x2="109.22" y2="139.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="139.7" x2="124.46" y2="139.7" width="0.1524" layer="91"/>
<wire x1="124.46" y1="139.7" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<wire x1="129.54" y1="139.7" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
<wire x1="129.54" y1="134.62" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
<wire x1="129.54" y1="129.54" x2="129.54" y2="127" width="0.1524" layer="91"/>
<wire x1="111.76" y1="134.62" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
<junction x="129.54" y="134.62"/>
<wire x1="96.52" y1="129.54" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
<junction x="129.54" y="129.54"/>
<pinref part="U4" gate="G$1" pin="C"/>
<pinref part="C32" gate="G$1" pin="2"/>
<junction x="109.22" y="139.7"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="129.54" y1="142.24" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="124.46" y1="142.24" x2="124.46" y2="139.7" width="0.1524" layer="91"/>
<junction x="124.46" y="139.7"/>
<junction x="129.54" y="139.7"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="238.76" y1="99.06" x2="241.3" y2="99.06" width="0.1524" layer="91"/>
<pinref part="+3V39" gate="G$1" pin="+3V3"/>
<wire x1="238.76" y1="104.14" x2="238.76" y2="99.06" width="0.1524" layer="91"/>
<junction x="238.76" y="99.06"/>
<wire x1="236.22" y1="99.06" x2="238.76" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="236.22" y1="96.52" x2="236.22" y2="99.06" width="0.1524" layer="91"/>
<label x="241.3" y="99.06" size="1.27" layer="95" xref="yes"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="231.14" y1="99.06" x2="233.68" y2="99.06" width="0.1524" layer="91"/>
<junction x="236.22" y="99.06"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="233.68" y1="99.06" x2="236.22" y2="99.06" width="0.1524" layer="91"/>
<wire x1="231.14" y1="104.14" x2="233.68" y2="104.14" width="0.1524" layer="91"/>
<wire x1="233.68" y1="104.14" x2="233.68" y2="99.06" width="0.1524" layer="91"/>
<junction x="233.68" y="99.06"/>
</segment>
<segment>
<pinref part="+3V310" gate="G$1" pin="+3V3"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="238.76" y1="53.34" x2="238.76" y2="55.88" width="0.1524" layer="91"/>
<wire x1="238.76" y1="53.34" x2="236.22" y2="53.34" width="0.1524" layer="91"/>
<junction x="238.76" y="53.34"/>
<pinref part="U2" gate="G$1" pin="P$8"/>
<wire x1="213.36" y1="45.72" x2="218.44" y2="45.72" width="0.1524" layer="91"/>
<wire x1="218.44" y1="45.72" x2="228.6" y2="45.72" width="0.1524" layer="91"/>
<wire x1="228.6" y1="45.72" x2="238.76" y2="45.72" width="0.1524" layer="91"/>
<wire x1="238.76" y1="45.72" x2="238.76" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="P$7"/>
<wire x1="213.36" y1="40.64" x2="218.44" y2="40.64" width="0.1524" layer="91"/>
<wire x1="218.44" y1="40.64" x2="218.44" y2="45.72" width="0.1524" layer="91"/>
<junction x="218.44" y="45.72"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="226.06" y1="38.1" x2="228.6" y2="38.1" width="0.1524" layer="91"/>
<wire x1="228.6" y1="38.1" x2="228.6" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="226.06" y1="33.02" x2="228.6" y2="33.02" width="0.1524" layer="91"/>
<wire x1="228.6" y1="33.02" x2="228.6" y2="38.1" width="0.1524" layer="91"/>
<junction x="228.6" y="45.72"/>
<junction x="228.6" y="38.1"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="162.56" y1="53.34" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="157.48" y1="53.34" x2="157.48" y2="55.88" width="0.1524" layer="91"/>
<pinref part="+3V38" gate="G$1" pin="+3V3"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="162.56" y1="50.8" x2="157.48" y2="50.8" width="0.1524" layer="91"/>
<wire x1="157.48" y1="50.8" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<junction x="157.48" y="53.34"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="162.56" y1="43.18" x2="157.48" y2="43.18" width="0.1524" layer="91"/>
<wire x1="157.48" y1="43.18" x2="157.48" y2="50.8" width="0.1524" layer="91"/>
<junction x="157.48" y="50.8"/>
</segment>
</net>
<net name="+9V" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="154.94" y1="96.52" x2="154.94" y2="99.06" width="0.1524" layer="91"/>
<wire x1="154.94" y1="99.06" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="152.4" y1="99.06" x2="149.86" y2="99.06" width="0.1524" layer="91"/>
<junction x="152.4" y="99.06"/>
<pinref part="SUPPLY22" gate="G$1" pin="+9V"/>
<wire x1="152.4" y1="101.6" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
<label x="149.86" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="154.94" y1="99.06" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<junction x="154.94" y="99.06"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="157.48" y1="149.86" x2="157.48" y2="152.4" width="0.1524" layer="91"/>
<wire x1="157.48" y1="152.4" x2="154.94" y2="152.4" width="0.1524" layer="91"/>
<pinref part="SUPPLY21" gate="G$1" pin="+9V"/>
<wire x1="154.94" y1="154.94" x2="154.94" y2="152.4" width="0.1524" layer="91"/>
<junction x="154.94" y="152.4"/>
<wire x1="154.94" y1="152.4" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
<label x="152.4" y="152.4" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="157.48" y1="152.4" x2="160.02" y2="152.4" width="0.1524" layer="91"/>
<junction x="157.48" y="152.4"/>
</segment>
<segment>
<pinref part="SUPPLY14" gate="G$1" pin="+9V"/>
<wire x1="132.08" y1="154.94" x2="132.08" y2="157.48" width="0.1524" layer="91"/>
<wire x1="132.08" y1="154.94" x2="134.62" y2="154.94" width="0.1524" layer="91"/>
<junction x="132.08" y="154.94"/>
<wire x1="129.54" y1="154.94" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="129.54" y1="152.4" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<junction x="129.54" y="154.94"/>
<wire x1="124.46" y1="154.94" x2="127" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="127" y1="154.94" x2="129.54" y2="154.94" width="0.1524" layer="91"/>
<wire x1="124.46" y1="152.4" x2="124.46" y2="154.94" width="0.1524" layer="91"/>
<label x="134.62" y="154.94" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="121.92" y1="154.94" x2="124.46" y2="154.94" width="0.1524" layer="91"/>
<junction x="124.46" y="154.94"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="121.92" y1="160.02" x2="127" y2="160.02" width="0.1524" layer="91"/>
<wire x1="127" y1="160.02" x2="127" y2="154.94" width="0.1524" layer="91"/>
<junction x="127" y="154.94"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="205.74" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<wire x1="205.74" y1="76.2" x2="193.04" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="193.04" y1="76.2" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="193.04" y="76.2"/>
<wire x1="193.04" y1="91.44" x2="193.04" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J101" gate="-1" pin="1"/>
<wire x1="33.02" y1="109.22" x2="45.72" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J100" gate="-1" pin="1"/>
<wire x1="33.02" y1="58.42" x2="45.72" y2="58.42" width="0.1524" layer="91"/>
<wire x1="45.72" y1="58.42" x2="45.72" y2="109.22" width="0.1524" layer="91"/>
<junction x="45.72" y="109.22"/>
<pinref part="F4" gate="G$1" pin="1"/>
<wire x1="45.72" y1="109.22" x2="88.9" y2="109.22" width="0.1524" layer="91"/>
<wire x1="88.9" y1="109.22" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U9" gate="G$1" pin="1"/>
<wire x1="195.58" y1="144.78" x2="195.58" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="208.28" y1="132.08" x2="208.28" y2="129.54" width="0.1524" layer="91"/>
<wire x1="208.28" y1="129.54" x2="195.58" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="195.58" y1="129.54" x2="193.04" y2="129.54" width="0.1524" layer="91"/>
<junction x="195.58" y="129.54"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="241.3" y1="152.4" x2="243.84" y2="152.4" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="241.3" y1="157.48" x2="241.3" y2="152.4" width="0.1524" layer="91"/>
<junction x="241.3" y="152.4"/>
<wire x1="238.76" y1="152.4" x2="241.3" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="238.76" y1="149.86" x2="238.76" y2="152.4" width="0.1524" layer="91"/>
<label x="243.84" y="152.4" size="1.27" layer="95" xref="yes"/>
<pinref part="L5" gate="G$1" pin="2"/>
<wire x1="233.68" y1="152.4" x2="236.22" y2="152.4" width="0.1524" layer="91"/>
<junction x="238.76" y="152.4"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="236.22" y1="152.4" x2="238.76" y2="152.4" width="0.1524" layer="91"/>
<wire x1="233.68" y1="157.48" x2="236.22" y2="157.48" width="0.1524" layer="91"/>
<wire x1="236.22" y1="157.48" x2="236.22" y2="152.4" width="0.1524" layer="91"/>
<junction x="236.22" y="152.4"/>
</segment>
</net>
<net name="RC3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="P$6"/>
<wire x1="213.36" y1="35.56" x2="215.9" y2="35.56" width="0.1524" layer="91"/>
<label x="231.14" y="35.56" size="1.27" layer="95" xref="yes"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="215.9" y1="35.56" x2="231.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="215.9" y1="38.1" x2="215.9" y2="35.56" width="0.1524" layer="91"/>
<junction x="215.9" y="35.56"/>
</segment>
</net>
<net name="RC5" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="P$5"/>
<wire x1="213.36" y1="30.48" x2="215.9" y2="30.48" width="0.1524" layer="91"/>
<label x="231.14" y="30.48" size="1.27" layer="95" xref="yes"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="215.9" y1="30.48" x2="231.14" y2="30.48" width="0.1524" layer="91"/>
<wire x1="215.9" y1="33.02" x2="215.9" y2="30.48" width="0.1524" layer="91"/>
<junction x="215.9" y="30.48"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="P$3"/>
<wire x1="182.88" y1="35.56" x2="175.26" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="172.72" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<wire x1="175.26" y1="53.34" x2="175.26" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="175.26" y1="35.56" x2="172.72" y2="35.56" width="0.1524" layer="91"/>
<junction x="175.26" y="35.56"/>
</segment>
</net>
<net name="RC4" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="P$2"/>
<wire x1="182.88" y1="40.64" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<label x="154.94" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="172.72" y1="40.64" x2="154.94" y2="40.64" width="0.1524" layer="91"/>
<wire x1="172.72" y1="43.18" x2="172.72" y2="40.64" width="0.1524" layer="91"/>
<junction x="172.72" y="40.64"/>
</segment>
</net>
<net name="RJ5" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="162.56" y1="35.56" x2="154.94" y2="35.56" width="0.1524" layer="91"/>
<label x="154.94" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RA3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="P$1"/>
<wire x1="182.88" y1="45.72" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="172.72" y1="45.72" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="172.72" y1="50.8" x2="172.72" y2="45.72" width="0.1524" layer="91"/>
<junction x="172.72" y="45.72"/>
<label x="154.94" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="+"/>
<pinref part="AGND2" gate="VR1" pin="AGND"/>
<wire x1="58.42" y1="55.88" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F3" gate="G$1" pin="2"/>
<pinref part="AGND3" gate="VR1" pin="AGND"/>
<wire x1="66.04" y1="55.88" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="2"/>
<pinref part="AGND4" gate="VR1" pin="AGND"/>
<wire x1="73.66" y1="55.88" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F2" gate="G$1" pin="2"/>
<pinref part="AGND5" gate="VR1" pin="AGND"/>
<wire x1="81.28" y1="55.88" x2="81.28" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F4" gate="G$1" pin="2"/>
<pinref part="AGND6" gate="VR1" pin="AGND"/>
<wire x1="88.9" y1="55.88" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J101" gate="-PAD" pin="1"/>
<wire x1="33.02" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
<pinref part="J100" gate="-PAD" pin="1"/>
<wire x1="33.02" y1="17.78" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<wire x1="38.1" y1="68.58" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<wire x1="38.1" y1="17.78" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<pinref part="AGND7" gate="VR1" pin="AGND"/>
<junction x="38.1" y="17.78"/>
</segment>
</net>
<net name="IN_P" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="106.68" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<label x="119.38" y="88.9" size="1.27" layer="95" xref="yes"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="109.22" y1="88.9" x2="119.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="109.22" y1="78.74" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<junction x="109.22" y="88.9"/>
</segment>
</net>
<net name="IN_N" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="106.68" y1="99.06" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<label x="119.38" y="99.06" size="1.27" layer="95" xref="yes"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="109.22" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<wire x1="114.3" y1="99.06" x2="119.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="114.3" y1="78.74" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<junction x="114.3" y="99.06"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="106.68" y1="93.98" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="93.98" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<junction x="109.22" y="99.06"/>
</segment>
</net>
<net name="AISG_P" class="0">
<segment>
<pinref part="J101" gate="-3" pin="1"/>
<wire x1="33.02" y1="99.06" x2="48.26" y2="99.06" width="0.1524" layer="91"/>
<pinref part="J100" gate="-3" pin="1"/>
<wire x1="33.02" y1="48.26" x2="48.26" y2="48.26" width="0.1524" layer="91"/>
<wire x1="48.26" y1="48.26" x2="48.26" y2="99.06" width="0.1524" layer="91"/>
<junction x="48.26" y="99.06"/>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="48.26" y1="99.06" x2="81.28" y2="99.06" width="0.1524" layer="91"/>
<wire x1="81.28" y1="99.06" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="81.28" y1="78.74" x2="81.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="81.28" y1="99.06" x2="96.52" y2="99.06" width="0.1524" layer="91"/>
<junction x="81.28" y="99.06"/>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="D11" gate="G$1" pin="2"/>
<wire x1="104.14" y1="68.58" x2="104.14" y2="78.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="78.74" x2="81.28" y2="78.74" width="0.1524" layer="91"/>
<junction x="81.28" y="78.74"/>
</segment>
</net>
<net name="AISG_N" class="0">
<segment>
<pinref part="J101" gate="-5" pin="1"/>
<wire x1="33.02" y1="88.9" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J100" gate="-5" pin="1"/>
<wire x1="33.02" y1="38.1" x2="50.8" y2="38.1" width="0.1524" layer="91"/>
<wire x1="50.8" y1="38.1" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<junction x="50.8" y="88.9"/>
<pinref part="F1" gate="G$1" pin="1"/>
<wire x1="50.8" y1="88.9" x2="73.66" y2="88.9" width="0.1524" layer="91"/>
<wire x1="73.66" y1="88.9" x2="73.66" y2="73.66" width="0.1524" layer="91"/>
<wire x1="73.66" y1="73.66" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="73.66" y1="88.9" x2="93.98" y2="88.9" width="0.1524" layer="91"/>
<junction x="73.66" y="88.9"/>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="93.98" y1="88.9" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="93.98" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<wire x1="93.98" y1="93.98" x2="93.98" y2="88.9" width="0.1524" layer="91"/>
<junction x="93.98" y="88.9"/>
<pinref part="D10" gate="G$1" pin="2"/>
<wire x1="96.52" y1="68.58" x2="96.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="96.52" y1="73.66" x2="73.66" y2="73.66" width="0.1524" layer="91"/>
<junction x="73.66" y="73.66"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="1"/>
<wire x1="83.82" y1="132.08" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="71.12" y1="132.08" x2="68.58" y2="132.08" width="0.1524" layer="91"/>
<junction x="71.12" y="132.08"/>
<wire x1="86.36" y1="129.54" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="129.54" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="83.82" y1="142.24" x2="83.82" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="83.82" y1="139.7" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<wire x1="86.36" y1="139.7" x2="83.82" y2="139.7" width="0.1524" layer="91"/>
<junction x="83.82" y="139.7"/>
<junction x="83.82" y="132.08"/>
<pinref part="U4" gate="G$1" pin="E"/>
<wire x1="71.12" y1="147.32" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="10-30VIN" class="0">
<segment>
<wire x1="10.16" y1="162.56" x2="10.16" y2="154.94" width="0.1524" layer="91"/>
<wire x1="10.16" y1="154.94" x2="20.32" y2="154.94" width="0.1524" layer="91"/>
<label x="10.16" y="162.56" size="1.778" layer="95" xref="yes"/>
<pinref part="D6" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="J100" gate="-6" pin="1"/>
<wire x1="33.02" y1="33.02" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="33.02" x2="53.34" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J101" gate="-6" pin="1"/>
<wire x1="33.02" y1="83.82" x2="53.34" y2="83.82" width="0.1524" layer="91"/>
<wire x1="53.34" y1="83.82" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
<junction x="53.34" y="83.82"/>
<pinref part="D8" gate="G$1" pin="-"/>
<wire x1="55.88" y1="83.82" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="58.42" y1="83.82" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<pinref part="F3" gate="G$1" pin="1"/>
<wire x1="58.42" y1="83.82" x2="66.04" y2="83.82" width="0.1524" layer="91"/>
<wire x1="66.04" y1="83.82" x2="66.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="66.04" y1="83.82" x2="116.84" y2="83.82" width="0.1524" layer="91"/>
<junction x="58.42" y="83.82"/>
<junction x="66.04" y="83.82"/>
<label x="116.84" y="83.82" size="1.27" layer="95" xref="yes"/>
<pinref part="D3" gate="G$1" pin="2"/>
<wire x1="55.88" y1="93.98" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
<junction x="55.88" y="83.82"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="3"/>
<wire x1="48.26" y1="154.94" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="154.94" x2="53.34" y2="154.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="154.94" x2="58.42" y2="154.94" width="0.1524" layer="91"/>
<wire x1="58.42" y1="154.94" x2="60.96" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="53.34" y1="152.4" x2="53.34" y2="154.94" width="0.1524" layer="91"/>
<junction x="53.34" y="154.94"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="58.42" y1="152.4" x2="58.42" y2="154.94" width="0.1524" layer="91"/>
<junction x="58.42" y="154.94"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="48.26" y1="152.4" x2="48.26" y2="154.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="154.94" x2="50.8" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="45.72" y1="162.56" x2="48.26" y2="162.56" width="0.1524" layer="91"/>
<wire x1="48.26" y1="162.56" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="2"/>
<wire x1="48.26" y1="160.02" x2="48.26" y2="157.48" width="0.1524" layer="91"/>
<wire x1="48.26" y1="157.48" x2="45.72" y2="157.48" width="0.1524" layer="91"/>
<wire x1="50.8" y1="160.02" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
<junction x="50.8" y="154.94"/>
<junction x="48.26" y="160.02"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="J100" gate="-8" pin="1"/>
<wire x1="33.02" y1="22.86" x2="35.56" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="J100" gate="-4" pin="1"/>
<wire x1="33.02" y1="43.18" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="J100" gate="-2" pin="1"/>
<wire x1="33.02" y1="53.34" x2="35.56" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="J101" gate="-8" pin="1"/>
<wire x1="33.02" y1="73.66" x2="35.56" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="J101" gate="-4" pin="1"/>
<wire x1="33.02" y1="93.98" x2="35.56" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="J101" gate="-2" pin="1"/>
<wire x1="33.02" y1="104.14" x2="35.56" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="96.52" y1="139.7" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<wire x1="99.06" y1="139.7" x2="99.06" y2="134.62" width="0.1524" layer="91"/>
<wire x1="99.06" y1="139.7" x2="99.06" y2="147.32" width="0.1524" layer="91"/>
<junction x="99.06" y="139.7"/>
<wire x1="91.44" y1="134.62" x2="99.06" y2="134.62" width="0.1524" layer="91"/>
<wire x1="101.6" y1="134.62" x2="99.06" y2="134.62" width="0.1524" layer="91"/>
<junction x="99.06" y="134.62"/>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="B"/>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="D7" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="185.42" y1="152.4" x2="182.88" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="182.88" y1="152.4" x2="177.8" y2="152.4" width="0.1524" layer="91"/>
<wire x1="177.8" y1="152.4" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<wire x1="172.72" y1="149.86" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="177.8" y1="149.86" x2="177.8" y2="152.4" width="0.1524" layer="91"/>
<junction x="177.8" y="152.4"/>
<pinref part="U9" gate="G$1" pin="3"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="182.88" y1="149.86" x2="182.88" y2="152.4" width="0.1524" layer="91"/>
<junction x="182.88" y="152.4"/>
<pinref part="L6" gate="G$1" pin="2"/>
<wire x1="170.18" y1="152.4" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<junction x="172.72" y="152.4"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="182.88" y1="99.06" x2="180.34" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="180.34" y1="99.06" x2="175.26" y2="99.06" width="0.1524" layer="91"/>
<wire x1="175.26" y1="99.06" x2="170.18" y2="99.06" width="0.1524" layer="91"/>
<wire x1="170.18" y1="96.52" x2="170.18" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="175.26" y1="96.52" x2="175.26" y2="99.06" width="0.1524" layer="91"/>
<junction x="175.26" y="99.06"/>
<pinref part="U1" gate="G$1" pin="3"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="180.34" y1="96.52" x2="180.34" y2="99.06" width="0.1524" layer="91"/>
<junction x="180.34" y="99.06"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="167.64" y1="99.06" x2="170.18" y2="99.06" width="0.1524" layer="91"/>
<junction x="170.18" y="99.06"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="205.74" y1="152.4" x2="208.28" y2="152.4" width="0.1524" layer="91"/>
<wire x1="208.28" y1="152.4" x2="213.36" y2="152.4" width="0.1524" layer="91"/>
<wire x1="213.36" y1="152.4" x2="218.44" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="218.44" y1="149.86" x2="218.44" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="213.36" y1="152.4" x2="213.36" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="208.28" y1="152.4" x2="208.28" y2="142.24" width="0.1524" layer="91"/>
<junction x="213.36" y="152.4"/>
<junction x="208.28" y="152.4"/>
<pinref part="U9" gate="G$1" pin="2"/>
<pinref part="L5" gate="G$1" pin="1"/>
<wire x1="218.44" y1="152.4" x2="220.98" y2="152.4" width="0.1524" layer="91"/>
<junction x="218.44" y="152.4"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="220.98" y1="152.4" x2="223.52" y2="152.4" width="0.1524" layer="91"/>
<wire x1="223.52" y1="157.48" x2="220.98" y2="157.48" width="0.1524" layer="91"/>
<wire x1="220.98" y1="157.48" x2="220.98" y2="152.4" width="0.1524" layer="91"/>
<junction x="220.98" y="152.4"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="203.2" y1="99.06" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="205.74" y1="99.06" x2="210.82" y2="99.06" width="0.1524" layer="91"/>
<wire x1="210.82" y1="99.06" x2="215.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="215.9" y1="96.52" x2="215.9" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="210.82" y1="99.06" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="205.74" y1="99.06" x2="205.74" y2="88.9" width="0.1524" layer="91"/>
<junction x="210.82" y="99.06"/>
<junction x="205.74" y="99.06"/>
<pinref part="U1" gate="G$1" pin="2"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="215.9" y1="99.06" x2="218.44" y2="99.06" width="0.1524" layer="91"/>
<junction x="215.9" y="99.06"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="218.44" y1="99.06" x2="220.98" y2="99.06" width="0.1524" layer="91"/>
<wire x1="220.98" y1="104.14" x2="218.44" y2="104.14" width="0.1524" layer="91"/>
<wire x1="218.44" y1="104.14" x2="218.44" y2="99.06" width="0.1524" layer="91"/>
<junction x="218.44" y="99.06"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="U6" gate="G$1" pin="2"/>
<wire x1="81.28" y1="154.94" x2="83.82" y2="154.94" width="0.1524" layer="91"/>
<wire x1="83.82" y1="154.94" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
<wire x1="88.9" y1="154.94" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="154.94" x2="106.68" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="106.68" y1="154.94" x2="109.22" y2="154.94" width="0.1524" layer="91"/>
<wire x1="83.82" y1="152.4" x2="83.82" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="109.22" y1="152.4" x2="109.22" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="104.14" y1="152.4" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<junction x="83.82" y="154.94"/>
<junction x="104.14" y="154.94"/>
<pinref part="D7" gate="G$1" pin="-"/>
<wire x1="88.9" y1="147.32" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
<junction x="88.9" y="154.94"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="111.76" y1="154.94" x2="109.22" y2="154.94" width="0.1524" layer="91"/>
<junction x="109.22" y="154.94"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="111.76" y1="160.02" x2="106.68" y2="160.02" width="0.1524" layer="91"/>
<wire x1="106.68" y1="160.02" x2="106.68" y2="154.94" width="0.1524" layer="91"/>
<junction x="106.68" y="154.94"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="33.02" y1="152.4" x2="33.02" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="33.02" y1="154.94" x2="33.02" y2="157.48" width="0.1524" layer="91"/>
<wire x1="33.02" y1="157.48" x2="33.02" y2="162.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="162.56" x2="35.56" y2="162.56" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="1"/>
<wire x1="30.48" y1="154.94" x2="33.02" y2="154.94" width="0.1524" layer="91"/>
<junction x="33.02" y="154.94"/>
<pinref part="L3" gate="G$1" pin="1"/>
<wire x1="35.56" y1="157.48" x2="33.02" y2="157.48" width="0.1524" layer="91"/>
<junction x="33.02" y="157.48"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="10.16" y1="27.94" x2="71.12" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="71.12" y1="27.94" x2="73.66" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<wire x1="73.66" y1="25.4" x2="73.66" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="73.66" y1="7.62" x2="71.12" y2="5.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="71.12" y1="5.08" x2="10.16" y2="5.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="5.08" x2="7.62" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="25.4" x2="10.16" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="170.18" x2="251.46" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="170.18" x2="254" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="167.64" x2="254" y2="124.46" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="124.46" x2="251.46" y2="121.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="121.92" x2="154.94" y2="121.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="121.92" x2="152.4" y2="124.46" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="167.64" x2="154.94" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="124.46" x2="152.4" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="170.18" x2="147.32" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<wire x1="147.32" y1="170.18" x2="149.86" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="149.86" y1="167.64" x2="149.86" y2="93.98" width="0.1524" layer="97" style="shortdash"/>
<wire x1="149.86" y1="93.98" x2="147.32" y2="91.44" width="0.1524" layer="97" style="shortdash"/>
<wire x1="147.32" y1="91.44" x2="10.16" y2="91.44" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="91.44" x2="7.62" y2="93.98" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="93.98" x2="7.62" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="167.64" x2="10.16" y2="170.18" width="0.1524" layer="97" style="shortdash"/>
<text x="10.16" y="25.4" size="1.27" layer="97">MOTOR DRIVE HEADER </text>
<text x="154.94" y="167.64" size="1.27" layer="97">MOTOR DRIVER - POWER DECOUPLING</text>
<text x="10.16" y="167.64" size="1.27" layer="97">STEPPER MOTOR DRIVER</text>
<text x="240.411" y="139.7" size="1.27" layer="97" rot="R90">0603</text>
<text x="232.791" y="139.7" size="1.27" layer="97" rot="R90">0603</text>
<text x="185.674" y="142.24" size="1.27" layer="97" rot="R270">0603</text>
<text x="193.294" y="142.24" size="1.27" layer="97" rot="R270">0603</text>
<wire x1="7.62" y1="83.82" x2="10.16" y2="86.36" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="86.36" x2="147.32" y2="86.36" width="0.1524" layer="97" style="shortdash"/>
<wire x1="147.32" y1="86.36" x2="149.86" y2="83.82" width="0.1524" layer="97" style="shortdash"/>
<wire x1="149.86" y1="83.82" x2="149.86" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="149.86" y1="33.02" x2="147.32" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="147.32" y1="30.48" x2="10.16" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="30.48" x2="7.62" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="33.02" x2="7.62" y2="83.82" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="119.38" x2="152.4" y2="116.84" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="116.84" x2="152.4" y2="93.98" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="93.98" x2="154.94" y2="91.44" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="91.44" x2="251.46" y2="91.44" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="91.44" x2="254" y2="93.98" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="93.98" x2="254" y2="116.84" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="116.84" x2="251.46" y2="119.38" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="119.38" x2="154.94" y2="119.38" width="0.1524" layer="97" style="shortdash"/>
<text x="154.94" y="116.84" size="1.27" layer="97">POWER LED</text>
<text x="10.16" y="83.82" size="1.27" layer="97">I2C LEVEL SHIFTER AND ACCELERTOR</text>
<text x="233.426" y="65.278" size="1.27" layer="94">Rx Out-&gt;RXD</text>
<text x="233.172" y="44.958" size="1.27" layer="94">Driver In &lt;-TXD</text>
<text x="233.426" y="55.118" size="1.27" layer="94">INT0</text>
<wire x1="154.94" y1="86.36" x2="251.46" y2="86.36" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="86.36" x2="254" y2="83.82" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="83.82" x2="254" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="254" y1="33.02" x2="251.46" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="251.46" y1="30.48" x2="154.94" y2="30.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="154.94" y1="30.48" x2="152.4" y2="33.02" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="33.02" x2="152.4" y2="83.82" width="0.1524" layer="97" style="shortdash"/>
<wire x1="152.4" y1="83.82" x2="154.94" y2="86.36" width="0.1524" layer="97" style="shortdash"/>
<text x="154.94" y="83.82" size="1.27" layer="97">RS485 to ASYNC EUSUART</text>
<text x="236.22" y="114.3" size="1.27" layer="97">GREEN</text>
<text x="236.22" y="106.68" size="1.27" layer="97">BLUE</text>
<text x="236.22" y="99.695" size="1.27" layer="97">YELLOW</text>
<wire x1="76.2" y1="25.4" x2="76.2" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="76.2" y1="7.62" x2="78.74" y2="5.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="78.74" y1="5.08" x2="147.32" y2="5.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="147.32" y1="5.08" x2="149.86" y2="7.62" width="0.1524" layer="97" style="shortdash"/>
<wire x1="149.86" y1="7.62" x2="149.86" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<wire x1="149.86" y1="25.4" x2="147.32" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="147.32" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="97" style="shortdash"/>
<wire x1="78.74" y1="27.94" x2="76.2" y2="25.4" width="0.1524" layer="97" style="shortdash"/>
<text x="78.74" y="25.4" size="1.27" layer="97">ANALOG-DIGITAL INTERFACE HEADER</text>
<text x="161.671" y="139.7" size="1.27" layer="97" rot="R90">0603</text>
<text x="179.451" y="139.7" size="1.27" layer="97" rot="R90">0603</text>
<text x="39.751" y="129.54" size="1.27" layer="97" rot="R90">0603</text>
<text x="52.451" y="106.68" size="1.27" layer="97" rot="R90">0603</text>
</plain>
<instances>
<instance part="U8" gate="G$1" x="83.82" y="162.56" smashed="yes">
<attribute name="NAME" x="83.82" y="165.1" size="1.27" layer="95"/>
<attribute name="VALUE" x="83.82" y="167.64" size="1.27" layer="96"/>
</instance>
<instance part="C45" gate="G$1" x="124.46" y="116.84" smashed="yes">
<attribute name="NAME" x="120.015" y="117.475" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="125.73" y="117.475" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C48" gate="G$1" x="124.46" y="121.92" smashed="yes">
<attribute name="NAME" x="120.015" y="122.555" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="125.73" y="122.555" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY39" gate="GND" x="116.84" y="101.6" smashed="yes">
<attribute name="VALUE" x="114.935" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY40" gate="G$1" x="154.94" y="157.48" smashed="yes">
<attribute name="VALUE" x="153.035" y="160.655" size="1.27" layer="96"/>
</instance>
<instance part="C56" gate="G$1" x="177.8" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="177.165" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="177.165" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C36" gate="G$1" x="215.9" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="215.265" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="215.265" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C35" gate="G$1" x="223.52" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="222.885" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="222.885" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C49" gate="G$1" x="208.28" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="207.645" y="140.335" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="207.645" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY37" gate="GND" x="73.66" y="101.6" smashed="yes">
<attribute name="VALUE" x="71.755" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="C47" gate="G$1" x="200.66" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="200.025" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="200.025" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C26" gate="G$1" x="231.14" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="230.505" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="230.505" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C30" gate="G$1" x="238.76" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="238.125" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="238.125" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C52" gate="G$1" x="193.04" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="192.405" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="192.405" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C53" gate="G$1" x="185.42" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="184.785" y="139.7" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="184.785" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY46" gate="GND" x="208.28" y="132.08" smashed="yes">
<attribute name="VALUE" x="206.375" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY47" gate="GND" x="238.76" y="132.08" smashed="yes">
<attribute name="VALUE" x="236.855" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="NC5" gate="G$1" x="76.2" y="124.46" smashed="yes"/>
<instance part="NC4" gate="G$1" x="76.2" y="139.7" smashed="yes"/>
<instance part="C57" gate="G$1" x="160.02" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="159.385" y="140.335" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="159.385" y="146.685" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY44" gate="GND" x="177.8" y="132.08" smashed="yes">
<attribute name="VALUE" x="175.895" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="NC6" gate="G$1" x="76.2" y="116.84" smashed="yes"/>
<instance part="C43" gate="G$1" x="50.8" y="111.76" smashed="yes" rot="R270">
<attribute name="NAME" x="50.165" y="106.68" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="50.165" y="113.665" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R44" gate="G$1" x="60.96" y="119.38" smashed="yes">
<attribute name="NAME" x="55.245" y="120.015" size="1.27" layer="95"/>
<attribute name="VALUE" x="64.135" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY36" gate="GND" x="50.8" y="101.6" smashed="yes">
<attribute name="VALUE" x="48.895" y="98.425" size="1.27" layer="96"/>
</instance>
<instance part="R37" gate="G$1" x="48.26" y="142.24" smashed="yes">
<attribute name="NAME" x="41.91" y="142.875" size="1.27" layer="95"/>
<attribute name="VALUE" x="51.435" y="142.748" size="1.27" layer="96"/>
</instance>
<instance part="C34" gate="G$1" x="38.1" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="37.465" y="129.54" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="37.465" y="135.89" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY34" gate="GND" x="38.1" y="124.46" smashed="yes">
<attribute name="VALUE" x="36.195" y="121.285" size="1.27" layer="96"/>
</instance>
<instance part="R38" gate="G$1" x="25.4" y="142.24" smashed="yes">
<attribute name="NAME" x="19.685" y="142.875" size="1.27" layer="95"/>
<attribute name="VALUE" x="29.083" y="142.748" size="1.27" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="25.4" y="119.38" smashed="yes">
<attribute name="NAME" x="20.32" y="120.2436" size="1.27" layer="95"/>
<attribute name="VALUE" x="28.956" y="119.888" size="1.27" layer="96"/>
</instance>
<instance part="R35" gate="G$1" x="25.4" y="137.16" smashed="yes">
<attribute name="NAME" x="19.685" y="137.795" size="1.27" layer="95"/>
<attribute name="VALUE" x="20.193" y="134.239" size="1.27" layer="96"/>
</instance>
<instance part="R46" gate="G$1" x="25.4" y="114.3" smashed="yes">
<attribute name="NAME" x="20.32" y="115.1636" size="1.27" layer="95"/>
<attribute name="VALUE" x="22.352" y="111.633" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY31" gate="GND" x="17.78" y="132.08" smashed="yes">
<attribute name="VALUE" x="15.875" y="128.905" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY32" gate="GND" x="17.78" y="109.22" smashed="yes">
<attribute name="VALUE" x="15.875" y="106.045" size="1.27" layer="96"/>
</instance>
<instance part="FRAME3" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="U7" gate="G$1" x="66.04" y="43.18" smashed="yes">
<attribute name="NAME" x="58.42" y="58.42" size="1.27" layer="95" ratio="15"/>
<attribute name="VALUE" x="58.42" y="60.96" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="C40" gate="G$1" x="111.76" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="111.125" y="63.5" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="111.125" y="70.485" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C31" gate="G$1" x="25.4" y="68.58" smashed="yes" rot="R270">
<attribute name="NAME" x="24.765" y="63.5" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="24.765" y="70.485" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY35" gate="GND" x="43.18" y="40.64" smashed="yes">
<attribute name="VALUE" x="41.275" y="37.465" size="1.27" layer="96"/>
</instance>
<instance part="R42" gate="G$1" x="124.46" y="76.2" smashed="yes">
<attribute name="NAME" x="118.745" y="76.835" size="1.27" layer="95"/>
<attribute name="VALUE" x="127.635" y="76.708" size="1.27" layer="96"/>
</instance>
<instance part="R43" gate="G$1" x="124.46" y="68.58" smashed="yes">
<attribute name="NAME" x="118.745" y="69.215" size="1.27" layer="95"/>
<attribute name="VALUE" x="127.635" y="69.088" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY33" gate="GND" x="25.4" y="58.42" smashed="yes">
<attribute name="VALUE" x="23.495" y="55.245" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY38" gate="GND" x="111.76" y="58.42" smashed="yes">
<attribute name="VALUE" x="109.855" y="55.245" size="1.27" layer="96"/>
</instance>
<instance part="R39" gate="G$1" x="96.52" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="95.885" y="56.515" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="96.012" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="104.14" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="103.505" y="56.515" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="103.632" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R34" gate="G$1" x="40.64" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="39.7764" y="57.15" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.132" y="66.04" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R15" gate="G$1" x="33.02" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="32.1564" y="62.23" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="32.512" y="71.12" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="+3V311" gate="G$1" x="17.78" y="81.28" smashed="yes">
<attribute name="VALUE" x="20.32" y="81.28" size="1.27" layer="96"/>
</instance>
<instance part="P+2" gate="1" x="132.08" y="83.82" smashed="yes">
<attribute name="VALUE" x="135.255" y="83.82" size="1.27" layer="96"/>
</instance>
<instance part="J4" gate="-1" x="25.4" y="20.32" smashed="yes">
<attribute name="NAME" x="22.098" y="20.828" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-2" x="25.4" y="17.78" smashed="yes">
<attribute name="NAME" x="22.098" y="18.288" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-3" x="25.4" y="15.24" smashed="yes">
<attribute name="NAME" x="22.098" y="15.748" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="-4" x="25.4" y="12.7" smashed="yes">
<attribute name="NAME" x="22.098" y="13.208" size="1.27" layer="95" ratio="15"/>
</instance>
<instance part="J4" gate="G$1" x="12.7" y="7.62" smashed="yes" rot="R90">
<attribute name="VALUE" x="12.7" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="208.28" y="114.3" smashed="yes">
<attribute name="NAME" x="200.66" y="115.57" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="190.5" y="111.76" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D4" gate="G$1" x="208.28" y="106.68" smashed="yes">
<attribute name="NAME" x="200.66" y="107.95" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="190.5" y="104.14" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="D1" gate="G$1" x="208.28" y="99.06" smashed="yes">
<attribute name="NAME" x="200.66" y="100.33" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="190.5" y="96.52" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="R12" gate="G$1" x="220.98" y="114.3" smashed="yes">
<attribute name="NAME" x="215.9" y="114.935" size="1.27" layer="95"/>
<attribute name="VALUE" x="224.79" y="114.808" size="1.27" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="220.98" y="106.68" smashed="yes">
<attribute name="NAME" x="215.9" y="107.5436" size="1.27" layer="95"/>
<attribute name="VALUE" x="224.155" y="107.188" size="1.27" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="220.98" y="99.06" smashed="yes">
<attribute name="NAME" x="215.265" y="99.695" size="1.27" layer="95"/>
<attribute name="VALUE" x="224.155" y="99.568" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY48" gate="GND" x="241.3" y="96.52" smashed="yes">
<attribute name="VALUE" x="239.395" y="93.345" size="1.27" layer="96"/>
</instance>
<instance part="U5" gate="G$1" x="215.9" y="71.12" smashed="yes" rot="MR0">
<attribute name="NAME" x="200.025" y="72.39" size="1.27" layer="95" ratio="15" rot="MR0"/>
<attribute name="VALUE" x="213.36" y="72.39" size="1.27" layer="96" ratio="15" rot="MR0"/>
</instance>
<instance part="P+3" gate="1" x="190.5" y="81.28" smashed="yes" rot="MR0">
<attribute name="VALUE" x="188.595" y="83.185" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="SUPPLY45" gate="GND" x="190.5" y="40.64" smashed="yes" rot="MR0">
<attribute name="VALUE" x="192.405" y="37.465" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R32" gate="G$1" x="226.06" y="63.5" smashed="yes">
<attribute name="NAME" x="220.345" y="64.135" size="1.27" layer="95"/>
<attribute name="VALUE" x="229.235" y="64.008" size="1.27" layer="96"/>
</instance>
<instance part="R24" gate="G$1" x="226.06" y="48.26" smashed="yes">
<attribute name="NAME" x="220.345" y="48.895" size="1.27" layer="95"/>
<attribute name="VALUE" x="229.235" y="48.768" size="1.27" layer="96"/>
</instance>
<instance part="D12" gate="G$1" x="167.64" y="152.4" smashed="yes">
<attribute name="NAME" x="166.37" y="156.21" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="162.56" y="147.32" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="C33" gate="G$1" x="182.88" y="76.2" smashed="yes">
<attribute name="NAME" x="187.96" y="75.565" size="1.27" layer="95" ratio="10" rot="R180"/>
<attribute name="VALUE" x="180.975" y="75.565" size="1.27" layer="96" ratio="10" rot="R180"/>
</instance>
<instance part="SUPPLY43" gate="GND" x="175.26" y="71.12" smashed="yes">
<attribute name="VALUE" x="173.355" y="67.945" size="1.27" layer="96"/>
</instance>
<instance part="R28" gate="G$1" x="182.88" y="58.42" smashed="yes">
<attribute name="NAME" x="177.165" y="59.055" size="1.27" layer="95"/>
<attribute name="VALUE" x="186.055" y="58.928" size="1.27" layer="96"/>
</instance>
<instance part="C22" gate="G$1" x="172.72" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="172.085" y="40.64" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="172.085" y="47.625" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C29" gate="G$1" x="167.64" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="167.005" y="40.64" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="167.005" y="47.625" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="182.88" y="55.88" smashed="yes">
<attribute name="NAME" x="177.165" y="56.515" size="1.27" layer="95"/>
<attribute name="VALUE" x="186.055" y="56.388" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY41" gate="GND" x="167.64" y="35.56" smashed="yes">
<attribute name="VALUE" x="165.735" y="32.385" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY42" gate="GND" x="172.72" y="35.56" smashed="yes">
<attribute name="VALUE" x="170.815" y="32.385" size="1.27" layer="96"/>
</instance>
<instance part="J2" gate="-1" x="83.82" y="20.32" rot="R180"/>
<instance part="J2" gate="-2" x="83.82" y="17.78" rot="R180"/>
<instance part="J2" gate="-3" x="83.82" y="15.24" rot="R180"/>
<instance part="J2" gate="-4" x="83.82" y="12.7" rot="R180"/>
<instance part="J2" gate="G$1" x="119.38" y="7.62"/>
<instance part="J3" gate="-1" x="121.92" y="20.32" rot="R180"/>
<instance part="J3" gate="-2" x="121.92" y="17.78" rot="R180"/>
<instance part="J3" gate="-3" x="121.92" y="15.24" rot="R180"/>
<instance part="J3" gate="-4" x="121.92" y="12.7" rot="R180"/>
<instance part="J3" gate="G$1" x="83.82" y="7.62"/>
<instance part="R25" gate="G$1" x="182.88" y="53.34" smashed="yes">
<attribute name="NAME" x="177.165" y="53.975" size="1.27" layer="95"/>
<attribute name="VALUE" x="186.055" y="53.848" size="1.27" layer="96"/>
</instance>
<instance part="J5" gate="-1" x="58.42" y="20.32"/>
<instance part="J5" gate="-2" x="58.42" y="17.78"/>
<instance part="J5" gate="-3" x="58.42" y="15.24"/>
<instance part="J5" gate="-4" x="58.42" y="12.7"/>
<instance part="J5" gate="G$1" x="43.18" y="7.62"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="32"/>
<wire x1="114.3" y1="154.94" x2="116.84" y2="154.94" width="0.1524" layer="91"/>
<wire x1="116.84" y1="154.94" x2="116.84" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="31"/>
<wire x1="116.84" y1="152.4" x2="116.84" y2="139.7" width="0.1524" layer="91"/>
<wire x1="116.84" y1="139.7" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<wire x1="116.84" y1="137.16" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
<wire x1="114.3" y1="152.4" x2="116.84" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="26"/>
<wire x1="114.3" y1="139.7" x2="116.84" y2="139.7" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="25"/>
<wire x1="114.3" y1="137.16" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<junction x="116.84" y="152.4"/>
<junction x="116.84" y="139.7"/>
<junction x="116.84" y="137.16"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
<pinref part="U8" gate="G$1" pin="33"/>
<wire x1="114.3" y1="157.48" x2="116.84" y2="157.48" width="0.1524" layer="91"/>
<wire x1="116.84" y1="157.48" x2="116.84" y2="154.94" width="0.1524" layer="91"/>
<junction x="116.84" y="154.94"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="11"/>
<wire x1="78.74" y1="129.54" x2="73.66" y2="129.54" width="0.1524" layer="91"/>
<wire x1="73.66" y1="129.54" x2="73.66" y2="127" width="0.1524" layer="91"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<pinref part="U8" gate="G$1" pin="12"/>
<wire x1="73.66" y1="127" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<wire x1="73.66" y1="121.92" x2="73.66" y2="104.14" width="0.1524" layer="91"/>
<wire x1="78.74" y1="127" x2="73.66" y2="127" width="0.1524" layer="91"/>
<junction x="73.66" y="127"/>
<pinref part="U8" gate="G$1" pin="14"/>
<wire x1="78.74" y1="121.92" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<junction x="73.66" y="121.92"/>
</segment>
<segment>
<wire x1="215.9" y1="137.16" x2="223.52" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="223.52" y1="137.16" x2="231.14" y2="137.16" width="0.1524" layer="91"/>
<wire x1="231.14" y1="137.16" x2="238.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="223.52" y1="139.7" x2="223.52" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="231.14" y1="139.7" x2="231.14" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="238.76" y1="139.7" x2="238.76" y2="137.16" width="0.1524" layer="91"/>
<junction x="238.76" y="137.16"/>
<wire x1="238.76" y1="137.16" x2="238.76" y2="134.62" width="0.1524" layer="91"/>
<junction x="223.52" y="137.16"/>
<junction x="231.14" y="137.16"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="215.9" y1="139.7" x2="215.9" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<wire x1="185.42" y1="139.7" x2="185.42" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="185.42" y1="137.16" x2="193.04" y2="137.16" width="0.1524" layer="91"/>
<wire x1="193.04" y1="137.16" x2="200.66" y2="137.16" width="0.1524" layer="91"/>
<wire x1="200.66" y1="137.16" x2="208.28" y2="137.16" width="0.1524" layer="91"/>
<wire x1="208.28" y1="137.16" x2="208.28" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="193.04" y1="139.7" x2="193.04" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="200.66" y1="139.7" x2="200.66" y2="137.16" width="0.1524" layer="91"/>
<junction x="193.04" y="137.16"/>
<junction x="200.66" y="137.16"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
<wire x1="208.28" y1="137.16" x2="208.28" y2="134.62" width="0.1524" layer="91"/>
<junction x="208.28" y="137.16"/>
</segment>
<segment>
<pinref part="C57" gate="G$1" pin="2"/>
<wire x1="160.02" y1="139.7" x2="160.02" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="2"/>
<wire x1="160.02" y1="137.16" x2="177.8" y2="137.16" width="0.1524" layer="91"/>
<wire x1="177.8" y1="137.16" x2="177.8" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
<wire x1="177.8" y1="137.16" x2="177.8" y2="134.62" width="0.1524" layer="91"/>
<junction x="177.8" y="137.16"/>
</segment>
<segment>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="50.8" y1="104.14" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
<wire x1="38.1" y1="129.54" x2="38.1" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
<wire x1="20.32" y1="137.16" x2="17.78" y2="137.16" width="0.1524" layer="91"/>
<wire x1="17.78" y1="137.16" x2="17.78" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="1"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
<wire x1="20.32" y1="114.3" x2="17.78" y2="114.3" width="0.1524" layer="91"/>
<wire x1="17.78" y1="114.3" x2="17.78" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="2"/>
<wire x1="53.34" y1="45.72" x2="43.18" y2="45.72" width="0.1524" layer="91"/>
<wire x1="43.18" y1="45.72" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
<wire x1="25.4" y1="63.5" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<wire x1="111.76" y1="63.5" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="226.06" y1="114.3" x2="233.68" y2="114.3" width="0.1524" layer="91"/>
<wire x1="233.68" y1="114.3" x2="233.68" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="233.68" y1="106.68" x2="233.68" y2="99.06" width="0.1524" layer="91"/>
<wire x1="226.06" y1="106.68" x2="233.68" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="226.06" y1="99.06" x2="233.68" y2="99.06" width="0.1524" layer="91"/>
<junction x="233.68" y="106.68"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
<wire x1="233.68" y1="99.06" x2="241.3" y2="99.06" width="0.1524" layer="91"/>
<junction x="233.68" y="99.06"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="5"/>
<wire x1="193.04" y1="48.26" x2="190.5" y2="48.26" width="0.1524" layer="91"/>
<wire x1="190.5" y1="48.26" x2="190.5" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
<wire x1="177.8" y1="76.2" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<wire x1="175.26" y1="76.2" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
<wire x1="172.72" y1="40.64" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
<wire x1="167.64" y1="40.64" x2="167.64" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="-2" pin="1"/>
<wire x1="88.9" y1="17.78" x2="91.44" y2="17.78" width="0.1524" layer="91"/>
<label x="91.44" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J3" gate="-2" pin="1"/>
<wire x1="127" y1="17.78" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<label x="129.54" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J5" gate="-1" pin="1"/>
<wire x1="53.34" y1="20.32" x2="50.8" y2="20.32" width="0.1524" layer="91"/>
<label x="50.8" y="20.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="3"/>
<wire x1="53.34" y1="40.64" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="40.64" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="48.26" y1="76.2" x2="33.02" y2="76.2" width="0.1524" layer="91"/>
<wire x1="33.02" y1="76.2" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="25.4" y1="76.2" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<wire x1="25.4" y1="73.66" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<junction x="25.4" y="76.2"/>
<pinref part="U7" gate="G$1" pin="6"/>
<wire x1="78.74" y1="40.64" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
<wire x1="83.82" y1="40.64" x2="83.82" y2="73.66" width="0.1524" layer="91"/>
<wire x1="83.82" y1="73.66" x2="83.82" y2="76.2" width="0.1524" layer="91"/>
<wire x1="83.82" y1="76.2" x2="48.26" y2="76.2" width="0.1524" layer="91"/>
<junction x="48.26" y="76.2"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="33.02" y1="76.2" x2="33.02" y2="73.66" width="0.1524" layer="91"/>
<junction x="33.02" y="76.2"/>
<pinref part="+3V311" gate="G$1" pin="+3V3"/>
<wire x1="17.78" y1="78.74" x2="17.78" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="104.14" y1="68.58" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<wire x1="104.14" y1="73.66" x2="83.82" y2="73.66" width="0.1524" layer="91"/>
<junction x="83.82" y="73.66"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="+"/>
<wire x1="203.2" y1="114.3" x2="187.96" y2="114.3" width="0.1524" layer="91"/>
<label x="187.96" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="17"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="114.3" y1="116.84" x2="119.38" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="18"/>
<wire x1="114.3" y1="119.38" x2="132.08" y2="119.38" width="0.1524" layer="91"/>
<wire x1="132.08" y1="119.38" x2="132.08" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="132.08" y1="116.84" x2="129.54" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="19"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="114.3" y1="121.92" x2="119.38" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="13"/>
<wire x1="78.74" y1="124.46" x2="76.2" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="7"/>
<wire x1="78.74" y1="139.7" x2="76.2" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBB" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="4"/>
<wire x1="78.74" y1="147.32" x2="76.2" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="3"/>
<wire x1="78.74" y1="149.86" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="5"/>
<wire x1="76.2" y1="149.86" x2="68.58" y2="149.86" width="0.1524" layer="91"/>
<wire x1="78.74" y1="144.78" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<wire x1="76.2" y1="144.78" x2="76.2" y2="147.32" width="0.1524" layer="91"/>
<junction x="76.2" y="149.86"/>
<junction x="76.2" y="147.32"/>
<wire x1="76.2" y1="147.32" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<label x="68.58" y="149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="129.54" y1="121.92" x2="132.08" y2="121.92" width="0.1524" layer="91"/>
<wire x1="132.08" y1="121.92" x2="132.08" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="20"/>
<wire x1="114.3" y1="124.46" x2="132.08" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="22"/>
<wire x1="114.3" y1="129.54" x2="132.08" y2="129.54" width="0.1524" layer="91"/>
<wire x1="132.08" y1="124.46" x2="132.08" y2="127" width="0.1524" layer="91"/>
<wire x1="132.08" y1="127" x2="132.08" y2="129.54" width="0.1524" layer="91"/>
<wire x1="132.08" y1="129.54" x2="134.62" y2="129.54" width="0.1524" layer="91"/>
<junction x="132.08" y="129.54"/>
<pinref part="U8" gate="G$1" pin="21"/>
<wire x1="114.3" y1="127" x2="132.08" y2="127" width="0.1524" layer="91"/>
<junction x="132.08" y="127"/>
<junction x="132.08" y="124.46"/>
<label x="134.62" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="20.32" y1="142.24" x2="17.78" y2="142.24" width="0.1524" layer="91"/>
<label x="17.78" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="20.32" y1="119.38" x2="17.78" y2="119.38" width="0.1524" layer="91"/>
<label x="17.78" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="1"/>
<pinref part="C49" gate="G$1" pin="1"/>
<wire x1="208.28" y1="149.86" x2="208.28" y2="152.4" width="0.1524" layer="91"/>
<wire x1="208.28" y1="152.4" x2="200.66" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="200.66" y1="149.86" x2="200.66" y2="152.4" width="0.1524" layer="91"/>
<junction x="200.66" y="152.4"/>
<wire x1="200.66" y1="152.4" x2="193.04" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="193.04" y1="149.86" x2="193.04" y2="152.4" width="0.1524" layer="91"/>
<junction x="193.04" y="152.4"/>
<wire x1="193.04" y1="152.4" x2="185.42" y2="152.4" width="0.1524" layer="91"/>
<wire x1="185.42" y1="152.4" x2="185.42" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="238.76" y1="152.4" x2="238.76" y2="149.86" width="0.1524" layer="91"/>
<wire x1="231.14" y1="152.4" x2="238.76" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="231.14" y1="149.86" x2="231.14" y2="152.4" width="0.1524" layer="91"/>
<junction x="231.14" y="152.4"/>
<wire x1="223.52" y1="152.4" x2="231.14" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="223.52" y1="149.86" x2="223.52" y2="152.4" width="0.1524" layer="91"/>
<junction x="223.52" y="152.4"/>
<wire x1="208.28" y1="152.4" x2="215.9" y2="152.4" width="0.1524" layer="91"/>
<junction x="208.28" y="152.4"/>
<wire x1="215.9" y1="152.4" x2="223.52" y2="152.4" width="0.1524" layer="91"/>
<wire x1="238.76" y1="152.4" x2="243.84" y2="152.4" width="0.1524" layer="91"/>
<label x="243.84" y="152.4" size="1.27" layer="95" xref="yes"/>
<junction x="238.76" y="152.4"/>
<pinref part="C36" gate="G$1" pin="1"/>
<wire x1="215.9" y1="149.86" x2="215.9" y2="152.4" width="0.1524" layer="91"/>
<junction x="215.9" y="152.4"/>
<pinref part="C56" gate="G$1" pin="1"/>
<wire x1="185.42" y1="152.4" x2="177.8" y2="152.4" width="0.1524" layer="91"/>
<wire x1="177.8" y1="152.4" x2="177.8" y2="149.86" width="0.1524" layer="91"/>
<junction x="185.42" y="152.4"/>
<pinref part="D12" gate="G$1" pin="1"/>
<wire x1="172.72" y1="152.4" x2="177.8" y2="152.4" width="0.1524" layer="91"/>
<junction x="177.8" y="152.4"/>
</segment>
<segment>
<pinref part="J5" gate="-2" pin="1"/>
<wire x1="53.34" y1="17.78" x2="50.8" y2="17.78" width="0.1524" layer="91"/>
<label x="50.8" y="17.78" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+9V" class="0">
<segment>
<pinref part="SUPPLY40" gate="G$1" pin="+9V"/>
<wire x1="154.94" y1="154.94" x2="154.94" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C57" gate="G$1" pin="1"/>
<wire x1="154.94" y1="152.4" x2="160.02" y2="152.4" width="0.1524" layer="91"/>
<wire x1="160.02" y1="152.4" x2="160.02" y2="149.86" width="0.1524" layer="91"/>
<pinref part="D12" gate="G$1" pin="2"/>
<wire x1="160.02" y1="152.4" x2="162.56" y2="152.4" width="0.1524" layer="91"/>
<junction x="160.02" y="152.4"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="+"/>
<wire x1="203.2" y1="99.06" x2="187.96" y2="99.06" width="0.1524" layer="91"/>
<label x="187.96" y="99.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA_5V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="8"/>
<wire x1="78.74" y1="137.16" x2="71.12" y2="137.16" width="0.1524" layer="91"/>
<wire x1="71.12" y1="137.16" x2="71.12" y2="139.7" width="0.1524" layer="91"/>
<wire x1="71.12" y1="139.7" x2="68.58" y2="139.7" width="0.1524" layer="91"/>
<label x="68.58" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="1"/>
<wire x1="53.34" y1="50.8" x2="40.64" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="40.64" y1="50.8" x2="25.4" y2="50.8" width="0.1524" layer="91"/>
<wire x1="40.64" y1="58.42" x2="40.64" y2="50.8" width="0.1524" layer="91"/>
<junction x="40.64" y="50.8"/>
<label x="25.4" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK_5V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="9"/>
<wire x1="78.74" y1="134.62" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<label x="68.58" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="8"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="78.74" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="50.8" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<wire x1="96.52" y1="50.8" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<label x="114.3" y="50.8" size="1.27" layer="95" xref="yes"/>
<junction x="96.52" y="50.8"/>
</segment>
</net>
<net name="VDD_5V" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="10"/>
<wire x1="78.74" y1="132.08" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="132.08" x2="71.12" y2="129.54" width="0.1524" layer="91"/>
<wire x1="71.12" y1="129.54" x2="68.58" y2="129.54" width="0.1524" layer="91"/>
<label x="68.58" y="129.54" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="129.54" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
<label x="134.62" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="15"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="78.74" y1="119.38" x2="66.04" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="6"/>
<wire x1="78.74" y1="142.24" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="MOTXP" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="2"/>
<wire x1="78.74" y1="152.4" x2="76.2" y2="152.4" width="0.1524" layer="91"/>
<wire x1="76.2" y1="152.4" x2="76.2" y2="154.94" width="0.1524" layer="91"/>
<wire x1="76.2" y1="154.94" x2="68.58" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="1"/>
<wire x1="78.74" y1="154.94" x2="76.2" y2="154.94" width="0.1524" layer="91"/>
<junction x="76.2" y="154.94"/>
<label x="68.58" y="154.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="20.32" y1="15.24" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
<label x="17.78" y="15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J4" gate="-3" pin="1"/>
</segment>
</net>
<net name="MOTXN" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="30"/>
<wire x1="114.3" y1="149.86" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="29"/>
<wire x1="119.38" y1="149.86" x2="134.62" y2="149.86" width="0.1524" layer="91"/>
<wire x1="114.3" y1="147.32" x2="119.38" y2="147.32" width="0.1524" layer="91"/>
<wire x1="119.38" y1="147.32" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
<junction x="119.38" y="149.86"/>
<label x="134.62" y="149.86" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="20.32" y1="12.7" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<label x="17.78" y="12.7" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J4" gate="-4" pin="1"/>
</segment>
</net>
<net name="MOTYP" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="28"/>
<wire x1="114.3" y1="144.78" x2="119.38" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="27"/>
<wire x1="119.38" y1="144.78" x2="134.62" y2="144.78" width="0.1524" layer="91"/>
<wire x1="114.3" y1="142.24" x2="119.38" y2="142.24" width="0.1524" layer="91"/>
<wire x1="119.38" y1="142.24" x2="119.38" y2="144.78" width="0.1524" layer="91"/>
<junction x="119.38" y="144.78"/>
<label x="134.62" y="144.78" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="20.32" y1="17.78" x2="17.78" y2="17.78" width="0.1524" layer="91"/>
<label x="17.78" y="17.78" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J4" gate="-2" pin="1"/>
</segment>
</net>
<net name="MOTYN" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="24"/>
<wire x1="114.3" y1="134.62" x2="119.38" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U8" gate="G$1" pin="23"/>
<wire x1="119.38" y1="134.62" x2="134.62" y2="134.62" width="0.1524" layer="91"/>
<wire x1="114.3" y1="132.08" x2="119.38" y2="132.08" width="0.1524" layer="91"/>
<wire x1="119.38" y1="132.08" x2="119.38" y2="134.62" width="0.1524" layer="91"/>
<junction x="119.38" y="134.62"/>
<label x="134.62" y="134.62" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="20.32" y1="20.32" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<label x="17.78" y="20.32" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="J4" gate="-1" pin="1"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="7"/>
<wire x1="78.74" y1="45.72" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<wire x1="88.9" y1="45.72" x2="88.9" y2="76.2" width="0.1524" layer="91"/>
<wire x1="88.9" y1="78.74" x2="88.9" y2="76.2" width="0.1524" layer="91"/>
<wire x1="88.9" y1="76.2" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="96.52" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<wire x1="111.76" y1="76.2" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<wire x1="116.84" y1="76.2" x2="119.38" y2="76.2" width="0.1524" layer="91"/>
<wire x1="119.38" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
<wire x1="116.84" y1="68.58" x2="116.84" y2="76.2" width="0.1524" layer="91"/>
<junction x="116.84" y="76.2"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="111.76" y1="73.66" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<junction x="111.76" y="76.2"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="96.52" y1="76.2" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<junction x="96.52" y="76.2"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="40.64" y1="68.58" x2="40.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="40.64" y1="78.74" x2="88.9" y2="78.74" width="0.1524" layer="91"/>
<junction x="88.9" y="76.2"/>
</segment>
</net>
<net name="RD5" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="4"/>
<wire x1="53.34" y1="35.56" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="33.02" y1="35.56" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="63.5" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<junction x="33.02" y="35.56"/>
<label x="25.4" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RD6" class="0">
<segment>
<pinref part="U7" gate="G$1" pin="5"/>
<wire x1="78.74" y1="35.56" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="104.14" y1="35.56" x2="114.3" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="58.42" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<junction x="104.14" y="35.56"/>
<label x="114.3" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="129.54" y1="76.2" x2="132.08" y2="76.2" width="0.1524" layer="91"/>
<label x="134.62" y="76.2" size="1.27" layer="95" xref="yes"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="132.08" y1="76.2" x2="134.62" y2="76.2" width="0.1524" layer="91"/>
<wire x1="132.08" y1="81.28" x2="132.08" y2="76.2" width="0.1524" layer="91"/>
<junction x="132.08" y="76.2"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="+"/>
<wire x1="203.2" y1="106.68" x2="187.96" y2="106.68" width="0.1524" layer="91"/>
<label x="187.96" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="190.5" y1="63.5" x2="185.42" y2="63.5" width="0.1524" layer="91"/>
<label x="185.42" y="63.5" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="190.5" y1="78.74" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="190.5" y="63.5"/>
<pinref part="U5" gate="G$1" pin="8"/>
<wire x1="190.5" y1="76.2" x2="190.5" y2="63.5" width="0.1524" layer="91"/>
<wire x1="193.04" y1="63.5" x2="190.5" y2="63.5" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="187.96" y1="76.2" x2="190.5" y2="76.2" width="0.1524" layer="91"/>
<junction x="190.5" y="76.2"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="-"/>
<wire x1="213.36" y1="114.3" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="-"/>
<wire x1="213.36" y1="106.68" x2="215.9" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="-"/>
<wire x1="213.36" y1="99.06" x2="215.9" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RG2" class="0">
<segment>
<label x="233.68" y="63.5" size="1.27" layer="95" xref="yes"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="231.14" y1="63.5" x2="233.68" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RG0" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="3"/>
<wire x1="218.44" y1="53.34" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<label x="233.68" y="53.34" size="1.27" layer="95" xref="yes"/>
<pinref part="U5" gate="G$1" pin="2"/>
<wire x1="223.52" y1="53.34" x2="233.68" y2="53.34" width="0.1524" layer="91"/>
<wire x1="218.44" y1="58.42" x2="223.52" y2="58.42" width="0.1524" layer="91"/>
<wire x1="223.52" y1="58.42" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<junction x="223.52" y="53.34"/>
</segment>
</net>
<net name="RG1" class="0">
<segment>
<label x="233.68" y="48.26" size="1.27" layer="95" xref="yes"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="231.14" y1="48.26" x2="233.68" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="1"/>
<wire x1="218.44" y1="63.5" x2="220.98" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="4"/>
<wire x1="218.44" y1="48.26" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="16"/>
<wire x1="76.2" y1="116.84" x2="78.74" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="7"/>
<wire x1="187.96" y1="58.42" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="190.5" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="187.96" y1="55.88" x2="190.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="190.5" y1="55.88" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<junction x="190.5" y="58.42"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="6"/>
<wire x1="187.96" y1="53.34" x2="193.04" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="10-30VIN" class="0">
<segment>
<pinref part="J3" gate="-1" pin="1"/>
<wire x1="127" y1="20.32" x2="129.54" y2="20.32" width="0.1524" layer="91"/>
<label x="129.54" y="20.32" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="-1" pin="1"/>
<wire x1="88.9" y1="20.32" x2="91.44" y2="20.32" width="0.1524" layer="91"/>
<label x="91.44" y="20.32" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="IN_N" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="177.8" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="167.64" y1="50.8" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="167.64" y1="58.42" x2="162.56" y2="58.42" width="0.1524" layer="91"/>
<junction x="167.64" y="58.42"/>
<label x="162.56" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J3" gate="-4" pin="1"/>
<wire x1="127" y1="12.7" x2="129.54" y2="12.7" width="0.1524" layer="91"/>
<label x="129.54" y="12.7" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="-4" pin="1"/>
<wire x1="88.9" y1="12.7" x2="91.44" y2="12.7" width="0.1524" layer="91"/>
<label x="91.44" y="12.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="IN_P" class="0">
<segment>
<wire x1="177.8" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="175.26" y1="53.34" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<wire x1="177.8" y1="55.88" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="175.26" y1="55.88" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="172.72" y1="50.8" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<junction x="175.26" y="53.34"/>
<wire x1="172.72" y1="53.34" x2="162.56" y2="53.34" width="0.1524" layer="91"/>
<junction x="172.72" y="53.34"/>
<label x="162.56" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R25" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="J3" gate="-3" pin="1"/>
<wire x1="127" y1="15.24" x2="129.54" y2="15.24" width="0.1524" layer="91"/>
<label x="129.54" y="15.24" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="-3" pin="1"/>
<wire x1="88.9" y1="15.24" x2="91.44" y2="15.24" width="0.1524" layer="91"/>
<label x="91.44" y="15.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="30.48" y1="142.24" x2="33.02" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="33.02" y1="142.24" x2="38.1" y2="142.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="142.24" x2="43.18" y2="142.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="139.7" x2="38.1" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="30.48" y1="137.16" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<wire x1="33.02" y1="137.16" x2="33.02" y2="142.24" width="0.1524" layer="91"/>
<junction x="33.02" y="142.24"/>
<junction x="38.1" y="142.24"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="30.48" y1="119.38" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="33.02" y1="119.38" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
<wire x1="50.8" y1="119.38" x2="55.88" y2="119.38" width="0.1524" layer="91"/>
<wire x1="50.8" y1="116.84" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="30.48" y1="114.3" x2="33.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="33.02" y1="114.3" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
<junction x="33.02" y="119.38"/>
<junction x="50.8" y="119.38"/>
</segment>
</net>
<net name="RH5" class="0">
<segment>
<pinref part="J5" gate="-3" pin="1"/>
<wire x1="53.34" y1="15.24" x2="50.8" y2="15.24" width="0.1524" layer="91"/>
<label x="50.8" y="15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RH4" class="0">
<segment>
<pinref part="J5" gate="-4" pin="1"/>
<wire x1="53.34" y1="12.7" x2="50.8" y2="12.7" width="0.1524" layer="91"/>
<label x="50.8" y="12.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="106,3,78.74,116.84,N$4,,,,,"/>
<approved hash="106,3,78.74,124.46,N$7,,,,,"/>
<approved hash="106,3,78.74,139.7,N$8,,,,,"/>
<approved hash="106,2,33.02,22.86,N$40,,,,,"/>
<approved hash="106,2,33.02,43.18,N$41,,,,,"/>
<approved hash="106,2,33.02,53.34,N$42,,,,,"/>
<approved hash="106,2,33.02,73.66,N$43,,,,,"/>
<approved hash="106,2,33.02,93.98,N$44,,,,,"/>
<approved hash="106,2,33.02,104.14,N$45,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
