//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

//############################################################################//
// This function will initialize the SPI
//############################################################################//
void SPI_Init (void)
{
    TRACE_INI(("START: SPI init\r\n"));    

    TRISCbits.RC3 = 0;
    TRISCbits.RC5 = 0;
    
    SSP1CON1 = SPI_MASTER;          // select 8-bit master mode, CKE=1, CKP=0
    SSP1STAT = SPI_ENABLE;          // enable SPI port, clear status

    PIR1bits.SSPIF = 0;
//[A.S.]    SSP1IF = 1;   // test interrupts later
}
        
#if 0
//############################################################################//
// This function will send a byte over the SPI
//############################################################################//
void SPI_Put(uint8_t value)
{
#if SPI_TIMEOUT_0
//initialize timers
T0CON = 0x87;
TMR0H = 0xD9;
TMR0L = 0xDA;
INTCONbits.TMR0IE = 0;
#endif

#if SPI_TIMEOUT_2
T2CON = 0x44; 						// Turn on timer2, 1:1 prescaler, 1:10 post scalar
PR2 = 250; 							// Timer2 compare register
T2CONbits.TMR2ON = 1;				// Enable timer2			
#endif

    // Reset the Global interrupt pin
    SSP1IF = 0;
    do
    {
		// Reset write collision bit
        WCOL1 = 0;
		// Load the buffer
        SSP1BUF = value;
		// Perform write again if write collision occurs
    } while(WCOL1);
	
	// Wait until interrupt is received from the MSSP module
	while (SSP1IF == 0)
	{
#if SPI_TIMEOUT_0
        if (INTCONbits.TMR0IF)
        {
            INTCONbits.TMR0IF = 0;
            TMR0H = 0xD9;
            TMR0L = 0xDA;
            T0CON = 0x87;
        }
#endif
#if SPI_TIMEOUT_2
		if (PIR1bits.TMR2IF)
		{
			// TMR2 interrupt
	 		PIR1bits.TMR2IF = 0; 				// Clear interrupt flag
			break;
	 	}
#endif
	}

	// Reset the interrupt
	SSP1IF = 0;
}

//############################################################################//
// This function will read a byte over the SPI
//############################################################################//
uint8_t SPI_Get(void)
{
    // Write a dummy value to read the data from the other SPI module
    SPI_Put(0x00);
	// Read the SSP1BUF value for data transmitted by other SSP module
    return SSP1BUF;
}

uint8_t SPI_ReadWrite (uint8_t value)
{
    SPI_Put(value);
	// Read the SSPBUF value for data transmitted by other SSP module
    return SSP1BUF;
}
#endif

uint8_t SPI_ReadWrite (uint8_t value)
{
    SSPBUF = value;
    while(!PIR1bits.SSPIF);
    PIR1bits.SSPIF = 0;
    return SSPBUF;
}
