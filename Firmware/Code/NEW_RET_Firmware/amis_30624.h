//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _AMIS_30624_H_
#define _AMIS_30624_H_

#include "system.h"

// AMIS 30624 chip
#define AMIS_30624_ADDRESS_WRITE 	(0xC0)
#define AMIS_30624_ADDRESS_READ 	(0xC1)
#define AMIS_30624_RESERVED			(0xFF)

// ---------------------
// Irun             (mA)
// ---------------------
// | 0 | 0 0 0 0 |  59 |
// | 1 | 0 0 0 1 |  71 |
// | 2 | 0 0 1 0 |  84 |
// | 3 | 0 0 1 1 | 100 |
// | 4 | 0 1 0 0 | 119 |
// | 5 | 0 1 0 1 | 141 |
// | 6 | 0 1 1 0 | 168 | <-
// | 7 | 0 1 1 1 | 200 |
// | 8 | 1 0 0 0 | 238 |
// | 9 | 1 0 0 1 | 283 |
// | A | 1 0 1 0 | 336 |
// | B | 1 0 1 1 | 400 |
// | C | 1 1 0 0 | 476 |
// | D | 1 1 0 1 | 566 |
// | E | 1 1 1 0 | 673 |
// | F | 1 1 1 1 | 800 |
// ---------------------
#define AMIS_30624_IRUN				(0x08)

// ---------------------
// Ihold            (mA)
// ---------------------
// | 0 | 0 0 0 0 |  59 |
// | 1 | 0 0 0 1 |  71 |
// | 2 | 0 0 1 0 |  84 |
// | 3 | 0 0 1 1 | 100 |
// | 4 | 0 1 0 0 | 119 |
// | 5 | 0 1 0 1 | 141 |
// | 6 | 0 1 1 0 | 168 |
// | 7 | 0 1 1 1 | 200 |
// | 8 | 1 0 0 0 | 238 |
// | 9 | 1 0 0 1 | 283 |
// | A | 1 0 1 0 | 336 |
// | B | 1 0 1 1 | 400 |
// | C | 1 1 0 0 | 476 |
// | D | 1 1 0 1 | 566 |
// | E | 1 1 1 0 | 673 |
// | F | 1 1 1 1 |   0 | <-
// ---------------------
#define AMIS_30624_IHOLD			(0x0F)

// -----------------------
// | Step Mode
// -----------------------
// | 0 0 |  1/2 stepping |
// | 0 1 |  1/4 stepping |
// | 1 0 |  1/8 stepping |
// | 1 1 | 1/16 stepping | <-
// -----------------------
#define AMIS_30624_STEPMODE 		(0x03)

#define AMIS_30624_SHAFT 			(0x01)

// SecPos[10:2] Secure Position of the stepper−motor.
// This is the position to which the motor is driven in case of a HW pin connection is lost.
// If <SecPos[10:2]> = 100 0000 00, secure positioning is disabled;
// the stepper-motor will be kept in the position occupied at the moment these events occur
#define AMIS_30624_SECPOS 			(0x0400)

// ---------------------
// | Vmax       (step/s)
// ---------------------
// | 0 | 0 0 0 0 |  99 |
// | 1 | 0 0 0 1 | 136 |
// | 2 | 0 0 1 0 | 167 |
// | 3 | 0 0 1 1 | 197 |
// | 4 | 0 1 0 0 | 213 |
// | 5 | 0 1 0 1 | 228 |
// | 6 | 0 1 1 0 | 243 |
// | 7 | 0 1 1 1 | 273 |
// | 8 | 1 0 0 0 | 303 |
// | 9 | 1 0 0 1 | 334 |
// | A | 1 0 1 0 | 364 | <-
// | B | 1 0 1 1 | 395 |
// | C | 1 1 0 0 | 456 |
// | D | 1 1 0 1 | 546 |
// | E | 1 1 1 0 | 729 |
// | F | 1 1 1 1 | 973 |
// ---------------------
#define AMIS_30624_VMAX 			(0x0A)

// -----------------------
//  Vmin          (step/s)
// -----------------------
// | 0 | 0 0 0 0 |     1 |
// | 1 | 0 0 0 1 |  1/32 | <-
// | 2 | 0 0 1 0 |  2/32 |
// | 3 | 0 0 1 1 |  3/32 |
// | 4 | 0 1 0 0 |  4/32 |
// | 5 | 0 1 0 1 |  5/32 |
// | 6 | 0 1 1 0 |  6/32 |
// | 7 | 0 1 1 1 |  7/32 |
// | 8 | 1 0 0 0 |  8/32 |
// | 9 | 1 0 0 1 |  9/32 |
// | A | 1 0 1 0 | 10/32 |
// | B | 1 0 1 1 | 11/32 |
// | C | 1 1 0 0 | 12/32 |
// | D | 1 1 0 1 | 13/32 |
// | E | 1 1 1 0 | 14/32 |
// | F | 1 1 1 1 | 15/32 |
// -----------------------
#define AMIS_30624_VMIN 			(0x01)

// -----------------------
// Acceleration (step/s^2)
// -----------------------
// | 0 | 0 0 0 0 |    49 |
// | 1 | 0 0 0 1 |   218 |
// | 2 | 0 0 1 0 |  1004 | <-
// | 3 | 0 0 1 1 |  3609 |
// | 4 | 0 1 0 0 |  6228 |
// | 5 | 0 1 0 1 |  8848 |
// | 6 | 0 1 1 0 | 11409 |
// | 7 | 0 1 1 1 | 13970 |
// | 8 | 1 0 0 0 | 16531 |
// | 9 | 1 0 0 1 | 19092 |
// | A | 1 0 1 0 | 21886 |
// | B | 1 0 1 1 | 24447 |
// | C | 1 1 0 0 | 27008 |
// | D | 1 1 0 1 | 29570 |
// | E | 1 1 1 0 | 34925 |
// | F | 1 1 1 1 | 40047 |
// -----------------------
#define AMIS_30624_ACC 				(0x02)

// if the following two are zero'd then motion detection is disabled
// you must tweak these values when IRUN gets adjusted, for proper stall detect

// -----------
// AbsThr
// -----------
// | 0 | Dis | <-
// | 1 | 0.5 |
// | 2 | 1.0 |
// | 3 | 1.5 |
// | 4 | 2.0 |
// | 5 | 2.5 |
// | 6 | 3.0 |
// | 7 | 3.5 |
// | 8 | 4.0 |
// | 9 | 4.5 |
// | A | 5.0 |
// | B | 5.5 |
// | C | 6.0 |
// | D | 6.5 |
// | E | 7.0 |
// | F | 7.5 |
// -----------
#define AMIS_30624_ABSTHR 				(0x00)

// ------------
//  DelThr
// ------------
// | 0 | Dis  |
// | 1 | 0.25 |
// | 2 | 0.50 |
// | 3 | 0.75 |
// | 4 | 1.00 |
// | 5 | 1.25 |
// | 6 | 1.50 |
// | 7 | 1.75 |
// | 8 | 2.00 |
// | 9 | 2.25 |
// | A | 2.50 |
// | B | 2.75 |
// | C | 3.00 |
// | D | 3.25 |
// | E | 3.50 |
// | F | 3.75 | <-
// ------------
#define AMIS_30624_DELTHR 				(0x0F)

// -----------------
// MinSamples   (s)
// -----------------
// | 0 | 000 |  87 |
// | 1 | 001 | 130 |
// | 2 | 010 | 174 |
// | 3 | 011 | 217 | <-
// | 4 | 100 | 261 |
// | 5 | 101 | 304 |
// | 6 | 110 | 348 |
// | 7 | 111 | 391 |
// -----------------
#define AMIS_30624_MINSAMPLES 			(0x03)

// ---------------
// FS2StallEn
// ---------------
// | 0 | 000 | 0 |
// | 1 | 001 | 1 |
// | 2 | 010 | 2 |
// | 3 | 011 | 3 |	<-
// | 4 | 100 | 4 |
// | 5 | 101 | 5 |
// | 6 | 110 | 6 |
// | 7 | 111 | 7 |
// ---------------
#define AMIS_30624_FS2STALLEN 			(0x03)

#define AMIS_30624_ACCSHAPE 			(0x00)
#define AMIS_30624_DC100STEN 			(0x01)

// --------------------
// PWMJEn Status
// --------------------
// | 0 | Single PWM   | <-
// | 1 | Added jitter |
// --------------------
#define AMIS_30624_PWMJEN 				(0x00)

// ----------------
// | PWMfreq
// ----------------
// | 0 | 22,8 kHz | <-
// | 1 | 45,6 kHz |
// ----------------
#define AMIS_30624_PWMFREQ 				(0x00)

enum AMIS_30624_COMMANDS
{
	AMIS_30624_GET_FULL_STATUS_1 	= 0x81, // Returns complete status of the chip
	AMIS_30624_GET_FULL_STATUS_2 	= 0xFC, // Returns actual, target and secure position
	AMIS_30624_GET_OTP_PARAM		= 0x82, // Returns OTP parameter
	AMIS_30624_GOTO_SECURE_POSITION = 0x84, // Drives motor to secure position
	AMIS_30624_HARD_STOP	 		= 0x85, // Immediate full stop
	AMIS_30624_RESET_POSITION 		= 0x86, // Sets actual position to zero
	AMIS_30624_RESET_TO_DEFAULT 	= 0x87, // Overwrites the chip RAM with OTP contents
	AMIS_30624_SET_DUAL_POSITION 	= 0x88, // Drives the motor to two different positions with different speed
	AMIS_30624_SET_MOTOR_PARAM 		= 0x89, // Sets motor parameter
	AMIS_30624_SET_OTP 				= 0x90, // Zaps the OTP memory
	AMIS_30624_SET_POSITION 		= 0x8B, // Programs a target and secure position
	AMIS_30624_SET_STALL_PARAM 		= 0x96, // Sets stall parameters
	AMIS_30624_SOFT_STOP 			= 0x8F, // Motor stopping with deceleration phase
	AMIS_30624_RUN_VELOCITY 		= 0x97, // Drives motor continuously
	AMIS_30624_TEST_BEMF 			= 0x9F, // Outputs Bemf voltage on pin SWI
};

typedef union _AMIS_30624_GetFullStatus
{
    uint8_t data[7];

    struct
    {
        uint8_t HW:1;
        uint8_t OTP:4;
        uint8_t Spare:3;
        uint8_t Ihold:4;
        uint8_t Irun:4;
        uint8_t Vmin:4;
        uint8_t Vmax:4;
        uint8_t Acc:4;
        uint8_t Shaft:1;
        uint8_t StepMode:2;
        uint8_t AccShape:1;
        uint8_t Tinfo:2;
        uint8_t TW:1;
        uint8_t TSD:1;
        uint8_t UV2:1;
        uint8_t ElDef:1;
        uint8_t StepLoss:1;
        uint8_t VddReset:1;
        uint8_t CPFail:1;
        uint8_t Stall:1;
        uint8_t OVC2:1;
        uint8_t OVC1:1;
        uint8_t ESW:1;
        uint8_t Motion:3;
        uint8_t Private:8;  // 0xFF
        uint8_t DelThr:4;   // 0x06
        uint8_t AbsThr:4;   // 0x00
    } bits;
} AMIS_30624_status_t;

AMIS_30624_status_t hw_status;

return_t AMIS_30624_Hard_Stop (void);

return_t AMIS_30624_Reset_To_Default (void);

return_t AMIS_30624_Reset_Position (void);

return_t AMIS_30624_Set_Motor_Param (void);

return_t AMIS_30624_Set_Stall_Param (void);

return_t AMIS_30624_Init (void);

return_t AMIS_30624_Set_Position (int16_t position);

return_t AMIS_30624_Get_Positions (int16_t *actual_position, int16_t *target_position);

return_t AMIS_30624_Get_HW_Status (bool init);

return_t AMIS_30624_Get_OTP_Param ();

return_t AMIS_30624_Set_OTP_Address ();

#endif // _AMIS_30624_H_