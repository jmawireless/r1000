//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _I2C_H_
#define _I2C_H_

#include "system.h"

void I2C_Init (void);

void I2C_Start (void);

void I2C_Stop (void);

void I2C_Restart (void);

void I2C_Ack (void);

void I2C_Nack (void);

void I2C_Wait (void);

void I2C_Write (uint8_t data);

bool I2C_Read (bool ack, uint8_t  *data);

void I2C_Clear (void);

#endif // _I2C_H_