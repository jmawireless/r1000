//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define __CLOCK__        Clock_Get_Time()
#define __HOURS__        Clock_Get_Hours()
#define __MINUTES__      Clock_Get_Minutes()
#define __SECONDS__      Clock_Get_Seconds()
#define __MILLISECONDS__ Clock_Get_MilliSeconds()

//############################################################################//
//
//############################################################################//
void putch (char ch)
{
    Uart1_Send_Byte(ch);
}

//############################################################################//
//
//############################################################################//
void TRACE_Header (void)
{
    CLRWDT();
    printf("[%.2d:%.2d:%.2d.%.3d] ",
        __HOURS__, __MINUTES__, __SECONDS__, __MILLISECONDS__);
}