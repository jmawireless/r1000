//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define round_float_to_int(x) ((x)>=0?(int32_t)((x)+0.5):(int32_t)((x)-0.5))

int32_t Min (int32_t a, int32_t b)
{
    if (a > b)
         return b;
    else
        return a;
}

bool pos_backslash = false;
bool neg_backslash = false;
int16_t ret_tilt = 0;
int16_t zero_tilt = 0;
int32_t motor_position = 0;
int32_t previous_position = 0;
int32_t delta_position = 0;
int16_t actual_position = 0;
int16_t target_position = 0;
int32_t backlash_position = 0;
int32_t unwind_position = 0;

//############################################################################//
//
//############################################################################//
return_t Motor_Init (void)
{
    TRACE_INI(("START: Motor task init\r\n"));

    return Motor_Configure();
}

//############################################################################//
//
//############################################################################//
return_t Motor_Configure (void)
{
    ret_tilt = max(Pars.AntennaMinTilt, Pars.Tilt);
	zero_tilt = Pars.Tilt;

    if ((Pars.AntennaMaxTilt - Pars.AntennaMinTilt) == 0)
	{
        backlash_position = 0;
		unwind_position = 0;
		return (RESULT_NOT_CONFIGURED);
	}
    else
    {
        backlash_position = round_float_to_int(
							(float)Pars.Backlash  * (float)Pars.TurnRatio /
							(float)(Pars.AntennaMaxTilt - Pars.AntennaMinTilt));
		unwind_position   = round_float_to_int(
							(float)Pars.Unwind  * (float)Pars.TurnRatio /
							(float)(Pars.AntennaMaxTilt - Pars.AntennaMinTilt));
		return (RESULT_SUCCESS);
	}
}

//############################################################################//
//
//############################################################################//
uint8_t Task_Motor (uint8_t state)
{
    static uint8_t timeout = 0;
    static enum TASK_MOTOR_STATES success_state = TASK_MOTOR_IDLE;
    static enum TASK_MOTOR_STATES failure_state = TASK_MOTOR_IDLE;
    static int32_t current_position = 0;
    int16_t motor_step;
    uint32_t diff;
    int32_t total_position;

    if (state == TASK_MOTOR_IDLE)
    {
        return (state);
    }

    TRACE_FUNCT(AMIS_30624_Get_HW_Status(false));

    switch (state)
    {
        default:
        break;

        case TASK_MOTOR_SET_POSITION :

            // Redefine the current stepper position as position zero.
            AMIS_30624_Reset_Position();

            if (delta_position >= 0)
            {
                if (current_position < delta_position)
                {
                    diff = Min((int32_t)32767, labs(current_position - delta_position));
                    motor_step = diff;
                    total_position = current_position;
                    TRACE_LOG(("MOTOR: current_position %ld, STEP UP %d\r\n", current_position, motor_step));
                    AMIS_30624_Set_Position(motor_step);
                    current_position += motor_step;
                    state = TASK_MOTOR_MOVING;
                }
                else
                {
                    current_position = 0;
                    state = success_state;
                }
            }
            else
            {
                if (current_position > delta_position)
                {
                    diff = Min((int32_t)32767, labs(current_position - delta_position));
                    motor_step = -diff;
                    total_position = current_position;
                    TRACE_LOG(("MOTOR: current_position %ld, STEP DW %d\r\n", current_position, motor_step));
                    AMIS_30624_Set_Position(motor_step);
                    current_position += motor_step;
                    state = TASK_MOTOR_MOVING;
                }
                else
                {
                    current_position = 0;
                    state = success_state;
                }
            }
        break;

        case TASK_MOTOR_MOVING :
            AMIS_30624_Get_Positions(&actual_position, &target_position);
            if (Motor_Is_Stopped() == true)
            {
                if (Motor_Is_Stalled())
                {
                    TRACE_LOG(("MOTOR: STALLED [%6.5d, actual:%7.5ld]\r\n", actual_position, actual_position + total_position));
                    current_position = 0;
                    state = failure_state;
                }
                else
                {
                    TRACE_LOG(("MOTOR: STOPPED [%6.5d, actual:%7.5ld]\r\n", actual_position, actual_position + total_position));
                    state = TASK_MOTOR_SET_POSITION;
                }
            }
            else
            {
                TRACE_LOG(("MOTOR: MOVING  [%6.5d, actual:%7.5ld]\r\n", actual_position, actual_position + total_position));
                if (timeout++ >= MOTOR_CALIBRATION_TIMEOUT_SEC)
                {
                    TRACE_LOG(("MOTOR: TIMEOUT\r\n"));
                    state = failure_state;
                }
            }
        break;

        case TASK_MOTOR_CALIBRATION_STARTED :
            timeout = 0;
            Motor_Move_To_End_Stop();
            state = TASK_MOTOR_SET_POSITION;
            // A stall during this movement is a wanted condition
            success_state = TASK_MOTOR_CALIBRATION_FAILED;
            failure_state = TASK_MOTOR_CALIBRATION_END_STOP;
        break;

        case TASK_MOTOR_CALIBRATION_END_STOP :
            Motor_Move_To_Unwind();
            state = TASK_MOTOR_SET_POSITION;
            success_state = TASK_MOTOR_CALIBRATION_UNWIND;
            failure_state = TASK_MOTOR_CALIBRATION_FAILED;
        break;

        case TASK_MOTOR_CALIBRATION_UNWIND :
            Motor_Move_To_Max_Tilt();
            state = TASK_MOTOR_SET_POSITION;
            success_state = TASK_MOTOR_CALIBRATION_MAX_POS;
            failure_state = TASK_MOTOR_CALIBRATION_FAILED;
        break;

        case TASK_MOTOR_CALIBRATION_MAX_POS :
            Motor_Move_To_Last_Tilt();
            state = TASK_MOTOR_SET_POSITION;
            success_state = TASK_MOTOR_CALIBRATION_COMPLETED;
            failure_state = TASK_MOTOR_CALIBRATION_FAILED;
        break;

        case TASK_MOTOR_TILT_STARTED :
            timeout = 0;
            Motor_Move_To_New_Tilt();
            state = TASK_MOTOR_SET_POSITION;
            success_state = TASK_MOTOR_TILT_COMPLETED;
            failure_state = TASK_MOTOR_TILT_FAILED;
        break;

        case TASK_MOTOR_CALIBRATION_FAILED :
            Alarm_Set(Actuator_Jam_Temporary);
            last_tcp.completed = true;
            last_tcp.return_code = Alarm_Get();
            state = TASK_MOTOR_IDLE;
        break;

        case TASK_MOTOR_CALIBRATION_COMPLETED :
            Motor_Calibration_Completed();
            state = TASK_MOTOR_IDLE;
        break;

        case TASK_MOTOR_TILT_FAILED :
            Alarm_Set(Actuator_Jam_Temporary);
            Motor_Set_Tilt_Completed();
            state = TASK_MOTOR_IDLE;
        break;

        case TASK_MOTOR_TILT_COMPLETED :
            Motor_Set_Tilt_Completed();
            state = TASK_MOTOR_IDLE;
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
return_t Motor_Get_Tilt (int16_t *tilt)
{
    return_t result;

    result = Alarm_Get();
    if (result == RESULT_SUCCESS)
    {
        *tilt = ret_tilt;
    }
    return (result);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Set_Tilt (int16_t tilt)
{
    return_t result;

    result = Alarm_Get();
    if (result == RESULT_SUCCESS)
    {
        if ((tilt > Pars.AntennaMaxTilt) || (tilt < Pars.AntennaMinTilt))
        {
            return (RESULT_OUT_OF_RANGE);
        }

        if (tilt == Pars.Tilt)
        {
            return (RESULT_SUCCESS);
        }

        ret_tilt = tilt;

        Pars.NotCalibrated = true;
        Parameter_Write(NotCalibrated);

        Task_Set_State(TASK_MOTO_ID, TASK_MOTOR_TILT_STARTED);
        return (RESULT_PROCEDURE_NOT_COMPLETED);
    }

    return (result);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Set_Tilt_Completed (void)
{
    return_t Result = Alarm_Get();

    last_tcp.completed = true;
    last_tcp.return_code = Result;

    if (Result == RESULT_SUCCESS)
    {
        Pars.Tilt = ret_tilt;
        Pars.NotCalibrated = false;
        Parameter_Write(Tilt);
        Parameter_Write(NotCalibrated);
    }
    
    return (Result);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Calibrate (void)
{
    if (Alarm_Is_Active(Not_Configured))
    {
        return (RESULT_NOT_CONFIGURED);
    }

    if (Alarm_Is_Active(Not_Calibrated))
	{
		Pars.Tilt = Pars.AntennaMinTilt;
        Parameter_Write(Tilt);
	}

    Pars.NotCalibrated = true;
    Parameter_Write(NotCalibrated);

    Task_Set_State(TASK_MOTO_ID, TASK_MOTOR_CALIBRATION_STARTED);
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Calibration_Completed (void)
{
    TRACE_LOG(("MOTOR: Reached last tilt\r\n"));

    Pars.NotCalibrated = false;
    Parameter_Write(NotCalibrated);

    pos_backslash = false;

    Alarm_Clr(Not_Calibrated);
    Alarm_Clr(Actuator_Jam_Temporary);
    Alarm_Clr(Actuator_Jam_Permanent);

    last_tcp.completed = true;
    last_tcp.return_code = RESULT_SUCCESS;

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_End_Stop (void)
{
    previous_position = motor_position;

	// Redefine the current tilt position as position zero.
    previous_position = 0;

	// Now we can turn in a large negative direction until we hit the end stop
	// Move to some very large negative value.
	// This will ensure we hit the end stop.  The motor controller will stop turning
	// on its own when it hits the end stop, so this line is just sure to get it there
    motor_position = -300000;

    delta_position = motor_position - previous_position;

    TRACE_LOG(("MOTOR: Move to end stop  [T=END M=%7.5ld P=%7.5ld D=%7.5ld]\r\n",
        motor_position, previous_position, delta_position));

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_Unwind (void)
{
    previous_position = motor_position;

    TRACE_LOG(("MOTOR: Reached end stop\r\n"));
    
    previous_position = 0;

    motor_position = unwind_position;

    delta_position = motor_position - previous_position;

    TRACE_LOG(("MOTOR: Move to unwind    [T=%3.3d M=%7.5ld P=%7.5ld D=%7.5ld]\r\n",
        Pars.Unwind, motor_position, previous_position, delta_position));

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_Max_Tilt (void)
{
    previous_position = motor_position;

    TRACE_LOG(("MOTOR: Reached unwind\r\n"));

    // 0 degree point now
    previous_position = 0;

    motor_position = Pars.TurnRatio;

    delta_position = motor_position - previous_position;

    TRACE_LOG(("MOTOR: Move to max tilt  [T=%3.3d M=%7.5ld P=%7.5ld D=%7.5ld]\r\n",
        Pars.AntennaMaxTilt, motor_position, previous_position, delta_position));

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_Last_Tilt (void)
{
    previous_position = motor_position;

    TRACE_LOG(("MOTOR: Reached max tilt\r\n"));

    // get former tilt position, restore after calibration
    ret_tilt = max(Pars.AntennaMinTilt, Pars.Tilt);
    zero_tilt = 0;

    // downwards direction
	neg_backslash = true;
    if (Pars.LastDirection != DIRECTION_DOWNWARDS)
    {
        Pars.LastDirection = DIRECTION_DOWNWARDS;
        Parameter_Write(LastDirection);
    }
    motor_position = round_float_to_int(
                    (float)(ret_tilt - Pars.AntennaMinTilt) * (float)Pars.TurnRatio /
                    (float)(Pars.AntennaMaxTilt - Pars.AntennaMinTilt));

    motor_position -= backlash_position;

    delta_position = motor_position - previous_position;

	TRACE_LOG(("MOTOR: Move to last tilt [T=%3.3d M=%7.5ld P=%7.5ld D=%7.5ld]\r\n",
        ret_tilt, motor_position, previous_position, delta_position));

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_New_Tilt (void)
{
    previous_position = motor_position;

    motor_position = round_float_to_int(
                      (float)(ret_tilt - zero_tilt) * (float)Pars.TurnRatio /
                      (float)(Pars.AntennaMaxTilt - Pars.AntennaMinTilt));

    if (ret_tilt > Pars.Tilt)
    {
        if ((Pars.LastDirection == DIRECTION_DOWNWARDS) && (neg_backslash == false))
        {
            motor_position += backlash_position;
            pos_backslash = true;
        }
        if (Pars.LastDirection != DIRECTION_UPWARDS)
        {
            Pars.LastDirection = DIRECTION_UPWARDS;
            Parameter_Write(LastDirection);
        }
    }
    else
    {
        // actual new position here will always have the slop factor
        // moving towards max tilt had the slop removed via the unwind number
        if ((Pars.LastDirection == DIRECTION_UPWARDS) && (pos_backslash == false) ||
            (Pars.LastDirection == DIRECTION_DOWNWARDS) && (neg_backslash == true))
        {
            motor_position -= backlash_position;
            neg_backslash = true;
        }
        if (Pars.LastDirection != DIRECTION_DOWNWARDS)
        {
            Pars.LastDirection = DIRECTION_DOWNWARDS;
            Parameter_Write(LastDirection);
        }
    }

    delta_position = motor_position - previous_position;

    TRACE_LOG(("MOTOR: Move to new tilt [T=%3.3d M=%7.5ld P=%7.5ld D=%7.5ld]\r\n",
        ret_tilt, motor_position, previous_position, delta_position));

    return (RESULT_SUCCESS);
}
