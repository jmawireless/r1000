//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

// peripheral configurations
#define EEPROM_CS      LATA3        // select line for Serial EEPROM
#define EEPROM_CS_TRIS TRISA3       // tris control for EEPROM_CS pin

// 25LC256 Serial EEPROM commands
#define SEE_WRSR    1               // write status register
#define SEE_WRITE   2               // write command
#define SEE_READ    3               // read command
#define SEE_WDI     4               // write disable
#define SEE_RDSR    5               // read status register
#define SEE_WEN     6               // write enable

//############################################################################//
// Initialise the Serial EEPROM
//############################################################################//
void Eeprom_Init (void) 
{
    TRACE_INI(("START: EEPROM init\r\n"));

    EEPROM_CS_TRIS = 0;             // configure EEPROM_CS direction
    EEPROM_CS = 1;                  // default EEPROM_CS state
#if 0
    Eeprom_Clear(0, sizeof(parameters_t));
    Eeprom_Dump(0, sizeof(parameters_t));
#endif
}

//############################################################################//
// Check the Serial EEPROM status register
//############################################################################//
uint8_t Eeprom_Writing (void)
{
    uint8_t data;
                          
    EEPROM_CS = 0;                  // select the Serial EEPROM
    SPI_Write(SEE_RDSR);            // send Read Status command
    data = SPI_Write(0);            // send dummy, read status
    EEPROM_CS = 1;                  // deselect Serial EEPROM
    return (data & 1);              // return status
}

//############################################################################//
// Send a Write Enable command
//############################################################################//
void Eeprom_WriteEnable (void)
{
    EEPROM_CS = 0;                  // select the Serial EEPROM
    SPI_Write(SEE_WEN);             // send Write Enabled command
    EEPROM_CS = 1;                  // deselect Serial EEPROM
}

//############################################################################//
// Read a 8-bit value at the specified address
//############################################################################//
uint8_t Eeprom_Read (uint16_t address)
{
    uint8_t data;

    // wait until any work in progress is completed
    while (Eeprom_Writing());       // check Work In Progress

    // Send READ command and addr, then read data
    EEPROM_CS = 0;                  // select the Serial EEPROM
    SPI_Write(SEE_READ);            // send Read command
    SPI_Write(address >> 8);        // address MSB first
    SPI_Write(address);             // address LSB (word aligned)
    data = SPI_Read();
    EEPROM_CS = 1;

    return (data);
}

//############################################################################//
// Write a 8-bit value at the specified address
//############################################################################//
void Eeprom_Write (uint16_t address, uint8_t data)
{
    while (Eeprom_Writing());       // wait until work in progress completed

    Eeprom_WriteEnable();           // set the write enable latch

    EEPROM_CS = 0;                  // select the Serial EEPROM
    SPI_Write(SEE_WRITE);           // write command
    SPI_Write(address >> 8);        // address MSB
    SPI_Write(address);             // address LSB
    SPI_Write(data);                // data
    EEPROM_CS = 1;                  // deselect the Serial EEPROM
}

//############################################################################//
//
//############################################################################//
return_t Eeprom_Clear (uint16_t start_address, uint16_t length)
{
    uint16_t i;
	volatile uint16_t eeprom_address = start_address;
	volatile uint8_t data = 0;

    for (i=0; i<length; i++)
    {
        data = 0xFF;
        Eeprom_Write(eeprom_address, data);
        CLRWDT();
        eeprom_address++;
    }

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
void Eeprom_Dump (uint16_t address, uint16_t length)
{
	uint8_t data;
   	uint16_t i = 0;

   	if (length < 1) 
		return;

	TRACE_PUT(DBG, ("  "));
    do 
	{
		data = Eeprom_Read(address);
		address++;
		TRACE_PUT(DBG, ("%02X ", data));

       	i++;
       	if (i == 16)
		{
        	i = 0;
            TRACE_PUT(DBG, ("\r\n  "));
        }
	} while(--length);

    // Print a linefeed when we are done, if we didn't just print one in the loop.
    if (i != 0) 
		TRACE_PUT(DBG, ("\r\n"));
	else
		TRACE_PUT(DBG, ("\r"));
}
