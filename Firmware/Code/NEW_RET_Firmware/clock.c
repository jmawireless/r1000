//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#if TRACE_ON
static uint32_t clockCount = 0;
static uint32_t clockAccess = 0;
static uint32_t clockSupport = 0;
static uint16_t clockMilliSeconds = 0;
static uint8_t clockSeconds = 0;
static uint8_t clockMinutes = 0;
static uint8_t clockHours   = 0;

//############################################################################//
//
//############################################################################//
void Clock_Tick (void)
{
    // update the clock tick and second counters

    // Use the non-blocking write protocol to ensure data consistency
    ++clockAccess;
    ++clockCount;
    Clock_Update_Time();
    ++clockAccess;
}

//############################################################################//
// Updates the Seconds, Minutes, and Hours based on a 1 sec timer clock source
//############################################################################//
void Clock_Update_Time (void)
{
    if (++clockSupport >= 10)
    {
        clockSupport = 0;
		if (++clockMilliSeconds >= 1000)
		{
			clockMilliSeconds = 0;
		    if (++clockSeconds >= 60)
		    {
		        clockSeconds = 0;
		        if (++clockMinutes >= 60)
		        {
		            clockMinutes = 0;
		            if (++clockHours >= 13)
		            {
		                clockHours = 1;
		            }
		        }
		    }
		}
    }
}

//############################################################################//
//
//############################################################################//
uint32_t Clock_Get_Time (void)
{
    uint32_t beginAccess, endAccess;
    uint32_t currentTime;

    // Use the non-blocking write protocol to ensure data consistency
    do 
	{
        beginAccess = clockAccess;
        currentTime = clockCount;
        endAccess = clockAccess;
    } while (beginAccess != endAccess);

    return (currentTime);
}

//############################################################################//
//
//############################################################################//
uint16_t Clock_Get_MilliSeconds (void)
{
    return (clockMilliSeconds);
}

//############################################################################//
//
//############################################################################//
uint8_t Clock_Get_Seconds (void)
{
    return (clockSeconds);
}

//############################################################################//
//
//############################################################################//
uint8_t Clock_Get_Minutes (void)
{
    return (clockMinutes);
}

//############################################################################//
//
//############################################################################//
uint8_t Clock_Get_Hours (void)
{
    return (clockHours);
}

#endif
