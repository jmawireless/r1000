//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _SYSTEM_H_
#define _SYSTEM_H_

/*-------------------------------------------------------------------*/
/* Include File's List		                                         */
/*-------------------------------------------------------------------*/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#endif

// Components
#include <p18cxxx.h>
#include <usart.h>
#include <flash.h>
#include <delays.h>

#include "typedef.h"
#include "trace.h"

#include "buffer.h"
#include "clock.h"
#include "crc.h"
#include "uart.h"
#include "hdlc.h"
#include "i2c.h"
#include "spi.h"
#include "eeprom.h"
#include "parameters.h"
#include "aisg.h"
#include "download.h"
#include "amis_30624.h"
#include "motor.h"
#include "alarm.h"

// C libraries
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <ctype.h>

//#define OSCILLATOR_PLL_ENABLED
#define SYS_FREQ                40000000L

#define TIMER_MS_TICK   (10)  

// Microcontroller MIPs (FCY)
#define _XTAL_FREQ              SYS_FREQ
#define FCY                     SYS_FREQ/4
#define BAUD_RATE               (9600)
#define SPBRG_VAL               (((SYS_FREQ/BAUD_RATE)/16) - 1)

// UARTx BAUDRATE (BRGH=1)
// UxBRG = ((((SYS_FREQ/BAUD_RATE)/4) - 1) = 0x0411
// DEBUG PORT BAUDRATE 115200
#define SPBRG1H_VAL              (0x00)
#define SPBRG1L_VAL              (0x56)
// AISG PORT BAUDRATE 9600
#define SPBRG2H_VAL              (0x04)
#define SPBRG2L_VAL              (0x11)

#define TIMER_2_1MS             (250) // (FCY/10000)

// Extract the high 8 bits of a 16-bit value (Most Significant Byte)
#define U16_MSB(data)           ((uint8_t)(((data) >> 8) & 0xFF))

// Extract the low 8 bits of a 16-bit value (Least Significant Byte)
#define U16_LSB(data)           ((uint8_t)((data) & 0xFF))

// I/O and Peripheral Initialization
void InitApp (void);

// Handles clock switching/osc initialization
void Configure_Oscillator (void);

extern void TimerISR (void);

extern bool TaskTimerIsOn;

extern bool reset_connection_timer;

extern bool download_reset_request;

extern bool layer2_reset_request;

#endif // _SYSTEM_H_
