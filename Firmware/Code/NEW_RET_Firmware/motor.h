//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _MOTOR_H_
#define _MOTOR_H_

#include "system.h"

#define MOTOR_CALIBRATION_TIMEOUT_SEC   (120)

#define DIRECTION_UPWARDS   (1)

#define DIRECTION_DOWNWARDS (0)

#define Motor_Is_Stopped() ((hw_status.bits.Motion & 0x03) == 0)

#define Motor_Is_Stalled() ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))

return_t Motor_Init (void);

return_t Motor_Configure (void);

return_t Motor_Get_Tilt (int16_t *tilt);

return_t Motor_Set_Tilt (int16_t tilt);

return_t Motor_Set_Tilt_Completed (void);

return_t Motor_Calibrate (void);

return_t Motor_Calibration_Completed (void);

return_t Motor_Move_To_End_Stop (void);

return_t Motor_Move_To_Unwind (void);

return_t Motor_Move_To_Max_Tilt (void);

return_t Motor_Move_To_Last_Tilt (void);

return_t Motor_Move_To_New_Tilt (void);

#endif // _MOTOR_H_