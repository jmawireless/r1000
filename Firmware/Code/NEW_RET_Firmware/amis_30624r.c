//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Hard_Stop (void)
{
    I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_HARD_STOP);
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Reset_To_Default (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_RESET_TO_DEFAULT);
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Reset_Position (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_RESET_POSITION);
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Set_Motor_Param (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_SET_MOTOR_PARAM);
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write((AMIS_30624_IRUN << 4) | AMIS_30624_IHOLD);  // 0x8F
	I2C_Write((AMIS_30624_VMAX << 4) | AMIS_30624_VMIN);  // 0xA1
	I2C_Write(((AMIS_30624_SECPOS >> 3) & 0xE0) | (AMIS_30624_SHAFT << 4) | AMIS_30624_ACC);  // 0x92
	I2C_Write(AMIS_30624_SECPOS & 0xFF); // 0x00
	I2C_Write(0xA2 | (AMIS_30624_PWMFREQ << 6) | (AMIS_30624_ACCSHAPE << 4) | (AMIS_30624_STEPMODE << 2) | AMIS_30624_PWMJEN); // 0xAE
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Set_Stall_Param (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_SET_STALL_PARAM);
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write((AMIS_30624_IRUN << 4) | AMIS_30624_IHOLD); // 0x8F
	I2C_Write((AMIS_30624_VMAX << 4) | AMIS_30624_VMIN); // 0xA1
	I2C_Write((AMIS_30624_MINSAMPLES << 5) | (AMIS_30624_SHAFT << 4) | AMIS_30624_ACC); // 0x72
	I2C_Write((AMIS_30624_ABSTHR << 4) | AMIS_30624_DELTHR);  // 0x0F
	I2C_Write((AMIS_30624_FS2STALLEN << 5) | (AMIS_30624_ACCSHAPE << 4) | (AMIS_30624_STEPMODE << 2) | (AMIS_30624_DC100STEN << 1) | AMIS_30624_PWMJEN); // 0x6E
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Init (void)
{
    TRACE_INI(("START: Stepper driver init\r\n"));
    
    // AMIS_30624_Get_OTP_Param();
    
	TRACE_FUNCT(AMIS_30624_Hard_Stop());
	
	TRACE_FUNCT(AMIS_30624_Reset_To_Default());
	
	TRACE_FUNCT(AMIS_30624_Reset_Position());
	
	TRACE_FUNCT(AMIS_30624_Set_Motor_Param());
	
	TRACE_FUNCT(AMIS_30624_Set_Stall_Param());

    TRACE_FUNCT(AMIS_30624_Get_HW_Status(true));
    
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Set_Position (int16_t position)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_SET_POSITION);
	I2C_Write(AMIS_30624_RESERVED);
	I2C_Write(AMIS_30624_RESERVED);
	I2C_Write(U16_MSB(position));
	I2C_Write(U16_LSB(position));
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Get_Positions (int16_t *actual_position, int16_t *target_position)
{
	uint8_t tmp;
	uint8_t lsb;
	uint8_t msb;
    bool i2c_res;

	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_GET_FULL_STATUS_2);
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_READ);
	i2c_res = I2C_Read(1, &tmp);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	i2c_res = I2C_Read(1, &msb);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	i2c_res = I2C_Read(1, &lsb);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	*actual_position = (msb << 8) | lsb;
	i2c_res = I2C_Read(1, &msb);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	i2c_res = I2C_Read(1, &lsb);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	*target_position = (msb << 8) | lsb;
	i2c_res = I2C_Read(1, &tmp);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	i2c_res = I2C_Read(1, &tmp);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	i2c_res = I2C_Read(0, &tmp);
    if (i2c_res == false)
    {
        I2C_Stop();
        return (RESULT_OTHER_HW_ERROR);
    }
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Get_HW_Status (bool init)
{
    uint8_t i;
    bool i2c_res;
	
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_GET_FULL_STATUS_1);
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_READ);

	for (i = 0; i < 8; i++)
	{
		if (i < 7)
			i2c_res = I2C_Read(1, &hw_status.data[i]);
		else
			i2c_res = I2C_Read(0, &hw_status.data[i]);
        if (i2c_res == false)
        {
            I2C_Stop();
            return (RESULT_OTHER_HW_ERROR);
        }
	} 
	I2C_Stop();

#if 0
    TRACE_DBG(("I2C R: 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X\r\n",
            hw_status.data[0], hw_status.data[1], hw_status.data[2], hw_status.data[3],
            hw_status.data[4], hw_status.data[5], hw_status.data[6]));
#endif
    
    if (init == true)
    {
        return (RESULT_SUCCESS);
    }

	// Electrical defect / charge pump failure / Over I coil X / Over I coil Y
    if (hw_status.bits.ElDef || hw_status.bits.OVC1 ||
        hw_status.bits.OVC2 || hw_status.bits.CPFail)
	{
        TRACE_LOG(("Actuator_Jam_Permanent\r\n"));
        Alarm_Set(Actuator_Jam_Permanent);
		return (RESULT_ACTUATOR_JAM_PERMANENT);
	}

    // digital supply reset / battery stop voltage / thermal shutdown
    if (hw_status.bits.VddReset || hw_status.bits.UV2 || hw_status.bits.TSD)
	{ 
        TRACE_LOG(("Actuator_Jam_Temporary\r\n"));
        Alarm_Set(Actuator_Jam_Temporary);
		return (RESULT_ACTUATOR_JAM_TEMPORARY);
	}

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Get_OTP_Param ()
{
    uint8_t i;
    uint8_t data[8];
    bool i2c_res;
	
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_GET_OTP_PARAM);
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_READ);
	for (i = 0; i < 8; i++)
	{
		if (i < 7)
			i2c_res = I2C_Read(1, &data[i]);
		else
            i2c_res = I2C_Read(0, &data[i]);
        if (i2c_res == false)
        {
            I2C_Stop();
            return (RESULT_OTHER_HW_ERROR);
        }
	}

    I2C_Stop();

#if 0
    TRACE_DBG(("I2C R: %0x.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X 0x%.2X\r\n",
        data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]));
#endif
    
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Set_OTP_Address ()
{
	uint8_t tmp;

	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_SET_OTP);

  	I2C_Write(AMIS_30624_RESERVED);
	I2C_Write(AMIS_30624_RESERVED);
    tmp = 0xF8 + 0x03;
	I2C_Write(tmp);
	I2C_Write(0x00);

    return (RESULT_SUCCESS);
}
