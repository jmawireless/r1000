//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _DOWNLOAD_H_
#define _DOWNLOAD_H_

#include "system.h"

#define APP_START               0x00000800
#define PROGRAM_MEMORY_START    (uint32_t)(APP_START)
#define PROGRAM_SIZE            (uint32_t)(0x0000FC00)
#define PROGRAM_MEMORY_END      (uint32_t)(PROGRAM_SIZE + PROGRAM_MEMORY_START - 1)
#define UPLOAD_MEMORY_START     (uint32_t)(PROGRAM_SIZE + PROGRAM_MEMORY_START)
#define UPLOAD_MEMORY_END       (uint32_t)(PROGRAM_SIZE + UPLOAD_MEMORY_START - 1)
#define MAGIC_CHECKSUM          0x5A5A
#define FIRWMARE_VERSION        1
#define FIRMWARE_TYPE           "CCAPP"
#define FIRMWARE_TYPE_LEN       5

typedef struct
{
    uint8_t Type[5];   // CSSAP
    uint8_t Version;   // 0x01
    uint32_t Length;   // File Length
    uint8_t Flags;     // For future use
    uint8_t HwID;      // Hardware Id (compatible with board HwId)
    char FileName[32]; // Image Name
    uint8_t Spare[18]; // To align the header to 64 bytes (which is a flash block size)
    uint16_t Checksum; // Checksum
} file_header_t;       // (64 bytes)

extern uint16_t flash_checksum;

extern buffer_t dwn_buffer;

extern bool dwn_error;

void Download_Init (void);

return_t Download_Write_Block (uint32_t address, uint8_t *pdata);

uint16_t Block_Checksum (uint8_t *pdata);

#endif // _DOWNLOAD_H_