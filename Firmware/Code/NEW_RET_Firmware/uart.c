//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define TX2_BUFFER_LENGTH 16
#define RX2_BUFFER_LENGTH 16

uint8_t rx2_data[RX2_BUFFER_LENGTH];
uint8_t tx2_data[TX2_BUFFER_LENGTH];
buffer_t rx2_buffer;
buffer_t tx2_buffer;

#define TX1_BUFFER_LENGTH 128
#define RX1_BUFFER_LENGTH 128

uint8_t rx1_data[TX1_BUFFER_LENGTH];
uint8_t tx1_data[RX1_BUFFER_LENGTH];
buffer_t rx1_buffer;
buffer_t tx1_buffer;

//############################################################################//
//
//############################################################################//
void Uart1_Init (void)
{
    uint8_t i;
    
    TRISC7 = 1;             // Input  RX-PIN
    TRISC6 = 0;             // Output TX-PIN

    TXSTA1bits.TX9    = 0;
    TXSTA1bits.TXEN   = 0;
    TXSTA1bits.SYNC   = 0;
    TXSTA1bits.SENDB  = 0;
    TXSTA1bits.BRGH   =	1;

    RCSTA1bits.SPEN   = 1;
    RCSTA1bits.RX9    =	0;
    RCSTA1bits.CREN   = 1;
    RCSTA1bits.ADDEN  =	0;

    BAUDCON1bits.RXDTP = 0;
    BAUDCON1bits.TXCKP = 0;
    BAUDCON1bits.BRG16 = 1;
    BAUDCON1bits.WUE   = 0;
    BAUDCON1bits.ABDEN = 0;

    SPBRGH1 = SPBRG1H_VAL;
    SPBRG1 = SPBRG1L_VAL;

    Buffer_Init(&rx1_buffer, rx1_data, RX1_BUFFER_LENGTH);
    Buffer_Init(&tx1_buffer, tx1_data, TX1_BUFFER_LENGTH);

    // Interrupt enable
    PIE1bits.TX1IE    = 1;
    PIE1bits.RC1IE    = 1;
    IPR1bits.RC1IP    = 1;
    IPR1bits.TX1IP    = 1;

    for (i=0; i<100; i++)   
    {
        __delay_ms(10);
    }    
}

//############################################################################//
//
//############################################################################//
bool Uart1_Send_Byte (uint8_t data)
{
    while (Buffer_Write(&tx1_buffer, data) == false);

    PIE1bits.TX1IE = 0;
    if (TXSTA1bits.TXEN == 0)
        TXSTA1bits.TXEN = 1;
    PIE1bits.TX1IE = 1;

    return (true);
}

//############################################################################//
//
//############################################################################//
bool Uart1_Read_Byte (uint8_t *data)
{
    return (Buffer_Read(&rx1_buffer, data));
}

//############################################################################//
//
//############################################################################//
void Uart1_Interrupt_Handler (void)
{
    uint8_t toTransmit, readed;

    if ((PIR1bits.TX1IF == 1) && (PIE1bits.TX1IE == 1))
    {
        if (Buffer_Read(&tx1_buffer,(char *)&toTransmit) == true)
        {
            TXREG1 = toTransmit;
        }
        else
        {
            if (TXSTA1bits.TRMT)
            {
                TXSTA1bits.TXEN = 0;
            }
        }
    }

    if ((PIR1bits.RC1IF == 1) && (PIE1bits.RC1IE == 1))
    {
        readed = RCREG1;
#if TRACE_ON
        if (readed == 19)
        {
            trace_change = (++trace_change % 3);
        }
#endif        
        Buffer_Write(&rx1_buffer, readed);
        PIR1bits.RC1IF = 0;
    }

    // Clear overrun error if it has occurred
    // New bytes cannot be received if the error occurs and isn't cleared
    if (OERR1)
    {
        CREN1 = 0;   // Disable UART receiver
        CREN1 = 1;   // Enable UART receiver
    }
}

//############################################################################//
//
//############################################################################//
void Uart2_Init (void)
{
    TRISG2 = 1;             // Input  RX-PIN
    TRISG1 = 0;             // Output TX-PIN
    TRISG0 = 0;             // Output TX-ENABLE [RG0]
    PORTGbits.RG0 = 0;
    
    TXSTA2bits.TX9    = 0;
    TXSTA2bits.TXEN   = 0;
    TXSTA2bits.SYNC   = 0;
    TXSTA2bits.SENDB  = 0;
    TXSTA2bits.BRGH   =	1;

    RCSTA2bits.SPEN   = 1;
    RCSTA2bits.RX9    =	0;
    RCSTA2bits.CREN   = 1;
    RCSTA2bits.ADDEN  =	0;

    BAUDCON2bits.RXDTP = 0;
    BAUDCON2bits.TXCKP = 0;
    BAUDCON2bits.BRG16 = 1;
    BAUDCON2bits.WUE   = 0;
    BAUDCON2bits.ABDEN = 0;

    SPBRGH2 = SPBRG2H_VAL;
    SPBRG2 = SPBRG2L_VAL;

    Buffer_Init(&rx2_buffer, rx2_data, RX2_BUFFER_LENGTH);
    Buffer_Init(&tx2_buffer, tx2_data, TX2_BUFFER_LENGTH);

    // Interrupt enable
    PIE3bits.TX2IE    = 1;
    PIE3bits.RC2IE    = 1;
    IPR3bits.RC2IP    = 1;
    IPR3bits.TX2IP    = 1;
}

//############################################################################//
//
//############################################################################//
bool Uart2_Send_Byte (uint8_t data)
{
    while (Buffer_Write(&tx2_buffer, data) == false);

    PIE3bits.TX2IE = 0;
    if (TXSTA2bits.TXEN == 0)
        TXSTA2bits.TXEN = 1;
    PIE3bits.TX2IE = 1;

    return (true);
}

//############################################################################//
//
//############################################################################//
bool Uart2_Read_Byte (uint8_t *data)
{
    return (Buffer_Read(&rx2_buffer, data));
}

//############################################################################//
//
//############################################################################//
void Uart2_Interrupt_Handler (void)
{
    uint8_t toTransmit, readed;

    if ((PIR3bits.TX2IF == 1) && (PIE3bits.TX2IE == 1))
    {
        if (Buffer_Read(&tx2_buffer,(char *)&toTransmit) == true)
        {
            TXREG2 = toTransmit;
        }
        else
        {
            if (TXSTA2bits.TRMT)
            {
                TXSTA2bits.TXEN = 0;
            }
        }
    }

    if ((PIR3bits.RC2IF == 1) && (PIE3bits.RC2IE == 1))
    {
        readed = RCREG2;
        Buffer_Write(&rx2_buffer, readed);
        PIR3bits.RC2IF = 0;
    }

    // Clear overrun error if it has occurred
    // New bytes cannot be received if the error occurs and isn't cleared
    if (OERR2)
    {
        CREN2 = 0;   // Disable UART receiver
        CREN2 = 1;   // Enable UART receiver
    }
}
