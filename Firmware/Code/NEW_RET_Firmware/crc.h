//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _CRC_H_
#define _CRC_H_

#include "system.h"

// The generator polynomial CRC16-CCITT: x^16 + x^12 + x^5 + x^0
#define HDLC_CRC_POLYNOMIAL  0x8408

// Initial CRC value
#define HDLC_CRC_INIT_VAL    0xffff

// Magic CRC value
#define HDLC_CRC_MAGIC_VAL   0xf0b8

//############################################################################//
// Calculates the CRC over one byte
//############################################################################//
uint16_t Crc_Update_U8 (uint16_t crc, uint8_t data);

//############################################################################//
// Calculates the CRC over a number of bytes
//############################################################################//
uint16_t Crc_Update_Data (uint16_t crc, void *data, size_t nr_of_bytes);

#endif // _CRC_H_
