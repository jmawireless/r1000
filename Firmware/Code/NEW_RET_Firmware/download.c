//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define DOWNLOAD_BUFFER_LENGTH  (256)
uint8_t flash_block[FLASH_WRITE_BLOCK];
uint32_t flash_address = 0;
uint16_t flash_checksum = 0;
uint32_t received_length = 0;
bool dwn_error = false;
uint8_t cb_buffer[DOWNLOAD_BUFFER_LENGTH];
buffer_t dwn_buffer;
bool is_header_block = true;

//############################################################################//
//
//############################################################################//
void Download_Init (void)
{
    Buffer_Init(&dwn_buffer, cb_buffer, DOWNLOAD_BUFFER_LENGTH);
    flash_address = UPLOAD_MEMORY_START;
    flash_checksum = 0;
    received_length = 0;
    dwn_error = false;
    is_header_block = true;
}

//############################################################################//
//
//############################################################################//
uint8_t Task_Download (uint8_t state)
{
    uint8_t buf_count;
    bool flash_written = false;
    file_header_t *header = (file_header_t *)flash_block;

    switch (state)
    {
        case TASK_DOWNLOAD_IDLE :
        break;

        case TASK_DOWNLOAD_START :
            Download_Init();
            TRACE_DBG(("Task_Download   (START)\r\n"));
            state = TASK_DOWNLOAD_WRITE_BLOCK;
        break;

        case TASK_DOWNLOAD_WRITE_BLOCK :
			if (Tasks[TASK_AISG_ID].state == TASK_AISG_TRANSMIT)
			    break;

            do
            {
                buf_count = Buffer_Get_Count(&dwn_buffer);
                TRACE_DBG(("buf_count= %u\r\n", buf_count));
                if (buf_count < FLASH_WRITE_BLOCK)
                    break;

                Buffer_ReadData(&dwn_buffer, flash_block, FLASH_WRITE_BLOCK);
                
                if (is_header_block == true)
                {
                    if (memcmp(header->Type, FIRMWARE_TYPE, FIRMWARE_TYPE_LEN) != 0)
                    {
                        TRACE_LOG(("wrong type\r\n"));
                        dwn_error = true;        
                    }

                    if (header->Version != FIRWMARE_VERSION)
                    {
                        TRACE_LOG(("wrong version\r\n"));
                        dwn_error = true;        
                    }

                    TRACE_LOG(("valid firmware header: Name: %s, Len: %ld, checksum: 0x%x\r\n", 
                            header->FileName, header->Length, header->Checksum));
                }
                
                if (received_length < PROGRAM_SIZE)
                {
                    Download_Write_Block(flash_address, flash_block);
                    if (is_header_block == true)
                    {
                        flash_checksum = header->Checksum;
                        is_header_block = false;
                    }
                    flash_address += FLASH_WRITE_BLOCK;
                }
                else
                {
                    dwn_error = true;
                }
                flash_written = true;
            } while (buf_count >= FLASH_WRITE_BLOCK);

            if (flash_written == true)
            {
                last_tcp.completed = true;
                last_tcp.return_code = RESULT_SUCCESS;
                flash_written = false;
            }
        break;

        case TASK_DOWNLOAD_STOP :
            TRACE_DBG(("Task_Download   (STOP)\r\n"));
            if (flash_checksum == MAGIC_CHECKSUM)
            {
                download_reset_request = true;
            }
            state = TASK_DOWNLOAD_IDLE;
        break;

        default:
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
return_t Download_Write_Block (uint32_t address, uint8_t *pdata)
{
    uint8_t flash_array[FLASH_WRITE_BLOCK];
    
// resolve the 1024 erase page size
//  EraseFlash(address, address + FLASH_BLOCK_SIZE);
    WriteBlockFlash(address, 1, pdata);
    received_length += FLASH_WRITE_BLOCK;

    // Compute the checksum
    memset(flash_array, 0, FLASH_WRITE_BLOCK);
    ReadFlash(address, FLASH_WRITE_BLOCK, flash_array);
    flash_checksum += Block_Checksum(flash_array);
    
    TRACE_LOG(("add: 0x%8.8lx, len: %.5ld, chk: 0x%4.4x\r\n",
            flash_address, received_length, flash_checksum));
    
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
uint16_t Block_Checksum (uint8_t *pdata)
{
    uint8_t i;
    uint16_t *pword = (uint16_t *)pdata;
    uint16_t crc = 0;

    for (i=0; i<FLASH_WRITE_BLOCK; i += 2)
    {
        crc += *pword;
        pword++;
    }

    return (crc);
}
