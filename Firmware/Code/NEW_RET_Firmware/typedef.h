//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _TYPEDEF_H_
#define _TYPEDEF_H_

// Global defines
//--------------------->"0123456789ABCDEF"<-------------------------------------
#define SW_VERSION      "      DW_V1.0.0";
#define HW_VERSION      "     HW_R1000_A";

#define SW_VERSION_LENGTH  15
#define HW_VERSION_LENGTH  15
extern volatile const uint8_t sw_version[SW_VERSION_LENGTH + 1];
extern volatile const uint8_t hw_version[HW_VERSION_LENGTH + 1];

#define TASK_MAIN_ID              (0)

#define TASK_AISG_ID              (1)

#define TASK_PARS_ID              (2)

#define TASK_MOTO_ID              (3)

#define TASK_DWNL_ID              (4)

#define MAX_TASKS                 (5) // Max allowed tasks

#define TASK_MAIN_PERIOD        (100) // Main task rate       [msec period]

#define TASK_PARAMETERS_PERIOD    (1) // Parameter task rate  [msec period]

#define TASK_MOTOR_PERIOD      (1000) // Motor task rate      [msec period]

#define TASK_BACKGROUND_PERIOD    (0) // Generic Background task rate

#define TASK_COMM_3MS_DELAY  (3 * TIMER_MS_TICK) // 3 msec TX delay

// From 3 mins re remove 15 secs init time (60 x 3 - 15) x 10
#define CONNECTION_TIMER       (1650) // 3 minutes inactivity timer

typedef uint8_t (*task_function)(uint8_t);

typedef struct
{
    uint32_t      period;     // Rate at which the task should tick
    uint32_t      timer;      // Time since task's last tick
    uint8_t       state;      // Task's current state
    task_function function;   // Task's function
} task_t;

enum RESET_CAUSE
{
    RESET_POWER_ON,
    RESET_BROWN_OUT,
    RESET_POWER_DOWN,
    RESET_WATCHDOG,
    RESET_INSTRUCTION,
    RESET_UNKNOWN
};

#define Task_Set_State(TASK,STATE)  Tasks[TASK].state = STATE

enum TASK_MAIN_STATES
{
    TASK_MAIN_RUNNING,
    TASK_MAIN_RESET
};
uint8_t Task_Main (uint8_t state);

enum TASK_PARAMETERS_STATES
{
    TASK_PARAMETERS_IDLE,
    TASK_PARAMETERS_RUNNING,
    TASK_PARAMETERS_WRITE_BYTE,
    TASK_PARAMETERS_WRITE_CHECKSUM,
    TASK_PARAMETERS_WAIT_DELAY,
    TASK_PARAMETERS_CONFIG_DATA
};
uint8_t Task_Parameters (uint8_t state);

enum TASK_AISG_STATES
{
    TASK_AISG_RECEIVE,
    TASK_AISG_TRANSMIT
};
uint8_t Task_Aisg (uint8_t state);

enum TASK_DOWNLOAD_STATES
{
    TASK_DOWNLOAD_IDLE,
    TASK_DOWNLOAD_START,
    TASK_DOWNLOAD_STOP,
    TASK_DOWNLOAD_WRITE_BLOCK
};
uint8_t Task_Download (uint8_t state);

enum TASK_MOTOR_STATES
{
    TASK_MOTOR_IDLE,
    TASK_MOTOR_SET_POSITION,
    TASK_MOTOR_MOVING,
    TASK_MOTOR_TILT_STARTED,
    TASK_MOTOR_TILT_COMPLETED,
    TASK_MOTOR_TILT_FAILED,
    TASK_MOTOR_CALIBRATION_STARTED,
    TASK_MOTOR_CALIBRATION_END_STOP,
    TASK_MOTOR_CALIBRATION_UNWIND,
    TASK_MOTOR_CALIBRATION_MAX_POS,
    TASK_MOTOR_CALIBRATION_COMPLETED,
    TASK_MOTOR_CALIBRATION_FAILED,
};
uint8_t Task_Motor (uint8_t state);

typedef enum
{
    RESULT_SUCCESS                 = 0x00, // AISG: Success
    RESULT_ACTUATOR_DETECTION_FAIL = 0x01, // AISG: Actuator Detection Fail
    RESULT_ACTUATOR_JAM_PERMANENT  = 0x02, // AISG: Actuator Jam Permanent
    RESULT_ACTUATOR_JAM_TEMPORARY  = 0x03, // AISG: Actuator Jam Temporary
    RESULT_BLOCK_SEQUENCE_ERROR    = 0x04, // AISG: Block Number Sequence Error
    RESULT_BUSY                    = 0x05, // AISG: Busy
    RESULT_CHECKSUM_ERROR          = 0x06, // AISG: Checksum Error
    RESULT_COMMAND_SEQUENCE_ERROR  = 0x07, // AISG: Command Sequence Error
    RESULT_DATA_ERROR              = 0x08, // AISG: Data Error
    RESULT_DEVICE_DISABLED         = 0x09, // AISG: Device Disabled
    RESULT_EEPROM_ERROR            = 0x0A, // AISG: Eeprom Error
    RESULT_FAIL                    = 0x0B, // AISG: Fail
    RESULT_FLASH_ERASE_ERROR       = 0x0C, // AISG: Flash Erase Error
    RESULT_FLASH_WRITE_ERROR       = 0x0D, // AISG: Flash Error
    RESULT_NOT_CALIBRATED          = 0x0E, // AISG: Not Calibrated
    RESULT_NOT_CONFIGURED          = 0x0F, // AISG: Not Scaled
    RESULT_OTHER_HW_ERROR          = 0x11, // AISG: Other Hardware Error
    RESULT_OTHER_SW_ERROR          = 0x12, // AISG: Other Software Error
    RESULT_OUT_OF_RANGE            = 0x13, // AISG: Out Of Range
    RESULT_POSITION_LOST           = 0x14, // AISG: Position Lost
    RESULT_RAM_ERROR               = 0x15, // AISG: Ram Error
    RESULT_SEGMENT_SEQUENCE_ERROR  = 0x16, // AISG: Segment Number Sequence Error
    RESULT_UART_ERROR              = 0x17, // AISG: Uart Error
    RESULT_UNKNOWN_COMMAND         = 0x19, // AISG: Unknown Command
    RESULT_READ_ONLY               = 0x1D, // AISG: Read Only
    RESULT_UNKNOWN_PARAMETER       = 0x1F, // AISG: Unknown Parameter
    RESULT_MISSING_SOFTWARE        = 0x21, // AISG: Working Software Missing
    RESULT_INVALID_FILE            = 0x22, // AISG: Invalid File Content
    RESULT_FORMAT_ERROR            = 0x24, // AISG: Format Error
    RESULT_UNSUPPORTED_PROCEDURE   = 0x25, // AISG: Unsupported Procedure
    RESULT_INVALID_PROCEDURE       = 0x26, // AISG: Invalid Procedure Sequence
    RESULT_ACTUATOR_INTERFERENCE   = 0x27, // AISG: Actuator Interference
    RESULT_INVALID_STATE           = 0x32, // PRIV: Invalid State
    RESULT_PROCEDURE_NOT_COMPLETED = 0x33
} return_t;

#define RET_VENDOR_CODE             "CC"

#define UNIQUE_ID_LENGTH            19

#define VENDOR_ID_LENGTH            2

#define MAX_HDLC_FRAME_LENGTH       74

#define MAX_AISG_FRAME_LENGTH       (MAX_HDLC_FRAME_LENGTH - 3)

// L2 address + L2 ctrl + L7 commands + L7 lenLSB + L7 lenMSB
#define HDLC_FRAME_HEADER_LENGTH 3

#define INFO_FRAME_HEADER_LENGTH 5

#define HDLC_BROADCAST_ADDRESS      (0xFF)

#define HDLC_DEFAULT_ADDRESS        (0x00)

typedef uint8_t unique_id_t [UNIQUE_ID_LENGTH];

#define AISG_VERSION_20                     (0x02)

#define TYPE_SINGLE_RET                     (0x01)

typedef enum
{
	STATE_NO_ADDRESS                       = 0x00,
	STATE_ADDRESS_ASSIGNED                 = 0x01,
	STATE_CONNECTED                        = 0x02,
    STATE_UNKNOWN                          = 0xFF
} state_t;

typedef struct
{
	uint8_t address;
	uint8_t ctrl;
	uint8_t data [MAX_HDLC_FRAME_LENGTH];
} hdlc_frame_t;

typedef struct
{
	uint8_t command;
	uint8_t lenLSB;
	uint8_t lenMSB;
} header_t;

typedef struct
{
    header_t header;
    uint8_t data [MAX_AISG_FRAME_LENGTH];
} info_frame_t;

typedef struct
{
	uint8_t address;
	state_t state;
    unique_id_t unique_id;
	unique_id_t scan_uid;
    unique_id_t scan_msk;
	uint8_t scan_length;
	uint8_t NR;                           	// Receive sequence number N(R)
	uint8_t NS;                           	// Send sequence number N(S)
	uint8_t VR;                           	// Receive state variable V(R)
	uint8_t VS;                           	// Send state variable V(S)
} device_t;

typedef enum
{
	V20_RESET                               = 0x03,
	V20_GET_ALARMS                          = 0x04,
	V20_GET_INFO                            = 0x05,
	V20_CLEAR_ALARMS                        = 0x06,
	V20_READ_USER_DATA                      = 0x10,
	V20_WRITE_USER_DATA                     = 0x11,
	V20_SUBSCRIBE_ALARMS                    = 0x12,
	V20_SELF_TEST                           = 0x0A,
    V20_VENDOR_SPECIFIC                     = 0x90,
	V20_DOWNLOAD_START                      = 0x40,
	V20_DOWNLOAD_APPL                       = 0x41,
	V20_DOWNLOAD_END                        = 0x42,
	V20_SRET_CALIBRATE                      = 0x31,
	V20_SRET_SEND_CONFIG                    = 0x32,
	V20_SRET_SET_TILT                       = 0x33,
	V20_SRET_GET_TILT                       = 0x34,
	V20_SRET_ALARM_INDICATION               = 0x07,
	V20_SRET_SET_DEVICE_DATA                = 0x0E,
	V20_SRET_GET_DEVICE_DATA                = 0x0F,
	COMMAND_TABLE_END                       = 0xFF,
} l7_commands_t;

typedef enum
{
	DATAFIELD_ANTENNA_MODEL_NUMBER          = 0x01,
	DATAFIELD_ANTENNA_SERIAL_NUMBER         = 0x02,
	DATAFIELD_ANTENNA_FREQUENCY_BAND_0      = 0x03,
	DATAFIELD_ANTENNA_BEAMWIDTH             = 0x04,
	DATAFIELD_ANTENNA_GAIN                  = 0x05,
	DATAFIELD_ANTENNA_MAX_TILT              = 0x06,
	DATAFIELD_ANTENNA_MIN_TILT              = 0x07,
    DATAFIELD_ANTENNA_FREQUENCY_BAND_1      = 0x08,
    DATAFIELD_ANTENNA_FREQUENCY_BAND_2      = 0x09,
	DATAFIELD_OPERATOR_INSTALL_DATE         = 0x21,
	DATAFIELD_OPERATOR_INSTALL_ID           = 0x22,
	DATAFIELD_OPERATOR_BASESTATION_ID       = 0x23,
	DATAFIELD_OPERATOR_SECTOR_ID            = 0x24,
	DATAFIELD_OPERATOR_ANTENNA_BEARING      = 0x25,
	DATAFIELD_OPERATOR_MECHANICAL_TILT      = 0x26,
	DATAFIELD_UNKNOWN                       = 0xFF
} datafield_t;

extern task_t Tasks[MAX_TASKS];

extern device_t RET;

#endif // _TYPEDEF_H_
