//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

alarm_db_t alarm;

//############################################################################//
//
//############################################################################//
return_t Alarm_Init (void)
{
    alarm.subscribed = false;
    alarm.current.value = 0x00;
    alarm.previous.value = 0x00;
    alarm.cleared.value = 0x00;
    alarm.raised.value = 0x00;

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
bool Alarm_Waiting_To_Be_Reported (void)
{
    uint8_t ret;

    if (alarm.subscribed == false)
    {
        return (false);
    }

    if (alarm.current.value == alarm.previous.value)
    {
        return (false);
    }

    ret = (alarm.current.value ^ alarm.previous.value);

    alarm.cleared.value = (alarm.previous.value & ret);

    alarm.raised.value = (alarm.current.value & ret);

    TRACE_DBG(("ALARM: 0x%.1x 0x%.1x 0x%.1x\r\n",
        alarm.cleared.value, alarm.raised.value));

    return (true);
}

//############################################################################//
//
//############################################################################//
return_t Alarm_Subscribe (void)
{
    alarm.subscribed = true;

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Alarm_Notify (void)
{
    alarm.previous.value = alarm.current.value;

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Alarm_Clear (void)
{
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Alarm_Get (void)
{
    return (Alarm_Map(alarm.current));
}

//############################################################################//
//
//############################################################################//
return_t Alarm_Get_Raised (void)
{
    return (Alarm_Map(alarm.raised));
}

//############################################################################//
//
//############################################################################//
return_t Alarm_Get_Cleared (void)
{
    return (Alarm_Map(alarm.cleared));
}

//############################################################################//
//
//############################################################################//
return_t Alarm_Map (alarm_t alm)
{
    if (alm.bits.Hardware_Error)
    {
        return (RESULT_OTHER_HW_ERROR);
    }
    else if (alm.bits.Not_Configured)
    {
        return (RESULT_NOT_CONFIGURED);
    }
    if (alm.bits.Actuator_Detection_Fail)
    {
        return (RESULT_ACTUATOR_DETECTION_FAIL);
    }
    else if (alm.bits.Actuator_Jam_Permanent)
    {
        return (RESULT_ACTUATOR_JAM_PERMANENT);
    }
    else if (alm.bits.Actuator_Jam_Temporary)
    {
        return (RESULT_ACTUATOR_JAM_TEMPORARY);
    }
    else if (alm.bits.Position_Lost)
    {
        return (RESULT_POSITION_LOST);
    }
    else if (alm.bits.Actuator_Interference)
    {
        return (RESULT_ACTUATOR_INTERFERENCE);
    }
    else if (alm.bits.Not_Calibrated)
    {
        return (RESULT_NOT_CALIBRATED);
    }

    return (RESULT_SUCCESS);
}
