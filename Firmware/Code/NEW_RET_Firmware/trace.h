//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _TRACE_H_
#define _TRACE_H_

// Flag to disable (TRACE_ON=0) or enable (TRACE_ON=1) debug.
#ifndef TRACE_ON
#define TRACE_ON 1
#endif

// Trace level definitions
typedef enum 
{
    ERR = 0,
    LOG = 1,
    DBG = 2
} trace_t;

#define INI ERR

void TRACE_Header (void);

/* _____TYPE DEFINITIONS_____________________________________________________ */

/* _____MACROS_______________________________________________________________ */
#if TRACE_ON

extern trace_t trace_level;

extern trace_t trace_change;

#define TRACE(SEV, MSG)                                             \
    do                                                              \
    {                                                               \
        if (trace_level >= SEV)                                     \
        {                                                           \
            TRACE_Header();                                         \
            printf MSG;                                             \
        }                                                           \
    } while (0)

#define TRACE_INI(MSG) TRACE(INI, MSG)

#define TRACE_ERR(MSG) TRACE(ERR, MSG)

#define TRACE_LOG(MSG) TRACE(LOG, MSG)

#define TRACE_DBG(MSG) TRACE(DBG, MSG)

#define TRACE_PUT(SEV, MSG)                                         \
    do                                                              \
    {                                                               \
        if (trace_level >= SEV)                                     \
        {                                                           \
            printf MSG;                                             \
            CLRWDT();                                               \
        }                                                           \
    } while (0)

#define TRACE_FUNCT(EXP)                                            \
    do                                                              \
    {                                                               \
        EXP;                                                        \
        if (trace_level >= DBG)                                     \
        {                                                           \
            TRACE_Header();                                         \
            printf ("DEBUG: " #EXP "\r\n");                         \
		}                                                           \
    } while (0)

#else

#define TRACE_INI(MSG)

#define TRACE_ERR(MSG)

#define TRACE_LOG(MSG)

#define TRACE_DBG(MSG)

#define TRACE_FUNCT(EXP)

#define TRACE_PUT(SEV, MSG)

#endif

#endif // _TRACE_H_
