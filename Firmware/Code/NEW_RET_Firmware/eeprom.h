//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _EEPROM_H_
#define _EEPROM_H_

// Intialize access to memory device
void Eeprom_Init (void);

uint8_t Eeprom_Read (uint16_t address);

void Eeprom_Write (uint16_t address, uint8_t data);

return_t Eeprom_Clear (uint16_t start_address, uint16_t length);

void Eeprom_Dump (uint16_t address, uint16_t length);

#endif // _EEPROM_H_
