//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

// TODO: Timer1 -> RTCC
// TODO: Check Timer2 is configured to give an interrupt every 100 usec
// DONE: Change Turn Ratio to uint32_t
// TODO: Improve traces for Aisg
// DONE: Improve traces for Motor
// TODO: I2C interrupt based
// TODO: SPI interrupt based
// TODO: I2C read write buffers with print
// TODO: Cleanup eeprom module
// DONE: If EEprom is completely blank somehow calibration fails
// TODO: Motor disconnected alarm
// DONE: Find a way to indicate the DeviceData parameter in Aisg print
// DONE: Checksum for parameters?
// DONE: If parameters are not valid refuse the motor calibration and tilt
// DONE: Par.MotorPosition is needed???
// TODO: Raise an alarm is Parameters are not valid
// DONE: Try scan when ProductSerialNumber is all 0xFF or 0x00
// DONE: Custom command to erase all data
// DONE: The very first calibration after parameters are erased fails
// DONE: During Tilt update LastDirection only if it is changed
// TODO: Motor Last Tilt and Motor New Tilt are the same?
// DONE: First time after burning a new app the eeprom parameter checksum is bad
// TODO: EEprom write fail alarm
// TODO: if overrun occurs when download is in progress respond with a polling ??

//############################################################################//
// Global Variable Declaration
//############################################################################//

volatile const uint8_t sw_version[SW_VERSION_LENGTH + 1] @ 0x08F0 = SW_VERSION;
volatile const uint8_t hw_version[HW_VERSION_LENGTH + 1] @ 0x0900 = HW_VERSION;

bool reset_connection_timer = false;
bool download_reset_request = false;
bool layer2_reset_request = false;

bool TaskTimerIsOn = false;
#if TRACE_ON
trace_t trace_level = LOG;
trace_t trace_change = LOG;
#endif
device_t RET;

task_t Tasks[MAX_TASKS] =
{
    {TASK_MAIN_PERIOD      , 0, TASK_MAIN_RUNNING, Task_Main},
    {TASK_BACKGROUND_PERIOD, 0, 0, Task_Aisg},
    {TASK_PARAMETERS_PERIOD, 0, TASK_PARAMETERS_RUNNING, Task_Parameters},
    {TASK_MOTOR_PERIOD     , 0, 0, Task_Motor},
    {TASK_BACKGROUND_PERIOD, 0, 0, Task_Download}
};

/*
 * Function:    Task_Main
 * Input:       uint8_t
 * Return:      uint8_t
 * Description: Implementation of different task like TASK_MAIN_RUNNING and 
 *              TASK_MAIN_RESET 
 */
uint8_t Task_Main (uint8_t state)
{
    static int connection_timer = 0;
    static uint32_t iter_count = 0;
    uint8_t trace_temp;

    switch (state)
    {
        case TASK_MAIN_RUNNING :
            CLRWDT();

            #if TRACE_ON
            // Handle trace change
            if (trace_level != trace_change)
            {
                trace_temp = (++trace_level) % 3;
                switch (trace_level)
                {
                    case LOG :
                        TRACE_LOG(("TRACE: LOG\r\n"));
                    break;
                    case DBG :
                        TRACE_LOG(("TRACE: DBG\r\n"));
                    break;
                    default :
                        TRACE_LOG(("TRACE: ERR\r\n"));
                    break;
                }
                trace_level = trace_temp;
                Pars.TraceLevel = trace_level;
                Parameter_Write(TraceLevel);
            }
            #endif                
            connection_timer++;
            if ((connection_timer % 10) == 0)
            {
                TRACE_DBG(("Main Running %d\r\n", iter_count++));
            }
            if (connection_timer == CONNECTION_TIMER)
            {
                state = TASK_MAIN_RESET;
                TRACE_LOG(("Inactivity timer reset\r\n"));
            }
            if (reset_connection_timer == true)
            {
                connection_timer = 0;
                reset_connection_timer = false;
            }
            if ((download_reset_request == true) ||
                (layer2_reset_request == true))
            {
                state = TASK_MAIN_RESET;
                TRACE_LOG(("Layer2 reset\r\n"));
            }
        break;

        case TASK_MAIN_RESET :
            RESET();
        break;

        default:
        break;
    }

    return state;
}

/*
 * Function:    main
 * Input:       void
 * Return:      void
 * Description: This function initializes Hardware peripherals; hardware flag 
 *              check, init firmware modules (SPI, EEPROM, ALARM, AISG, Motor
 *              driver etc)
 */
void main (void)
{
    uint8_t i;
    uint8_t reset_cause = RESET_UNKNOWN;
    uint8_t board_id = 0;
    uint8_t mode = 0;

    //-------------------------------------------------------------
    // STATUS/PCON BITS AND THEIR SIGNIFICANCE
    //-------------------------------------------------------------
    //| POR | BOR | TO |  PD |
    //------------------------
    //|  0  |  u  |  1 |  1  | Power-on Reset
    //------------------------
    //|  1  |  0  |  1 |  1  | Brown-out Detect
    //------------------------
    //|  u  |  u  |  0 |  u  | WDT Reset
    //------------------------
    //|  u  |  u  |  0 |  0  | WDT Wake-up
    //------------------------
    //|  u  |  u  |  u |  u  | MCLR Reset during normal operation
    //------------------------
    //|  u  |  u  |  1 |  0  | MCLR Reset during SLEEP
    //-------------------------------------------------------------
    if ((RCONbits.nBOR == 0) && (RCONbits.nPOR == 0))
    {
        reset_cause = RESET_POWER_ON;
        RCONbits.nBOR = 1;
        RCONbits.nPOR = 1;
    }
    else if ((RCONbits.nBOR == 0) && (RCONbits.nPOR == 1))
    {
        reset_cause = RESET_BROWN_OUT;
        RCONbits.nBOR = 1;
    }
    else if (RCONbits.nPD == 0)
    {
        reset_cause = RESET_POWER_DOWN;
    }
    else if (RCONbits.nTO == 0)
    {
        reset_cause = RESET_WATCHDOG;
    }
    else if (RCONbits.nRI == 0)
    {
        reset_cause = RESET_INSTRUCTION;
        RCONbits.nRI = 1;
    }
    
    // Configure the oscillator for the device
    Configure_Oscillator();

    // Watchdog disabled for now
    SWDTEN = 0;

    // Initialize I/O and Peripherals for application
    InitApp();

    // Read the Board ID
    board_id = PORTB & 0x3F;
   
    // Print the banner
//  TRACE_PUT(INI, ("\r\n"));
    TRACE_INI(("###########################################################\r\n"));
    TRACE_INI(("VERSI: %.16s\r\n", sw_version));
    TRACE_INI(("BUILD: %s @ %s\r\n", __DATE__, __TIME__));
    TRACE_INI(("HW_ID: %d%d%d%d%d%d\r\n",
               (board_id & 0x20) == 0, 
               (board_id & 0x10) == 0,
               (board_id & 0x08) == 0, 
               (board_id & 0x04) == 0, 
               (board_id & 0x02) == 0, 
               (board_id & 0x01) == 0));
    
    mode = (PORTB & 0xC0) >> 6;
    
    if (mode != 0x03)
        TRACE_INI(("DEBUG: ACTIVE\r\n"));

    switch (reset_cause)
    {
        case RESET_POWER_ON:
            TRACE_INI(("RESET: POWER-ON\r\n"));
        break;

        case RESET_BROWN_OUT:
            TRACE_INI(("RESET: BROWN OUT\r\n"));
        break;

        case RESET_POWER_DOWN:
            TRACE_INI(("RESET: POWER DOWN\r\n"));
        break;

        case RESET_WATCHDOG:
            TRACE_INI(("RESET: WATCHDOG TIMEOUT\r\n"));
        break;

        case RESET_INSTRUCTION:
            TRACE_INI(("RESET: RESET INSTRUCTION\r\n"));
        break;

        case RESET_UNKNOWN:
        default:
            TRACE_INI(("RESET: UNKNOWN\r\n"));
        break;
    }

    // Initialize the alarm module
    Alarm_Init();
    // Initialize SPI for memory interface.
    SPI_Init();
    // Initialize Eeprom
    Eeprom_Init();

    // Initialize EEPROM parameters
    Parameters_Init();

    // Initialize I2C peripheral
    I2C_Init();

    // Initialize stepper driver
    AMIS_30624_Init();
 
    // Initialize motor
    Motor_Init();
 
    // Initialize HDLC protocol layer
    Hdlc_Init();

    // Initialize AISG protocol layer
    Aisg_Init();
    
    // Initialize ret object
    RET.address = HDLC_DEFAULT_ADDRESS;
    RET.state = STATE_NO_ADDRESS;
    memcpy(RET.unique_id, RET_VENDOR_CODE, VENDOR_ID_LENGTH);
    memcpy(&RET.unique_id[VENDOR_ID_LENGTH], &Pars.ProductSerialNumber,
           UNIQUE_ID_LENGTH - VENDOR_ID_LENGTH);

    TaskTimerIsOn = true;

    // Enabling watchdog
    SWDTEN = 1;

    Parameters_Print();
   
    TRACE_INI(("START: Scheduler started\r\n"));
    TRACE_INI(("###########################################################\r\n"));

    // Scheduler loop
    while (1)
    {
        // Clear watchdog timer
        CLRWDT();

        // Scheduler main loop
        for (i = 0; i < MAX_TASKS; ++i)
        {
            if ((Tasks[i].function != NULL) &&
                (Tasks[i].timer >= Tasks[i].period))
            {
                // Task's timer has expired. Check if Task_Main, Task_Aisg, 
                // Task_Parameters, Task_Motor, Task_Download pending execution
                Tasks[i].state = Tasks[i].function(Tasks[i].state);
                Tasks[i].timer = 0;
            }
        }
    }
}

/*
 * Function:    TimerISR
 * Input:       void
 * Return:      void
 * Description: Implementation of TimerISR; Looks for pending timer based task 
 *              and increments task count value
 */
void TimerISR (void)
{
    uint8_t i;
    
    // Update task timers
    for (i = 0; i < MAX_TASKS; ++i)
    {
        if (Tasks[i].function != NULL)
        {
            Tasks[i].timer++;
        }
    }
}
