//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define I2C_READ_TIMEOUT    (0xFFFF)

/*
 * Function:    I2C_Init
 * Input:       void
 * Return:      void
 * Description: Initializes I2C in master mode, Sets the required baudrate
 */
void I2C_Init (void)
{
    uint8_t i;
    
    TRACE_INI(("START: I2C init\r\n"));

    // Clear I2C bus
    I2C_Clear();

    // SDA and SCL as input pin
    // these pins can be configured either i/p or o/p
	TRISD5 = 1;
	TRISD6 = 1;
    // Slew rate disabled
	SSP2STAT |= 0x80;
    // SSPEN = 1, I2C Master mode, clock = FOSC/(4 * (SSPADD + 1))
	SSP2CON1  = 0x28;
    // 100Khz @ 40Mhz Fosc
	SSP2ADD   = 0x63;
    // 1 sec Delay
    for (i=0; i<100; i++)   
    {
        __delay_ms(10);
    }
}

/*
 * Function:    I2C_Start
 * Input:       void
 * Return:      void
 * Description: Sends a start condition on I2C Bus
 */
void I2C_Start (void)
{
    // Start condition enabled
	SEN2 = 1;
    // wait for start condition to finish
    // SEN automatically cleared by hardware */
	while(SEN2);
}

/*
 * Function:    I2C_Stop
 * Input:       void
 * Return:      void
 * Description: Sends a stop condition on I2C Bus
 */
void I2C_Stop (void)
{
    // Stop condition enabled
	PEN2 = 1;
    // Wait for stop condition to finish
    // PEN automatically cleared by hardware
	while(PEN2);
}

/*
 * Function:    I2C_Restart
 * Input:       void
 * Return:      void
 * Description: Sends a repeated start condition on I2C Bus
 */
void I2C_Restart (void)
{
    // Repeated start enabled
	RSEN2 = 1;
    // wait for condition to finish
	while(RSEN2);
}

/*
 * Function:    I2C_Ack
 * Input:       void
 * Return:      void
 * Description: Generates acknowledge for a transfer
 */
void I2C_Ack (void)
{
    // Acknowledge data bit, 0 = ACK
	ACKDT2 = 0;         
    // Ack data enabled
	ACKEN2 = 1;
    // wait for ack data to send on bus
	while(ACKEN2);
}

/*
 * Function:    I2C_Nack
 * Input:       void
 * Return:      void
 * Description: Generates Not-acknowledge for a transfer
 */
void I2C_Nack (void)
{
    // Acknowledge data bit, 1 = NAK
	ACKDT2 = 1;
    // Ack data enabled
	ACKEN2 = 1;
    // wait for ack data to send on bus
	while(ACKEN2);
}

/*
 * Function:    I2C_Wait
 * Input:       void
 * Return:      void
 * Description: Waits for transfer to finish
 */
void I2C_Wait (void)
{
    // wait for any pending transfer
	while ((SSP2CON2 & 0x1F) || (SSP2STAT & 0x04));
}

/*
 * Function:    I2C_Write
 * Input:       void
 * Return:      void
 * Description: Sends 8-bit data on I2C bus
 */
void I2C_Write (uint8_t data)
{
    // Move data to SSPBUF
	SSP2BUF = data;
    // wait till complete data is sent from buffer
	while(BF2);
    // wait for any pending transfer
	I2C_Wait();         
}

/*
 * Function:    I2C_Write
 * Input:       void
 * Return:      void
 * Description: Reads 8-bit data from I2C bus. Returns false if a timeout occurs
 */
bool I2C_Read (bool ack, uint8_t *data)
{
    uint32_t i2c_read_timeout = I2C_READ_TIMEOUT;

    // Reception works if transfer is initiated in read mode
    // Enable data reception
	RCEN2 = 1;

    // wait for buffer full or timeout
	while(!BF2 && i2c_read_timeout > 0)
    {
        // Clear watchdog timer
        CLRWDT();
        // decrement the i2c read timerout count
        i2c_read_timeout--;
    }
    // check if i2c timeout count expired
    if (i2c_read_timeout == 0)
    {
        // send error message on debug UART
        TRACE_ERR(("ERROR: I2C read timeout\r\n"));
        // Set Alarm  to flag Hardware error 
        Alarm_Set(Hardware_Error);
        // return command failed flag
        return (false);
    }
    
    // Read serial buffer and store in temp register
	*data = SSP2BUF;
    // wait to check any pending transfer
	I2C_Wait();
    // check if ack required
    if (ack)
        // send Ack
        I2C_Ack(); 
    // Return the read data from bus
	return (true);
}

/*
 * Function:    I2C_Clear
 * Input:       void
 * Return:      void
 * Description: Clears i2c if stuck
 */
void I2C_Clear (void)
{
    uint8_t sspcon1_orig;
    uint8_t i;
    // store port state
	sspcon1_orig = SSP2CON1;
	SSP2CON1 = 0x00;
    // Setup i2c pin date to start/enable transmit
	TRISDbits.RD5 = 1; // SDA
    TRISDbits.RD6 = 0; // SCL

    // clear the I2C bus
    // Generate clock pulses on SCL
    for (i=0; i<16; i++)
	{
		PORTDbits.RD6 = ~PORTDbits.RD6;
		__delay_us(5);
	}
    // setup i2c pin state to disable/end of transmit
	TRISDbits.RD5 = 1;
    TRISDbits.RD6 = 1;
    // reconfigure i2c port
	SSP2CON1 = sspcon1_orig;
    //10ms delay
    __delay_ms(10);
}
