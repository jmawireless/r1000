//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

/*
 * Function:    Crc_Update_U8
 * Input:       uint16_t, uint8_t
 * Return:      uint16_t
 * Description: Calculate/Update CRC on 1 byte data
 */
uint16_t Crc_Update_U8 (uint16_t crc, uint8_t data)
{
    uint8_t i;
    // Exor CRC with Data
    crc ^= data;
    // Assign weighted value for each logic 1 and return crc new value 
    for (i=8; i!=0; i--)
    {
        if (crc & 1)
        {
            crc = (crc >> 1) ^ HDLC_CRC_POLYNOMIAL;
        }
        else
        {
            crc = (crc >> 1);
        }
    }

    return crc;
}

/*
 * Function:    Crc_Update_Data
 * Input:       uint16_t, void*, size_t
 * Return:      uint16_t
 * Description: Calculate/Update CRC on data length
 */
uint16_t Crc_Update_Data (uint16_t crc, void *data, size_t nr_of_bytes)
{
    uint8_t *data_u8 = (uint8_t *)data;
    // for complete length of data calculate CRC and return new CRC value
    while (nr_of_bytes)
    {
        crc = Crc_Update_U8(crc, *data_u8++);
        nr_of_bytes--;
    }

    return crc;
}
