//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _UART_H_
#define _UART_H_

#include "system.h"

void Uart1_Init (void);

bool Uart1_Send_Byte (uint8_t data);

bool Uart1_Read_Byte (uint8_t *data);

void Uart1_Interrupt_Handler (void);

void Uart2_Init (void);

bool Uart2_Send_Byte (uint8_t data);

bool Uart2_Read_Byte (uint8_t *data);

void Uart2_Interrupt_Handler (void);

#endif // _UART_H_