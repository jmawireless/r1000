//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

// Define Tx/Rx buffer length
#define TX2_BUFFER_LENGTH 16
#define RX2_BUFFER_LENGTH 16

// Create circular buffer
uint8_t rx2_data[RX2_BUFFER_LENGTH];
uint8_t tx2_data[TX2_BUFFER_LENGTH];
buffer_t rx2_buffer;
buffer_t tx2_buffer;

// Define Tx/Rx buffer length
#define TX1_BUFFER_LENGTH 128
#define RX1_BUFFER_LENGTH 128

// Create circular buffer 
uint8_t rx1_data[TX1_BUFFER_LENGTH];
uint8_t tx1_data[RX1_BUFFER_LENGTH];
buffer_t rx1_buffer;
buffer_t tx1_buffer;

/*
 * Function:    Uart1_Init
 * Input:       void
 * Return:      void
 * Description: Initialize UART port pin, set the mode and baud rate
 */
void Uart1_Init (void)
{
    uint8_t i;
    // Set port to UART port pin direction
    TRISC7 = 1;             // Input  RX-PIN
    TRISC6 = 0;             // Output TX-PIN
    // Setup port pin to Tx
    TXSTA1bits.TX9    = 0;
    TXSTA1bits.TXEN   = 0;
    TXSTA1bits.SYNC   = 0;
    TXSTA1bits.SENDB  = 0;
    TXSTA1bits.BRGH   =	1;
    // Setup port pin to Rx 
    RCSTA1bits.SPEN   = 1;
    RCSTA1bits.RX9    =	0;
    RCSTA1bits.CREN   = 1;
    RCSTA1bits.ADDEN  =	0;
    // Set up UART config
    BAUDCON1bits.RXDTP = 0;
    BAUDCON1bits.TXCKP = 0;
    BAUDCON1bits.BRG16 = 1;
    BAUDCON1bits.WUE   = 0;
    BAUDCON1bits.ABDEN = 0;
    // Setup debug port baud rate to 115200 baud
    SPBRGH1 = SPBRG1H_VAL;
    SPBRG1 = SPBRG1L_VAL;
    // Initialize the debug Tx/Rx buffer
    Buffer_Init(&rx1_buffer, rx1_data, RX1_BUFFER_LENGTH);
    Buffer_Init(&tx1_buffer, tx1_data, TX1_BUFFER_LENGTH);

    // Interrupt enable
    PIE1bits.TX1IE    = 1;
    PIE1bits.RC1IE    = 1;
    IPR1bits.RC1IP    = 1;
    IPR1bits.TX1IP    = 1;
    // Delay to assure hardware setup complete
    for (i=0; i<100; i++)   
    {
        __delay_ms(10);
    }    
}

/*
 * Function:    Uart1_Send_Byte
 * Input:       uint8_t
 * Return:      bool
 * Description: Send 'n' byte data on UART1 (Debug UART)
 */
bool Uart1_Send_Byte (uint8_t data)
{
    // Store data to circular buffer 
    while (Buffer_Write(&tx1_buffer, data) == false);
    // Reset UART interrupt flag
    PIE1bits.TX1IE = 0;
    // Set UART TX enable if UART TX enable flag not set
    // todo: check if the flag cleared by any function, should never disable Tx enable
    if (TXSTA1bits.TXEN == 0)
        TXSTA1bits.TXEN = 1;
    // Set UART Tx interrupt flag
    PIE1bits.TX1IE = 1;
    // return true
    return (true);
}

/*
 * Function:    Uart1_Read_Byte
 * Input:       uint8_t
 * Return:      bool
 * Description: Read data received on UART1 (Debug UART) and store in buffer
 */
bool Uart1_Read_Byte (uint8_t *data)
{
    // store data and return if the data stored in buffer was success/failure
    return (Buffer_Read(&rx1_buffer, data));
}

/*
 * Function:    Uart1_Interrupt_Handler
 * Input:       void
 * Return:      void
 * Description: Read data received on UART1 (Debug UART) and store in buffer
 */
void Uart1_Interrupt_Handler (void)
{
    uint8_t toTransmit, readed;
    // Check if Tx buffer empty and Tx interrupt enable
    if ((PIR1bits.TX1IF == 1) && (PIE1bits.TX1IE == 1))
    {
        // check if there is pending data to transmit
        // todo: Check if global interrupt and INTCON set??
        // todo: check for TRMT flag if empty
        if (Buffer_Read(&tx1_buffer,(char *)&toTransmit) == true)
        {
            // load data byte to tx register
            TXREG1 = toTransmit;
        }
        // todo: check if you can remove else condition
        else
        {   
            // Check if transmit shift register status empty 
            if (TXSTA1bits.TRMT)
            {
                // Disable UART tx status flag
                // todo: check if disable UART Tx needed 
                TXSTA1bits.TXEN = 0;
            }
        }
    }
    // Check of Rx interrupt flag set and Rx interrupt enable (Rx msg)
    if ((PIR1bits.RC1IF == 1) && (PIE1bits.RC1IE == 1))
    {
        // Read the rx byte
        readed = RCREG1;
        //Debug message
        #if TRACE_ON
        if (readed == 19)
        {
            trace_change = (++trace_change % 3);
        }
        #endif     
        // store the Rx message to buffer
        Buffer_Write(&rx1_buffer, readed);
        // Clear UART Rx interrupt flag
        PIR1bits.RC1IF = 0;
    }

    // Clear overrun error if it has occurred
    // New bytes cannot be received if the error occurs and isn't cleared
    if (OERR1)
    {
        CREN1 = 0;   // Disable UART receiver
        CREN1 = 1;   // Enable UART receiver
    }
}

/*
 * Function:    Uart2_Init
 * Input:       void
 * Return:      void
 * Description: Initialize UART port pin, set the mode and baud rate
 */
void Uart2_Init (void)
{
    // Set port to UART port pin direction 
    TRISG2 = 1;             // Input  RX-PIN
    TRISG1 = 0;             // Output TX-PIN
    TRISG0 = 0;             // Output TX-ENABLE [RG0]
    PORTGbits.RG0 = 0;
    // Setup port pin to Tx
    TXSTA2bits.TX9    = 0;
    TXSTA2bits.TXEN   = 0;
    TXSTA2bits.SYNC   = 0;
    TXSTA2bits.SENDB  = 0;
    TXSTA2bits.BRGH   =	1;
    // Setup port pin to Rx
    RCSTA2bits.SPEN   = 1;
    RCSTA2bits.RX9    =	0;
    RCSTA2bits.CREN   = 1;
    RCSTA2bits.ADDEN  =	0;
    // Set up UART Config
    BAUDCON2bits.RXDTP = 0;
    BAUDCON2bits.TXCKP = 0;
    BAUDCON2bits.BRG16 = 1;
    BAUDCON2bits.WUE   = 0;
    BAUDCON2bits.ABDEN = 0;
    // Setup debug port baud rate to 115200 baud
    SPBRGH2 = SPBRG2H_VAL;
    SPBRG2 = SPBRG2L_VAL;
    // Initialize the debug Tx/Rx buffer
    Buffer_Init(&rx2_buffer, rx2_data, RX2_BUFFER_LENGTH);
    Buffer_Init(&tx2_buffer, tx2_data, TX2_BUFFER_LENGTH);

    // Interrupt enable
    PIE3bits.TX2IE    = 1;
    PIE3bits.RC2IE    = 1;
    IPR3bits.RC2IP    = 1;
    IPR3bits.TX2IP    = 1;
}

/*
 * Function:    Uart2_Send_Byte
 * Input:       uint8_t
 * Return:      bool
 * Description: Send 'n' byte data on UART2 (AISG UART)
 */
bool Uart2_Send_Byte (uint8_t data)
{
    // Store data to circular buffer 
    while (Buffer_Write(&tx2_buffer, data) == false);
    // Reset UART interrupt flag
    PIE3bits.TX2IE = 0;
    // Set UART TX enable if UART TX enable flag not set
    // todo: check if the flag cleared by any function, should never disable Tx enable
    if (TXSTA2bits.TXEN == 0)
        TXSTA2bits.TXEN = 1;
    // Set UART Tx interrupt flag
    PIE3bits.TX2IE = 1;
    // return true
    return (true);
}

/*
 * Function:    Uart2_Read_Byte
 * Input:       uint8_t
 * Return:      bool
 * Description: Read data received on UART2 (AISG UART) and store in buffer
 */
bool Uart2_Read_Byte (uint8_t *data)
{
    // store data and return if the data stored in buffer was success/failure
    return (Buffer_Read(&rx2_buffer, data));
}

/*
 * Function:    Uart2_Interrupt_Handler
 * Input:       void
 * Return:      void
 * Description: Read data received on UART2 (AISG UART) and store in buffer
 */
void Uart2_Interrupt_Handler (void)
{
    uint8_t toTransmit, readed;
    // Check if Tx buffer empty and Tx interrupt enable
    if ((PIR3bits.TX2IF == 1) && (PIE3bits.TX2IE == 1))
    {
        // check if there is pending data to transmit
        // todo: Check if global interrupt and INTCON set??
        // todo: check for TRMT flag if empty
        if (Buffer_Read(&tx2_buffer,(char *)&toTransmit) == true)
        {
            // load data byte to tx register
            TXREG2 = toTransmit;
        }
        // todo: check if you can remove else condition
        else
        {
            // Check if transmit shift register status empty 
            if (TXSTA2bits.TRMT)
            {
                // Disable UART tx status flag
                // todo: check if disable UART Tx needed
                TXSTA2bits.TXEN = 0;
            }
        }
    }
    // Check of Rx interrupt flag set and Rx interrupt enable (Rx msg)
    if ((PIR3bits.RC2IF == 1) && (PIE3bits.RC2IE == 1))
    {
        // Read the rx byte
        readed = RCREG2;
        // store the Rx message to buffer 
        Buffer_Write(&rx2_buffer, readed);
        // Clear UART Rx interrupt flag
        PIR3bits.RC2IF = 0;
    }

    // Clear overrun error if it has occurred
    // New bytes cannot be received if the error occurs and isn't cleared
    if (OERR2)
    {
        CREN2 = 0;   // Disable UART receiver
        CREN2 = 1;   // Enable UART receiver
    }
}
