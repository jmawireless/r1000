//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"
// Watchdog Timer Enable bit (WDT enabled)
#pragma config WDTEN  = OFF  
// Watchdog Timer Postscale Select bits (1:1024)
#pragma config WDTPS  = 64          
#pragma config DEBUG  = OFF
#pragma config XINST  = OFF

// Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
//[A.S.]#pragma config LVP    = OFF   
// Data EEPROM Code Protection bit (Data EEPROM code-protected)
//#pragma config CPD    = ON       


// CONFIG1H
// Oscillator Selection bits (HS oscillator)
#pragma config FOSC = HSPLL     
// Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor enabled)
#pragma config FCMEN = ON  
// Internal/External Oscillator Switchover bit (Oscillator Switchover mode enabled)
#pragma config IESO = ON        

// CONFIG2L
// Power-up Timer Enable bit (PWRT disabled)
//[A.S.]#pragma config PWRT = OFF    
// Brown-out Reset Enable bits (Brown-out Reset disabled in hardware and software)
//[A.S.]#pragma config BOREN = OFF   
// Brown Out Reset Voltage bits ()
//[A.S.]#pragma config BORV = 2         

// CONFIG3H
// CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
//[A.S.]#pragma config CCP2MX = PORTC 
// PORTB A/D Enable bit (PORTB<4:0> pins are configured as analog input channels on Reset)
//[A.S.]#pragma config PBADEN = ON    
// Low-Power Timer1 Oscillator Enable bit (Timer1 configured for low-power operation)
//[A.S.]#pragma config LPT1OSC = ON  
// MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)
//[A.S.]#pragma config MCLRE = ON       

// CONFIG4L
// Stack Full/Underflow Reset Enable bit (Stack full/underflow will not cause Reset)
//[A.S.]#pragma config STVREN = OFF  
// Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
//[A.S.]#pragma config LVP = OFF   
// Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))
//[A.S.]#pragma config XINST = OFF      

uint8_t timer_ms_tick = 0;


#define USE_TIMER_1

/*
 * Function:    Configure_Oscillator
 * Input:       void
 * Return:      void
 * Description: Configure Crystal Oscillator
 */
void Configure_Oscillator (void)
{
    /* Typical actions in this function are to tweak the oscillator tuning
    register, select new clock sources, and to wait until new clock sources
    are stable before resuming execution of the main project. */
    OSCCON = 0x48; // Turn off Sleep, Use 1Mhz clock from default clock source
    OSCTUNE = 0x40; // HSPLL
    PIE2 = 0x00;
    PIR2 = 0x10;
}

/*
 * Function:    InitApp
 * Input:       void
 * Return:      void
 * Description: Initialize ports, timer, uart and interrupts
 */
void InitApp (void)
{
    ADCON0 = 0x00;
    ADCON1 = 0x0F;
    
    // Initialize ports
    LATB = 0x00;
    TRISB = 0x00;
    PORTB = 0xFF;

    TRISA = 0x00;

    LATG = 0x00;
    TRISG = 0x00;
    PORTG = 0xFE;

    // Initialize timers
    INTCONbits.RBIF = 0;    //
    //
    INTCONbits.INT0IF = 0;  //
    //
    INTCONbits.TMR0IF = 0;  //
    //
    INTCONbits.RBIE = 0;    //
    //
    INTCONbits.INT0IE = 0;  //
    //
    INTCONbits.TMR0IE = 0;  //
    //
    INTCONbits.PEIE = 0;    //
    // TMR0 high priority
    INTCON2 = 0x85;         

#ifdef USE_TIMER_0
    // Timer0 Registers Prescaler= 1 - TMR0 Preset = 6 - Freq = 1000.00 Hz - Period = 0.001000 seconds
    // Bit 7 Enable Timer0
    T0CONbits.TMR0ON = 1;   
    // Bit 6 TMR0 8 bits
    T0CONbits.T08BIT = 0; 
    // Bit 5 TMR0 Clock Source Select bit...0 = Internal Clock (CLKO) 1 = Transition on T0CKI pin
    T0CONbits.T0CS = 0; 
    // Bit 4 TMR0 Source Edge Select bit 0 = low/high 1 = high/low    
    T0CONbits.T0SE = 0; 
    // Bit 3 Prescaler Assignment bit...0 = Prescaler is assigned to the WDT    
    T0CONbits.PSA = 1;  
    // Bit 2 Prescaler Rate Select bits    
    T0CONbits.T0PS2 = 0; 
    // Bit 1 Prescaler Rate Select bits
    T0CONbits.T0PS1 = 0;   
    // Bit 0 Prescaler Rate Select bits
    T0CONbits.T0PS0 = 0; 
    // Preset for Timer0 MSB register
    TMR0H = 0; 
    // Preset for Timer0 LSB register
    TMR0L = 6;              
#endif
    
#ifdef USE_TIMER_1
    // Timer1 Registers Prescaler= 1 - TMR1 Preset = 64536 - Freq = 1000.00 Hz - Period = 0.001000 seconds
    // Bit 5 Prescaler Rate Select bits
    T1CONbits.T1CKPS1 = 0;  
    // Bit 4 Prescaler Rate Select bits
    T1CONbits.T1CKPS0 = 0;  
    // Bit 3 Timer1 Oscillator Enable Control bit 1 = on
    T1CONbits.T1OSCEN = 1; 
    // Bit 2 Timer1 External Clock Input Synchronization Control bit...1 = Do not synchronize external clock input
    T1CONbits.T1SYNC = 1;   
    // Bit 1 Timer1 Clock Source Select bit...0 = Internal clock (FOSC/4)
    T1CONbits.TMR1CS = 0; 
    // Bit 0 Enable Timer1
    T1CONbits.TMR1ON = 1;  
    // Preset for Timer1 MSB register
    TMR1H = 0xFC;   
    // Preset for Timer1 LSB register
    TMR1L = 0x10; 
    // Clear Timer1 interrupt
    PIR1bits.TMR1IF = 0;  
    // Turn on Timer1 interrupts
    PIE1bits.TMR1IE = 1;    
#endif

    // Timer2 Registers Prescaler = 1 - TMR2 PostScaler = 4 - PR2 = 250 - Freq = 10000.00 Hz - Period = 0.0001000 seconds
    // Bit 6 Prescaler Rate Select bits
    T2CONbits.T2CKPS0 = 0;  
    // Bit 5 Prescaler Rate Select bits
    T2CONbits.T2CKPS1 = 0;  
    // Bit 4 Enable Timer2
    T2CONbits.TMR2ON = 1; 
    // Bit 3 Postscaler Rate Select bits
    T2CONbits.T2OUTPS0 = 1; 
    // Bit 2 Postscaler Rate Select bits
    T2CONbits.T2OUTPS1 = 1; 
    // Bit 1 Postscaler Rate Select bits
    T2CONbits.T2OUTPS2 = 0; 
    // Bit 0 Postscaler Rate Select bits
    T2CONbits.T2OUTPS3 = 0; 
    // Timer2 compare register
    PR2 = TIMER_2_1MS;   
    // Clear Timer2 interrupt
    PIR1bits.TMR2IF = 0;   
    // Turn on Timer2 interrupt
    PIE1bits.TMR2IE = 1;    
    // Initialize serial ports
    Uart1_Init();
    Uart2_Init();
    // Enable priority levels
    RCONbits.IPEN = 1;  
    // Enable interrupts    
    INTCONbits.GIE = 1; 
    // Enable peripheral interrupt sources       
    INTCONbits.PEIE   = 1;  
}

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/*
 * Function:    high_isr
 * Input:       void
 * Return:      void
 * Description: High-priority interrupt service routine
 */


#if defined(__XC) || defined(HI_TECH_C)
void interrupt high_isr(void)
#elif defined (__18CXX)
#pragma code high_isr=0x08
#pragma interrupt high_isr
void high_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{
    if (INTCONbits.RBIF)
    {
        INTCONbits.RBIF = 0;
    }

    #ifdef USE_TIMER_1
    // Timer1 Interrupt - Freq = 500.00 Hz - Period = 0.002000 seconds
    if (PIR1bits.TMR1IF == 1)
    {
        // Interrupt must be cleared by software
        PIR1bits.TMR1IF = 0; 
        // Re-enable the interrupt
        PIE1bits.TMR1IE = 1; 
        // Preset for Timer1 MSB register
        TMR1H = 0xFC; 
        // Preset for Timer1 LSB register
        TMR1L = 0x32;   
        // Toggle LED
        LATDbits.LATD3 ^= 1;       	
    }
    #endif

    // Timer2 Interrupt- Freq = 1000.00 Hz - Period = 0.001000 seconds
    if (PIR1bits.TMR2IF)
    {
        PIR1bits.TMR2IF = 0;        // Clear interrupt flag
        #if TRACE_ON
        Clock_Tick();
        #endif
        // check if 3ms timer enabled 
        if (aisg_3ms_timer_enabled == true)
        {
            // check if count++ value greater than delay count
			if (aisg_3ms_timer_count++ > TASK_COMM_3MS_DELAY)
			{
                // disable 3ms timer counter
				aisg_3ms_timer_enabled = false;
                // reset the timer count value to 0
    	        aisg_3ms_timer_count = 0;
			}
        }
        // check if the scheduler is running
        if (TaskTimerIsOn == true)// Run the scheduler
        {
            // check if timer count tick greater than timer tick
            if (timer_ms_tick++ >= TIMER_MS_TICK)
            {
                // reset the timer tick count to 0
                timer_ms_tick = 0;
                // run timer ISR (scheduled tast/event)
                TimerISR();
            }
        }
    }

    Uart1_Interrupt_Handler();
    Uart2_Interrupt_Handler();
}

/*
 * Function:    low_isr
 * Input:       void
 * Return:      void
 * Description: Low-priority interrupt routine
 */
#if defined(__XC) || defined(HI_TECH_C)
void low_priority interrupt low_isr(void)
#elif defined (__18CXX)
#pragma code low_isr=0x18
#pragma interruptlow low_isr
void low_isr(void)
#else
#error "Invalid compiler selection for implemented ISR routines"
#endif
{

}
