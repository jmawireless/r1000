//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#include "system.h"

#define PARAMETER_N_BANDS		    			     4
#define PARAMETER_MODEL_NUMBER_LENGTH               15
#define PARAMETER_SERIAL_NUMBER_LENGTH    	        17
#define PARAMETER_N_FREQUENCY_BANDS                  3
#define PARAMETER_OPERATOR_INSTALL_DATE_LENGTH     	 6
#define PARAMETER_OPERATOR_INSTALL_ID_LENGTH         5
#define PARAMETER_OPERATOR_BASESTATION_ID_LENGTH   	32
#define PARAMETER_OPERATOR_SECTOR_ID_LENGTH        	32
#define CONFIG_DATA_VERSION                          2
#define CONFIG_DATA_TYPE                       "CCACF"
#define CONFIG_DATA_TYPE_LEN                         5

#define Parameter_Write(X)          ParsUpdate.Bits.X = 1

#define Parameter_Length(X)         sizeof(Pars.X)

#define Parameter_Name(X)           (#X)

#define Parameter_Process(X)                                                    \
{                                                                               \
    if (ParsUpdate.Bits.X == 1)                                                 \
    {                                                                           \
        Parameters_Update((uint8_t *)(&(Pars.X)), Parameter_Length(X),          \
                          (uint8_t *)(&(Pars.Checksum.X)));                     \
        ParsUpdate.Bits.X = 0;                                                  \
        TRACE_LOG(("PARAM: Updated %s\r\n", #X));                               \
    }                                                                           \
}

#define Parameter_Verify(X)                                                     \
    do                                                                          \
    {                                                                           \
        TRACE_DBG(("PARAM: Validating %s\r\n", #X));                            \
        if (Parameters_Validate((uint8_t *)(&(Pars.X)), Parameter_Length(X),    \
                      (uint8_t *)(&(Pars.Checksum.X))) != RESULT_SUCCESS)       \
        {                                                                       \
            ParsValid.Bits.X = 0;                                               \
        }                                                                       \
    } while(0)

#define Parameter_Good(X)  (ParsValid.Bits.X) ? true : false

#define Parameter_Status() (par_valid ? "[VAL]" : "[BAD]")

typedef union
{
    uint32_t Value;

    struct
    {
        uint8_t ProductSerialNumber:1;
        uint8_t ProductModelNumber:1;
        uint8_t AntennaSerialNumber:1;
        uint8_t AntennaModelNumber:1;
        uint8_t OperatorInstallDate:1;
        uint8_t OperatorInstallId:1;
        uint8_t OperatorBasestationId:1;
        uint8_t OperatorSectorId:1;
        uint8_t OperatorAntennaBearing:1;
        uint8_t OperatorMechanicalTilt:1;
        uint8_t AntennaFrequencyBand:1;
        uint8_t AntennaBeamwidth:1;
        uint8_t AntennaGain:1;
        uint8_t AntennaMaxTilt:1;
        uint8_t AntennaMinTilt:1;
        uint8_t TurnRatio:1;
        uint8_t Backlash:1;
        uint8_t Unwind:1;
        uint8_t Tilt:1;
        uint8_t NotCalibrated:1;
        uint8_t LastDirection:1;
        uint8_t TraceLevel:1;
        uint8_t Spare1:1;
        uint8_t Spare2:8;
    } Bits;
} parameters_flags_t;

typedef struct
{
    uint8_t ProductSerialNumber;
    uint8_t ProductModelNumber;
    uint8_t AntennaSerialNumber;
    uint8_t AntennaModelNumber;
    uint8_t OperatorInstallDate;
    uint8_t OperatorInstallId;
    uint8_t OperatorBasestationId;
    uint8_t OperatorSectorId;
    uint8_t OperatorAntennaBearing;
    uint8_t OperatorMechanicalTilt;
    uint8_t AntennaFrequencyBand;
    uint8_t AntennaBeamwidth;
    uint8_t AntennaGain;
    uint8_t AntennaMaxTilt;
    uint8_t AntennaMinTilt;
    uint8_t TurnRatio;
    uint8_t Backlash;
    uint8_t Unwind;
    uint8_t Tilt;
    uint8_t NotCalibrated;
    uint8_t LastDirection;
    uint8_t TraceLevel;
    uint8_t Spare[10];
} parameters_checksum_t;

typedef struct
{
    char ProductSerialNumber[PARAMETER_SERIAL_NUMBER_LENGTH];
    char ProductModelNumber[PARAMETER_MODEL_NUMBER_LENGTH];
    char AntennaSerialNumber[PARAMETER_SERIAL_NUMBER_LENGTH];
    char AntennaModelNumber[PARAMETER_MODEL_NUMBER_LENGTH];
    char OperatorInstallDate[PARAMETER_OPERATOR_INSTALL_DATE_LENGTH];
    char OperatorInstallId[PARAMETER_OPERATOR_INSTALL_ID_LENGTH];
    char OperatorBasestationId[PARAMETER_OPERATOR_BASESTATION_ID_LENGTH];
    char OperatorSectorId[PARAMETER_OPERATOR_SECTOR_ID_LENGTH];
    uint16_t OperatorAntennaBearing;
    int16_t OperatorMechanicalTilt;
    uint16_t AntennaFrequencyBand[PARAMETER_N_FREQUENCY_BANDS];
	uint16_t AntennaBeamwidth[PARAMETER_N_BANDS];
    uint8_t AntennaGain[PARAMETER_N_BANDS];
	int16_t AntennaMaxTilt;
    int16_t AntennaMinTilt;
    int32_t TurnRatio;
    int16_t Backlash;
    int16_t Unwind;
    int16_t Tilt;
    uint8_t NotCalibrated;
    uint8_t LastDirection;
    uint8_t TraceLevel;
    uint8_t Spare[45];
    parameters_checksum_t Checksum;
} parameters_t;

typedef struct
{
	uint8_t Type[5];
	uint8_t Version;
    uint16_t Length;
    char ProductModelNumber[PARAMETER_MODEL_NUMBER_LENGTH];
	char AntennaModelNumber[PARAMETER_MODEL_NUMBER_LENGTH];
	uint16_t AntennaFrequencyBand[PARAMETER_N_FREQUENCY_BANDS];
	uint16_t AntennaBeamwidth[PARAMETER_N_BANDS];
	uint8_t AntennaGain[PARAMETER_N_BANDS];
	int16_t AntennaMaxTilt;
	int16_t AntennaMinTilt;
	int32_t TurnRatio;
	int16_t Backlash;
	int16_t Unwind;
	uint16_t Checksum;
} config_data_file_t; // (70 bytes) 

extern parameters_t Pars;

extern parameters_flags_t ParsUpdate;

extern parameters_flags_t ParsValid;

return_t Parameters_Init (void);

return_t Parameters_Print (void);

return_t Parameters_Update (uint8_t *pData, uint8_t Length, uint8_t *pChecksum);

return_t Parameters_Validate (uint8_t *pData, uint8_t Length, uint8_t *pChecksum);

return_t Parameters_Send_Config_Data (uint8_t *pData, uint8_t length);

return_t Parameters_Process_Config_Data (void);

return_t Parameters_Config_Data_Completed (return_t result);

#endif // _PARAMETERS_H_