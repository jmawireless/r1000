//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _SPI_H__
#define _SPI_H__

// I/O definitions
#define SPI_MASTER 0x20             // select 8-bit master mode, CKE=1, CKP=0
#define SPI_ENABLE 0x40             // enable SPI port, clear status

void SPI_Init (void);

//void SPI_Write (uint8_t value);

//uint8_t SPI_Read (void);

uint8_t SPI_ReadWrite (uint8_t value);

#define SPI_Write(X) SPI_ReadWrite(X)

#define SPI_Read()   SPI_ReadWrite(0)

#endif // _SPI_H__
