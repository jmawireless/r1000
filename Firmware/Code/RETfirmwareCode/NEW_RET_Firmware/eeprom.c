//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

// peripheral configurations
#define EEPROM_CS      LATA3        // select line for Serial EEPROM
#define EEPROM_CS_TRIS TRISA3       // tris control for EEPROM_CS pin

// 25LC256 Serial EEPROM commands
#define SEE_WRSR    1               // write status register
#define SEE_WRITE   2               // write command
#define SEE_READ    3               // read command
#define SEE_WDI     4               // write disable
#define SEE_RDSR    5               // read status register
#define SEE_WEN     6               // write enable

/*
 * Function:    Eeprom_Init
 * Input:       void
 * Return:      void
 * Description: Initialise the Serial EEPROM
 */
void Eeprom_Init (void) 
{
    TRACE_INI(("START: EEPROM init\r\n"));
    // configure EEPROM_CS direction
    EEPROM_CS_TRIS = 0;  
    // default EEPROM_CS state
    EEPROM_CS = 1;                  
    #if 0
    Eeprom_Clear(0, sizeof(parameters_t));
    Eeprom_Dump(0, sizeof(parameters_t));
    #endif
}


/*
 * Function:    Eeprom_Writing 
 * Input:       void
 * Return:      uint8_t
 * Description: Check the Serial EEPROM status register
 */
uint8_t Eeprom_Writing (void)
{
    uint8_t data;
    // select the Serial EEPROM                     
    EEPROM_CS = 0;     
    // send Read Status command
    SPI_Write(SEE_RDSR); 
    // send dummy, read status
    data = SPI_Write(0);  
    // deselect Serial EEPROM
    EEPROM_CS = 1;   
    // return status
    return (data & 1);              
}

/*
 * Function:    Eeprom_WriteEnable 
 * Input:       void
 * Return:      void
 * Description: Send a Write Enable command
 */
void Eeprom_WriteEnable (void)
{
    // select the Serial EEPROM
    EEPROM_CS = 0;    
    // send Write Enabled command
    SPI_Write(SEE_WEN); 
    // deselect Serial EEPROM
    EEPROM_CS = 1;                  
}

/*
 * Function:    Eeprom_Read
 * Input:       address
 * Return:      uint8_t
 * Description: Read a 8-bit value at the specified address
 */
uint8_t Eeprom_Read (uint16_t address)
{
    uint8_t data;

    // wait until any work in progress is completed
    // check Work In Progress
    while (Eeprom_Writing());       

    // Send READ command and addr, then read data
    // select the Serial EEPROM
    EEPROM_CS = 0;   
    // send Read command
    SPI_Write(SEE_READ); 
    // address MSB first
    SPI_Write(address >> 8); 
    // address LSB (word aligned)
    SPI_Write(address);   
    // Read single byte over SPI
    data = SPI_Read();
    // deselect the EEPROM
    EEPROM_CS = 1;
    // return read data value
    return (data);
}

/*
 * Function:    Eeprom_Write
 * Input:       uint16_t, uint8_t
 * Return:      void
 * Description: Write a 8-bit value at the specified address
 */
void Eeprom_Write (uint16_t address, uint8_t data)
{
    // wait until work in progress completed
    while (Eeprom_Writing());       
    // set the write enable latch
    Eeprom_WriteEnable();           
    // select the Serial EEPROM
    EEPROM_CS = 0;     
    // write command
    SPI_Write(SEE_WRITE);  
    // address MSB
    SPI_Write(address >> 8);
    // address LSB
    SPI_Write(address);  
    // data
    SPI_Write(data);   
    // deselect the Serial EEPROM
    EEPROM_CS = 1;                  
}

/*
 * Function:    Eeprom_Clear   
 * Input:       uint16_t, uint16_t
 * Return:      return_t
 * Description: Erase/ Clear Eeprom
 */
return_t Eeprom_Clear (uint16_t start_address, uint16_t length)
{
    uint16_t i;
	volatile uint16_t eeprom_address = start_address;
	volatile uint8_t data = 0;
    // Clear a 'lenght' (total number) or memory starting form address specified
    for (i=0; i<length; i++)
    {
        // write 0xFF to data variable
        data = 0xFF;
        // write data variable value to eeprom address
        Eeprom_Write(eeprom_address, data);
        // clear watch dogtimer
        CLRWDT();
        // increment address to point to next location
        eeprom_address++;
    }

    return (RESULT_SUCCESS);
}

/*
 * Function:    Eeprom_Dump
 * Input:       uint16_t, uint16_t
 * Return:      void
 * Description: Read/Dump Eeprom content for debugging
 */
void Eeprom_Dump (uint16_t address, uint16_t length)
{
	uint8_t data;
   	uint16_t i = 0;
    // check if valid length to dump
   	if (length < 1) 
		return;
    // start reading data from address specified and dump it to debug UART
	TRACE_PUT(DBG, ("  "));
    do 
	{
		data = Eeprom_Read(address);
		address++;
		TRACE_PUT(DBG, ("%02X ", data));
        // format read memory data in 16 bytes per line format 
        // check if 16 bytes read, add return and new line character 
       	i++;
       	if (i == 16)
		{
        	i = 0;
            TRACE_PUT(DBG, ("\r\n  "));
        }
	} while(--length);

    // Print a linefeed when we are done, if we didn't just print one in the loop.
    if (i != 0) 
		TRACE_PUT(DBG, ("\r\n"));
	else
		TRACE_PUT(DBG, ("\r"));
}
