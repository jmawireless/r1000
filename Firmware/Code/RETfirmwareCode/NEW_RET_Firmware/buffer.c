//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

/*
 * Function:    Buffer_Init
 * Input:       *buffer, *data, size
 * Return:      void
 * Description: Initalize the Buffer
 */
void Buffer_Init (buffer_t *buffer, uint8_t *data, uint16_t size)
{
    buffer->data = data;
    buffer->head = 0;
    buffer->tail = 0;
    buffer->size = size;
}

/*
 * Function:    Buffer_write
 * Input:       buffer_t, uint8_t
 * Return:      bool
 * Description: Write data to buffer
 */
bool Buffer_Write (buffer_t *buffer, uint8_t data)
{
    // check if there is place to write data
    if (Buffer_Get_Count(buffer) != buffer->size)
    {
        // copy data to buffer
        buffer->data[buffer->tail++ % buffer->size] = data;
        // return true
        return (true);
    }
    // return false if data not store
    return (false);
}

/*
 * Function:    Buffer_WriteData
 * Input:       buffer_t, uint8_t, uint8_t
 * Return:      bool
 * Description: Write data to buffer's circular memory
 */
bool Buffer_WriteData (buffer_t *buffer, uint8_t *data, uint8_t length)
{
    uint8_t count = Buffer_Get_Count(buffer);
    size_t size_1;
    size_t size_2;
    // check if valid length to write and is there is enough space in buffer 
    if ((length == 0) || (count + length > buffer->size))
    {
          return (false);
    }
  
    // Write in a single step 
    // Write to circular buffer top down without roll over
    if (length <= buffer->size - buffer->tail)
    {
        memcpy(buffer->data + buffer->tail, data, length);
        buffer->tail += length;
        if (buffer->tail == buffer->size)
        {
            buffer->tail = 0;
        }
    }
    // Write in two steps
    // Write to circular buffer top down with roll over
    else
    {
        size_1 = buffer->size - buffer->tail;
        memcpy(buffer->data + buffer->tail, data, size_1);
        size_2 = length - size_1;
        memcpy(buffer->data, data + size_1, size_2);
        buffer->tail = size_2;
    }

    return (true);
}

/*
 * Function:    Buffer_Read
 * Input:       buffer_t, uint8_t
 * Return:      bool
 * Description: Read single byte data from buffer
 */
bool Buffer_Read (buffer_t *buffer, uint8_t *data)
{
    if (Buffer_Get_Count(buffer) != 0)
    {
        *data = buffer->data[buffer->head++ % buffer->size];
        return (true);
    }

    return (false);
}

/*
 * Function:    Buffer_ReadData
 * Input:       buffer_t, uint8_t, uint8_t
 * Return:      bool
 * Description: Read 'n' byte data from buffer
 */
bool Buffer_ReadData (buffer_t *buffer, uint8_t *data, uint8_t length)
{
    uint8_t count = Buffer_Get_Count(buffer);

    if ((length == 0) || (count < length))
	{
		return (false);
	}

	// Read in a single step
    // Read buffer of circular memory top down without roll over    
	if (length <= buffer->size - buffer->head)
	{
		memcpy(data, buffer->data + buffer->head, length);
		buffer->head += length;
		if (buffer->head == buffer->size)
		{
			buffer->head = 0;
		}
	}
	// Read in two steps
    // Read buffer of circular memory top down with roll over
	else
	{
		size_t size_1 = buffer->size - buffer->head;
		memcpy(data, buffer->data + buffer->head, size_1);
		size_t size_2 = length - size_1;
		memcpy(data + size_1, buffer->data, size_2);
		buffer->head = size_2;
	}

	return (true);
}