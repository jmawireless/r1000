//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

/*
 * Function:    SPI_Init
 * Input:       void
 * Return:      void
 * Description: This function will initialize the SPI
 */
void SPI_Init (void)
{
    TRACE_INI(("START: SPI init\r\n"));    
    // Setup SPI bits to output (RC3 - SCK; RC5 - MOSI) and input (RC4 - MISO)
    TRISCbits.RC3 = 0;
    TRISCbits.RC5 = 0;
    TRISCbits.RC4 = 1; 
    // select 8-bit master mode, CKE=1, CKP=0
    SSP1CON1 = SPI_MASTER;   
    // enable SPI port, clear status
    SSP1STAT = SPI_ENABLE;          
    // Reset SPI interrput flag
    PIR1bits.SSPIF = 0;
//[A.S.]    SSP1IF = 1;   // test interrupts later
}
        
#if 0
//############################################################################//
// This function will send a byte over the SPI
//############################################################################//
void SPI_Put(uint8_t value)
{
#if SPI_TIMEOUT_0
//initialize timers
T0CON = 0x87;
TMR0H = 0xD9;
TMR0L = 0xDA;
INTCONbits.TMR0IE = 0;
#endif

#if SPI_TIMEOUT_2
// Turn on timer2, 1:1 prescaler, 1:10 post scalar
T2CON = 0x44; 
// Timer2 compare register
PR2 = 250; 		
// Enable timer2	
T2CONbits.TMR2ON = 1;						
#endif

    // Reset the Global interrupt pin
    SSP1IF = 0;
    do
    {
		// Reset write collision bit
        WCOL1 = 0;
		// Load the buffer
        SSP1BUF = value;
		// Perform write again if write collision occurs
    } while(WCOL1);
	
	// Wait until interrupt is received from the MSSP module
	while (SSP1IF == 0)
	{
#if SPI_TIMEOUT_0
        if (INTCONbits.TMR0IF)
        {
            INTCONbits.TMR0IF = 0;
            TMR0H = 0xD9;
            TMR0L = 0xDA;
            T0CON = 0x87;
        }
#endif
#if SPI_TIMEOUT_2
		if (PIR1bits.TMR2IF)
		{
			// TMR2 interrupt
	 		PIR1bits.TMR2IF = 0; 				// Clear interrupt flag
			break;
	 	}
#endif
	}

	// Reset the interrupt
	SSP1IF = 0;
}

//############################################################################//
// This function will read a byte over the SPI
//############################################################################//
uint8_t SPI_Get(void)
{
    // Write a dummy value to read the data from the other SPI module
    SPI_Put(0x00);
	// Read the SSP1BUF value for data transmitted by other SSP module
    return SSP1BUF;
}

uint8_t SPI_ReadWrite (uint8_t value)
{
    SPI_Put(value);
	// Read the SSPBUF value for data transmitted by other SSP module
    return SSP1BUF;
}
#endif

/*
 * Function:    SPI_ReadWrite
 * Input:       value
 * Return:      SSPBUF
 * Description: This function is used to read current value written over SPI
 */
uint8_t SPI_ReadWrite (uint8_t value)
{
    // Load the value to SPI buffer
    SSPBUF = value;
    // wait until the SPI interrupt flag cleared
    // todo: endless loop could potentially lock up
    while(!PIR1bits.SSPIF);
    // Clear SPI interrupt flag
    // todo: At this point the flag is already cleared else will be stuck 
    //      in above statement
    PIR1bits.SSPIF = 0;
    // return most recent written value
    // todo: split read write based on address
    return SSPBUF;
}
