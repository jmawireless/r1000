:: ######################################################
:: Parameters
:: Leave PRODUCT="" or HWVERSION="" or SWVERSION=""
:: for tag not needed
:: ######################################################
ECHO OFF

SET PRODUCT="CSS RET-200" 
SET HWVERSION="HW" 
SET SWVERSION="" 
SET SOURCE_DIR=%CD%
SET RELEASE_DIR=%CD%\releases\
SET BUILD_DIR=%CD%\build\
SET DEBUG_DIR=%CD%\debug\
SET DIST_DIR=%CD%\dist\
SET CONFIG_DIR=%CD%\nbproject
SET VERSION_FILE=%SOURCE_DIR%\typedef.h
SETLOCAL
FOR /f %%i IN ("%SOURCE_DIR%") DO (	SET PROJECT_NAME=%%~ni )
ECHO BUILDING PROJECT %PROJECT_NAME%

:: Remove old build folders
IF EXIST %BUILD_DIR% RD /s /q %BUILD_DIR%
IF EXIST %DEBUG_DIR% RD /s /q %DEBUG_DIR%
IF EXIST %DIST_DIR% RD /s /q %DIST_DIR%

CALL prjMakefilesGenerator.bat .

ECHO "DONE MAKEFILE GENERATION"

:: Create the makefile for the default configuration
make -f nbproject/Makefile-default.mk SUBPROJECTS= .build-conf

:: Get the file containing the version information

SETLOCAL enabledelayedexpansion
FOR /f "tokens=2* delims= " %%a IN ('type "%VERSION_FILE%" ^| FIND /i "#define SW_VERSION "') DO (
																   SET VERSION_STR=%%b)

:: Remove quotes from version string
SET VERSION_STR=%VERSION_STR:"=%

:: Remove ';' from version string
SET VERSION_STR=%VERSION_STR:;=%
SET HEX_FILENAME=%VERSION_STR%

:: Find and copy the build file .hex in release folder
FOR /R %DIST_DIR%\default\production %%f IN (*.hex) DO SET BUILD_FILE=%%f
ECHO BUILD_FILE=%BUILD_FILE%
COPY /B %BUILD_FILE% %RELEASE_DIR%%HEX_FILENAME%.hex

:: ######################################################
:: Convert hex to binary
:: ######################################################
%RELEASE_DIR%hex_to_css.exe -x 0xFFFF -m 64 -b -i %RELEASE_DIR%%HEX_FILENAME%.hex -o %RELEASE_DIR%%HEX_FILENAME%.jma

:: ######################################################
:: .css file generation
:: ######################################################

:: ######################################################
:: Compute checksum
:: Override file with valid checksum if it was invalid
:: Checksum is the 16-bit sum of the file without last 2
:: bytes 2-complemented with the magic word 0x5A5A
:: Last 2 bytes in the file are updated with the checksum
:: Therefore is Checksum is computed on the entire file 
:: it should return the magic word0x5A5A
:: ######################################################

:: Clean unnecessary files
IF EXIST %BUILD_DIR% RD /s /q %BUILD_DIR%
IF EXIST %DEBUG_DIR% RD /s /q %DEBUG_DIR%
IF EXIST %DIST_DIR% RD /s /q %DIST_DIR%
IF EXIST %SOURCE_DIR%\funclist DEL %SOURCE_DIR%\funclist
IF EXIST %SOURCE_DIR%\l.obj DEL %SOURCE_DIR%\l.obj
IF EXIST %CONFIG_DIR%\Makefile-default.mk DEL %CONFIG_DIR%\Makefile-default.mk
IF EXIST %CONFIG_DIR%\Makefile-impl.mk DEL %CONFIG_DIR%\Makefile-impl.mk
IF EXIST %CONFIG_DIR%\Makefile-local-default.mk DEL %CONFIG_DIR%\Makefile-local-default.mk
IF EXIST %CONFIG_DIR%\Makefile-genesis.properties DEL %CONFIG_DIR%\Makefile-genesis.properties
IF EXIST %CONFIG_DIR%\Makefile-variables.mk DEL %CONFIG_DIR%\Makefile-variables.mk
IF EXIST %CONFIG_DIR%\Package-default.bash DEL %CONFIG_DIR%\Package-default.bash

ECHO "BUILD COMPLETED!"

PAUSE