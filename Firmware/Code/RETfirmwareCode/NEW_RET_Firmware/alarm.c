//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

alarm_db_t alarm;

/*
 * Function:    Alarm_Init
 * Input:       void
 * Return:      return_t
 * Description: clear all flag related to alarm
 */
return_t Alarm_Init (void)
{
    alarm.subscribed = false;
    alarm.current.value = 0x00;
    alarm.previous.value = 0x00;
    alarm.cleared.value = 0x00;
    alarm.raised.value = 0x00;

    return (RESULT_SUCCESS);
}

/*
 * Function:    Alarm_Waiting_To_Be_Reported
 * Input:       void
 * Return:      bool
 * Description: function report any pending, cleared alarms
 */
bool Alarm_Waiting_To_Be_Reported (void)
{
    uint8_t ret;
    // check if alarm subscribed
    if (alarm.subscribed == false)
    {
        // return false flag as alarms not subscribed
        return (false);
    }
    // check if current alarm and previous alarms flags are similar
    if (alarm.current.value == alarm.previous.value)
    {
        // return false of current and previous flags are similar
        return (false);
    }

    // ExOR current and previous value flag 
    ret = (alarm.current.value ^ alarm.previous.value);
    // update alarm clear value flag
    alarm.cleared.value = (alarm.previous.value & ret);
    // update alarm raised value flag
    alarm.raised.value = (alarm.current.value & ret);
    // send debug message out to debug uart
    TRACE_DBG(("ALARM: 0x%.1x 0x%.1x 0x%.1x\r\n",
        alarm.cleared.value, alarm.raised.value));
    // return function success value
    return (true);
}

/*
 * Function:    Alarm_Subscribe
 * Input:       void
 * Return:      return_t
 * Description: Update alarm subscribe flag to true and return the success 
 *              each time alarm subscribe is executed
 */
return_t Alarm_Subscribe (void)
{
    alarm.subscribed = true;

    return (RESULT_SUCCESS);
}

/*
 * Function:    Alarm_Notify
 * Input:       void
 * Return:      return_t
 * Description: Update previous alarm value with current and return the success 
 *              each time alarm notified executed
 */
return_t Alarm_Notify (void)
{
    alarm.previous.value = alarm.current.value;

    return (RESULT_SUCCESS);
}

/*
 * Function:    Alarm_Clear
 * Input:       void
 * Return:      return_t
 * Description: return the success each time alarm clear executed
 */
return_t Alarm_Clear (void)
{
    // todo: missing logic
    return (RESULT_SUCCESS);
}

/*
 * Function:    Alarm_Get
 * Input:       alarm_t
 * Return:      return_t
 * Description: return the constant variable value from alarm map to get an alarm
 */
return_t Alarm_Get (void)
{
    return (Alarm_Map(alarm.current));
}

/*
 * Function:    Alarm_Get_Raised
 * Input:       void
 * Return:      return_t
 * Description: return the constant variable value from alarm map to raise an alarm
 */
return_t Alarm_Get_Raised (void)
{
    return (Alarm_Map(alarm.raised));
}

/*
 * Function:    Alarm_Get_Cleared
 * Input:       void
 * Return:      return_t
 * Description: return the constant variable value from alarm map to clear alarm
 */
return_t Alarm_Get_Cleared (void)
{
    return (Alarm_Map(alarm.cleared));
}

/*
 * Function:    Alarm_Map
 * Input:       alarm_t
 * Return:      return_t
 * Description: Map the alarm flag to constant error value
 */
return_t Alarm_Map (alarm_t alm)
{
    if (alm.bits.Hardware_Error)
    {
        return (RESULT_OTHER_HW_ERROR);
    }
    else if (alm.bits.Not_Configured)
    {
        return (RESULT_NOT_CONFIGURED);
    }
    if (alm.bits.Actuator_Detection_Fail)
    {
        return (RESULT_ACTUATOR_DETECTION_FAIL);
    }
    else if (alm.bits.Actuator_Jam_Permanent)
    {
        return (RESULT_ACTUATOR_JAM_PERMANENT);
    }
    else if (alm.bits.Actuator_Jam_Temporary)
    {
        return (RESULT_ACTUATOR_JAM_TEMPORARY);
    }
    else if (alm.bits.Position_Lost)
    {
        return (RESULT_POSITION_LOST);
    }
    else if (alm.bits.Actuator_Interference)
    {
        return (RESULT_ACTUATOR_INTERFERENCE);
    }
    else if (alm.bits.Not_Calibrated)
    {
        return (RESULT_NOT_CALIBRATED);
    }

    return (RESULT_SUCCESS);
}
