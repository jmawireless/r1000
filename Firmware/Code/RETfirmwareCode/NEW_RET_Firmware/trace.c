//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define __CLOCK__        Clock_Get_Time()
#define __HOURS__        Clock_Get_Hours()
#define __MINUTES__      Clock_Get_Minutes()
#define __SECONDS__      Clock_Get_Seconds()
#define __MILLISECONDS__ Clock_Get_MilliSeconds()

/*
 * Function:    putch
 * Input:       char
 * Return:      void
 * Description: Send 1 byte to UART 1
 */
void putch (char ch)
{
    Uart1_Send_Byte(ch);
}

/*
 * Function:    TRACE_Header
 * Input:       void
 * Return:      void
 * Description: Stop WDT and print time stamp
 */
void TRACE_Header (void)
{
    // Clear watch dog timer
    CLRWDT();
    // print current time stamp
    printf("[%.2d:%.2d:%.2d.%.3d] ",
        __HOURS__, __MINUTES__, __SECONDS__, __MILLISECONDS__);
}