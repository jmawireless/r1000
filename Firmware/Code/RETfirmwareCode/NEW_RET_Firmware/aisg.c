//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

// Defines command, Flag and Function structure for all L7 supported cmd.
l7_handler_t aisg_handlers[] =
{
    { V20_RESET                 , 0x00, AISG_V20_Reset                 },
    { V20_GET_ALARMS            , 0x00, AISG_V20_Get_Alarms            },
    { V20_GET_INFO              , 0x00, AISG_V20_Get_Info              },
    { V20_CLEAR_ALARMS          , 0x00, AISG_V20_Clear_Alarms          },
    { V20_READ_USER_DATA        , 0x00, AISG_V20_Read_User_Data        },
    { V20_WRITE_USER_DATA       , 0x00, AISG_V20_Write_User_Data       },
    { V20_SUBSCRIBE_ALARMS      , 0x00, AISG_V20_Subscribe_Alarms      },
    { V20_SELF_TEST             , 0x00, AISG_V20_Self_Test             },
    { V20_VENDOR_SPECIFIC       , 0x00, AISG_V20_Vendor_Specific       },
    { V20_DOWNLOAD_START        , 0x00, AISG_V20_Download_Start        },
    { V20_DOWNLOAD_APPL         , 0x00, AISG_V20_Download_Appl         },
    { V20_DOWNLOAD_END          , 0x00, AISG_V20_Download_End          },
    { V20_SRET_CALIBRATE        , 0x00, AISG_V20_SRet_Calibrate        },
    { V20_SRET_SEND_CONFIG      , 0x00, AISG_V20_SRet_Send_Config      },
    { V20_SRET_SET_TILT         , 0x00, AISG_V20_SRet_Set_Tilt         },
    { V20_SRET_GET_TILT         , 0x00, AISG_V20_SRet_Get_Tilt         },
    { V20_SRET_ALARM_INDICATION , 0x00, AISG_V20_SRet_Alarm_Indication },
    { V20_SRET_SET_DEVICE_DATA  , 0x00, AISG_V20_SRet_Set_Device_Data  },
    { V20_SRET_GET_DEVICE_DATA  , 0x00, AISG_V20_SRet_Get_Device_Data  },
    { COMMAND_TABLE_END         , 0x00, NULL                           }
};

// Macro to calculate the length of an array
#define ARRAY_LENGTH(array)             (sizeof(array)/sizeof((array)[0]))

// Create l7_tcp_t object last_tcp
l7_tcp_t last_tcp;
//Initialize 3ms timer and default timer count value
bool aisg_3ms_timer_enabled = false;
uint8_t aisg_3ms_timer_count = 0;

#define TRACE_AISG TRACE_DBG

/*
 * Function:    Aisg_Init
 * Input:       void
 * Return:      return_t
 * Description: Initialize L7 variable to default state.
 *              This function will initialize the Layer-7 object variable to 
 *              state indicating no pending command and no commands pending to 
 *              process and protocol current is not busy.
 */
return_t Aisg_Init (void)
{
    TRACE_INI(("START: AISG init\r\n"));

    last_tcp.command = COMMAND_TABLE_END;
    last_tcp.completed = false;
    last_tcp.in_progress = false;
    last_tcp.return_code = RESULT_SUCCESS;

    return (RESULT_SUCCESS);
}

/*
 * Function:    Task_Aisg
 * Input:       uint8 (TASK_AISG_RECEIVE/ TASK_AISG_TRANSMIT)
 * Return:      uint8_t
 * Description: This function will execute if data being received or it time to 
 *              send response to received commands. 
 *              
 */
uint8_t Task_Aisg (uint8_t state)
{
    bool res = false;
    uint8_t data;
    
    switch (state)
    {
        case TASK_AISG_RECEIVE :
            // Process received data
            // Set RG0 pin to low (Receive)
            Aisg_Set_Rx_Mode();                 
            // while date data received
            // todo: Can wait for ever if data error; break out logic needed
            while (Uart2_Read_Byte(&data) == true)
            {
                // Feed received data to HDLC layer
                res = Hdlc_On_Rx_Byte(data);
                // if Complete frame received break while loop
                // todo: break look if stuck in Rx mode. 
                if (res == true)
                    break;
            }
            // Set state to AISG Transmit id valid frame received and frame 
            // length non zero and not reset request pending
            if ((res == true) && (hdlc_tx_buffer_length != 0) &&
                (download_reset_request == false))
            {
                // set current state to transmit
                state = TASK_AISG_TRANSMIT;
            }
        break;

        case TASK_AISG_TRANSMIT :
            // Check 3ms timer flag is not running
            if (aisg_3ms_timer_enabled == false)
            {
                // Set RG0 pin to high (Transmit)
                Aisg_Set_Tx_Mode();
                // Tansmit hdlc frame
                Hdlc_Tx_Frame(hdlc_tx_buffer, hdlc_tx_buffer_length);
                // todo: Check if transmit was complete/successful
                // Check if layer 2 request pending
                if (layer2_reset_request == true)
                {
                    download_reset_request = true;
                }
                // set current state to receive
                state = TASK_AISG_RECEIVE;
            }
        break;

        default:
            // todo : flag error 
        break;
    }

    return state;
}

/*
 * Function:    Aisg_Get_Command_Handler
 * Input:       l7_command_t
 * Return:      function_t
 * Description: This function will return L7 command function pointer/handle 
 *              from predefined l7_commands structure 
 */

function_t Aisg_Get_Command_Handler (l7_commands_t command)
{
	uint8_t i;

	for (i=0; i<ARRAY_LENGTH(aisg_handlers); i++)
    {
        if (aisg_handlers[i].command == command)
        {
			return (aisg_handlers[i].function);
		}
    }
    // return end of table 
    // todo: flag as error in response msg or cmd not supported response
	return (NULL);
}

/*
 * Function:    AISG_V20_Build_Error_Response
 * Input:       info_frame_t (response pointer); return_t (AISG error type)
 * Return:      return_t
 * Description: This function populates the *rsp structure with error flag, 
 *              data and length
 */
return_t AISG_V20_Build_Error_Response (info_frame_t *rsp, return_t error)
{
	// define length (visiblity local)
    uint16_t length;
    // Assign length value to 2
	length = 2;
    // Convert length to 2 bytes and store
	rsp->header.lenLSB = U16_LSB(length);
	rsp->header.lenMSB = U16_MSB(length);
    // store the result Fail in data[0] field
    rsp->data[0] = RESULT_FAIL;
    // store the error msg in data[1] field
    rsp->data[1] = error;
    // Send message on debug UART
    TRACE_DBG(("PrepareError [0x%.2x]\r\n", error));
    // return value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_Reset
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 reset response cmd
 */
return_t AISG_V20_Reset(info_frame_t *cmd, info_frame_t *rsp)
{
    //todo: compare response message with 3gpp
	// store the address pointer of cmd and rsp frame
    aisg_v20_reset_cmd_t *icmd = (aisg_v20_reset_cmd_t *)cmd;
	aisg_v20_reset_rsp_t *irsp = (aisg_v20_reset_rsp_t *)rsp;
	uint16_t length;
    // store V2.0 Reset commands to rsp header command field
	irsp->header.command = V20_RESET;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built  
        return (RESULT_SUCCESS);
        //todo: send out error message if error cmd received
    }
    // update frame return code value to success
    irsp->return_code = RESULT_SUCCESS;
    // send debug message out to debug uart
    TRACE_DBG(("Reset [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_Get_Alarms
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 Get Alarms response cmd
 */
return_t AISG_V20_Get_Alarms(info_frame_t *cmd, info_frame_t *rsp)
{
    //todo: compare response message with 3gpp
    // store the address pointer of cmd and rsp frame
	aisg_v20_get_alarms_cmd_t *icmd = (aisg_v20_get_alarms_cmd_t *)cmd;
	aisg_v20_get_alarms_rsp_t *irsp = (aisg_v20_get_alarms_rsp_t *)rsp;
	uint16_t length;
    return_t alarm;
    // store V2.0 Get Alarms commands to rsp header command field
	irsp->header.command = V20_GET_ALARMS;
    // Calculate header length
    length = 1;
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built  
        return (RESULT_SUCCESS);
        //todo: send out error message if error cmd received
    }
    // execute Alarm_get function and store the result to register
    alarm = Alarm_Get();
    // check if Alarm_get function had valid msg to send
    if (alarm != RESULT_SUCCESS)
    {
        // Calculate info length
    	length = sizeof (*irsp) - sizeof (header_t);
        // store the Alarm_Get function response to frame info
        irsp->alarm = alarm;
    }
    // store info msg LSB and MSB lengths to rsp frame
    irsp->header.lenLSB = U16_LSB(length);
    irsp->header.lenMSB = U16_MSB(length);
    // update frame return code value to success
    irsp->return_code = RESULT_SUCCESS;
    // send debug message out to debug uart
    TRACE_AISG(("Get_Alarms [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_Get_Info
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 
 */
return_t AISG_V20_Get_Info (info_frame_t *cmd, info_frame_t *rsp)
{
    //todo: compare response message with 3gpp
    // store the address pointer of cmd and rsp frame
	aisg_v20_get_info_cmd_t *icmd = (aisg_v20_get_info_cmd_t *)cmd;
	aisg_v20_get_info_rsp_t *irsp = (aisg_v20_get_info_rsp_t *)rsp;
	uint16_t length;
    // store V2.0 Get info commands to rsp header command field
	irsp->header.command = V20_GET_INFO;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built  
        return (RESULT_SUCCESS);
        //todo: send out error message if error cmd received
    }
    // store product model number
    irsp->product_number_length = Parameter_Length(ProductModelNumber);
    memcpy(irsp->product_number, Pars.ProductModelNumber, irsp->product_number_length);
    // store serial number
    irsp->serial_number_length = Parameter_Length(ProductSerialNumber);
    memcpy(irsp->serial_number, Pars.ProductSerialNumber, irsp->serial_number_length);
    // store hardware version number
    irsp->hardware_version_length = HW_VERSION_LENGTH;
    memcpy(irsp->hardware_version, hw_version, irsp->hardware_version_length);
    // store software version number
    irsp->software_version_length = SW_VERSION_LENGTH;
    memcpy(irsp->software_version, sw_version, irsp->software_version_length);
    // update frame return code value to success
    irsp->return_code = RESULT_SUCCESS;
    // send debug message out to debug uart
    TRACE_AISG(("Get_Info [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_Clear_Alarms
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 clear Alarm
 */
return_t AISG_V20_Clear_Alarms (info_frame_t *cmd, info_frame_t *rsp)
{
    //todo: compare response message with 3gpp
    // store the address pointer of cmd and rsp frame
	aisg_v20_clear_alarms_cmd_t *icmd = (aisg_v20_clear_alarms_cmd_t *)cmd;
	aisg_v20_clear_alarms_rsp_t *irsp = (aisg_v20_clear_alarms_rsp_t *)rsp;
	uint16_t length;
    // store V2.0 Clear Alarms commands to rsp header command field
	irsp->header.command = V20_CLEAR_ALARMS;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built  
        return (RESULT_SUCCESS);
        //todo: send out error message if error cmd received
    }
    // update frame return code value to success
    irsp->return_code = RESULT_SUCCESS;
    // Execute alarm clear
    //todo: check logic to clear alarm
    Alarm_Clear();
    // send debug message out to debug uart
    TRACE_AISG(("Clear_Alarms [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_Read_User_Data
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 read user data - unsupported cmd
 */
return_t AISG_V20_Read_User_Data (info_frame_t *cmd, info_frame_t *rsp)
{
    // send debug message out to debug uart
    TRACE_AISG(("Read_User_Data [0x%.2x]\r\n", RESULT_UNSUPPORTED_PROCEDURE));
    // return function success value
    return (RESULT_UNSUPPORTED_PROCEDURE);
}

/*
 * Function:    AISG_V20_Write_User_Data
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 write user data - unsupported cmd
 */
return_t AISG_V20_Write_User_Data (info_frame_t *cmd, info_frame_t *rsp)
{
    TRACE_AISG(("Write_User_Data [0x%.2x]\r\n", RESULT_UNSUPPORTED_PROCEDURE));
    // return function success value
    return (RESULT_UNSUPPORTED_PROCEDURE);
}

/*
 * Function:    AISG_V20_Subscribe_Alarms
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement setup flag to subscribe for alarms
 */
return_t AISG_V20_Subscribe_Alarms (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
	aisg_v20_subscribe_alarms_cmd_t *icmd = (aisg_v20_subscribe_alarms_cmd_t *)cmd;
	aisg_v20_subscribe_alarms_rsp_t *irsp = (aisg_v20_subscribe_alarms_rsp_t *)rsp;
	uint16_t length;
    // store V2.0 subscribe commands to rsp header command field
	irsp->header.command = V20_SUBSCRIBE_ALARMS;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built 
        return (RESULT_SUCCESS);
        //todo: send out error message if error cmd received
    }
    // update frame return code value to success
    irsp->return_code = RESULT_SUCCESS;
    // Set Alarm subscribe flag 
    Alarm_Subscribe();
    // send debug message out to debug uart
    TRACE_AISG(("Subscribe_Alarms [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 self test response msg
 */
return_t AISG_V20_Self_Test (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
   	aisg_v20_self_test_cmd_t *icmd = (aisg_v20_self_test_cmd_t *)cmd;
   	aisg_v20_self_test_rsp_t *irsp = (aisg_v20_self_test_rsp_t *)rsp;
	uint16_t length;
    // store V2.0 self test commands to rsp header command field
	irsp->header.command = V20_SELF_TEST;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built 
        return (RESULT_SUCCESS);
        //todo: send out error message if error cmd received
    }
    // return rsp frame successfully built  
    irsp->return_code = RESULT_SUCCESS;
    // send debug message out to debug uart
    TRACE_AISG(("Self_Test [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 vendor specific response
 */
return_t AISG_V20_Vendor_Specific         (info_frame_t *cmd, info_frame_t *rsp)
{
    // update frame return code value to success

    // store the address pointer of cmd and rsp frame
   	aisg_v20_vendor_specific_cmd_t *icmd = (aisg_v20_vendor_specific_cmd_t *)cmd;
   	aisg_v20_vendor_specific_rsp_t *irsp = (aisg_v20_vendor_specific_rsp_t *)rsp;
	uint16_t length;
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // Erase Eeprom
        Eeprom_Clear(0, sizeof(parameters_t));
        // send debug message out to debug uart
        TRACE_AISG(("Clearing eeprom content and resetting\r\n"));
        // Issue Reset 
        RESET();
        // return rsp frame successfully built 
        return (RESULT_SUCCESS);
    }
    memcpy(Pars.ProductSerialNumber, icmd->serial_number, Parameter_Length(ProductSerialNumber));
    memcpy(Pars.ProductModelNumber, icmd->product_number, Parameter_Length(ProductModelNumber));
    Parameter_Write(ProductSerialNumber);
    Parameter_Write(ProductModelNumber);

	irsp->header.command = V20_VENDOR_SPECIFIC;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    irsp->return_code = RESULT_SUCCESS;
    // send debug message out to debug uart
    TRACE_AISG(("Vendor_Specific [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_Download_Start
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 download start
 */
return_t AISG_V20_Download_Start (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
   	aisg_v20_download_start_cmd_t *icmd = (aisg_v20_download_start_cmd_t *)cmd;
   	aisg_v20_download_start_rsp_t *irsp = (aisg_v20_download_start_rsp_t *)rsp;
	uint16_t length;
    // store V2.0 download start commands to rsp header command field
	irsp->header.command = V20_DOWNLOAD_START;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built  
        return (RESULT_SUCCESS);
    }
    // update frame return code value to success
    irsp->return_code = RESULT_SUCCESS;
    // Set the protocol state to download
    Task_Set_State(TASK_DWNL_ID, TASK_DOWNLOAD_START);
    // send debug message out to debug uart
    TRACE_AISG(("Download_Start [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 download application
 */
return_t AISG_V20_Download_Appl           (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
   	aisg_v20_download_appl_cmd_t *icmd = (aisg_v20_download_appl_cmd_t *)cmd;
   	aisg_v20_download_appl_rsp_t *irsp = (aisg_v20_download_appl_rsp_t *)rsp;
	uint8_t length;
    uint8_t *pdata;
    // store V2.0 download application to rsp header command field
	irsp->header.command = V20_DOWNLOAD_APPL;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);

    if (cmd != NULL)
    {
        length = icmd->header.lenLSB;
        pdata = icmd->data;
        Buffer_WriteData(&dwn_buffer, pdata, length);

        if (dwn_error == false)
        {
            // update frame return code value to success
            irsp->return_code = RESULT_SUCCESS;
            if (length >= FLASH_WRITE_BLOCK)
            {
                return (RESULT_PROCEDURE_NOT_COMPLETED);
            }
            else
            {
                last_tcp.in_progress = false;
                return (RESULT_SUCCESS);
            }
        }
        else
        {
            // update frame return code value to success
            irsp->return_code = RESULT_INVALID_FILE;
            last_tcp.in_progress = false;
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, irsp->return_code);
            //todo: send download error message
            // return function success value
            return (RESULT_SUCCESS);
        }
    }
    else
    {
        //todo: Add: irsp->return_code = RESULT_SUCCESS else random value will be picked from memory
        if (irsp->return_code != RESULT_SUCCESS)
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, irsp->return_code);
        }
        // send debug message out to debug uart
		TRACE_AISG(("Download_Appl [0x%.2x]\r\n", irsp->return_code));
        // return function success value
        return (RESULT_SUCCESS);
    }
}

/*
 * Function:    
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 download end
 * 
 */
return_t AISG_V20_Download_End            (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
   	aisg_v20_download_end_cmd_t *icmd = (aisg_v20_download_end_cmd_t *)cmd;
   	aisg_v20_download_end_rsp_t *irsp = (aisg_v20_download_end_rsp_t *)rsp;
	uint16_t length;
    // store V2.0 Download End commands to rsp header command field
	irsp->header.command = V20_DOWNLOAD_END;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built  
        return (RESULT_SUCCESS);
    }
    
    if (flash_checksum == MAGIC_CHECKSUM)
    {
        // update frame return code value to success
        irsp->return_code = RESULT_SUCCESS;
    }
    else
    {
        // Build frame with checksum error
        AISG_V20_Build_Error_Response(rsp, RESULT_CHECKSUM_ERROR);
        //todo: Add: irsp->return_code = RESULT_FAILED;
    }
    //Set up task to stop download
    Task_Set_State(TASK_DWNL_ID, TASK_DOWNLOAD_STOP);
	// send debug message out to debug uart
    TRACE_AISG(("Download_End [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_SRet_Calibrate
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 set RET to calibrate
 */
return_t AISG_V20_SRet_Calibrate          (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
   	aisg_v20_sret_calibrate_cmd_t *icmd = (aisg_v20_sret_calibrate_cmd_t *)cmd;
   	aisg_v20_sret_calibrate_rsp_t *irsp = (aisg_v20_sret_calibrate_rsp_t *)rsp;
	uint16_t length;
    return_t result;
    // store V2.0 RET calibrate commands to rsp header command field
	irsp->header.command = V20_SRET_CALIBRATE;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // check if commands received
    if (cmd != NULL)
    {
        // Check if the Rx cmd was inaccurate
        if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
            // return rsp frame successfully built  
            return (RESULT_SUCCESS);
        }
        // Implement motor calibrate
        result = Motor_Calibrate();
        // send debug message out to debug uart
        TRACE_AISG(("Calibrate [0x%.2x]\r\n", result));
        // check if motor calibration was successful
        if (result != RESULT_SUCCESS)
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, result);
            //todo: send debug message
            // return function success value
            return (RESULT_SUCCESS);
        }
        else
        {
            // update frame return code value to success
            irsp->return_code = RESULT_SUCCESS;
            //todo: send debug message
            // return function not completed status 
            return (RESULT_PROCEDURE_NOT_COMPLETED);
        }
    }
    else
    {
        // update frame return code value to success
        result = irsp->return_code;
        // send debug message out to debug uart
        TRACE_AISG(("Calibrate done [0x%.2x]\r\n", result));
        if (result != RESULT_SUCCESS)
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, result);
        }
        // return function success value
        return (RESULT_SUCCESS);
    }
}

/*
 * Function:    AISG_V20_SRet_Send_Config
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 send config file
 */
return_t AISG_V20_SRet_Send_Config (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
	aisg_v20_sret_send_config_cmd_t *icmd = (aisg_v20_sret_send_config_cmd_t *)cmd;
	aisg_v20_sret_send_config_rsp_t *irsp = (aisg_v20_sret_send_config_rsp_t *)rsp;
	uint16_t length;
    return_t result;
    // store V2.0 Send config commands to rsp header command field
	irsp->header.command = V20_SRET_SEND_CONFIG;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // check if command was received 
    if (cmd != NULL)
    {
        // Implement send config data
        result = Parameters_Send_Config_Data(icmd->data, icmd->header.lenLSB);
        // send debug message out to debug uart
        TRACE_AISG(("Send_Config_data [0x%.2x]\r\n", result));
        result = RESULT_SUCCESS;
        if (result != RESULT_SUCCESS)
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, result);
            return (RESULT_SUCCESS);
        }
        else
        {
            // update frame return code value to success
            irsp->return_code = RESULT_SUCCESS;
            // return function success value
            return (RESULT_PROCEDURE_NOT_COMPLETED);
        }
    }
    else
    {
        // update frame return code value to success
        result = irsp->return_code;
        // send debug message out to debug uart
        TRACE_AISG(("Send_Config_Data done [0x%.2x]\r\n", result));
        //check is result variable is success
        if (result != RESULT_SUCCESS)
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, result);
        }
        // return function success value
        return (RESULT_SUCCESS);
    }
}

/*
 * Function:    AISG_V20_SRet_Set_Tilt
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 set tilt angle
 */
return_t AISG_V20_SRet_Set_Tilt (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
	aisg_v20_sret_set_tilt_cmd_t *icmd = (aisg_v20_sret_set_tilt_cmd_t *)cmd;
	aisg_v20_sret_set_tilt_rsp_t *irsp = (aisg_v20_sret_set_tilt_rsp_t *)rsp;
	uint16_t length;
    return_t result;
    // store V2.0 set tilt commands to rsp header command field
    irsp->header.command = V20_SRET_SET_TILT;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);   
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // check if the info frame is not empty
    if (cmd != NULL)
    {
        // Check if the Rx cmd was inaccurate
        if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
            // return rsp frame successfully built 
            return (RESULT_SUCCESS);
        }
        // Implement Motor Set tilt
        result = Motor_Set_Tilt(icmd->tilt);
        // send debug message out to debug uart
        TRACE_AISG(("Set_Tilt [0x%.2x]\r\n", result));
        // check if Motor set tilt was success
        if (result != RESULT_SUCCESS)
        {
            // if motor set tilt not success
            if (result == RESULT_PROCEDURE_NOT_COMPLETED)
            {
                // Update the return code that function was executed 
                irsp->return_code = RESULT_SUCCESS;
                // Return function with motor tilt procedure not completed status
                return (RESULT_PROCEDURE_NOT_COMPLETED);
            }
            // if motor set tilt function was successful
            else 
            {
                // store format error message to rsp info frame
                AISG_V20_Build_Error_Response(rsp, result);
                // return function success value
                return (RESULT_SUCCESS);
            }
        }
        // if motor set tilt function failed
        else
        {
            // send debug message on UART indicating failure
            TRACE_AISG(("Set_Tilt no change [0x%.2x]\r\n", result));
            // update return flag to success
            irsp->return_code = RESULT_SUCCESS;
            // update function return value to success    
            return (RESULT_SUCCESS);
        }
    }
    else
    {
        // update result flag with unsuccessful error code
        result = irsp->return_code;
        // send debug message on UART
        TRACE_AISG(("Set_Tilt done [0x%.2x]\r\n", result));
        // check if result flag is not success
        // todo redundant flag check as it was true all time 
        if (result != RESULT_SUCCESS)
        {
            // store format error message to rsp info frame
            AISG_V20_Build_Error_Response(rsp, result);
        }
        // return success value
        return (RESULT_SUCCESS);
    }
}

/*
 * Function:    AISG_V20_SRet_Get_Tilt
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 get tilt angle
 */
return_t AISG_V20_SRet_Get_Tilt           (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
	aisg_v20_sret_get_tilt_cmd_t *icmd = (aisg_v20_sret_get_tilt_cmd_t *)cmd;
	aisg_v20_sret_get_tilt_rsp_t *irsp = (aisg_v20_sret_get_tilt_rsp_t *)rsp;
	uint16_t length;
    return_t result;
    // store V2.0 get tilt commands to rsp header command field
	irsp->header.command = V20_SRET_GET_TILT;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    
    // Check if the Rx cmd was inaccurate
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return rsp frame successfully built 
        return (RESULT_SUCCESS);
    }
    // Implement Motor Get tilt 
    result = Motor_Get_Tilt(&irsp->tilt);
    // Check if the function was successfully executed
    if (result != RESULT_SUCCESS)
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, result);
    }
    else
    {
        // update frame return code value to success
        irsp->return_code = RESULT_SUCCESS;
    }
    // send debug message out to debug uart
    TRACE_AISG(("Get_Tilt [0x%.2x]\r\n", irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 setting alarm indication 
 */
return_t AISG_V20_SRet_Alarm_Indication   (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of rsp frame
	aisg_v20_sret_alarm_indication_rsp_t *irsp = (aisg_v20_sret_alarm_indication_rsp_t *)rsp;
	uint16_t length;
    return_t alarm_cleared;
    return_t alarm_raised;
    uint8_t i = 0;
    // store V2.0 SRET alarm indication commands to rsp header command field
	irsp->header.command = V20_SRET_ALARM_INDICATION;
    // get cleared alarm flag
    alarm_cleared = Alarm_Get_Cleared();
    // get alarm raised flag
    alarm_raised = Alarm_Get_Raised();
    // check if alarm cleared flag was success
    if (alarm_cleared != RESULT_SUCCESS)
    {
        // construct and store the alarm in response msg
        irsp->alarms[i].return_code = alarm_cleared;
        irsp->alarms[i].status = 0;
        // check if alarm rasied flag was success
        if (alarm_raised != RESULT_SUCCESS)
            // increment the alarm msg count
            i++;
    }
    // check if alarm raised was not success 
    if (alarm_raised != RESULT_SUCCESS)
    {
        // construct and store the alarm in response msg
        irsp->alarms[i].return_code = alarm_raised;
        irsp->alarms[i].status = 1;
    }
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t) - ((1 - i) * 2);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // replace previous alarm flag by new alarm flag
    Alarm_Notify();
    // send debug message out to debug uart
    TRACE_AISG(("Alarm_Indication [0x%.2x]\r\n", irsp->alarms[0].return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 set device data
 */
return_t AISG_V20_SRet_Set_Device_Data    (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
	aisg_v20_sret_set_device_data_cmd_t *icmd = (aisg_v20_sret_set_device_data_cmd_t *)cmd;
	aisg_v20_sret_set_device_data_rsp_t *irsp = (aisg_v20_sret_set_device_data_rsp_t *)rsp;
	uint16_t length;
    return_t result = RESULT_SUCCESS;
    // Calculate header length
    length = icmd->header.lenLSB - 1;
    // check which command field to be set 
    switch (icmd->field)
    {
        // Set Antenna model number
        case DATAFIELD_ANTENNA_MODEL_NUMBER        :
            // copy the antenna model number from cmd data
            memcpy(Pars.AntennaModelNumber, icmd->data, Parameter_Length(AntennaModelNumber));
            // check if the antenna model length matches
            if (length != Parameter_Length(AntennaModelNumber))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update the antenna model number
                Parameter_Write(AntennaModelNumber);
            }
        break;
        // set antenna serial number
        case DATAFIELD_ANTENNA_SERIAL_NUMBER       :
            // copy the antenna serial number from cmd data
            memcpy(Pars.AntennaSerialNumber, icmd->data, Parameter_Length(AntennaSerialNumber));
            // check if the antenna serial length matches
            if (length != Parameter_Length(AntennaSerialNumber))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update the antenna serial number
                Parameter_Write(AntennaSerialNumber);
            }
        break;
        /* For Antenna freq 0,1,2, beamwidth, antenna gain, max/min tilt are 
         * read only they cant be modified 
         */
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_0    :
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_1    :
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_2    :
        case DATAFIELD_ANTENNA_BEAMWIDTH           :
        case DATAFIELD_ANTENNA_GAIN                :
        case DATAFIELD_ANTENNA_MAX_TILT            :
        case DATAFIELD_ANTENNA_MIN_TILT            :
            // all above field are read only and cant be modified
            result = RESULT_READ_ONLY;
        break;

        // set operator install date
        case DATAFIELD_OPERATOR_INSTALL_DATE       :
            // copy the operator install data from cmd data
            memcpy(Pars.OperatorInstallDate, icmd->data, Parameter_Length(OperatorInstallDate));
            // check if operator install date length matches
            // todo validate date too
            if (length != Parameter_Length(OperatorInstallDate))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update operation install date
                Parameter_Write(OperatorInstallDate);
            }
        break;

        // set operator install id
        case DATAFIELD_OPERATOR_INSTALL_ID         :
            // copy the operator install id number from cmd data
            memcpy(Pars.OperatorInstallId, icmd->data, Parameter_Length(OperatorInstallId));
            // check if operator install id length matches
            if (length != Parameter_Length(OperatorInstallId))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update operation install id
                Parameter_Write(OperatorInstallId);
            }
        break;

        // set operator basestation id
        case DATAFIELD_OPERATOR_BASESTATION_ID     :
            // copy the operator basestation id number from cmd data
            memcpy(Pars.OperatorBasestationId, icmd->data, Parameter_Length(OperatorBasestationId));
            // check if the operator base station length matches
            if (length != Parameter_Length(OperatorBasestationId))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update operator basestation id
                 Parameter_Write(OperatorBasestationId);
            }
        break;

        // set operator sector id
        case DATAFIELD_OPERATOR_SECTOR_ID          :
            // copy the operator sector id number from cmd data
            memcpy(Pars.OperatorSectorId, icmd->data, Parameter_Length(OperatorSectorId));
            // check if the operator sector id length matches
            if (length != Parameter_Length(OperatorSectorId))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update operator sector id
                Parameter_Write(OperatorSectorId);
            }
        break;

        // set operator antenna bearing
        case DATAFIELD_OPERATOR_ANTENNA_BEARING    :
            // copy the operator antenna bearing number from cmd data
            memcpy(&Pars.OperatorAntennaBearing, icmd->data, Parameter_Length(OperatorAntennaBearing));
            // check if the operator antenna bearing number matches
            if (length != Parameter_Length(OperatorAntennaBearing))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update operator antenna bearing number
                Parameter_Write(OperatorAntennaBearing);
            }
        break;

        // set operator mechanical tilt
        case DATAFIELD_OPERATOR_MECHANICAL_TILT    :
            // copy the operator mechnical tilt value from cmd data
            memcpy(&Pars.OperatorMechanicalTilt, icmd->data, Parameter_Length(OperatorMechanicalTilt));
            // check if the operator mechincal tilt length matches
            if (length != Parameter_Length(OperatorMechanicalTilt))
            {
                // flag format error
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                // update operator mechnical tilt value
                Parameter_Write(OperatorMechanicalTilt);
            }
            break;
        // all other parameter are unknown
        default                                    :
            result = RESULT_UNKNOWN_PARAMETER;
        break;
    }

	irsp->header.command = V20_SRET_SET_DEVICE_DATA;
    // Calculate header length
	length = sizeof (*irsp) - sizeof (header_t);
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // Check if device data flag is not successful
    if (result != RESULT_SUCCESS)
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, result);
    }
    else
    {
        // return rsp frame successfully built  
        irsp->return_code = RESULT_SUCCESS;
    }
    // send debug message out to debug uart
    TRACE_AISG(("Set_Device_Data %s [0x%.2x]\r\n", 
            Get_DatafieldName(icmd->field), irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    AISG_V20_SRet_Get_Device_Data
 * Input:       info_frame_t, info_frame_t
 * Return:      return_t
 * Description: Implement ASIG v2.0 get device data
 */
return_t AISG_V20_SRet_Get_Device_Data (info_frame_t *cmd, info_frame_t *rsp)
{
    // store the address pointer of cmd and rsp frame
	aisg_v20_sret_get_device_data_cmd_t *icmd = (aisg_v20_sret_get_device_data_cmd_t *)cmd;
	aisg_v20_sret_get_device_data_rsp_t *irsp = (aisg_v20_sret_get_device_data_rsp_t *)rsp;
	uint16_t length;
    return_t result = RESULT_SUCCESS;
    // Calculate the reponse length
    length = 1;
    // load the response command header with device data
	irsp->header.command = V20_SRET_GET_DEVICE_DATA;
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // check if cmd is valid
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        // return function success value
        return (RESULT_SUCCESS);
    }
    // check info command field requesting antenna information
    switch (icmd->field)
    {
        // read antenna model number and length
        case DATAFIELD_ANTENNA_MODEL_NUMBER        :
            length = Parameter_Length(AntennaModelNumber);
            memcpy(irsp->data, Pars.AntennaModelNumber, length);
        break;
        // read antenna serial number and length
        case DATAFIELD_ANTENNA_SERIAL_NUMBER       :
            length = Parameter_Length(AntennaSerialNumber);
            memcpy(irsp->data, Pars.AntennaSerialNumber, length);
        break;
        // read antenna frequency band 0 and length
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_0      :
            length = Parameter_Length(AntennaFrequencyBand[0]);
            memcpy(irsp->data, &Pars.AntennaFrequencyBand[0], length);
        break;
        // read antenna freq band 1 and length
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_1      :
            length = Parameter_Length(AntennaFrequencyBand[1]);
            memcpy(irsp->data, &Pars.AntennaFrequencyBand[1], length);
        break;
        // read antenna freq band 2 and length
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_2      :
            length = Parameter_Length(AntennaFrequencyBand[2]);
            memcpy(irsp->data, &Pars.AntennaFrequencyBand[2], length);
        break;
        // read antenna beamwidth and length
        case DATAFIELD_ANTENNA_BEAMWIDTH           :
            length = Parameter_Length(AntennaBeamwidth);
            memcpy(irsp->data, &Pars.AntennaBeamwidth, length);
        break;
        // read antenna gain and length
        case DATAFIELD_ANTENNA_GAIN                :
            length = Parameter_Length(AntennaGain);
            memcpy(irsp->data, Pars.AntennaGain, length);
        break;
        //read antenna max tilt and length
        case DATAFIELD_ANTENNA_MAX_TILT            :
            length = Parameter_Length(AntennaMaxTilt);
            memcpy(irsp->data, &Pars.AntennaMaxTilt, length);
        break;
        // read antenna min tilt and length
        case DATAFIELD_ANTENNA_MIN_TILT            :
            length = Parameter_Length(AntennaMinTilt);
            memcpy(irsp->data, &Pars.AntennaMinTilt, length);
        break;
        // read operator install date and length
        case DATAFIELD_OPERATOR_INSTALL_DATE       :
            length = Parameter_Length(OperatorInstallDate);
            memcpy(irsp->data, Pars.OperatorInstallDate, length);
        break;
        // read operator install id and length
        case DATAFIELD_OPERATOR_INSTALL_ID         :
            length = Parameter_Length(OperatorInstallId);
            memcpy(irsp->data, Pars.OperatorInstallId, length);
        break;
        // read operator basestation id and length
        case DATAFIELD_OPERATOR_BASESTATION_ID     :
            length = Parameter_Length(OperatorBasestationId);
            memcpy(irsp->data, Pars.OperatorBasestationId, length);
        break;
        // read operator sector id and length
        case DATAFIELD_OPERATOR_SECTOR_ID          :
            length = Parameter_Length(OperatorSectorId);
          memcpy(irsp->data, Pars.OperatorSectorId, length);
        break;
        // read operator antenna bearing and length
        case DATAFIELD_OPERATOR_ANTENNA_BEARING    :
            length = Parameter_Length(OperatorAntennaBearing);
            memcpy(irsp->data, &Pars.OperatorAntennaBearing, length);
        break;
        // read operator mechnical tilt and length
        case DATAFIELD_OPERATOR_MECHANICAL_TILT    :
            length = Parameter_Length(OperatorMechanicalTilt);
            memcpy(irsp->data, &Pars.OperatorMechanicalTilt, length);
        break;
        // all other paramter read are unknown
        default                                    :
            result = RESULT_UNKNOWN_PARAMETER;
        break;
    }
    // construct lenght filed (above set length + 1)
    length += 1;
    // laod the response header to reflect get device data response
	irsp->header.command = V20_SRET_GET_DEVICE_DATA;
    // store header LSB and MSB lengths to rsp frame
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    // check if the result flag is successful 
    if (result != RESULT_SUCCESS)
    {
        // store format error message to rsp info frame
        AISG_V20_Build_Error_Response(rsp, result);
    }
    else
    {
        // return rsp frame successfully built   
        irsp->return_code = RESULT_SUCCESS;
    }
    // send debug message out to debug uart
    TRACE_AISG(("Get_Device_Data %s [0x%.2x]\r\n", 
            Get_DatafieldName(icmd->field), irsp->return_code));
    // return function success value
    return (RESULT_SUCCESS);
}

/*
 * Function:    Get_DatafieldName
 * Input:       datafield_t
 * Return:      const Char*
 * Description: Function return constant char string based on datafield
 */
const char * Get_DatafieldName (datafield_t Datafield)
{
    switch (Datafield)
    {
        case DATAFIELD_ANTENNA_MODEL_NUMBER :
            return ("Antenna Model Number");
        case DATAFIELD_ANTENNA_SERIAL_NUMBER :
            return ("Antenna Serial Number");
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_0 :
            return ("Antenna Frequency Band");
        case DATAFIELD_ANTENNA_BEAMWIDTH :
            return ("Antenna Beamwidth");
        case DATAFIELD_ANTENNA_GAIN :
            return ("Antenna Gain");
        case DATAFIELD_ANTENNA_MAX_TILT :
            return ("Antenna Max Tilt");
        case DATAFIELD_ANTENNA_MIN_TILT :
            return ("Antenna Min Tilt");
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_1 :
            return ("Antenna Frequency Band Ext 0x08");
        case DATAFIELD_ANTENNA_FREQUENCY_BAND_2 :
            return ("Antenna Frequency Band Ext 0x09");
        case DATAFIELD_OPERATOR_INSTALL_DATE :
            return ("Antenna Install Date");
        case DATAFIELD_OPERATOR_INSTALL_ID :
            return ("Antenna Install Id");
        case DATAFIELD_OPERATOR_BASESTATION_ID :
            return ("Operator Basestation Id");
        case DATAFIELD_OPERATOR_SECTOR_ID :
            return ("Operator Sector Id");
        case DATAFIELD_OPERATOR_ANTENNA_BEARING :
            return ("Operator Antenna Bearing");
        case DATAFIELD_OPERATOR_MECHANICAL_TILT :
            return ("Operator Mechanical Tilt");
        default:
    	case DATAFIELD_UNKNOWN :
            return ("Unknown Datafield");
    }
}
