//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

static uint8_t  uart_rx_buffer[HDLC_MRU];
static uint8_t  uart_rx_buffer_index;
static uint8_t  uart_tx_buffer[HDLC_MRU];
static uint8_t  uart_tx_buffer_index;

static uint8_t  hdlc_rx_frame[HDLC_MRU];
static uint8_t  hdlc_rx_frame_index;
static uint16_t hdlc_rx_frame_fcs;
static bool     hdlc_rx_char_esc;

/*static*/ uint8_t  hdlc_tx_buffer[HDLC_MRU];
/*static*/ uint8_t  hdlc_tx_buffer_length;
/*static*/ uint8_t  hdlc_tx_old_I_frame[HDLC_MRU];
/*static*/ uint8_t  hdlc_tx_old_I_frame_length;

info_frame_t info_tx_buffer;

static function_t handler = NULL;

//############################################################################//
//
//############################################################################//
void Hdlc_On_Rx_Frame (uint8_t *buffer, uint16_t bytes_received)
{
    hdlc_frame_t *pRxFrame = (hdlc_frame_t *)buffer;
    hdlc_frame_type_t frame_type;
    return_t res;

    hdlc_tx_buffer_length = 0;

    if (((pRxFrame->address == HDLC_BROADCAST_ADDRESS) ||
         (pRxFrame->address == RET.address)) &&
         (pRxFrame->address != HDLC_DEFAULT_ADDRESS))
    {
        if ((pRxFrame->address != HDLC_BROADCAST_ADDRESS) &&
            (pRxFrame->address == RET.address))
        {
            reset_connection_timer = true;
        }

        aisg_3ms_timer_enabled = true;

        frame_type = Hdlc_Get_Frame_Type(pRxFrame->ctrl);
        switch (frame_type)
        {
            case U_FRAME_XID:
                res = Hdlc_Process_Xid_Request(pRxFrame, bytes_received);
            break;

            case U_FRAME_SNRM:
                res = Hdlc_Process_Snrm_Request(pRxFrame, bytes_received);
            break;

            case U_FRAME_DISC:
                res = Hdlc_Process_Disc_Request(pRxFrame, bytes_received);
            break;

            case S_FRAME_RR :
                res = Hdlc_Process_Rr_Request(pRxFrame, bytes_received);
            break;

            case I_FRAME :
                res = Hdlc_Process_Info_Request(pRxFrame, bytes_received);
            break;
        }
        if (res == RESULT_FORMAT_ERROR)
        {
            TRACE_ERR(("hdlc_process_xid_request format error\r\n"));
        }
    }
    else
    {
        TRACE_DBG(("Frame not for me\r\n"));
    }
}

//############################################################################//
//
//############################################################################//
static void Hdlc_Esc_Tx_Byte (uint8_t data)
{
    // See if data should be escaped
    if ((data == HDLC_CONTROL_ESCAPE) || (data == HDLC_FLAG_SEQUENCE))
    {
        Hdlc_Tx_Byte(HDLC_CONTROL_ESCAPE);
        uart_tx_buffer[uart_tx_buffer_index++] = HDLC_CONTROL_ESCAPE;
        data ^= HDLC_ESCAPE_BIT;
    }
    
    // Send data
    Hdlc_Tx_Byte(data);
    uart_tx_buffer[uart_tx_buffer_index++] = data;
}

//############################################################################//
//
//############################################################################//
void Hdlc_Init (void)
{
    TRACE_INI(("START: HDLC init\r\n"));

    uart_rx_buffer_index = 0;
    uart_tx_buffer_index = 0;
    hdlc_rx_frame_index = 0;
    hdlc_rx_frame_fcs   = HDLC_CRC_INIT_VAL;
    hdlc_rx_char_esc    = false;
}

//############################################################################//
//
//############################################################################//
bool Hdlc_On_Rx_Byte (uint8_t data)
{
    bool hdlc_frame_received = false;
    uart_rx_buffer[uart_rx_buffer_index++] = data;

    // Start/End sequence
    if (data == HDLC_FLAG_SEQUENCE)
    {
        // If Escape sequence + End sequence is received then this packet must
        // be silently discarded.
        if (hdlc_rx_char_esc == true)
        {
            hdlc_rx_char_esc = false;
        }
        // Minimum requirement for a valid frame is reception of good FCS
        else if ((hdlc_rx_frame_index >= sizeof(hdlc_rx_frame_fcs)) &&
                 (hdlc_rx_frame_fcs == HDLC_CRC_MAGIC_VAL))
        {
            // Pass on frame with FCS field removed
            Hdlc_Print_Frame(true, uart_rx_buffer, uart_rx_buffer_index);
            uart_rx_buffer_index = 0;
            Hdlc_On_Rx_Frame(hdlc_rx_frame, (uint8_t)(hdlc_rx_frame_index-2));
            hdlc_frame_received = true;
        }
        // Reset for next packet
        hdlc_rx_frame_index = 0;
        hdlc_rx_frame_fcs   = HDLC_CRC_INIT_VAL;
        return (hdlc_frame_received);
    }

    // Escape sequence processing
    if (hdlc_rx_char_esc)
    {
        hdlc_rx_char_esc  = false;
        data             ^= HDLC_ESCAPE_BIT;
    }
    else if (data == HDLC_CONTROL_ESCAPE)
    {
        hdlc_rx_char_esc = true;
        return (hdlc_frame_received);
    }

    // Store received data
    hdlc_rx_frame[hdlc_rx_frame_index] = data;

    // Calculate checksum
    hdlc_rx_frame_fcs = Crc_Update_U8(hdlc_rx_frame_fcs, data);

    // Go to next position in buffer
    hdlc_rx_frame_index++;

    // Check for buffer overflow
    if (hdlc_rx_frame_index >= HDLC_MRU)
    {
        // Wrap index
        hdlc_rx_frame_index  = 0;

        // Invalidate FCS so that packet will be rejected
        hdlc_rx_frame_fcs  ^= 0xFFFF;
    }
    if (uart_rx_buffer_index >= HDLC_MRU)
    {
        // Wrap index
        uart_rx_buffer_index  = 0;
    }

    return (hdlc_frame_received);
}

//############################################################################//
//
//############################################################################//
void Hdlc_Tx_Frame (uint8_t *buffer, uint8_t bytes_to_send)
{
    uint8_t  data;
    uint16_t fcs = HDLC_CRC_INIT_VAL;

    // Start marker
    Hdlc_Tx_Byte(HDLC_FLAG_SEQUENCE);
    uart_tx_buffer[uart_tx_buffer_index++] = HDLC_FLAG_SEQUENCE;

    // Send escaped data
    while (bytes_to_send)
    {
        // Get next data
        data = *buffer++;
        
        // Update checksum
        fcs = Crc_Update_U8(fcs, data);
        
        // ESC send data
        Hdlc_Esc_Tx_Byte(data);
        
        // decrement counter
        bytes_to_send--;
    }

    // Invert checksum
    fcs ^= 0xffff;

    // Low byte of inverted FCS
    data = U16_LSB(fcs);

    // ESC send data
    Hdlc_Esc_Tx_Byte(data);

    // High byte of inverted FCS
    data = U16_MSB(fcs);

    // ESC send data
    Hdlc_Esc_Tx_Byte(data);

    // End marker
    Hdlc_Tx_Byte(HDLC_FLAG_SEQUENCE);
    uart_tx_buffer[uart_tx_buffer_index++] = HDLC_FLAG_SEQUENCE;
    Hdlc_Print_Frame(false, uart_tx_buffer, uart_tx_buffer_index);
    uart_tx_buffer_index = 0;
}

//############################################################################//
//
//############################################################################//
hdlc_frame_type_t Hdlc_Get_Frame_Type (uint8_t ctrl)
{
    // Bit 0=0 means I frame
    if ((ctrl & 0x01) == 0)
    {
//      TRACE_DBG(("I_FRAME\r\n"));
        return (I_FRAME);
    }

    // Bit 0=1 and Bit 1=0 mean S frame
    if ((ctrl & 0x02) == 0x00)
    {
        // Bits 2-3 select the S frame type
        switch (ctrl & 0x0F)
        {
            case S_FRAME_RR :
            {
//              TRACE_DBG(("S_FRAME_RR\r\n"));
                return (S_FRAME_RR);
            }

            case S_FRAME_RNR :
            {
//              TRACE_DBG(("S_FRAME_RNR\r\n"));
                return (S_FRAME_RNR);
            }
            default :
            {
                TRACE_ERR(("INVALID_FRAME\r\n"));
                return (INVALID_FRAME);
            }
        }
    }

    // Bit 0=1 and Bit 1=1 mean U frame
    if ((ctrl & 0x02) == 0x02)
    {
        // Bits 0, 1, 2, 3, 5, 6, 7 select the frame
        // Only Unnumbered responses are allowed
        switch (ctrl & 0xEF)
        {
            case U_FRAME_XID :
            {
//              TRACE_DBG(("U_FRAME_XID\r\n"));
                return (U_FRAME_XID);
            }

            case U_FRAME_SNRM :
            {
//              TRACE_DBG(("U_FRAME_SNRM\r\n"));
                return (U_FRAME_SNRM);
            }

            case U_FRAME_DISC :
            {
//              TRACE_DBG(("U_FRAME_SNRM\r\n"));
                return (U_FRAME_DISC);
            }

            case U_FRAME_UIH :      // Should be supported but how???
            case U_FRAME_UA :
            case U_FRAME_FRMR :
            case U_FRAME_DM :
            default :
            {
                TRACE_ERR(("INVALID_FRAME\r\n"));
                return (INVALID_FRAME);
            }
        }
    }

    TRACE_ERR(("INVALID_FRAME\r\n"));
    return (INVALID_FRAME);
}

//############################################################################//
//
//############################################################################//
uint8_t Hdlc_Get_PF (uint8_t ctrl)
{
    // P/F field is bit 4
    return ((ctrl & 0x10) >> 4);
}

//############################################################################//
//
//############################################################################//
uint8_t Hdlc_Get_NR (uint8_t ctrl)
{
    // N(R) field are bits 5, 6, 7
    return ((ctrl & 0xE0) >> 5);
}

//############################################################################//
//
//############################################################################//
uint8_t Hdlc_Get_NS (uint8_t ctrl)
{
    // N(S) field are bits 1, 2, 3
    return ((ctrl & 0x0E) >> 1);
}

//############################################################################//
//
//############################################################################//
void Hdlc_Set_NS (uint8_t *ctrl, uint8_t value)
{
    // N(S) field are bits 1, 2, 3
    *ctrl = (*ctrl & ~0x0E) | (value << 1);
}

//############################################################################//
//
//############################################################################//
void Hdlc_Set_NR (uint8_t *ctrl, uint8_t value)
{
    // N(R) field are bits 5, 6, 7
    *ctrl = (*ctrl & ~0xE0) | (value << 5);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Process_Xid_Request (hdlc_frame_t *pRxFrame, uint8_t length)
{
	uint8_t GI;
	uint8_t *pData;

    pData = pRxFrame->data;

    // Format identifier field
    if (*pData++ != UFRAME_FORMAT_ID)
	{
        return (RESULT_FORMAT_ERROR);
	}

    // Group identifier field
    GI = *pData++;
    if (GI == UFRAME_USER_GROUP_ID)
	{
        return (Hdlc_Process_Xid_User_Request (pRxFrame, length));
	}
    else if (GI == UFRAME_HDLC_GROUP_ID)
    {
        return (Hdlc_Process_Xid_Hdlc_Request (pRxFrame, length));
    }

    return (RESULT_FORMAT_ERROR);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Process_Xid_User_Request (hdlc_frame_t *pRxFrame, uint8_t length)
{
	uint8_t FI, GI, GL, PI, PL;
	uint8_t *pData;
	bool UniqueIdOk = false;
	bool MaskOk = false;
	bool TypeOk = false;
	bool VersionOk = false;
    bool ProtocolVersionOk = false;
    bool Release3GppOk = false;
    bool DevTypeSubVersionOk = false;
	bool AddressOk = false;
    bool ResetOk = false;
    bool scan_match = false;
    bool address_match = false;
    uint8_t Address;
	uint8_t i;

    pData = pRxFrame->data;

    // Format identifier field
    FI = *pData++;

    // Group identifier field
    GI = *pData++;

    // Group length
    GL = *pData++;
    if (GL >= MAX_HDLC_FRAME_LENGTH)
	{
        return (RESULT_FORMAT_ERROR);
	}

    // Scan all the parameters
    while (GL > 0)
    {
        PI = *pData++;
        PL = *pData++;
        switch (PI)
        {
            case UFRAME_PARAMETER_ID_UNIQUE :
				if (PL > UNIQUE_ID_LENGTH)
                {
                    return (RESULT_FORMAT_ERROR);
                }

                // Remove 0x00 padding from "UniqueId" data
                for (i=0; i<PL; i++)
                {
                    RET.scan_uid[i] = pData[i];
                }
                RET.scan_length = PL;
                UniqueIdOk = true;
            break;

            case UFRAME_PARAMETER_ID_BITMASK :
				if (PL > UNIQUE_ID_LENGTH)
                {
                    return (RESULT_FORMAT_ERROR);
                }

                // Remove 0x00 padding from "UniqueId" data
                for (i=0; i<PL; i++)
                {
                    RET.scan_msk[i] = pData[i];
                }
                MaskOk = true;
            break;

            case UFRAME_PARAMETER_ID_DEVICE :
                if ((PL != 1) || (*pData != TYPE_SINGLE_RET))
                {
                    return (RESULT_FORMAT_ERROR);
                }
                TypeOk = true;
            break;

            case UFRAME_PARAMETER_ID_VENDOR :
                if ((PL != 2) || (pData[0] != 'C') || (pData[1] != 'C'))
                {
                    return (RESULT_FORMAT_ERROR);
                }
            break;

            case UFRAME_PARAMETER_ID_VERSION :
                if ((PL != 1) || (*pData > 2))
                {
                    return (RESULT_FORMAT_ERROR);
                }
				VersionOk = true;
            break;

            case UFRAME_PARAMETER_ID_PROTOCOL :
                if ((PL != 1) || ((*pData > 3)))
                {
                    return (RESULT_FORMAT_ERROR);
                }
                ProtocolVersionOk = true;
            break;

            case UFRAME_PARAMETER_ID_3GPPRELEASE :
                if (PL != 1)
                {
                    return (RESULT_FORMAT_ERROR);
                }
                Release3GppOk = true;
            break;

            case UFRAME_PARAMETER_ID_DEVTYPESUBVERSION :
                if (PL != 2)
                {
                    return (RESULT_FORMAT_ERROR);
                }
                DevTypeSubVersionOk = true;
            break;

            case UFRAME_PARAMETER_ID_ADDRESS :
                if ((PL != 1) || (*pData == HDLC_BROADCAST_ADDRESS))
                {
                    return (RESULT_FORMAT_ERROR);
                }
                Address = *pData;
                AddressOk = true;
            break;

            case UFRAME_PARAMETER_ID_RESET :
                if (PL != 0)
                {
                    return (RESULT_FORMAT_ERROR);
                }
                ResetOk = true;
            break;

            default:
            break;
        }
        pData += PL;
        GL -= (PL + 2);
    }

    if (ResetOk == true)
    {
        TRACE_DBG(("Performing Layer2 reset\r\n"));
        if (pRxFrame->address != HDLC_BROADCAST_ADDRESS)
        {
            Hdlc_Prepare_Xid_Reset_Response();
            layer2_reset_request = true;
        }
        else
        {
            RESET();
        }
        return (RESULT_SUCCESS);
    }

    if ((ProtocolVersionOk == true) || (Release3GppOk == true) ||
        (DevTypeSubVersionOk == true))
    {
        Hdlc_Prepare_Xid_Versions_Response(ProtocolVersionOk,
                                           Release3GppOk,
                                           DevTypeSubVersionOk);
        return (RESULT_SUCCESS);
    }

    if ((UniqueIdOk == true) && (MaskOk == true))
    {
        if (RET.address == HDLC_DEFAULT_ADDRESS)
        {
            if (RET.scan_length != 0)
            {
                scan_match = Aisg_Scan_Match(RET.scan_uid, RET.scan_msk, RET.scan_length);
            }
            else
            {
                TRACE_DBG(("No mask XID scan\r\n"));
                scan_match = true;
            }
            if (scan_match == true)
            {
                Hdlc_Prepare_Xid_Scan_Response();
            }
        }
		return (RESULT_SUCCESS);
    }

    if ((UniqueIdOk == true) && (AddressOk == true))
    {
        memset(RET.scan_msk, 0xFF, RET.scan_length);
        address_match = Aisg_Address_Match(RET.scan_uid, RET.scan_msk, RET.scan_length);
        if (address_match == true)
        {
            RET.address = Address;
            RET.state = STATE_ADDRESS_ASSIGNED;
            Hdlc_Prepare_Xid_Address_Response();
        }
        return (RESULT_SUCCESS);
    }

    return (RESULT_FORMAT_ERROR);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Process_Xid_Hdlc_Request (hdlc_frame_t *pRxFrame, uint8_t length)
{
	uint8_t FI, GI, GL, PI, PL;
	uint8_t *pData;
    bool IFrameBitsTxOk = false;
    bool IFrameBitsRxOk = false;
    bool WindowSizeTxOk = false;
    bool WindowSizeRxOk = false;
    uint32_t MaxIframeBitsTx = 0;
    uint32_t MaxIframeBitsRx = 0;
    uint8_t WindowSizeBitsTx;
    uint8_t WindowSizeBitsRx;
    uint8_t i;

    pData = pRxFrame->data;

    // Format identifier field
    FI = *pData++;

    // Group identifier field
    GI = *pData++;

    // Group length
    GL = *pData++;
    if (GL >= MAX_HDLC_FRAME_LENGTH)
	{
        return (RESULT_FORMAT_ERROR);
	}

    // Scan all the parameters
    while (GL > 0)
    {
        PI = *pData++;
        PL = *pData++;
        switch (PI)
        {
            case UFRAME_PARAMETER_ID_MAX_IFRAMEBITSTX :
                if (PL != 4)
                {
                    return (RESULT_FORMAT_ERROR);
                }
                for (i=0; i<PL; i++)
                {
                    MaxIframeBitsTx |= (pData[i] << ((PL-i-1)*8));
                }
                IFrameBitsTxOk = true;
            break;

            case UFRAME_PARAMETER_ID_MAX_IFRAMEBITSRX :
                if (PL != 4)
                {
                    return (RESULT_FORMAT_ERROR);
                }
                for (i=0; i<PL; i++)
                {
                    MaxIframeBitsRx |= (pData[i] << ((PL-i-1)*8));
                }
                IFrameBitsRxOk = true;
            break;

            case UFRAME_PARAMETER_ID_MAX_WINDOWSIZETX :
                if (PL != 1)
                {
                    return (RESULT_FORMAT_ERROR);
                }
                WindowSizeBitsTx = *pData;
                WindowSizeTxOk = true;
            break;

            case UFRAME_PARAMETER_ID_MAX_WINDOWSIZERX :
                if (PL != 1)
                {
                    return (RESULT_FORMAT_ERROR);
                }
                WindowSizeBitsRx = *pData;
                WindowSizeRxOk = true;
            break;

            default:
            break;
        }
        pData += PL;
        GL -= (PL + 2);
    }

    if ((IFrameBitsTxOk == true) && (IFrameBitsRxOk == true) &&
        (WindowSizeTxOk == true) && (WindowSizeRxOk == true))
    {
        Hdlc_Prepare_Xid_Hdlc_Parameters_Response();
    }

    return (RESULT_FORMAT_ERROR);
}

//##################################################################################################
//
//##################################################################################################
bool Aisg_Scan_Match (unique_id_t uid, unique_id_t msk, uint8_t id_length)
{
    uint8_t len;
    uint8_t i;
    uint8_t tmp_uid;
    uint8_t tmp_msk;
    uint8_t tmp_and;
    uint8_t loc_and;
    bool found = true;

    // left compare vendor
    len = min(id_length, 2);
    for (i=0; i<len; i++)
    {
        tmp_uid = uid[i];
        tmp_msk = msk[i];
        tmp_and = tmp_uid & tmp_msk;
        loc_and = RET.unique_id[i] & tmp_msk;
        if (loc_and != tmp_and)
        {
            return (false);
        }
    }

    if (len < 2)
    {
        return (found);
    }

    // right compare unique id
    len = id_length - 2;
    for (i=0; i<len; i++)
    {
        tmp_uid = uid[i+2];
        tmp_msk = msk[i+2];
        tmp_and = tmp_uid & tmp_msk;
        loc_and = Pars.ProductSerialNumber[i] & tmp_msk;
        if (loc_and != tmp_and)
        {
            return (false);
        }
    }

    return (found);
}

//##################################################################################################
//
//##################################################################################################
bool Aisg_Address_Match (unique_id_t uid, unique_id_t msk, uint8_t id_length)
{
    uint8_t len;
    uint8_t matching_len;
    uint8_t i;
    uint8_t tmp_uid;
    uint8_t tmp_msk;
    uint8_t tmp_and;
    uint8_t loc_and;
    bool found = true;

    // right compare unique id
    len = id_length;
    for (i=0; i<len; i++)
    {
        tmp_uid = uid[i];
        tmp_msk = msk[i];
        tmp_and = tmp_uid & tmp_msk;
        matching_len = min(id_length, UNIQUE_ID_LENGTH);
        loc_and = RET.unique_id[UNIQUE_ID_LENGTH-matching_len+i] & tmp_msk;
        if (loc_and != tmp_and)
        {
            return (false);
        }
    }

    return (found);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Prepare_Xid_Scan_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = HDLC_DEFAULT_ADDRESS;
    pTxFrame->ctrl = U_FRAME_XID | POLL_FINAL_BIT;
    *pData++ = UFRAME_FORMAT_ID;
    *pData++ = UFRAME_USER_GROUP_ID;
    *pData++ = 7 + UNIQUE_ID_LENGTH + VENDOR_ID_LENGTH;
    *pData++ = UFRAME_PARAMETER_ID_UNIQUE;
    *pData++ = UNIQUE_ID_LENGTH;
	memcpy(pData, RET.unique_id, UNIQUE_ID_LENGTH);
	pData += UNIQUE_ID_LENGTH;
    *pData++ = UFRAME_PARAMETER_ID_VENDOR;
    *pData++ = VENDOR_ID_LENGTH;
	memcpy(pData, RET_VENDOR_CODE, VENDOR_ID_LENGTH);
    pData += VENDOR_ID_LENGTH;
    *pData++ = UFRAME_PARAMETER_ID_DEVICE;
    *pData++ = 1;
    *pData++ = TYPE_SINGLE_RET;
	hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Prepare_Xid_Address_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = U_FRAME_XID | POLL_FINAL_BIT;
    *pData++ = UFRAME_FORMAT_ID;
    *pData++ = UFRAME_USER_GROUP_ID;
    *pData++ = 3 + UNIQUE_ID_LENGTH + VENDOR_ID_LENGTH;
    *pData++ = UFRAME_PARAMETER_ID_UNIQUE;
    *pData++ = UNIQUE_ID_LENGTH;
	memcpy(pData, RET.unique_id, UNIQUE_ID_LENGTH);
	pData += UNIQUE_ID_LENGTH;
    *pData++ = UFRAME_PARAMETER_ID_DEVICE;
    *pData++ = 1;
    *pData++ = TYPE_SINGLE_RET;
	hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Prepare_Xid_Reset_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = U_FRAME_XID | POLL_FINAL_BIT;
    *pData++ = UFRAME_FORMAT_ID;
    *pData++ = UFRAME_USER_GROUP_ID;
    *pData++ = 2;
    *pData++ = UFRAME_PARAMETER_ID_RESET;
    *pData++ = 0;
	hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Prepare_Xid_Versions_Response (bool protocol, bool release3gpp, bool devtypesub)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = U_FRAME_XID | POLL_FINAL_BIT;
    *pData++ = UFRAME_FORMAT_ID;
    *pData++ = UFRAME_USER_GROUP_ID;
    *pData++ = 3 * protocol + 3 * release3gpp + 4 * devtypesub;
    if (protocol == true)
    {
        *pData++ = UFRAME_PARAMETER_ID_PROTOCOL;
        *pData++ = 1;
        *pData++ = AISG_VERSION_20;
    }
    if (release3gpp == true)
    {
        *pData++ = UFRAME_PARAMETER_ID_3GPPRELEASE;
        *pData++ = 1;
        *pData++ = RELEASE_3GPP;
    }
    if (devtypesub == true)
    {
        *pData++ = UFRAME_PARAMETER_ID_DEVTYPESUBVERSION;
        *pData++ = 2;
        *pData++ = TYPE_SINGLE_RET;
        *pData++ = DEVTYPESUB_VERSION;
    }
    hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}
//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Prepare_Xid_Hdlc_Parameters_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;
    uint32_t MaxIframeBitsTx = MAX_HDLC_FRAME_LENGTH * 8;
    uint32_t MaxIframeBitsRx = MAX_HDLC_FRAME_LENGTH * 8;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = U_FRAME_XID | POLL_FINAL_BIT;
    *pData++ = UFRAME_FORMAT_ID;
    *pData++ = UFRAME_HDLC_GROUP_ID;
    *pData++ = 18;
    *pData++ = UFRAME_PARAMETER_ID_MAX_IFRAMEBITSTX;  // PI [MaxIframeBitsTx]
    *pData++ = 4;                                     // PL
    *pData++ = (MaxIframeBitsTx >> 24) & 0xFF;        // PV[0]
    *pData++ = (MaxIframeBitsTx >> 16) & 0xFF;        // PV[1]
    *pData++ = (MaxIframeBitsTx >>  8) & 0xFF;        // PV[2]
    *pData++ = (MaxIframeBitsTx >>  0) & 0xFF;        // PV[3]
    *pData++ = UFRAME_PARAMETER_ID_MAX_IFRAMEBITSRX;  // PI [MaxIframeBitsRx]
    *pData++ = 4;                                     // PL
    *pData++ = (MaxIframeBitsRx >> 24) & 0xFF;        // PV[0]
    *pData++ = (MaxIframeBitsRx >> 16) & 0xFF;        // PV[1]
    *pData++ = (MaxIframeBitsRx >>  8) & 0xFF;        // PV[2]
    *pData++ = (MaxIframeBitsRx >>  0) & 0xFF;        // PV[3]
    *pData++ = UFRAME_PARAMETER_ID_MAX_WINDOWSIZETX;  // PI [MaxWindowSizeTx]
    *pData++ = 1;                                     // PL
    *pData++ = HDLC_WINDOWSIZE;                       // PV
    *pData++ = UFRAME_PARAMETER_ID_MAX_WINDOWSIZERX;  // PI [MaxWindowSizeRx]
    *pData++ = 1;                                     // PL
    *pData++ = HDLC_WINDOWSIZE;                       // PV

    hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Process_Snrm_Request (hdlc_frame_t *pRxFrame, uint8_t length)
{
    if (length != 2)
    {
        return (RESULT_INVALID_STATE);
    }
    
    RET.state = STATE_CONNECTED;
    RET.NR = 0;
    RET.NS = 0;

    Hdlc_Prepare_Snrm_Response();

    return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Process_Disc_Request (hdlc_frame_t *pRxFrame, uint8_t length)
{
    RET.state = STATE_NO_ADDRESS;

    Hdlc_Prepare_Disc_Response();

    return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Process_Rr_Request (hdlc_frame_t *pRxFrame, uint8_t length)
{
    uint8_t VR;
    info_frame_t *tx_info = (info_frame_t *)&(info_tx_buffer);

    // check pRxFrame lenght is 0
    // check we are in connected state
    if (RET.state != STATE_CONNECTED)
    {
        return (RESULT_INVALID_STATE);
    }

    VR = Hdlc_Get_NR(pRxFrame->ctrl);

    if (VR == RET.NS)
    {
        if (Alarm_Waiting_To_Be_Reported() == true)
        {
            tx_info->data[0] = last_tcp.return_code;
            handler = Aisg_Get_Command_Handler(V20_SRET_ALARM_INDICATION);
            if (handler == NULL)
            {
                return (RESULT_INVALID_PROCEDURE);
            }
            handler(NULL, tx_info);
            Hdlc_Prepare_Info_Response(tx_info);
            return (RESULT_SUCCESS);
        }

        if (last_tcp.completed == false)
        {
			if ((last_tcp.in_progress == true) &&
                ((last_tcp.command == V20_SRET_SEND_CONFIG) ||
                 (last_tcp.command == V20_DOWNLOAD_APPL)))
            {
				Hdlc_Prepare_Rnr_Response();
            }
			else
				Hdlc_Prepare_Rr_Response();
        }
        else
        {
			if ((last_tcp.in_progress == false) &&
                ((last_tcp.command == V20_SRET_SEND_CONFIG) ||
                 (last_tcp.command == V20_DOWNLOAD_APPL)))
            {
                TRACE_DBG(("TCP ABORTED ... SENDING RR\r\n"));
				Hdlc_Prepare_Rr_Response();
            }
            else
            {
                TRACE_DBG(("TCP COMPLETED ... SENDING ANSWER\r\n"));
                last_tcp.in_progress = false;
                last_tcp.completed = false;
                tx_info->data[0] = last_tcp.return_code;
                handler = Aisg_Get_Command_Handler(last_tcp.command);
                if (handler == NULL)
                {
                    Hdlc_Prepare_Rr_Response();
                    return (RESULT_INVALID_PROCEDURE);
                }
                handler(NULL, tx_info);
                Hdlc_Prepare_Info_Response(tx_info);
            }
        }
    }
    else
    {
        TRACE_DBG(("PRIMARY NR(%d) <=> SECONDARY NS(%d)     > MISMATCH %s\r\n",
                VR, RET.NS, "[CONTROLLER MISSES A FRAME SENT FROM RET]"));
        hdlc_tx_buffer_length = hdlc_tx_old_I_frame_length;
        memcpy(hdlc_tx_buffer, hdlc_tx_old_I_frame, hdlc_tx_buffer_length);
    }

    return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t Hdlc_Process_Info_Request (hdlc_frame_t *pRxFrame, uint8_t length)
{
    info_frame_t *rx_info = (info_frame_t *)&(pRxFrame->data);
    info_frame_t *tx_info = (info_frame_t *)&(info_tx_buffer);
    uint8_t VR;
    uint8_t VS;
    uint8_t iframe_len = length - INFO_FRAME_HEADER_LENGTH;
// FLAGS are escluded by HDLC - 2 bytes
// CRC is also removed from HDLC layer  - 2 bytes

    if (RET.state != STATE_CONNECTED)
    {
        return (RESULT_INVALID_STATE);
    }

    VR = Hdlc_Get_NR(pRxFrame->ctrl);
    VS = Hdlc_Get_NS(pRxFrame->ctrl);

    if (VS != RET.NR)
    {
        TRACE_DBG(("PRIMARY NS(%d) <=> SECONDARY NR(%d)     > MISMATCH %s]\r\n",
                VS, RET.NR, "[RET MISSES A FRAME SENT FROM CONTROLLER]"));
        IncrementFrameCounter(VS);
        if (VS == RET.NR)
        {
            Hdlc_Prepare_Rnr_Response();
            return (RESULT_SUCCESS);
        }
    }

    if (VR != RET.NS)
    {
        TRACE_DBG(("PRIMARY NR(%d) <=> SECONDARY NS(%d)     > MISMATCH %s\r\n",
                VR, RET.NS, "[CONTROLLER MISSES A FRAME SENT FROM RET]"));
        hdlc_tx_buffer_length = hdlc_tx_old_I_frame_length;
        memcpy(hdlc_tx_buffer, hdlc_tx_old_I_frame, hdlc_tx_buffer_length);
        return (RESULT_SUCCESS);
    }

    IncrementFrameCounter(RET.NR);

// [Verify length is consistent]
    if ((rx_info->header.lenMSB != 0) || (rx_info->header.lenLSB != iframe_len))
    {
// frame too long for us!!!!
        return (RESULT_FORMAT_ERROR);
    }

// Check is a valid command
    handler = Aisg_Get_Command_Handler(rx_info->header.command);
    if (handler == NULL)
    {
        return (RESULT_INVALID_PROCEDURE);
    }

    if (handler(rx_info, tx_info) != RESULT_PROCEDURE_NOT_COMPLETED)
    {
        Hdlc_Prepare_Info_Response(tx_info);
    }
    else
    {
        last_tcp.command = rx_info->header.command;
        last_tcp.in_progress = true;
        Hdlc_Prepare_Rnr_Response();
    }

    return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t  Hdlc_Prepare_Snrm_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = U_FRAME_UA | POLL_FINAL_BIT;
	hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t  Hdlc_Prepare_Disc_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = U_FRAME_UA | POLL_FINAL_BIT;
	hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t  Hdlc_Prepare_Rr_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = S_FRAME_RR | POLL_FINAL_BIT;
    Hdlc_Set_NR(&pTxFrame->ctrl, RET.NR);
	hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t  Hdlc_Prepare_Rnr_Response (void)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;
	uint8_t *pData, *pStart;

    pData = pStart = pTxFrame->data;
	pTxFrame->address = RET.address;
    pTxFrame->ctrl = S_FRAME_RNR | POLL_FINAL_BIT;
    Hdlc_Set_NR(&pTxFrame->ctrl, RET.NR);
	hdlc_tx_buffer_length = pData - pStart + 2;

	return (RESULT_SUCCESS);
}

//##################################################################################################
//
//##################################################################################################
return_t  Hdlc_Prepare_Info_Response (info_frame_t *info_frame_tx)
{
	hdlc_frame_t *pTxFrame = (hdlc_frame_t *)hdlc_tx_buffer;

	pTxFrame->address = RET.address;
    pTxFrame->ctrl = I_FRAME | POLL_FINAL_BIT;
    Hdlc_Set_NR(&pTxFrame->ctrl, RET.NR);
    Hdlc_Set_NS(&pTxFrame->ctrl, RET.NS);

    memcpy(pTxFrame->data, info_frame_tx, info_frame_tx->header.lenLSB + HDLC_FRAME_HEADER_LENGTH);
    IncrementFrameCounter(RET.NS);
    hdlc_tx_buffer_length = info_frame_tx->header.lenLSB + INFO_FRAME_HEADER_LENGTH;

	hdlc_tx_old_I_frame_length = hdlc_tx_buffer_length;
	memcpy(hdlc_tx_old_I_frame, hdlc_tx_buffer, hdlc_tx_buffer_length);

   	return (RESULT_SUCCESS);
 }

//############################################################################//
//
//############################################################################//
void Hdlc_Print_Frame (bool is_rx, uint8_t *buffer, uint8_t length)
{
    if (is_rx == true)
    {
        TRACE_DBG(("RX %.2d: ", length));
    }
    else
    {
        TRACE_DBG(("TX %.2d: ", length));
    }

    while (length != 0)
    {
        TRACE_PUT(DBG, ("%02X ", *buffer++));
        length--;
    }
    TRACE_PUT(DBG, ("\r\n"));
}
