//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

parameters_t Pars;
parameters_flags_t ParsUpdate;
parameters_flags_t ParsValid;
uint8_t config_data[MAX_AISG_FRAME_LENGTH+1];
uint8_t config_data_length = 0;

#define CONFIG_DATA_MAX_PAR_LENGTH 32
#define CONFIG_DATA_MIN_TOT_LENGTH 13
//############################################################################//
//
//############################################################################//
return_t Parameters_Init (void)
{
    uint8_t *pData = (uint8_t *)&Pars;
    uint16_t i;
    uint16_t Length = sizeof(parameters_t);
 
    TRACE_INI(("START: PARAMETERS init\r\n"));

    ParsUpdate.Value = 0x00000000;
    ParsValid.Value  = 0xFFFFFFFF;
    
    for (i=0; i<Length; i++)
    {
        *pData++ = Eeprom_Read(i);
    }

    if (Pars.TraceLevel > DBG)
    {
        Pars.TraceLevel = ERR;
    }
    trace_level = Pars.TraceLevel;
    trace_change = Pars.TraceLevel;
    
    Parameter_Verify(ProductSerialNumber);
    Parameter_Verify(ProductModelNumber);
    Parameter_Verify(AntennaSerialNumber);
    Parameter_Verify(AntennaModelNumber);
    Parameter_Verify(OperatorInstallDate);
    Parameter_Verify(OperatorInstallId);
    Parameter_Verify(OperatorBasestationId);
    Parameter_Verify(OperatorSectorId);
    Parameter_Verify(OperatorAntennaBearing);
    Parameter_Verify(OperatorMechanicalTilt);
    Parameter_Verify(AntennaFrequencyBand);
    Parameter_Verify(AntennaBeamwidth);
    Parameter_Verify(AntennaGain);
    Parameter_Verify(AntennaMaxTilt);
    Parameter_Verify(AntennaMinTilt);
    Parameter_Verify(TurnRatio);
    Parameter_Verify(Backlash);
    Parameter_Verify(Unwind);
    Parameter_Verify(Tilt);
    Parameter_Verify(NotCalibrated);
    Parameter_Verify(LastDirection);
    Parameter_Verify(TraceLevel);

    for (i=strlen(Pars.ProductModelNumber); i<Parameter_Length(ProductModelNumber); i++)
    {
        Pars.ProductModelNumber[i] = 0x00;
    }

    pData = Pars.ProductSerialNumber;
    for (i=0; i<Parameter_Length(ProductSerialNumber); i++)
    {
        if ((*pData != 0xFF) && (*pData != 0x00))
        {
            if (Pars.TurnRatio == 0)
            {
                Alarm_Set(Not_Configured);
                return (RESULT_NOT_CONFIGURED);
            }
            if (Pars.NotCalibrated == true)
            {
                Alarm_Set(Not_Calibrated);
                return (RESULT_NOT_CALIBRATED);
            }
            return (RESULT_SUCCESS);
        }
        pData++;
    }

    memset(Pars.ProductSerialNumber, '0', Parameter_Length(ProductSerialNumber));

    Alarm_Set(Not_Configured);
    return (RESULT_NOT_CONFIGURED);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Print (void)
{
    uint8_t tmp_str[33];
    uint8_t i;
    bool par_valid;
    char par_val[] = "[VAL]";
    char par_bad[] = "[BAD]";
    char *par_str;

    par_valid = Parameter_Good(ProductSerialNumber);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_SERIAL_NUMBER_LENGTH; i++)
    {
        if ((Pars.ProductSerialNumber[i] == 0x00) || (Pars.ProductSerialNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.ProductSerialNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Product Serial Number %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(ProductModelNumber);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_MODEL_NUMBER_LENGTH; i++)
    {
        if ((Pars.ProductModelNumber[i] == 0x00) || (Pars.ProductModelNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.ProductModelNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Product Model Number  %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(AntennaSerialNumber);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_SERIAL_NUMBER_LENGTH; i++)
    {
        if ((Pars.AntennaSerialNumber[i] == 0x00) || (Pars.AntennaSerialNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.AntennaSerialNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Antenna Serial Number %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(AntennaModelNumber);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_MODEL_NUMBER_LENGTH; i++)
    {
        if ((Pars.AntennaModelNumber[i] == 0x00) || (Pars.AntennaModelNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.AntennaModelNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Antenna Model Number  %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(OperatorInstallDate);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_OPERATOR_INSTALL_DATE_LENGTH; i++)
    {
        if ((Pars.OperatorInstallDate[i] == 0x00) || (Pars.OperatorInstallDate[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.OperatorInstallDate[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Install Date          %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(OperatorInstallId);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_OPERATOR_INSTALL_ID_LENGTH; i++)
    {
        if ((Pars.OperatorInstallId[i] == 0x00) || (Pars.OperatorInstallId[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.OperatorInstallId[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Install Id            %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(OperatorBasestationId);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_OPERATOR_BASESTATION_ID_LENGTH; i++)
    {
        if ((Pars.OperatorBasestationId[i] == 0x00) || (Pars.OperatorBasestationId[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.OperatorBasestationId[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Basestation Id        %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(OperatorSectorId);
    par_str = (par_valid == true) ? par_val : par_bad;
    for (i=0; i<PARAMETER_OPERATOR_SECTOR_ID_LENGTH; i++)
    {
        if ((Pars.OperatorSectorId[i] == 0x00) || (Pars.OperatorSectorId[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = Pars.OperatorSectorId[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_INI(("PARAM: Sector Id             %s %s\r\n", par_str, tmp_str));

    par_valid = Parameter_Good(OperatorAntennaBearing);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Antenna Bearing       %s %d\r\n", par_str, Pars.OperatorAntennaBearing));

    par_valid = Parameter_Good(OperatorMechanicalTilt);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Mechanical Tilt       %s %d\r\n", par_str, Pars.OperatorMechanicalTilt));

    par_valid = Parameter_Good(AntennaFrequencyBand);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Frequency Band[3]     %s 0x%.4x 0x%.4x 0x%.4x\r\n", par_str,
            Pars.AntennaFrequencyBand[0],
            Pars.AntennaFrequencyBand[1],
            Pars.AntennaFrequencyBand[2]));
    
    par_valid = Parameter_Good(AntennaBeamwidth);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Beamwidth[4]          %s %d %d %d %d\r\n", par_str,
            Pars.AntennaBeamwidth[0], Pars.AntennaBeamwidth[1],
            Pars.AntennaBeamwidth[2], Pars.AntennaBeamwidth[3]));

    par_valid = Parameter_Good(AntennaGain);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Gain[4]               %s %d %d %d %d\r\n", par_str,
            Pars.AntennaGain[0], Pars.AntennaGain[1],
            Pars.AntennaGain[2], Pars.AntennaGain[3]));

    par_valid = Parameter_Good(AntennaMaxTilt);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Max Tilt              %s %d\r\n", par_str, Pars.AntennaMaxTilt));
    
    par_valid = Parameter_Good(AntennaMinTilt);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Min Tilt              %s %d\r\n", par_str, Pars.AntennaMinTilt));

    par_valid = Parameter_Good(TurnRatio);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Tilt Conversion       %s %ld\r\n", par_str, Pars.TurnRatio));

    par_valid = Parameter_Good(Backlash);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Backlash              %s %d\r\n", par_str, Pars.Backlash));

    par_valid = Parameter_Good(Unwind);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Unwind                %s %d\r\n", par_str, Pars.Unwind));

    par_valid = Parameter_Good(NotCalibrated);
    par_str = (par_valid == true) ? par_val : par_bad;
    if (Pars.NotCalibrated == false)
        TRACE_INI(("PARAM: Calibrated            %s YES\r\n", par_str));
    else
        TRACE_INI(("PARAM: Calibrated            %s NO\r\n", par_str));

    par_valid = Parameter_Good(Tilt);
    par_str = (par_valid == true) ? par_val : par_bad;
    TRACE_INI(("PARAM: Tilt Position         %s %d\r\n", par_str, Pars.Tilt));

    par_valid = Parameter_Good(LastDirection);
    par_str = (par_valid == true) ? par_val : par_bad;
    if (Pars.LastDirection == DIRECTION_DOWNWARDS)
        TRACE_INI(("PARAM: Last Direction        %s DOWNWARDS\r\n", par_str));
    else
        TRACE_INI(("PARAM: Last Direction        %s UPWARDS\r\n", par_str));

    par_valid = Parameter_Good(TraceLevel);
    par_str = (par_valid == true) ? par_val : par_bad;
    switch (Pars.TraceLevel)
    {
        case ERR :
            TRACE_INI(("PARAM: Trace Level           %s ERR\r\n", par_str));
        break;
        case LOG :
            TRACE_INI(("PARAM: Trace Level           %s LOG\r\n", par_str));
        break;
        case DBG :
            TRACE_INI(("PARAM: Trace Level           %s DBG\r\n", par_str));
        break;
        default :
            TRACE_INI(("PARAM: Trace Level           %s UNKNOWN\r\n", par_str));
        break;
    }
    
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Update (uint8_t *pData, uint8_t Length, uint8_t *pChecksum)
{
    uint8_t i;
	volatile uint16_t eeprom_address = (uint16_t)pData - (uint16_t)&Pars;
	volatile uint16_t checksum_address = (uint16_t)pChecksum - (uint16_t)&Pars;
	volatile uint8_t data = 0;
    uint8_t checksum = 0;

    TRACE_DBG(("eeprom write address: 0x%.2X, len: %.2d, data: ",
            eeprom_address, Length));

    for (i=0; i<Length; i++)
    {
        data = *pData;
        Eeprom_Write(eeprom_address, data);
        TRACE_PUT(DBG, ("0x%.2x ", data));
        pData++;
        checksum += data;
        eeprom_address++;
    }
    TRACE_PUT(DBG, ("\r\n"));

    *pChecksum = ~checksum;

    TRACE_DBG(("eeprom write checksum address: 0x%.2X, data: 0x%.2X",
            checksum_address, *pChecksum));
    Eeprom_Write(checksum_address, *pChecksum);
    TRACE_PUT(DBG, ("\r\n"));

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Validate (uint8_t *pData, uint8_t Length, uint8_t *pChecksum)
{
    uint8_t i;
	volatile uint8_t data = 0;
    uint8_t checksum = 0;

    for (i=0; i<Length; i++)
    {
        data = *pData;
        pData++;
        checksum += data;
    }

    checksum = ~checksum;

    TRACE_DBG(("checksum cal: 0x%1.1X, exp: 0x%1.1X %s\r\n",
            checksum, *pChecksum, (checksum == *pChecksum) ? "valid" : "invalid"));

    if (checksum == *pChecksum)
        return (RESULT_SUCCESS);
    else
        return (RESULT_CHECKSUM_ERROR);
}

//############################################################################//
//
//############################################################################//
uint8_t Task_Parameters (uint8_t state)
{
    return_t result;

    switch (state)
    {
        case TASK_PARAMETERS_IDLE :
            if (ParsUpdate.Value != 0)
            {
                state = TASK_PARAMETERS_RUNNING;
            }
        break;

        case TASK_PARAMETERS_RUNNING :
            Parameter_Process(ProductSerialNumber);
            Parameter_Process(ProductModelNumber);
            Parameter_Process(AntennaSerialNumber);
            Parameter_Process(AntennaModelNumber);
            Parameter_Process(OperatorInstallDate);
            Parameter_Process(OperatorInstallId);
            Parameter_Process(OperatorBasestationId);
            Parameter_Process(OperatorSectorId);
            Parameter_Process(OperatorAntennaBearing);
            Parameter_Process(OperatorMechanicalTilt);
            Parameter_Process(AntennaFrequencyBand);
            Parameter_Process(AntennaBeamwidth);
            Parameter_Process(AntennaGain);
            Parameter_Process(AntennaMaxTilt);
            Parameter_Process(AntennaMinTilt);
            Parameter_Process(TurnRatio);
            Parameter_Process(Backlash);
            Parameter_Process(Unwind);
            Parameter_Process(Tilt);
            Parameter_Process(NotCalibrated);
            Parameter_Process(LastDirection);
            Parameter_Process(TraceLevel);
            state = TASK_PARAMETERS_IDLE;
        break;

        case TASK_PARAMETERS_WAIT_DELAY:
            state = TASK_PARAMETERS_CONFIG_DATA;
            break;

        case TASK_PARAMETERS_CONFIG_DATA:
            result = Parameters_Process_Config_Data();
            Parameters_Config_Data_Completed(result);
            state = TASK_PARAMETERS_IDLE;
        break;

        default:
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Send_Config_Data (uint8_t *pData, uint8_t length)
{
    memcpy(config_data, pData, length);
    config_data_length = length;
    config_data[length] = '\0';

    Task_Set_State(TASK_PARS_ID, TASK_PARAMETERS_WAIT_DELAY);
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Config_Data_Completed (return_t result)
{
    last_tcp.completed = true;
    last_tcp.return_code = result;

    TRACE_DBG(("Task_Parameters   (SEND CONFIG DATA DONE)\r\n"));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Process_Config_Data (void)
{
	uint16_t length;
    uint16_t *pdata;
    config_data_file_t *config;
    uint16_t checksum;
    uint16_t i;
    uint8_t Type[CONFIG_DATA_TYPE_LEN+1] = CONFIG_DATA_TYPE;

    length = config_data_length;
    config = (config_data_file_t *)&config_data;

	// Checksum Verification
    checksum = 0;
    pdata = (uint16_t *)config;
    for (i=0; i<length; i+= 2)
    {
    	checksum += *pdata;
    	pdata++;
    }

    if (checksum != MAGIC_CHECKSUM)
    {
        TRACE_LOG(("wrong checksum\r\n"));
        return (RESULT_CHECKSUM_ERROR);
    }

    if (memcmp(config->Type, Type, CONFIG_DATA_TYPE_LEN) != 0)
    {
        TRACE_LOG(("wrong type\r\n"));
        return (RESULT_INVALID_FILE);
    }

    if (config->Version != CONFIG_DATA_VERSION)
    {
        TRACE_LOG(("wrong version\r\n"));
        return (RESULT_INVALID_FILE);
    }

    if (memcmp(config->ProductModelNumber, Pars.ProductModelNumber, PARAMETER_MODEL_NUMBER_LENGTH) != 0)
    {
        TRACE_LOG(("wrong product number\r\n"));
        return (RESULT_INVALID_FILE);
    }

    TRACE_LOG(("length               : %d\r\n", config->Length));
    TRACE_LOG(("checksum             : 0x%.4x\r\n", checksum));
    TRACE_LOG(("Product Model Number : %s\r\n", config->ProductModelNumber));
    TRACE_LOG(("Antenna Model Number : %s\r\n", config->AntennaModelNumber));
    TRACE_LOG(("Frequency Band[0]    : 0x%.4x\r\n", config->AntennaFrequencyBand[0]));
    TRACE_LOG(("Frequency Band[1]    : 0x%.4x\r\n", config->AntennaFrequencyBand[1]));
    TRACE_LOG(("Frequency Band[2]    : 0x%.4x\r\n", config->AntennaFrequencyBand[2]));
    TRACE_LOG(("Beamwidth[0]         : %d\r\n", config->AntennaBeamwidth[0]));
    TRACE_LOG(("Beamwidth[1]         : %d\r\n", config->AntennaBeamwidth[1]));
    TRACE_LOG(("Beamwidth[2]         : %d\r\n", config->AntennaBeamwidth[2]));
    TRACE_LOG(("Beamwidth[3]         : %d\r\n", config->AntennaBeamwidth[3]));
    TRACE_LOG(("Gain[0]              : %d\r\n", config->AntennaGain[0]));
    TRACE_LOG(("Gain[1]              : %d\r\n", config->AntennaGain[1]));
    TRACE_LOG(("Gain[2]              : %d\r\n", config->AntennaGain[2]));
    TRACE_LOG(("Gain[3]              : %d\r\n", config->AntennaGain[3]));
    TRACE_LOG(("Max Tilt             : %d\r\n", config->AntennaMaxTilt));
    TRACE_LOG(("Min Tilt             : %d\r\n", config->AntennaMinTilt));
    TRACE_LOG(("Turn Ratio           : %ld\r\n", config->TurnRatio));
    TRACE_LOG(("Backlash             : %d\r\n", config->Backlash));
    TRACE_LOG(("Unwind               : %d\r\n", config->Unwind));

    memcpy(Pars.AntennaModelNumber, config->AntennaModelNumber, Parameter_Length(AntennaModelNumber));
    Pars.TurnRatio = config->TurnRatio;
    Pars.Unwind = config->Unwind;
    Pars.Backlash = config->Backlash;
    Pars.AntennaMinTilt = config->AntennaMinTilt;
    Pars.AntennaMaxTilt = config->AntennaMaxTilt;
    for (i=0; i< PARAMETER_N_FREQUENCY_BANDS; i++)
    {
        Pars.AntennaFrequencyBand[i] = config->AntennaFrequencyBand[i];
    }
    for (i=0; i< PARAMETER_N_BANDS; i++)
    {
        Pars.AntennaBeamwidth[i] = config->AntennaBeamwidth[i];
        Pars.AntennaGain[i] = config->AntennaGain[i];
    }

    Parameter_Write(AntennaModelNumber);
    Parameter_Process(AntennaModelNumber);
    if (strcmp(Pars.AntennaModelNumber, "Not Configured") == 0)
    {
        Pars.TurnRatio = 0;
        Parameter_Write(TurnRatio);
        Parameter_Process(TurnRatio);
        Alarm_Set(Not_Configured);
        i = (uint16_t)&Pars.AntennaFrequencyBand - (uint16_t)&Pars;
        length = (uint16_t)&Pars.Unwind - (uint16_t)&Pars.AntennaFrequencyBand;
        Eeprom_Clear(i, length);
    }
    else
    {
        Pars.NotCalibrated = true;
        Parameter_Write(TurnRatio);
        Parameter_Process(TurnRatio);
        Parameter_Write(Unwind);
        Parameter_Process(Unwind);
        Parameter_Write(Backlash);
        Parameter_Process(Backlash);
        Parameter_Write(AntennaMinTilt);
        Parameter_Process(AntennaMinTilt);
        Parameter_Write(AntennaMaxTilt);
        Parameter_Process(AntennaMaxTilt);
        Parameter_Write(AntennaFrequencyBand);
        Parameter_Process(AntennaFrequencyBand);
        Parameter_Write(AntennaBeamwidth);
        Parameter_Process(AntennaBeamwidth);
        Parameter_Write(AntennaGain);
        Parameter_Process(AntennaGain);
        Parameter_Write(NotCalibrated);
        Parameter_Process(NotCalibrated);
        Alarm_Clr(Not_Configured);
        Alarm_Set(Not_Calibrated);
    }

    Motor_Init();
    return (RESULT_SUCCESS);
}
