//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _BOOT_H_
#define _BOOT_H_

/*-------------------------------------------------------------------*/
/* Include File's List		                                         */
/*-------------------------------------------------------------------*/

#include <xc.h>         /* XC8 General Include File */
#include <stdint.h>     /* For uint8_t definition */
#include <stdbool.h>    /* For true/false definition */
#include <string.h>
#include <p18cxxx.h>
#include <flash.h>

//--------------------->"0123456789ABCDEF"<-------------------------------------
#define SW_VERSION      "[RET_BOOT_V10]";

#define SW_VERSION_LENGTH  15

//#define OSCILLATOR_PLL_ENABLED
#define SYS_FREQ                40000000L

// UARTx BAUDRATE (BRGH=1)
// UxBRG = ((((SYS_FREQ/BAUD_RATE)/4) - 1) = 0x0411
// DEBUG PORT BAUDRATE 115200
#define SPBRG1H_VAL              (0x00)
#define SPBRG1L_VAL              (0x56)

#define PUTCHAR(X)    while (TXSTA1bits.TRMT == 0); TXREG1 = X

// Flag to disable (TRACE_ON=0) or enable (TRACE_ON=1) debug.
#ifndef TRACE_ON
#define TRACE_ON 1
#endif

#if TRACE_ON
    #define PRINT_STRING(X) print_string(X)
    #define PRINT_HEX(X)    print_hex(X)
#else
    #define PRINT_STRING(X) 
    #define PRINT_HEX(X)    
#endif

#define APP_START               0x00000840
#define PROGRAM_MEMORY_START    (uint32_t)(0x00000800)
#define PROGRAM_SIZE            (uint32_t)(0x0000FC00)
#define PROGRAM_MEMORY_END      (uint32_t)(PROGRAM_SIZE + PROGRAM_MEMORY_START - 1)
#define UPLOAD_MEMORY_START     (uint32_t)(PROGRAM_SIZE + PROGRAM_MEMORY_START)
#define UPLOAD_MEMORY_END       (uint32_t)(PROGRAM_SIZE + UPLOAD_MEMORY_START - 1)
#define UPLOAD_ERASE_END        (uint32_t)(UPLOAD_MEMORY_END - FLASH_ERASE_BLOCK)
#define MAGIC_CHECKSUM          0x5A5A
#define FIRWMARE_VERSION        1

typedef struct
{
    uint8_t Type[5];   // CSSAP
    uint8_t Version;   // 0x01
    uint32_t Length;   // File Length
    uint8_t Flags;     // For future use
    uint8_t HwID;      // Hardware Id (compatible with board HwId)
    char FileName[32]; // Image Name
    uint8_t Spare[18]; // To align the header to 64 bytes (which is a flash block size)
    uint16_t Checksum; // Checksum
} file_header_t;       // (64 bytes)

typedef enum
{
    IMAGE_NOT_PRESENT = 0,
    IMAGE_VALID       = 1,
    IMAGE_CORRUPTED   = 2
} image_status_t;

void print_string (const char *str);

void print_hex (uint32_t value);

uint16_t Flash_Sector_Checksum (uint8_t *pdata);

void Boot_Copy_Image (void);

void Boot_Erase_Image (uint32_t ini_address, uint32_t end_address);

image_status_t Boot_Verify_Image (bool active);

#endif // _SYSTEM_H_
