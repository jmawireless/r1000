//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "boot.h"

volatile const uint8_t sw_version[SW_VERSION_LENGTH + 1] @ 0x0020 = SW_VERSION;

static uint8_t flash_array[FLASH_WRITE_BLOCK];
    
//############################################################################//
//
//############################################################################//
#asm
    PSECT intcode
    goto APP_START + 0x8
    PSECT intcodelo
    goto APP_START + 0x18
#endasm

#pragma config WDTEN = OFF     // Watchdog Timer Enable bit (WDT enabled)
#pragma config WDTPS = 64      // Watchdog Timer Postscale Select bits (1:1024)
#pragma config DEBUG = OFF
#pragma config XINST = OFF

// CONFIG1H
#pragma config FOSC  = HSPLL   // Oscillator Selection bits (HS oscillator)
#pragma config FCMEN = ON      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor enabled)
#pragma config IESO  = ON      // Internal/External Oscillator Switchover bit (Oscillator Switchover mode enabled)

//############################################################################//
//
//############################################################################//
void print_string (const char *str)
{
    char c;
    
    while (c = *str++)
    {
        PUTCHAR(c);
    }
}

//############################################################################//
//
//############################################################################//
void print_hex (uint32_t value)
{
    uint8_t nibble;
    char i = 8;
 
    PUTCHAR('0');
    PUTCHAR('x');
    
    do 
    {
        i--;
        nibble = (value >> i * 4 ) & 0x0000000f;
        if (nibble < 10)
            nibble += 48;
        else
            nibble += 55;
        PUTCHAR(nibble);
    } while(i);     
}

//############################################################################//
//
//############################################################################//
void Boot_Copy_Image (void)
{
    uint32_t ini_address = UPLOAD_MEMORY_START;
    uint32_t end_address = UPLOAD_MEMORY_END - FLASH_WRITE_BLOCK;

    PRINT_STRING("Write ");
    PRINT_HEX(ini_address);
    PUTCHAR(':');
    PRINT_HEX(end_address + 1);
    PRINT_STRING(" -> ");
    PRINT_HEX(ini_address - PROGRAM_SIZE);
    PUTCHAR(':');
    PRINT_HEX(end_address - PROGRAM_SIZE + 1);
    PRINT_STRING("\r\n");

    while (ini_address < end_address)
    {
        ReadFlash(ini_address, FLASH_WRITE_BLOCK, flash_array);
        WriteBlockFlash(ini_address - PROGRAM_SIZE, 1, flash_array);
        ini_address += FLASH_WRITE_BLOCK;
        CLRWDT();
    }
}

//############################################################################//
//
//############################################################################//
void Boot_Erase_Image (uint32_t ini_address, uint32_t end_address)
{
    PRINT_STRING("Erase ");
    PRINT_HEX(ini_address);
    PRINT_STRING(" -> ");
    PRINT_HEX(end_address + 1);
    PRINT_STRING("\r\n");

    while (ini_address < end_address)
    {
        EraseFlash(end_address - FLASH_ERASE_BLOCK + 1, end_address + 1);
        end_address -= FLASH_ERASE_BLOCK;
        CLRWDT();
    }
}

//############################################################################//
//
//############################################################################//
uint16_t Flash_Sector_Checksum (uint8_t *pdata)
{
    uint8_t i;
    uint16_t *pword = (uint16_t *)pdata;
    uint16_t crc = 0;

    for (i=0; i<FLASH_WRITE_BLOCK; i += 2)
    {
        crc += *pword++;
    }

    return (crc);
}

//############################################################################//
//
//############################################################################//
image_status_t Boot_Verify_Image (bool active)
{
    uint8_t i;
    uint32_t read_address;
    uint32_t end_address;
    file_header_t *header = (file_header_t *)flash_array;
    uint16_t flash_checksum;

    read_address = (active == true) ? PROGRAM_MEMORY_START : UPLOAD_MEMORY_START;
    ReadFlash(read_address, FLASH_WRITE_BLOCK, flash_array);
    
    for (i=0; i<FLASH_WRITE_BLOCK; i++)
    {
        if (flash_array[i] != 0xFF)
        {
            break;
        }
    }
    
    if (i == FLASH_WRITE_BLOCK)
    {
        return (IMAGE_NOT_PRESENT);
    }

    if ((header->Type[0] == 'C') && (header->Type[1] && 'C') &&
        (header->Type[2] == 'A') && (header->Type[3] && 'P') &&
        (header->Type[4] == 'P') && (header->Length != 0))
    {
        PRINT_STRING(header->FileName);

        read_address += FLASH_WRITE_BLOCK;
        end_address = read_address + header->Length;
        flash_checksum = header->Checksum;
        while (read_address < end_address)
        {
            ReadFlash(read_address, FLASH_WRITE_BLOCK, flash_array);
            read_address += FLASH_WRITE_BLOCK;
            flash_checksum += Flash_Sector_Checksum(flash_array);
            CLRWDT();
        }

        if (flash_checksum == MAGIC_CHECKSUM)
        {
            PRINT_STRING(" Good\r\n");
            return (IMAGE_VALID);
        }
    }
    PRINT_STRING(" Bad\r\n");
    return (IMAGE_CORRUPTED);
}

//############################################################################//
//
//############################################################################//
void main (void)
{
    uint16_t i;

    // Configure the oscillator for the device
    OSCCON = 0x48;
    OSCTUNE = 0x40; // HSPLL
    PIE2 = 0x00;
    PIR2 = 0x10;

    // Watchdog disabled for now
    SWDTEN = 0;

#if TRACE_ON
    // Initialize serial port
    TRISC7 = 1;             // Input  RX-PIN
    TRISC6 = 0;             // Output TX-PIN
    TXSTA1 = 0x26;
    RCSTA1 = 0x90;
    BAUDCON1 = 0x48;
    SPBRGH1 = SPBRG1H_VAL;
    SPBRG1 = SPBRG1L_VAL;

    // Wait a little for the uart to become ready
    for (i=0; i<1000; i++)   
    {
        CLRWDT();
    }
#endif
    
    PRINT_STRING((const char *)sw_version);
    PRINT_STRING("\r\n");
    
    switch (Boot_Verify_Image(false))
    {
        case IMAGE_NOT_PRESENT :
        break;
        
        case IMAGE_VALID :
            Boot_Erase_Image (PROGRAM_MEMORY_START, PROGRAM_MEMORY_END);
            Boot_Copy_Image();
            if (Boot_Verify_Image(true) == IMAGE_VALID)
            {
                Boot_Erase_Image (UPLOAD_MEMORY_START, UPLOAD_ERASE_END);
            }
        break;
        
        case IMAGE_CORRUPTED :
            Boot_Erase_Image (UPLOAD_MEMORY_START, UPLOAD_ERASE_END);
        break;
    }
	
    asm("goto " ___mkstr(APP_START));
}
