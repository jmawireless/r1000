//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//
#ifndef _TRACE_H_
#define _TRACE_H_

// Flag to disable (DBG=0) or enable (DBG=1) debug.
#ifndef DBG
#define DBG 0
#endif

// Trace level bitmask definitions

// None
#define TRACE_LEVEL_NONE 0
// Errors
#define TRACE_LEVEL_ERR (1 << 0)
// Informations
#define TRACE_LEVEL_LOG (1 << 1)
// Debug
#define TRACE_LEVEL_DBG (1 << 2)
// All
#define TRACE_LEVEL_ALL (TRACE_LEVEL_ERR | TRACE_LEVEL_LOG | TRACE_LEVEL_DBG)

#ifndef TRACE_LEVEL
/**
 * Global debug output level.
 *
 * It is a bitmask that sets which debug info will be emmitted. E.g.
 * - #TRACE_LEVEL = TRACE_LEVEL_NONE : No debug output
 * - #TRACE_LEVEL = TRACE_LEVEL_ERR  : Report errors only
 * - #TRACE_LEVEL = TRACE_LEVEL_ERR | TRACE_LEVEL_LOG) : Report errors + informations
 */
#define TRACE_LEVEL TRACE_LEVEL_ERR
#endif

#define __CLOCK__ Clock_Get_Time()

/* _____TYPE DEFINITIONS_____________________________________________________ */

/* _____MACROS_______________________________________________________________ */
#if DBG

// 1st part macro to convert a number to a string (GCC specific)
#define _DBG_STRINGIFY(number) #number

// 2nd part macro to convert a number to a string (GCC specific)
#define _DBG_NR_TO_STR(number) _DBG_STRINGIFY(number)

extern uint8_t trace_level;

#define TRACE_ERR(msg)                                              \
    do                                                              \
    {                                                               \
        if (trace_level & TRACE_LEVEL_ERR)                          \
        {                                                           \
            printf("[ERR F:%.4s L:%.4d T:%.8lu] ",                  \
                   __FILE__, __LINE__, __CLOCK__);                  \
            printf msg;                                             \
        }                                                           \
    } while (0)

#define TRACE_LOG(msg)                                              \
    do                                                              \
    {                                                               \
        if (trace_level & TRACE_LEVEL_LOG)                          \
        {                                                           \
            printf("[LOG F:%.4s L:%.4d T:%.8lu] ",                  \
                   __FILE__, __LINE__, __CLOCK__);                  \
            printf msg;                                             \
            CLRWDT();                                               \
        }                                                           \
    } while (0)
/*
#define TRACE_DBG(msg)                                              \
    do                                                              \
    {                                                               \
        if (trace_level & TRACE_LEVEL_DBG)                          \
        {                                                           \
            printf("[DBG F:%.4s L:%.4d T:%.8lu] ",                  \
                   __FILE__, __LINE__, __CLOCK__);                  \
            printf msg;                                             \
        }                                                           \
    } while (0)
*/
#define TRACE_DBG(msg)

#define TRACE_PUT(msg)                                              \
    do                                                              \
    {                                                               \
        if (trace_level & TRACE_LEVEL_DBG)                          \
        {                                                           \
            printf msg;                                             \
            CLRWDT();                                               \
        }                                                           \
    } while (0)

#else

#define TRACE_ERR(msg)

#define TRACE_LOG(msg)

#define TRACE_DBG(msg)

#define TRACE_PUT(msg)

#endif

void Trace_Init (void);

void Trace_Frame (bool is_rx, uint8_t *buffer, uint8_t length);

#endif // _TRACE_H_
