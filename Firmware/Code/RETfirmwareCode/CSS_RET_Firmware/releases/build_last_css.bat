:: ######################################################
:: Parameters
:: Leave PRODUCT="" or HWVERSION="" or SWVERSION=""
:: for tag not needed
:: ######################################################
echo OFF

set PRODUCT="CSS RET-200" 
set HWVERSION="HW" 
set SWVERSION="" 

ECHO off

:: Get the file containing the version information
SET INPUT_FILE=%CD%\..\typedef.h

SETLOCAL enabledelayedexpansion
FOR /f "tokens=2* delims= " %%a IN ('type "%INPUT_FILE%" ^| FIND /i "#define SW_VERSION "') DO (
																   SET VERSION_STR=%%b)
																   
:: Remove quotes from version string
SET VERSION_STR=%VERSION_STR:"=%

:: Remove ';' from version string
SET VERSION_STR=%VERSION_STR:;=%
SET FILENAME=%VERSION_STR%

:: Copying latest build file in releases folder
SET BUILD_FILE=%CD%\..\dist\default\production\CSS_RET_Firmware.production.hex
COPY /B %BUILD_FILE% %FILENAME%.hex

:: ######################################################
:: .css file generation
:: ######################################################

:: ######################################################
:: Convert hex to binary
:: ######################################################
hex2bin.exe -l 07D00 -e css %FILENAME%.hex

:: ######################################################
:: Compute checksum
:: Override file with valid checksum if it was invalid
:: Checksum is the 16-bit sum of the file without last 2
:: bytes 2-complemented with the magic word 0x5A5A
:: Last 2 bytes in the file are updated with the checksum
:: Therefore is Checksum is computed on the entire file 
:: it should return the magic word0x5A5A
:: ######################################################
checksum_builder.exe %FILENAME%.css

:: ######################################################
:: .xml file generation
:: ######################################################
FirmwareDownloadXmlEncapsulator.exe %FILENAME%.css %PRODUCT% %HWVERSION% %SWVERSION%
