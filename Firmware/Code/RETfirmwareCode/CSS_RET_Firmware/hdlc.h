//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _HDLC_H_
#define _HDLC_H_

#include "system.h"

//  HDLC frames defines

#define HDLC_MRU                        (128)       // [TODO] 74 x 2 ???

#define HDLC_FLAGS_LENGTH				(2)			// Address + Control

#define HDLC_FCS_LENGTH					(2)         // 16bits FCS

#define HDLC_MAX_FRAME_LENGTH			(HDLC_FLAGS_LENGTH + MAX_HDLC_FRAME_LENGTH + 2)

#define HDLC_FLAG_SEQUENCE				(0x7E)		// Flag Sequence

#define HDLC_CONTROL_ESCAPE				(0x7D)		// Control Escape

#define HDLC_ESCAPE_BIT					(0x20)		// Transparency Modifier

// AISG HDLC Control Fields
#define I_FRAME_MODULUS					(8)

#define HDLC_WINDOWSIZE                 (1)

#define IncrementFrameCounter(X)		(X = (++X % I_FRAME_MODULUS))

// -----------------------------------------------------------------------------
// AISG is a subset of HDLC. It is HDLC "UNC1,15.1, TWA"
// 'U' means Unbalanced operation (master slave operation)
// 'N' means Normal response mode (sequence numbers used in data frames)
// 'C' means Class
// '1' means additional support for XID negotiation
// "15.1" means use of start/stop transmission with basic transparency
// "TWA" (Two Way Alternate) means half duplex
// -----------------------------------------------------------------------------
typedef enum
{
    I_FRAME        = 0x00       ,
    S_FRAME_RR     = 0x00 | 0x01, // bits 0 must be 1, bit 1 must be 0,
    S_FRAME_RNR    = 0x04 | 0x01,
    U_FRAME_UI     = 0x00 | 0x03, // bits 0-1 must be 1
    U_FRAME_XID    = 0xAC | 0x03,
    U_FRAME_SNRM   = 0x80 | 0x03,
    U_FRAME_DISC   = 0x40 | 0x03,
    U_FRAME_SIM    = 0x04 | 0x03,
    U_FRAME_UA     = 0x60 | 0x03,
    U_FRAME_FRMR   = 0x84 | 0x03,
    U_FRAME_DM     = 0x0C | 0x03,
    U_FRAME_UIH    = 0xEC | 0x03,
    INVALID_FRAME  = 0xFF
} hdlc_frame_type_t;

#define POLL_FINAL_BIT                          (1 << 4);  // P/F field is bit 4

#define UFRAME_FORMAT_ID                        0x81

#define UFRAME_USER_GROUP_ID                    0xF0

#define UFRAME_HDLC_GROUP_ID                    0x80

// USER Parameters
#define UFRAME_PARAMETER_ID_UNIQUE              0x01

#define UFRAME_PARAMETER_ID_ADDRESS             0x02

#define UFRAME_PARAMETER_ID_BITMASK             0x03

#define UFRAME_PARAMETER_ID_DEVICE              0x04

#define UFRAME_PARAMETER_ID_3GPPRELEASE         0x05

#define UFRAME_PARAMETER_ID_VENDOR              0x06

#define UFRAME_PARAMETER_ID_RESET               0x07

#define UFRAME_PARAMETER_ID_VERSION             0x09

#define UFRAME_PARAMETER_ID_PROTOCOL            0x14

#define UFRAME_PARAMETER_ID_DEVTYPESUBVERSION   0x15

// HDLC Parameters
#define UFRAME_PARAMETER_ID_MAX_IFRAMEBITSTX    0x05

#define UFRAME_PARAMETER_ID_MAX_IFRAMEBITSRX    0x06

#define UFRAME_PARAMETER_ID_MAX_WINDOWSIZETX    0x07

#define UFRAME_PARAMETER_ID_MAX_WINDOWSIZERX    0x08

#define RELEASE_3GPP                            0x07

#define DEVTYPESUB_VERSION                      0x00

#define Hdlc_Tx_Byte Uart_Send_Byte

extern uint8_t hdlc_tx_buffer[HDLC_MRU];

extern uint8_t hdlc_tx_buffer_length;

void Hdlc_Init();

hdlc_frame_type_t Hdlc_Get_Frame_Type (uint8_t ctrl);

uint8_t Hdlc_Get_PF (uint8_t ctrl);

uint8_t Hdlc_Get_NR (uint8_t ctrl);

uint8_t Hdlc_Get_NS (uint8_t ctrl);

void Hdlc_Set_NS (uint8_t *ctrl, uint8_t value);

void Hdlc_Set_NR (uint8_t *ctrl, uint8_t value);

void Hdlc_Put_Char (char data);

bool Hdlc_On_Rx_Byte (uint8_t data);

void Hdlc_On_Rx_Frame (uint8_t *buffer, uint16_t bytes_received);

void Hdlc_Tx_Frame (uint8_t *buffer, uint8_t bytes_to_send);

return_t Hdlc_Process_Xid_Request      (hdlc_frame_t *pRxFrame, uint8_t length);

return_t Hdlc_Process_Xid_User_Request (hdlc_frame_t *pRxFrame, uint8_t length);

return_t Hdlc_Process_Xid_Hdlc_Request (hdlc_frame_t *pRxFrame, uint8_t length);

return_t Hdlc_Process_Snrm_Request     (hdlc_frame_t *pRxFrame, uint8_t length);

return_t Hdlc_Process_Disc_Request     (hdlc_frame_t *pRxFrame, uint8_t length);

return_t Hdlc_Process_Rr_Request       (hdlc_frame_t *pRxFrame, uint8_t length);

return_t Hdlc_Process_Info_Request     (hdlc_frame_t *pRxFrame, uint8_t length);

bool Aisg_Scan_Match (unique_id_t uid, unique_id_t msk, uint8_t id_length);

bool Aisg_Address_Match (unique_id_t uid, unique_id_t msk, uint8_t id_length);

return_t Hdlc_Prepare_Xid_Scan_Response (void);

return_t Hdlc_Prepare_Xid_Address_Response (void);

return_t Hdlc_Prepare_Xid_Reset_Response (void);

return_t Hdlc_Prepare_Xid_Versions_Response (bool protocol, bool release3gpp, bool devtypesub);

return_t Hdlc_Prepare_Xid_Hdlc_Parameters_Response (void);

return_t Hdlc_Prepare_Snrm_Response (void);

return_t Hdlc_Prepare_Disc_Response (void);

return_t Hdlc_Prepare_Rr_Response (void);

return_t Hdlc_Prepare_Rnr_Response (void);

return_t Hdlc_Prepare_Info_Response (info_frame_t *info_frame_tx);

#endif // _HDLC_H_
