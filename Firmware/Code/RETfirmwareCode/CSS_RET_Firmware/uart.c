//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define TX_BUFFER_LENGTH 128
#define RX_BUFFER_LENGTH 128

uint8_t rx_data[RX_BUFFER_LENGTH];
uint8_t tx_data[TX_BUFFER_LENGTH];
buffer_t rx_buffer;
buffer_t tx_buffer;

//############################################################################//
//
//############################################################################//
bool Uart_Send_Byte (uint8_t data)
{
    while (Buffer_Write(&tx_buffer, data) == false);

    PIE1bits.TX1IE = 0;
    if (TXSTA1bits.TXEN == 0)
        TXSTA1bits.TXEN = 1;
    PIE1bits.TX1IE = 1;

    return (true);
}

//############################################################################//
//
//############################################################################//
bool Uart_Read_Byte (uint8_t *data)
{
    return (Buffer_Read(&rx_buffer, data));
}

//############################################################################//
//
//############################################################################//
void Uart_Init (void)
{
    TRISC7 = 1;             // Input  RX-PIN
    TRISC6 = 0;             // Output TX-PIN
    TRISC5 = 0;             // Output TX-ENABLE
    
    PORTCbits.RC5 = 0;

    TXSTA1bits.TX9    = 0;
    TXSTA1bits.TXEN   = 0;
    TXSTA1bits.SYNC   = 0;
    TXSTA1bits.SENDB  = 0;
#ifndef OSCILLATOR_PLL_ENABLED
    TXSTA1bits.BRGH   =	1;
#else
    TXSTA1bits.BRGH   =	0;
#endif
    RCSTA1bits.SPEN   = 1;
    RCSTA1bits.RX9    =	0;
    RCSTA1bits.CREN   = 1;
    RCSTA1bits.ADDEN  =	0;

    BAUDCONbits.RXDTP = 0;
    BAUDCONbits.TXCKP = 0;
    BAUDCONbits.BRG16 = 0;
    BAUDCONbits.WUE   = 0;
    BAUDCONbits.ABDEN = 0;

    SPBRGH = 0;
    SPBRG = SPBRG_VAL;

    Buffer_Init(&rx_buffer, rx_data, RX_BUFFER_LENGTH);
    Buffer_Init(&tx_buffer, tx_data, TX_BUFFER_LENGTH);

    // Interrupt enable
    PIE1bits.TX1IE    = 1;
    PIE1bits.RC1IE    = 1;
    IPR1bits.RC1IP    = 1;
    IPR1bits.TX1IP    = 1;

    RCONbits.IPEN     = 1;
    INTCONbits.GIE    = 1;
    INTCONbits.PEIE   = 1;
}

//############################################################################//
//
//############################################################################//
void Uart_Interrupt_Handler (void)
{
    uint8_t toTransmit, readed;

    if ((PIR1bits.TX1IF == 1) && (PIE1bits.TX1IE == 1))
    {
        if (Buffer_Read(&tx_buffer, &toTransmit) == true)
        {
            TXREG1 = toTransmit;
        }
        else
        {
            if (TXSTA1bits.TRMT)
            {
                TXSTA1bits.TXEN = 0;
            }
        }
    }

    if ((PIR1bits.RC1IF == 1) && (PIE1bits.RC1IE == 1))
    {
        readed = RCREG1;
        Buffer_Write(&rx_buffer, readed);
        PIR1bits.RC1IF = 0;
    }

    // Clear overrun error if it has occurred
    // New bytes cannot be received if the error occurs and isn't cleared
    if (OERR)
    {
        CREN = 0;   // Disable UART receiver
        CREN = 1;   // Enable UART receiver
    }
}
