//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#include "system.h"

#define PARAMETER_N_BANDS		    			     4
#define PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH     	15
#define PARAMETER_ANTENNA_SERIAL_NUMBER_LENGTH    	17
#define PARAMETER_OPERATOR_INSTALL_DATE_LENGTH     	 6
#define PARAMETER_OPERATOR_INSTALL_ID_LENGTH         5
#define PARAMETER_OPERATOR_BASESTATION_ID_LENGTH   	32
#define PARAMETER_OPERATOR_SECTOR_ID_LENGTH        	32
#define CONFIG_DATA_VERSION                          1
#define CONFIG_DATA_TYPE                       "CCACF"
#define CONFIG_DATA_TYPE_LEN                         5

#define Parameter_Write(X)       pars.flags.update.X = 1

#define Parameter_Length(X)      sizeof(pars.X)

#define Parameter_Process(X)                                    \
{                                                               \
    if (pars.flags.update.X == 1)                               \
    {                                                           \
        Eeprom_Update((uint8_t *)(&(pars.X)),                   \
                      Parameter_Length(X));                     \
        pars.flags.update.X = 0;                                \
    }                                                           \
}

typedef union
{
    uint32_t value;

    struct
    {
        unsigned char AntennaModelNumber:1;
        unsigned char SerialNumber:1;
        unsigned char AntennaFrequencyBand:1;
        unsigned char AntennaBeamwidth:1;
        unsigned char AntennaGain:1;
        unsigned char AntennaMaxTilt:1;
        unsigned char AntennaMinTilt:1;
        unsigned char ProductNumber:1;
        unsigned char AntennaSerialNumber:1;
        unsigned char OperatorInstallDate:1;
        unsigned char OperatorInstallId:1;
        unsigned char OperatorBasestationId:1;
        unsigned char OperatorSectorId:1;
        unsigned char OperatorAntennaBearing:1;
        unsigned char OperatorMechanicalTilt:1;
        unsigned char Spare0:1;
        unsigned char NotCalibrated:1;
        unsigned char TurnRatio:1;
        unsigned char Tilt:1;
        unsigned char MotorPosition:1;
        unsigned char Backlash:1;
        unsigned char LastDirection:1;
        unsigned char Unwind:1;
        unsigned char Spar:1;
        unsigned char Spare2:8;
    } update;
} parameters_update_t;

typedef struct
{
    char AntennaModelNumber[PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH];
    char SerialNumber[PARAMETER_ANTENNA_SERIAL_NUMBER_LENGTH];
    uint16_t AntennaFrequencyBand;
	uint16_t AntennaBeamwidth[PARAMETER_N_BANDS];
    uint8_t AntennaGain[PARAMETER_N_BANDS];
	int16_t AntennaMaxTilt;
    int16_t AntennaMinTilt;
    char ProductNumber[PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH];
    char AntennaSerialNumber[PARAMETER_ANTENNA_SERIAL_NUMBER_LENGTH];
    uint8_t Spare0[12];
    char OperatorInstallDate[PARAMETER_OPERATOR_INSTALL_DATE_LENGTH];
    char OperatorInstallId[PARAMETER_OPERATOR_INSTALL_ID_LENGTH];
    char OperatorBasestationId[PARAMETER_OPERATOR_BASESTATION_ID_LENGTH];
    char OperatorSectorId[PARAMETER_OPERATOR_SECTOR_ID_LENGTH];
    uint16_t OperatorAntennaBearing;
    int16_t OperatorMechanicalTilt;
    uint8_t Spare1;
    uint8_t NotCalibrated;
    int16_t TurnRatio;
    int16_t Tilt;
    int16_t MotorPosition;
    int16_t Backlash;
    uint8_t LastDirection;
    int8_t Unwind;
    int8_t Spare2;
    parameters_update_t flags;
} parameters_t;

typedef struct
{
	BYTE Type[5];
	BYTE Version;
    BYTE Flags;
	uint16_t Length;
    char ProductNumber[PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH];
	char AntennaModelNumber[PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH];
	uint16_t AntennaFrequencyBand;
	uint16_t AntennaBeamwidth[PARAMETER_N_BANDS];
	uint8_t AntennaGain[PARAMETER_N_BANDS];
	int16_t AntennaMaxTilt;
	int16_t AntennaMinTilt;
	int16_t TurnRatio;
	int16_t Backlash;
	int8_t Unwind;
	uint16_t Checksum;
} config_data_file_t; // (64 bytes)

extern parameters_t pars;

return_t Parameters_Init (void);

return_t Parameters_Print (void);

return_t Eeprom_Update (uint8_t *pData, uint8_t Length);

return_t Parameters_Send_Config_Data (uint8_t *pData, uint8_t length);

return_t Parameters_Process_Config_Data (void);

return_t Parameters_Config_Data_Completed (return_t result);

#endif // _PARAMETERS_H_