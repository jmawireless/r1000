//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

//############################################################################//
// Initializes I2C in master mode, Sets the required baudrate
//############################################################################//
void I2C_Init (void)
{
	TRISC3 = 1;         /* SDA and SCL as input pin */
	TRISC4 = 1;         /* these pins can be configured either i/p or o/p */
	SSPSTAT |= 0x80;    /* Slew rate disabled */
	SSPCON1  = 0x28;    /* SSPEN = 1, I2C Master mode, clock = FOSC/(4 * (SSPADD + 1)) */
	SSPADD   = 0x28;    /* 100Khz @ 4Mhz Fosc */
}

//############################################################################//
// Sends a start condition on I2C Bus
//############################################################################//
void I2C_Start (void)
{
	SEN = 1;         /* Start condition enabled */
	while(SEN);      /* wait for start condition to finish */
                     /* SEN automatically cleared by hardware */
}

//############################################################################//
// Sends a stop condition on I2C Bus
//############################################################################//
void I2C_Stop (void)
{
	PEN = 1;         /* Stop condition enabled */
	while(PEN);      /* Wait for stop condition to finish */
                     /* PEN automatically cleared by hardware */
}

//############################################################################//
// Sends a repeated start condition on I2C Bus
//############################################################################//
void I2C_Restart (void)
{
	RSEN = 1;        /* Repeated start enabled */
	while(RSEN);     /* wait for condition to finish */
}

//############################################################################//
// Generates acknowledge for a transfer
//############################################################################//
void I2C_Ack (void)
{
	ACKDT = 0;       /* Acknowledge data bit, 0 = ACK */
	ACKEN = 1;       /* Ack data enabled */
	while(ACKEN);    /* wait for ack data to send on bus */
}

//############################################################################//
// Generates Not-acknowledge for a transfer
//############################################################################//
void I2C_Nack (void)
{
	ACKDT = 1;       /* Acknowledge data bit, 1 = NAK */
	ACKEN = 1;       /* Ack data enabled */
	while(ACKEN);    /* wait for ack data to send on bus */
}

//############################################################################//
// Waits for transfer to finish
//############################################################################//
void I2C_Wait (void)
{
	while ((SSPCON2 & 0x1F) || (SSPSTAT & 0x04));
    /* wait for any pending transfer */
}

//############################################################################//
// Sends 8-bit data on I2C bus
//############################################################################//
void I2C_Write (uint8_t dat)
{
	SSPBUF = dat;    /* Move data to SSPBUF */
	while(BF);       /* wait till complete data is sent from buffer */
	I2C_Wait();      /* wait for any pending transfer */
}

//############################################################################//
// Reads 8-bit data from I2C bus
//############################################################################//
uint8_t I2C_Read (bool ack)
{
	uint8_t data;

    /* Reception works if transfer is initiated in read mode */
	RCEN = 1;        /* Enable data reception */
	while(!BF);      /* wait for buffer full */
	data = SSPBUF;   /* Read serial buffer and store in temp register */
	I2C_Wait();      /* wait to check any pending transfer */
    if (ack)
        I2C_Ack();
    
	return data;     /* Return the read data from bus */
}

//############################################################################//
// Clears i2c if stuck
//############################################################################//
void I2C_Clear (void)
{
    uint8_t sspcon1_orig;
    uint8_t i;

	sspcon1_orig = SSPCON1;
	SSPCON1 = 0x00;
	TRISC3 = 0;

    for (i=0; i<16; i++) /* clear the I2C bus */
	{
		PORTCbits.RC3 = ~PORTCbits.RC3;
		__delay_us(5);
	}
	TRISC3 = 1;
	SSPCON1 = sspcon1_orig;
    __delay_ms(10);
}

