//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

// TX Bit Delay = ((((2*4*4Mhz)/(4*57600))+1)/2)-12 cycles = 57.94
#define SW_UART_DELAY       58
#define SW_UART_BAUDRATE    57600
#define Delay_TXBitUART() __delay_us(SW_UART_DELAY) //196.33 @ 4800

//############################################################################//
//
//############################################################################//
void Trace_Init (void)
{
    TRISBbits.TRISB1 = 0x00;
	PORTBbits.RB1 = 0;
}

//############################################################################//
//
//############################################################################//
void putch (char ch)
{
	unsigned char bitcount = 8;

    // Disable interrupts
    INTCONbits.GIEH = 0;

    // Start
    PORTBbits.RB1 = 0;
    Delay_TXBitUART();

	while (bitcount--)
    {
        PORTBbits.RB1 = ch & 0x01;
		Delay_TXBitUART();
		ch >>= 1;
	}

	// Stop
    PORTBbits.RB1 = 1;
	Delay_TXBitUART();

    // Enable interrupts
    INTCONbits.GIEH = 1;
}

//############################################################################//
//
//############################################################################//
void Trace_Frame (bool is_rx, uint8_t *buffer, uint8_t length)
{
#if 0
    if (is_rx == true)
    {
        TRACE_LOG(("RX [%.2d] ", length));
    }
    else
    {
        TRACE_LOG(("TX [%.2d] ", length));
    }

    while (length != 0)
    {
        TRACE_PUT(("%02X ", *buffer++));
        length--;
    }
    TRACE_PUT(("\r\n"));
#endif
}
