//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#if DBG
static uint32_t clockCount = 0;
static uint32_t clockAccess = 0;

//############################################################################//
//
//############################################################################//
void Clock_Tick (void)
{
    // update the clock tick and second counters

    // Use the non-blocking write protocol to ensure data consistency
    ++clockAccess;
    ++clockCount;
    ++clockAccess;
}

//############################################################################//
//
//############################################################################//
uint32_t Clock_Get_Time()
{
    uint32_t beginAccess, endAccess;
    uint32_t currentTime;

    // Use the non-blocking write protocol to ensure data consistency
    do 
	{
        beginAccess = clockAccess;
        currentTime = clockCount;
        endAccess = clockAccess;
    } while (beginAccess != endAccess);

    return currentTime;
}

#endif
