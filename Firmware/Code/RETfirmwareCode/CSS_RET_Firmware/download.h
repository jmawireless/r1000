//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _DOWNLOAD_H_
#define _DOWNLOAD_H_

#include "system.h"

#define BOOT_SIZE               0x00000400
#define BOOT_END                (BOOT_SIZE - 1)

#define PROGRAM_SIZE            0x00007D00
#define PROGRAM_MEMORY_START    (BOOT_END + 1)
#define PROGRAM_MEMORY_END      (PROGRAM_SIZE + PROGRAM_MEMORY_START - 1)

#define UPLOAD_SIZE             0x00007D00
#define UPLOAD_MEMORY_START     (PROGRAM_MEMORY_END + 1)
#define UPLOAD_MEMORY_END       (UPLOAD_SIZE + UPLOAD_MEMORY_START - 1)

#define FLASH_BLOCK_SIZE        64

#define MAGIC_CRC               0x5A5A

extern uint16_t flash_crc;

extern buffer_t dwn_buffer;

extern bool dwn_error;

void Download_Init (void);

return_t Download_Write_Block (uint32_t address, uint8_t *pdata);

bool Download_Is_Block_Empty (uint32_t address);

uint16_t Block_Checksum (uint8_t *pdata);

#endif // _DOWNLOAD_H_