//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _ALARM_H_
#define _ALARM_H_

#include "system.h"

typedef union alarm_union
{
    uint8_t value;
    struct
    {
        uint8_t Hardware_Error:1;          // AISG: Other Hardware Error
        uint8_t Not_Configured:1;          // AISG: Not Scaled
        uint8_t Not_Calibrated:1;          // AISG: Not Calibrated
        uint8_t Actuator_Detection_Fail:1; // AISG: Actuator Detection Fail
        uint8_t Actuator_Jam_Permanent:1;  // AISG: Actuator Jam Permanent
        uint8_t Actuator_Jam_Temporary:1;  // AISG: Actuator Jam Temporary
        uint8_t Position_Lost:1;           // AISG: Position Lost
        uint8_t Actuator_Interference:1;   // AISG: Actuator Interference
    } bits;
} alarm_t;

typedef struct
{
    alarm_t current;
    alarm_t previous;
    alarm_t cleared;
    alarm_t raised;
    return_t last;
    bool subscribed;
} alarm_db_t;

extern alarm_db_t alarm;

return_t Alarm_Init (void);

bool Alarm_Waiting_To_Be_Reported (void);

return_t Alarm_Subscribe (void);

return_t Alarm_Notify (void);

return_t Alarm_Clear (void);

return_t Alarm_Get (void);

return_t Alarm_Get_Cleared (void);

return_t Alarm_Get_Raised (void);

return_t Alarm_Map (alarm_t alm);

#define Alarm_Is_Active(X) alarm.current.bits.X == 1

#define Alarm_Set(X) alarm.current.bits.X = 1

#define Alarm_Clr(X) alarm.current.bits.X = 0

#endif //_ALARM_H_
