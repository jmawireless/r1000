//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _AISG_H_
#define _AISG_H_

#include "system.h"


#define Aisg_Set_Tx_Mode() PORTCbits.RC5 = 1;

#define Aisg_Set_Rx_Mode() PORTCbits.RC5 = 0;

#define AISG_MAX_DEVICE_DATA_LENGTH         (32)
#define AISG_MAX_ALARMS					     (2)
#define AISG_STRING_LENGTH                  (32)

//############################################################################//
// aisg_v20 generic command and response
//############################################################################//
typedef struct
{
	header_t header;
} aisg_v20_cmd_t;

typedef struct
{
	header_t header;
    return_t return_code;
} aisg_v20_rsp_t;

//############################################################################//
// aisg_v20_reset
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_reset_cmd_t;

typedef struct
{
	header_t header;
    return_t return_code;
} aisg_v20_reset_rsp_t;

//############################################################################//
// aisg_v20_get_alarms
//############################################################################//
typedef struct
{
	header_t header;
} aisg_v20_get_alarms_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
    uint8_t alarm;
//	uint8_t alarms[AISG_MAX_ALARMS];
} aisg_v20_get_alarms_rsp_t;

//############################################################################//
// aisg_v20_get_info
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_get_info_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
    uint8_t product_number_length;
    uint8_t product_number[PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH];
    uint8_t serial_number_length;
    uint8_t serial_number[PARAMETER_ANTENNA_SERIAL_NUMBER_LENGTH];
    uint8_t hardware_version_length;
    uint8_t hardware_version[HW_VERSION_LENGTH];
    uint8_t software_version_length;
    uint8_t software_version[SW_VERSION_LENGTH];
} aisg_v20_get_info_rsp_t;

//############################################################################//
// aisg_v20_clear_alarms
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_clear_alarms_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_clear_alarms_rsp_t;

//############################################################################//
// aisg_v20_read_user_data
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_read_user_data_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_read_user_data_rsp_t;

//############################################################################//
// aisg_v20_write_user_data
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_write_user_data_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_write_user_data_rsp_t;

//############################################################################//
// aisg_v20_subscribe_alarms
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_subscribe_alarms_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_subscribe_alarms_rsp_t;

//############################################################################//
// aisg_v20_self_test
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_self_test_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_self_test_rsp_t;

//############################################################################//
// aisg_v20_vendor_specific
//############################################################################//
typedef struct
{
	header_t header;
    uint8_t serial_number[PARAMETER_ANTENNA_SERIAL_NUMBER_LENGTH];
    uint8_t product_number[PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH];
} aisg_v20_vendor_specific_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
} aisg_v20_vendor_specific_rsp_t;

//############################################################################//
// aisg_v20_download_start
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_download_start_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_download_start_rsp_t;

//############################################################################//
// aisg_v20_download_appl
//############################################################################//
typedef struct
{
	header_t header;
    uint8_t data[MAX_AISG_FRAME_LENGTH];
} aisg_v20_download_appl_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_download_appl_rsp_t;

//############################################################################//
// aisg_v20_download_end
//############################################################################//
typedef struct 
{
	header_t header;
} aisg_v20_download_end_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
} aisg_v20_download_end_rsp_t;

//############################################################################//
// aisg_v20_sret_calibrate
//############################################################################//
typedef struct 
{
	header_t header;
}  aisg_v20_sret_calibrate_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
} aisg_v20_sret_calibrate_rsp_t;

//############################################################################//
// aisg_v20_sret_send_config_data
//############################################################################//
typedef struct
{
	header_t header;
    uint8_t data[MAX_AISG_FRAME_LENGTH];
} aisg_v20_sret_send_config_cmd_t;

typedef struct 
{
	header_t header;
	return_t return_code;
}  aisg_v20_sret_send_config_rsp_t;

//############################################################################//
// aisg_v20_sret_set_tilt
//############################################################################//
typedef struct
{
	header_t header;
    int16_t tilt;
}  aisg_v20_sret_set_tilt_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
} aisg_v20_sret_set_tilt_rsp_t;

//############################################################################//
// aisg_v20_sret_get_tilt
//############################################################################//
typedef struct
{
	header_t header;
} aisg_v20_sret_get_tilt_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
    int16_t tilt;
} aisg_v20_sret_get_tilt_rsp_t;

//############################################################################//
// aisg_v20_sret_alarm_indication
//############################################################################//
typedef struct
{
	header_t header;
} aisg_v20_sret_alarm_indication_cmd_t;

typedef struct
{
	header_t header;
    struct
	{
		return_t return_code;
		uint8_t status;
	} alarms[AISG_MAX_ALARMS];
} aisg_v20_sret_alarm_indication_rsp_t;

//############################################################################//
// aisg_v20_sret_set_device_data
//############################################################################//
typedef struct
{
	header_t header;
    datafield_t field;
    uint8_t data[AISG_MAX_DEVICE_DATA_LENGTH];
} aisg_v20_sret_set_device_data_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
} aisg_v20_sret_set_device_data_rsp_t;

//############################################################################//
// aisg_v20_sret_get_device_data
//############################################################################//
typedef struct
{
	header_t header;
	datafield_t field;
} aisg_v20_sret_get_device_data_cmd_t;

typedef struct
{
	header_t header;
	return_t return_code;
    uint8_t data[AISG_MAX_DEVICE_DATA_LENGTH];
} aisg_v20_sret_get_device_data_rsp_t;

typedef return_t (*function_t)           (info_frame_t *cmd, info_frame_t *rsp);

enum
{
    PAR_MODEL       = 0x00,
    PAR_TURN_RATIO  = 0x01,
    PAR_UNWIND 	    = 0x02,
    PAR_BACKLASH    = 0x03,
    PAR_MIN_TILT    = 0x04,
    PAR_MAX_TILT    = 0x05,
    PAR_FREQ_BANDS  = 0x06,
    PAR_BEAMWIDTH   = 0x07,
    PAR_GAIN	    = 0x08,
    DATA_SEPERATOR  = 0x0A
};

typedef struct
{
	l7_commands_t command;
    uint8_t flag;
    function_t function;
} l7_handler_t;

typedef struct
{
    bool in_progress;
    bool completed;
	l7_commands_t command;
    return_t return_code;
} l7_tcp_t;

return_t Aisg_Init (void);

function_t Aisg_Get_Command_Handler (l7_commands_t command);

return_t AISG_V20_Build_Error_Response   (info_frame_t *rsp, return_t error);

return_t AISG_V20_Reset                  (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Get_Alarms             (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Get_Info               (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Clear_Alarms           (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Read_User_Data         (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Write_User_Data        (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Subscribe_Alarms       (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Self_Test              (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Vendor_Specific        (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Download_Start         (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Download_Appl          (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_Download_End           (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_SRet_Calibrate         (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_SRet_Send_Config       (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_SRet_Set_Tilt          (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_SRet_Get_Tilt          (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_SRet_Alarm_Indication  (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_SRet_Set_Device_Data   (info_frame_t *cmd, info_frame_t *rsp);

return_t AISG_V20_SRet_Get_Device_Data   (info_frame_t *cmd, info_frame_t *rsp);

extern l7_tcp_t last_tcp;

extern bool aisg_3ms_timer_enabled;

extern uint8_t aisg_3ms_timer_count;

#endif // _AISG_H_
