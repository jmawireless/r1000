//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

//############################################################################//
// Global Variable Declaration
//############################################################################//

volatile const uint8_t sw_version[SW_VERSION_LENGTH + 1] @ 0x04B0 = SW_VERSION;
volatile const uint8_t hw_version[HW_VERSION_LENGTH + 1] @ 0x04C0 = HW_VERSION;

bool reset_connection_timer = false;
bool download_reset_request = false;
bool layer2_reset_request = false;

bool TaskTimerIsOn = false;
#if DBG
uint8_t trace_level = TRACE_LEVEL_ALL;
#endif
device_t RET;

task_t Tasks[MAX_TASKS] =
{
    {TASK_MAIN_PERIOD      , 0, TASK_MAIN_RUNNING, Task_Main},
    {TASK_BACKGROUND_PERIOD, 0, 0, Task_Aisg},
    {TASK_PARAMETERS_PERIOD, 0, TASK_PARAMETERS_RUNNING, Task_Parameters},
    {TASK_MOTOR_PERIOD     , 0, 0, Task_Motor},
    {TASK_BACKGROUND_PERIOD, 0, 0, Task_Download}
};

//############################################################################//
//
//############################################################################//
uint8_t Task_Main (uint8_t state)
{
    static int connection_timer = 0;

    switch (state)
    {
        case TASK_MAIN_RUNNING :
            CLRWDT();
            connection_timer++;
            if (connection_timer == CONNECTION_TIMER)
            {
                state = TASK_MAIN_RESET;
            }
            if (reset_connection_timer == true)
            {
                connection_timer = 0;
                reset_connection_timer = false;
            }
            if ((download_reset_request == true) ||
                (layer2_reset_request == true))
            {
                state = TASK_MAIN_RESET;
            }
        break;

        case TASK_MAIN_RESET :
            TRACE_DBG(("Performing reset\r\n"));
            RESET();
        break;

        default:
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
void main (void)
{
    uint8_t i;
    uint8_t reset_cause = 0;
    
    // Configure the oscillator for the device
    Configure_Oscillator();

    // Watchdog disabled for now
    SWDTEN = 0;

    // Initialize I/O and Peripherals for application
    InitApp();

    // Initialize alarm module
    Alarm_Init();

    // Initialize EEPROM parameters
    Parameters_Init();

    if ((RCONbits.nBOR == 0) && (RCONbits.nPOR == 0))
    {
        TRACE_DBG(("RESET   : POWER-ON\r\n"));
        RCONbits.nBOR = 1;
        RCONbits.nPOR = 1;
        RCONbits.nRI = 1;
        reset_cause |= (1 << 0);
    }

    if ((RCONbits.nBOR == 0) && (RCONbits.nPOR == 1))
    {
        TRACE_DBG(("RESET   : BROWN OUT\r\n"));
        RCONbits.nBOR = 1;
        RCONbits.nPOR = 1;
        RCONbits.nRI = 1;
        reset_cause |= (1 << 1);
    }

    if (RCONbits.nPD == 0)
    {
        TRACE_DBG(("RESET   : POWER DOWN\r\n"));
        reset_cause |= (1 << 2);
    }

    if (RCONbits.nTO == 0)
    {
        TRACE_DBG(("RESET   : WATCHDOG TIMEOUT\r\n"));
        reset_cause |= (1 << 3);
    }

    if (RCONbits.nRI == 0)
    {
        TRACE_DBG(("RESET   : RESET INSTRUCTION\r\n"));
        reset_cause |= (1 << 4);
    }

    TRACE_LOG(("VERS: %.16s\r\n", sw_version));
    TRACE_LOG(("DATE: %s\r\n", __DATE__));
    TRACE_LOG(("TIME: %s\r\n", __TIME__));
    TRACE_DBG(("REST: %d\r\n", reset_cause));

    // Clear I2C bus
    I2C_Clear();

    // Initialize I2C peripheral
    I2C_Init();

    AMIS_30624_Get_HW_Status(true);

    // Initialize HDLC protocol layer
    Hdlc_Init();

    // Initialize AISG protocol layer
    Aisg_Init();

    // Initialize motor
    Motor_Init();
    
    TRACE_DBG(("Initialization completed -> result = [TODO ERROR HANDLING]\r\n"));

    // Initialize ret object
    RET.address = HDLC_DEFAULT_ADDRESS;
    RET.state = STATE_NO_ADDRESS;
    memcpy(RET.unique_id, RET_VENDOR_CODE, VENDOR_ID_LENGTH);
    memcpy(&RET.unique_id[VENDOR_ID_LENGTH], &pars.SerialNumber,
           UNIQUE_ID_LENGTH - VENDOR_ID_LENGTH);

    TaskTimerIsOn = true;

    // Enabling watchdog
    SWDTEN = 1;

    TRACE_DBG(("Scheduler started\r\n"));

    // Scheduler loop
    while (1)
    {
        CLRWDT();

        // Scheduler main loop
        for (i = 0; i < MAX_TASKS; ++i)
        {
            if ((Tasks[i].function != NULL) &&
                (Tasks[i].timer >= Tasks[i].period))
            {
                // Task's timer has expired. Call the function
                Tasks[i].state = Tasks[i].function(Tasks[i].state);
                Tasks[i].timer = 0;
            }
        }
    }
}

//############################################################################//
//
//############################################################################//
void TimerISR (void)
{
    uint8_t i;
    
    // Update task timers
    for (i = 0; i < MAX_TASKS; ++i)
    {
        if (Tasks[i].function != NULL)
        {
            Tasks[i].timer++;
        }
    }
}
