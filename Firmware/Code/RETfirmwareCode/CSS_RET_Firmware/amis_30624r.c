//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Hard_Stop (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_HARD_STOP);
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Reset_To_Default (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_RESET_TO_DEFAULT);
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Reset_Position (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_RESET_POSITION);
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Set_Motor_Param (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_SET_MOTOR_PARAM);
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write((AMIS_30624_IRUN << 4) | AMIS_30624_IHOLD);  // 0x8F
	I2C_Write((AMIS_30624_VMAX << 4) | AMIS_30624_VMIN);  // 0x01
	I2C_Write(((AMIS_30624_SECPOS >> 3) & 0xE0) | (AMIS_30624_SHAFT << 4) | AMIS_30624_ACC);  // 0x92
	I2C_Write(AMIS_30624_SECPOS & 0xFF); // 0x00
	I2C_Write(0xA2 | (AMIS_30624_PWMFREQ << 6) | (AMIS_30624_ACCSHAPE << 4) | (AMIS_30624_STEPMODE << 2) | AMIS_30624_PWMJEN); // 0xAE
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Set_Stall_Param (void)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_SET_STALL_PARAM);
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write(AMIS_30624_RESERVED); // 0xFF
	I2C_Write((AMIS_30624_IRUN << 4) | AMIS_30624_IHOLD); // 0x8F
	I2C_Write((AMIS_30624_VMAX << 4) | AMIS_30624_VMIN); // 0x01
	I2C_Write((AMIS_30624_MINSAMPLES << 5) | (AMIS_30624_SHAFT << 4) | AMIS_30624_ACC); // 0x72
	I2C_Write((AMIS_30624_ABSTHR << 4) | AMIS_30624_DELTHR);  // 0x06
	I2C_Write((AMIS_30624_FS2STALLEN << 5) | (AMIS_30624_ACCSHAPE << 4) | (AMIS_30624_STEPMODE << 2) | (AMIS_30624_DC100STEN << 1) | AMIS_30624_PWMJEN); // 0x6E
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Reset (void)
{
	AMIS_30624_Hard_Stop();
	
	AMIS_30624_Reset_To_Default();
	
	AMIS_30624_Reset_Position();
	
	AMIS_30624_Set_Motor_Param();
	
	AMIS_30624_Set_Stall_Param();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Set_Position (int16_t position)
{
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_SET_POSITION);
	I2C_Write(AMIS_30624_RESERVED);
	I2C_Write(AMIS_30624_RESERVED);
	I2C_Write(U16_MSB(position));
	I2C_Write(U16_LSB(position));
	I2C_Stop();

    TRACE_DBG(("AMIS_30624_Set_Position: %d\r\n", position));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Get_Positions (int16_t *actual_position, int16_t *target_position)
{
	uint8_t tmp;
	uint8_t lsb;
	uint8_t msb;

	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_GET_FULL_STATUS_2);
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_READ);
	tmp = I2C_Read(1);
	msb = I2C_Read(1);
	lsb = I2C_Read(1);
	*actual_position = (msb << 8) | lsb;
	msb = I2C_Read(1);
	lsb = I2C_Read(1);
	*target_position = (msb << 8) | lsb;
	tmp = I2C_Read(1);
	tmp = I2C_Read(1);
	tmp = I2C_Read(0);
	I2C_Stop();

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AMIS_30624_Get_HW_Status (bool init)
{
    uint8_t i;
	
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_WRITE);
	I2C_Write(AMIS_30624_GET_FULL_STATUS_1);
	I2C_Start();
	I2C_Write(AMIS_30624_ADDRESS_READ);
	for (i = 0; i < 8; i++)
	{
		if (i < 7)
			hw_status.data[i] = I2C_Read(1);
		else
			hw_status.data[i] = I2C_Read(0);
	} 
	I2C_Stop();

    if (init == true)
    {
        return (RESULT_SUCCESS);
    }

	// Electrical defect / charge pump failure / Over I coil X / Over I coil Y
    if (hw_status.bits.ElDef || hw_status.bits.OVC1 ||
        hw_status.bits.OVC2 || hw_status.bits.CPFail)
	{
        Alarm_Set(Actuator_Jam_Permanent);
		return (RESULT_ACTUATOR_JAM_PERMANENT);
	}

    // digital supply reset / battery stop voltage / thermal shutdown
    if (hw_status.bits.VddReset || hw_status.bits.UV2 || hw_status.bits.TSD)
	{ 
        Alarm_Set(Actuator_Jam_Temporary);
		return (RESULT_ACTUATOR_JAM_TEMPORARY);
	}

    if (hw_status.bits.Motion & 0x03)
    {
        TRACE_DBG(("Stepper in motion\r\n"));
    }

    if (hw_status.bits.Stall)
    {
        TRACE_DBG(("Stepper in stall\r\n"));
    }

    return (RESULT_SUCCESS);
}
