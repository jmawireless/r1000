//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _UART_H_
#define _UART_H_

#include "system.h"

void Uart_Init (void);

void Uart_Interrupt_Handler (void);

bool Uart_Send_Byte (uint8_t data);

bool Uart_Read_Byte (uint8_t *data);

#endif // _UART_H_