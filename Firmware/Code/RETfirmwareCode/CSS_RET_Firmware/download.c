//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define DOWNLOAD_BUFFER_LENGTH  (256)
uint8_t flash_block[FLASH_BLOCK_SIZE];
uint32_t flash_address = 0;
uint16_t flash_crc = 0;
uint16_t received_length = 0;
bool dwn_error = false;
uint8_t cb_buffer[DOWNLOAD_BUFFER_LENGTH];
buffer_t dwn_buffer;

//############################################################################//
//
//############################################################################//
void Download_Init (void)
{
    Buffer_Init(&dwn_buffer, cb_buffer, DOWNLOAD_BUFFER_LENGTH);
    flash_address = (uint32_t)(UPLOAD_MEMORY_START & 0x0000FFFF);
    flash_crc = 0;
    received_length = 0;
    dwn_error = false;
}

//############################################################################//
//
//############################################################################//
uint8_t Task_Download (uint8_t state)
{
    uint8_t buf_count;
    bool flash_written = false;

    switch (state)
    {
        case TASK_DOWNLOAD_IDLE :
        break;

        case TASK_DOWNLOAD_START :
            Download_Init();
            TRACE_DBG(("Task_Download   (START)\r\n"));
            state = TASK_DOWNLOAD_WRITE_BLOCK;
        break;

        case TASK_DOWNLOAD_WRITE_BLOCK :
			if (Tasks[TASK_AISG_ID].state == TASK_AISG_TRANSMIT)
			    break;

            do
            {
                buf_count = Buffer_Get_Count(&dwn_buffer);
                TRACE_DBG(("buf_count= %u\r\n", buf_count));
                if (buf_count < FLASH_BLOCK_SIZE)
                    break;

                //LATBbits.LATB1 ^= 1;
                Buffer_ReadData(&dwn_buffer, flash_block, FLASH_BLOCK_SIZE);
                if (received_length < UPLOAD_SIZE)
                {
                    Download_Write_Block(flash_address, flash_block);
                    flash_address += FLASH_BLOCK_SIZE;
                }
                else
                {
                    dwn_error = true;
                }
                flash_written = true;
                //LATBbits.LATB1 ^= 1;
            } while (buf_count >= FLASH_BLOCK_SIZE);

            if (flash_written == true)
            {
                last_tcp.completed = true;
                last_tcp.return_code = RESULT_SUCCESS;
                flash_written = false;
            }
        break;

        case TASK_DOWNLOAD_STOP :
            TRACE_DBG(("Task_Download   (STOP)\r\n"));
            if (flash_crc == MAGIC_CRC)
            {
                download_reset_request = true;
            }
            state = TASK_DOWNLOAD_IDLE;
        break;

        default:
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
return_t Download_Write_Block (uint32_t address, uint8_t *pdata)
{
    EraseFlash(address, address + FLASH_BLOCK_SIZE);
    WriteBlockFlash(address, 1, pdata);
    received_length += FLASH_BLOCK_SIZE;
    Download_Is_Block_Empty(address);
    
    TRACE_DBG(("len: %.5d, crc: 0x%4.4x, [0x%8.8x]\r\n",
            received_length, flash_crc, flash_address));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
uint16_t Block_Checksum (uint8_t *pdata)
{
    uint8_t i;
    uint16_t *pword = (uint16_t *)pdata;
    uint16_t crc = 0;

    for (i=0; i<FLASH_BLOCK_SIZE; i += 2)
    {
        crc += *pword;
        pword++;
    }

    return (crc);
}

//############################################################################//
//
//############################################################################//
bool Download_Is_Block_Empty (uint32_t address)
{
    uint8_t flash_array[FLASH_BLOCK_SIZE];
    bool empty = true;
    uint8_t i;
    
    memset(flash_array, 0, FLASH_BLOCK_SIZE);
    ReadFlash(address, FLASH_BLOCK_SIZE, flash_array);
    flash_crc += Block_Checksum(flash_array);

    for (i=0; i<FLASH_BLOCK_SIZE; i++)
    {
        if (flash_array[i] != 0xFF)
        {
            empty = false;
            break;
        }
    }
    
    return (empty);
}
