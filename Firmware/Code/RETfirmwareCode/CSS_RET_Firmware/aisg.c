//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

l7_handler_t aisg_handlers[] =
{
    { V20_RESET                 , 0x00, AISG_V20_Reset                 },
    { V20_GET_ALARMS            , 0x00, AISG_V20_Get_Alarms            },
    { V20_GET_INFO              , 0x00, AISG_V20_Get_Info              },
    { V20_CLEAR_ALARMS          , 0x00, AISG_V20_Clear_Alarms          },
    { V20_READ_USER_DATA        , 0x00, AISG_V20_Read_User_Data        },
    { V20_WRITE_USER_DATA       , 0x00, AISG_V20_Write_User_Data       },
    { V20_SUBSCRIBE_ALARMS      , 0x00, AISG_V20_Subscribe_Alarms      },
    { V20_SELF_TEST             , 0x00, AISG_V20_Self_Test             },
    { V20_VENDOR_SPECIFIC       , 0x00, AISG_V20_Vendor_Specific       },
    { V20_DOWNLOAD_START        , 0x00, AISG_V20_Download_Start        },
    { V20_DOWNLOAD_APPL         , 0x00, AISG_V20_Download_Appl         },
    { V20_DOWNLOAD_END          , 0x00, AISG_V20_Download_End          },
    { V20_SRET_CALIBRATE        , 0x00, AISG_V20_SRet_Calibrate        },
    { V20_SRET_SEND_CONFIG      , 0x00, AISG_V20_SRet_Send_Config      },
    { V20_SRET_SET_TILT         , 0x00, AISG_V20_SRet_Set_Tilt         },
    { V20_SRET_GET_TILT         , 0x00, AISG_V20_SRet_Get_Tilt         },
    { V20_SRET_ALARM_INDICATION , 0x00, AISG_V20_SRet_Alarm_Indication },
    { V20_SRET_SET_DEVICE_DATA  , 0x00, AISG_V20_SRet_Set_Device_Data  },
    { V20_SRET_GET_DEVICE_DATA  , 0x00, AISG_V20_SRet_Get_Device_Data  },
    { COMMAND_TABLE_END         , 0x00, NULL                           }
};

// Macro to calculate the length of an array
#define ARRAY_LENGTH(array)             (sizeof(array)/sizeof((array)[0]))

l7_tcp_t last_tcp;
bool aisg_3ms_timer_enabled = false;
uint8_t aisg_3ms_timer_count = 0;

//############################################################################//
//
//############################################################################//
return_t Aisg_Init (void)
{
    last_tcp.command = COMMAND_TABLE_END;
    last_tcp.completed = false;
    last_tcp.in_progress = false;
    last_tcp.return_code = RESULT_SUCCESS;

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
uint8_t Task_Aisg (uint8_t state)
{
    bool res = false;
    uint8_t data;

    switch (state)
    {
        case TASK_AISG_RECEIVE :
            // Process received data
            Aisg_Set_Rx_Mode();

            while (Uart_Read_Byte(&data) == true)
            {
                // Feed received data to HDLC layer
                res = Hdlc_On_Rx_Byte(data);
                if (res == true)
                    break;
            }
            if ((res == true) && (hdlc_tx_buffer_length != 0) &&
                (download_reset_request == false))
            {
                state = TASK_AISG_TRANSMIT;
            }
        break;

        case TASK_AISG_TRANSMIT :
            if (aisg_3ms_timer_enabled == false)
            {
                Aisg_Set_Tx_Mode();
                Hdlc_Tx_Frame(hdlc_tx_buffer, hdlc_tx_buffer_length);

                if (layer2_reset_request == true)
                {
                    download_reset_request = true;
                }
                state = TASK_AISG_RECEIVE;
            }
        break;

        default:
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
function_t Aisg_Get_Command_Handler (l7_commands_t command)
{
	uint8_t i;

	for (i=0; i<ARRAY_LENGTH(aisg_handlers); i++)
    {
        if (aisg_handlers[i].command == command)
        {
			return (aisg_handlers[i].function);
		}
    }

	return (NULL);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Build_Error_Response       (info_frame_t *rsp, return_t error)
{
	uint16_t length;

	length = 2;
	rsp->header.lenLSB = U16_LSB(length);
	rsp->header.lenMSB = U16_MSB(length);
    rsp->data[0] = RESULT_FAIL;
    rsp->data[1] = error;

    TRACE_DBG(("PrepareError [0x%.2x]\r\n", error));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Reset                   (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_reset_cmd_t *icmd = (aisg_v20_reset_cmd_t *)cmd;
	aisg_v20_reset_rsp_t *irsp = (aisg_v20_reset_rsp_t *)rsp;
	uint16_t length;

	irsp->header.command = V20_RESET;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }
    irsp->return_code = RESULT_SUCCESS;

    TRACE_DBG(("Reset [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Get_Alarms              (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_get_alarms_cmd_t *icmd = (aisg_v20_get_alarms_cmd_t *)cmd;
	aisg_v20_get_alarms_rsp_t *irsp = (aisg_v20_get_alarms_rsp_t *)rsp;
	uint16_t length;
    return_t alarm;

	irsp->header.command = V20_GET_ALARMS;
    length = 1;
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }
    
    alarm = Alarm_Get();
    if (alarm != RESULT_SUCCESS)
    {
    	length = sizeof (*irsp) - sizeof (header_t);
        irsp->alarm = alarm;
    }

    irsp->header.lenLSB = U16_LSB(length);
    irsp->header.lenMSB = U16_MSB(length);
    irsp->return_code = RESULT_SUCCESS;

    TRACE_DBG(("Get_Alarms [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Get_Info                (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_get_info_cmd_t *icmd = (aisg_v20_get_info_cmd_t *)cmd;
	aisg_v20_get_info_rsp_t *irsp = (aisg_v20_get_info_rsp_t *)rsp;
	uint16_t length;

	irsp->header.command = V20_GET_INFO;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }

    irsp->product_number_length = Parameter_Length(ProductNumber);
    memcpy(irsp->product_number, pars.ProductNumber, irsp->product_number_length);

    irsp->serial_number_length = Parameter_Length(SerialNumber);
    memcpy(irsp->serial_number, pars.SerialNumber, irsp->serial_number_length);

    irsp->hardware_version_length = HW_VERSION_LENGTH;
    memcpy(irsp->hardware_version, hw_version, irsp->hardware_version_length);

    irsp->software_version_length = SW_VERSION_LENGTH;
    memcpy(irsp->software_version, sw_version, irsp->software_version_length);

    irsp->return_code = RESULT_SUCCESS;

    TRACE_DBG(("Get_Info [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Clear_Alarms            (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_clear_alarms_cmd_t *icmd = (aisg_v20_clear_alarms_cmd_t *)cmd;
	aisg_v20_clear_alarms_rsp_t *irsp = (aisg_v20_clear_alarms_rsp_t *)rsp;
	uint16_t length;

	irsp->header.command = V20_CLEAR_ALARMS;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }
    irsp->return_code = RESULT_SUCCESS;
    Alarm_Clear();
    
    TRACE_DBG(("Clear_Alarms [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Read_User_Data          (info_frame_t *cmd, info_frame_t *rsp)
{
    TRACE_DBG(("Read_User_Data [0x%.2x]\r\n", RESULT_UNSUPPORTED_PROCEDURE));
    return (RESULT_UNSUPPORTED_PROCEDURE);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Write_User_Data         (info_frame_t *cmd, info_frame_t *rsp)
{
    TRACE_DBG(("Write_User_Data [0x%.2x]\r\n", RESULT_UNSUPPORTED_PROCEDURE));
    return (RESULT_UNSUPPORTED_PROCEDURE);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Subscribe_Alarms        (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_subscribe_alarms_cmd_t *icmd = (aisg_v20_subscribe_alarms_cmd_t *)cmd;
	aisg_v20_subscribe_alarms_rsp_t *irsp = (aisg_v20_subscribe_alarms_rsp_t *)rsp;
	uint16_t length;

	irsp->header.command = V20_SUBSCRIBE_ALARMS;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }
    irsp->return_code = RESULT_SUCCESS;

    Alarm_Subscribe();

    TRACE_DBG(("Subscribe_Alarms [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Self_Test               (info_frame_t *cmd, info_frame_t *rsp)
{
   	aisg_v20_self_test_cmd_t *icmd = (aisg_v20_self_test_cmd_t *)cmd;
   	aisg_v20_self_test_rsp_t *irsp = (aisg_v20_self_test_rsp_t *)rsp;
	uint16_t length;

	irsp->header.command = V20_SELF_TEST;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }
    irsp->return_code = RESULT_SUCCESS;

    TRACE_DBG(("Self_Test [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Vendor_Specific         (info_frame_t *cmd, info_frame_t *rsp)
{
   	aisg_v20_vendor_specific_cmd_t *icmd = (aisg_v20_vendor_specific_cmd_t *)cmd;
   	aisg_v20_vendor_specific_rsp_t *irsp = (aisg_v20_vendor_specific_rsp_t *)rsp;
	uint16_t length;

    memcpy(pars.SerialNumber, icmd->serial_number, Parameter_Length(SerialNumber));
    memcpy(pars.ProductNumber, icmd->product_number, Parameter_Length(ProductNumber));
    Parameter_Write(SerialNumber);
    Parameter_Write(ProductNumber);


	irsp->header.command = V20_VENDOR_SPECIFIC;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    irsp->return_code = RESULT_SUCCESS;

    TRACE_DBG(("Vendor_Specific [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Download_Start          (info_frame_t *cmd, info_frame_t *rsp)
{
   	aisg_v20_download_start_cmd_t *icmd = (aisg_v20_download_start_cmd_t *)cmd;
   	aisg_v20_download_start_rsp_t *irsp = (aisg_v20_download_start_rsp_t *)rsp;
	uint16_t length;

	irsp->header.command = V20_DOWNLOAD_START;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }
    irsp->return_code = RESULT_SUCCESS;

    Task_Set_State(TASK_DWNL_ID, TASK_DOWNLOAD_START);

    TRACE_DBG(("Download_Start [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Download_Appl           (info_frame_t *cmd, info_frame_t *rsp)
{
   	aisg_v20_download_appl_cmd_t *icmd = (aisg_v20_download_appl_cmd_t *)cmd;
   	aisg_v20_download_appl_rsp_t *irsp = (aisg_v20_download_appl_rsp_t *)rsp;
	uint8_t length;
    uint8_t *pdata;

	irsp->header.command = V20_DOWNLOAD_APPL;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);

    if (cmd != NULL)
    {
        length = icmd->header.lenLSB;
        pdata = icmd->data;
        Buffer_WriteData(&dwn_buffer, pdata, length);

        if (dwn_error == false)
        {
            irsp->return_code = RESULT_SUCCESS;
            if (length >= FLASH_BLOCK_SIZE)
            {
                return (RESULT_PROCEDURE_NOT_COMPLETED);
            }
            else
            {
                last_tcp.in_progress = false;
                return (RESULT_SUCCESS);
            }
        }
        else
        {
            irsp->return_code = RESULT_INVALID_FILE;
            last_tcp.in_progress = false;
            AISG_V20_Build_Error_Response(rsp, irsp->return_code);
            return (RESULT_SUCCESS);
        }
    }
    else
    {
        if (irsp->return_code != RESULT_SUCCESS)
        {
            AISG_V20_Build_Error_Response(rsp, irsp->return_code);
        }
		TRACE_DBG(("Download_Appl [0x%.2x]\r\n", irsp->return_code));
        return (RESULT_SUCCESS);
    }
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_Download_End            (info_frame_t *cmd, info_frame_t *rsp)
{
   	aisg_v20_download_end_cmd_t *icmd = (aisg_v20_download_end_cmd_t *)cmd;
   	aisg_v20_download_end_rsp_t *irsp = (aisg_v20_download_end_rsp_t *)rsp;
	uint16_t length;

	irsp->header.command = V20_DOWNLOAD_END;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }
    if (flash_crc == MAGIC_CRC)
    {
        irsp->return_code = RESULT_SUCCESS;
    }
    else
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_CHECKSUM_ERROR);
    }

    Task_Set_State(TASK_DWNL_ID, TASK_DOWNLOAD_STOP);
	
    TRACE_DBG(("Download_End [0x%.2x, 0x%.4x]\r\n", irsp->return_code, flash_crc));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_SRet_Calibrate          (info_frame_t *cmd, info_frame_t *rsp)
{
   	aisg_v20_sret_calibrate_cmd_t *icmd = (aisg_v20_sret_calibrate_cmd_t *)cmd;
   	aisg_v20_sret_calibrate_rsp_t *irsp = (aisg_v20_sret_calibrate_rsp_t *)rsp;
	uint16_t length;
    return_t result;

	irsp->header.command = V20_SRET_CALIBRATE;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd != NULL)
    {
        if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
        {
            AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
            return (RESULT_SUCCESS);
        }

        result = Motor_Calibrate();
        TRACE_DBG(("Calibrate [0x%.2x]\r\n", result));
        if (result != RESULT_SUCCESS)
        {
            AISG_V20_Build_Error_Response(rsp, result);
            return (RESULT_SUCCESS);
        }
        else
        {
            irsp->return_code = RESULT_SUCCESS;
            return (RESULT_PROCEDURE_NOT_COMPLETED);
        }
    }
    else
    {
        result = irsp->return_code;
        TRACE_DBG(("Calibrate done [0x%.2x]\r\n", result));
        if (result != RESULT_SUCCESS)
        {
            AISG_V20_Build_Error_Response(rsp, result);
        }
        Alarm_Clr(Actuator_Detection_Fail);
        return (RESULT_SUCCESS);
    }
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_SRet_Send_Config        (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_sret_send_config_cmd_t *icmd = (aisg_v20_sret_send_config_cmd_t *)cmd;
	aisg_v20_sret_send_config_rsp_t *irsp = (aisg_v20_sret_send_config_rsp_t *)rsp;
	uint16_t length;
    return_t result;

	irsp->header.command = V20_SRET_SEND_CONFIG;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);

    if (cmd != NULL)
    {
        result = Parameters_Send_Config_Data(icmd->data, icmd->header.lenLSB);
//      TRACE_DBG(("Send_Config_data [0x%.2x]\r\n", result));
        result = RESULT_SUCCESS;
        if (result != RESULT_SUCCESS)
        {
            AISG_V20_Build_Error_Response(rsp, result);
            return (RESULT_SUCCESS);
        }
        else
        {
            irsp->return_code = RESULT_SUCCESS;
            return (RESULT_PROCEDURE_NOT_COMPLETED);
        }
    }
    else
    {
        result = irsp->return_code;
//      TRACE_DBG(("Send_Config_Data done [0x%.2x]\r\n", result));
        if (result != RESULT_SUCCESS)
        {
            AISG_V20_Build_Error_Response(rsp, result);
        }
        return (RESULT_SUCCESS);
    }
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_SRet_Set_Tilt           (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_sret_set_tilt_cmd_t *icmd = (aisg_v20_sret_set_tilt_cmd_t *)cmd;
	aisg_v20_sret_set_tilt_rsp_t *irsp = (aisg_v20_sret_set_tilt_rsp_t *)rsp;
	uint16_t length;
    return_t result;

    irsp->header.command = V20_SRET_SET_TILT;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);

    if (cmd != NULL)
    {
        if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
        {
            AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
            return (RESULT_SUCCESS);
        }
        result = Motor_Set_Tilt(icmd->tilt);
        TRACE_DBG(("Set_Tilt [0x%.2x]\r\n", result));
        if (result != RESULT_SUCCESS)
        {
            AISG_V20_Build_Error_Response(rsp, result);
            return (RESULT_SUCCESS);
        }
        else
        {
            irsp->return_code = RESULT_SUCCESS;
            return (RESULT_PROCEDURE_NOT_COMPLETED);
        }
    }
    else
    {
        result = irsp->return_code;
        TRACE_DBG(("Set_Tilt done [0x%.2x]\r\n", result));
        if (result != RESULT_SUCCESS)
        {
            AISG_V20_Build_Error_Response(rsp, result);
        }
        return (RESULT_SUCCESS);
    }
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_SRet_Get_Tilt           (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_sret_get_tilt_cmd_t *icmd = (aisg_v20_sret_get_tilt_cmd_t *)cmd;
	aisg_v20_sret_get_tilt_rsp_t *irsp = (aisg_v20_sret_get_tilt_rsp_t *)rsp;
	uint16_t length;
    return_t result;

	irsp->header.command = V20_SRET_GET_TILT;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }

    result = Motor_Get_Tilt(&irsp->tilt);
    if (result != RESULT_SUCCESS)
    {
        AISG_V20_Build_Error_Response(rsp, result);
    }
    else
    {
        irsp->return_code = RESULT_SUCCESS;
    }

    TRACE_DBG(("Get_Tilt [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_SRet_Alarm_Indication   (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_sret_alarm_indication_rsp_t *irsp = (aisg_v20_sret_alarm_indication_rsp_t *)rsp;
	uint16_t length;
    return_t alarm_cleared;
    return_t alarm_raised;
    uint8_t i = 0;

	irsp->header.command = V20_SRET_ALARM_INDICATION;

    alarm_cleared = Alarm_Get_Cleared();
    alarm_raised = Alarm_Get_Raised();
    if (alarm_cleared != RESULT_SUCCESS)
    {
        irsp->alarms[i].return_code = alarm_cleared;
        irsp->alarms[i].status = 0;
        if (alarm_raised != RESULT_SUCCESS)
            i++;
    }

    if (alarm_raised != RESULT_SUCCESS)
    {
        irsp->alarms[i].return_code = alarm_raised;
        irsp->alarms[i].status = 1;
    }

	length = sizeof (*irsp) - sizeof (header_t) - ((1 - i) * 2);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);

    Alarm_Notify();

    TRACE_DBG(("Alarm_Indication [0x%.2x]\r\n", irsp->alarms[0].return_code));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_SRet_Set_Device_Data    (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_sret_set_device_data_cmd_t *icmd = (aisg_v20_sret_set_device_data_cmd_t *)cmd;
	aisg_v20_sret_set_device_data_rsp_t *irsp = (aisg_v20_sret_set_device_data_rsp_t *)rsp;
	uint16_t length;
    return_t result = RESULT_SUCCESS;

    length = icmd->header.lenLSB - 1;

    switch (icmd->field)
    {
        case DATAFIELD_ANTENNA_MODEL_NUMBER        :
            memcpy(pars.AntennaModelNumber, icmd->data, Parameter_Length(AntennaModelNumber));
            if (length != Parameter_Length(AntennaModelNumber))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                Parameter_Write(AntennaModelNumber);
            }
        break;

        case DATAFIELD_ANTENNA_SERIAL_NUMBER       :
            memcpy(pars.AntennaSerialNumber, icmd->data, Parameter_Length(AntennaSerialNumber));
            if (length != Parameter_Length(AntennaSerialNumber))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                Parameter_Write(AntennaSerialNumber);
            }
        break;

        case DATAFIELD_ANTENNA_FREQUENCY_BAND      :
        case DATAFIELD_ANTENNA_BEAMWIDTH           :
        case DATAFIELD_ANTENNA_GAIN                :
        case DATAFIELD_ANTENNA_MAX_TILT            :
        case DATAFIELD_ANTENNA_MIN_TILT            :
                result = RESULT_READ_ONLY;
        break;

        case DATAFIELD_OPERATOR_INSTALL_DATE       :
            memcpy(pars.OperatorInstallDate, icmd->data, Parameter_Length(OperatorInstallDate));
            if (length != Parameter_Length(OperatorInstallDate))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                Parameter_Write(OperatorInstallDate);
            }
        break;

        case DATAFIELD_OPERATOR_INSTALL_ID         :
            memcpy(pars.OperatorInstallId, icmd->data, Parameter_Length(OperatorInstallId));
            if (length != Parameter_Length(OperatorInstallId))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                Parameter_Write(OperatorInstallId);
            }
        break;

        case DATAFIELD_OPERATOR_BASESTATION_ID     :
            memcpy(pars.OperatorBasestationId, icmd->data, Parameter_Length(OperatorBasestationId));
            if (length != Parameter_Length(OperatorBasestationId))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                 Parameter_Write(OperatorBasestationId);
            }
        break;

        case DATAFIELD_OPERATOR_SECTOR_ID          :
            memcpy(pars.OperatorSectorId, icmd->data, Parameter_Length(OperatorSectorId));
            if (length != Parameter_Length(OperatorSectorId))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                Parameter_Write(OperatorSectorId);
            }
        break;

        case DATAFIELD_OPERATOR_ANTENNA_BEARING    :
            memcpy(&pars.OperatorAntennaBearing, icmd->data, Parameter_Length(OperatorAntennaBearing));
            if (length != Parameter_Length(OperatorAntennaBearing))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                Parameter_Write(OperatorAntennaBearing);
            }
        break;

        case DATAFIELD_OPERATOR_MECHANICAL_TILT    :
            memcpy(&pars.OperatorMechanicalTilt, icmd->data, Parameter_Length(OperatorMechanicalTilt));
            if (length != Parameter_Length(OperatorMechanicalTilt))
            {
                result = RESULT_FORMAT_ERROR;
            }
            else
            {
                Parameter_Write(OperatorMechanicalTilt);
            }
            break;
        
        default                                    :
            result = RESULT_UNKNOWN_PARAMETER;
        break;
    }

	irsp->header.command = V20_SRET_SET_DEVICE_DATA;
	length = sizeof (*irsp) - sizeof (header_t);
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (result != RESULT_SUCCESS)
    {
        AISG_V20_Build_Error_Response(rsp, result);
    }
    else
    {
        irsp->return_code = RESULT_SUCCESS;
    }
    
    TRACE_DBG(("Set_Device_Data [0x%.2x]\r\n", irsp->return_code));

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t AISG_V20_SRet_Get_Device_Data    (info_frame_t *cmd, info_frame_t *rsp)
{
	aisg_v20_sret_get_device_data_cmd_t *icmd = (aisg_v20_sret_get_device_data_cmd_t *)cmd;
	aisg_v20_sret_get_device_data_rsp_t *irsp = (aisg_v20_sret_get_device_data_rsp_t *)rsp;
	uint16_t length;
    return_t result = RESULT_SUCCESS;
    
    length = 1;
	irsp->header.command = V20_SRET_GET_DEVICE_DATA;
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (cmd->header.lenLSB != (sizeof (*icmd) - sizeof (header_t)))
    {
        AISG_V20_Build_Error_Response(rsp, RESULT_FORMAT_ERROR);
        return (RESULT_SUCCESS);
    }

    switch (icmd->field)
    {
        case DATAFIELD_ANTENNA_MODEL_NUMBER        :
            length = Parameter_Length(AntennaModelNumber);
            memcpy(irsp->data, pars.AntennaModelNumber, length);
        break;

        case DATAFIELD_ANTENNA_SERIAL_NUMBER       :
            length = Parameter_Length(AntennaSerialNumber);
            memcpy(irsp->data, pars.AntennaSerialNumber, length);
        break;

        case DATAFIELD_ANTENNA_FREQUENCY_BAND      :
            length = Parameter_Length(AntennaFrequencyBand);
            memcpy(irsp->data, &pars.AntennaFrequencyBand, length);
        break;

        case DATAFIELD_ANTENNA_BEAMWIDTH           :
            length = Parameter_Length(AntennaBeamwidth);
            memcpy(irsp->data, &pars.AntennaBeamwidth, length);
        break;

        case DATAFIELD_ANTENNA_GAIN                :
            length = Parameter_Length(AntennaGain);
            memcpy(irsp->data, pars.AntennaGain, length);
        break;

        case DATAFIELD_ANTENNA_MAX_TILT            :
            length = Parameter_Length(AntennaMaxTilt);
            memcpy(irsp->data, &pars.AntennaMaxTilt, length);
        break;

        case DATAFIELD_ANTENNA_MIN_TILT            :
            length = Parameter_Length(AntennaMinTilt);
            memcpy(irsp->data, &pars.AntennaMinTilt, length);
        break;

        case DATAFIELD_OPERATOR_INSTALL_DATE       :
            length = Parameter_Length(OperatorInstallDate);
            memcpy(irsp->data, pars.OperatorInstallDate, length);
        break;

        case DATAFIELD_OPERATOR_INSTALL_ID         :
            length = Parameter_Length(OperatorInstallId);
            memcpy(irsp->data, pars.OperatorInstallId, length);
        break;

        case DATAFIELD_OPERATOR_BASESTATION_ID     :
            length = Parameter_Length(OperatorBasestationId);
            memcpy(irsp->data, pars.OperatorBasestationId, length);
        break;

        case DATAFIELD_OPERATOR_SECTOR_ID          :
            length = Parameter_Length(OperatorSectorId);
            memcpy(irsp->data, pars.OperatorSectorId, length);
        break;

        case DATAFIELD_OPERATOR_ANTENNA_BEARING    :
            length = Parameter_Length(OperatorAntennaBearing);
            memcpy(irsp->data, &pars.OperatorAntennaBearing, length);
        break;

        case DATAFIELD_OPERATOR_MECHANICAL_TILT    :
            length = Parameter_Length(OperatorMechanicalTilt);
            memcpy(irsp->data, &pars.OperatorMechanicalTilt, length);
        break;

        default                                    :
            result = RESULT_UNKNOWN_PARAMETER;
        break;
    }

    length += 1;
	irsp->header.command = V20_SRET_GET_DEVICE_DATA;
	irsp->header.lenLSB = U16_LSB(length);
	irsp->header.lenMSB = U16_MSB(length);
    if (result != RESULT_SUCCESS)
    {
        AISG_V20_Build_Error_Response(rsp, result);
    }
    else
    {
        irsp->return_code = RESULT_SUCCESS;
    }

    TRACE_DBG(("Get_Device_Data [0x%.2x]\r\n", irsp->return_code));
    return (RESULT_SUCCESS);
}
