//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

parameters_t pars;
uint8_t config_data[MAX_AISG_FRAME_LENGTH+1];
uint8_t config_data_length = 0;

#define CONFIG_DATA_MAX_PAR_LENGTH 32
#define CONFIG_DATA_MIN_TOT_LENGTH 13
//############################################################################//
//
//############################################################################//
return_t Parameters_Init (void)
{
    uint8_t *pData = (uint8_t *)&pars;
    uint16_t i;

    uint16_t length = sizeof(parameters_t) - sizeof(parameters_update_t);

    pars.flags.value = 0;
    
    for (i=0; i<length; i++)
    {
        *pData++ = eeprom_read(i);
    }

    for (i=strlen(pars.ProductNumber); i<Parameter_Length(ProductNumber); i++)
    {
        pars.ProductNumber[i] = 0x00;
    }

    pData = pars.SerialNumber;
    for (i=0; i<Parameter_Length(SerialNumber); i++)
    {
        if ((*pData != 0xFF) && (*pData != 0x00))
        {
            if (pars.NotCalibrated == 0xFF)
            {
                Alarm_Set(Not_Configured);
                return (RESULT_NOT_CONFIGURED);
            }
            else if (pars.NotCalibrated == 0x0F)
            {
                Alarm_Set(Not_Calibrated);
                return (RESULT_NOT_CALIBRATED);
            }
            // Parameters_Print();
            return (RESULT_SUCCESS);
        }
        pData++;
    }

    memcpy(pars.SerialNumber, "00000000000000000", Parameter_Length(SerialNumber));

    Alarm_Set(Not_Configured);
    return (RESULT_NOT_CONFIGURED);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Print (void)
{
    uint8_t tmp_str[33];
    uint8_t i;

    for (i=0; i<PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH; i++)
    {
        if ((pars.AntennaModelNumber[i] == 0x00) || (pars.AntennaModelNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.AntennaModelNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Antenna  Model Number    : %s\r\n", tmp_str));

    for (i=0; i<PARAMETER_ANTENNA_SERIAL_NUMBER_LENGTH; i++)
    {
        if ((pars.AntennaSerialNumber[i] == 0x00) || (pars.AntennaSerialNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.AntennaSerialNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Antenna  Serial Number   : %s\r\n", tmp_str));

    TRACE_PUT(("Antenna  Frequency Band  : %d\r\n", pars.AntennaFrequencyBand));

    for (i=0; i<PARAMETER_N_BANDS; i++)
    {
        TRACE_PUT(("Antenna  Beamwidth[%d]    : %d\r\n", i, pars.AntennaBeamwidth[i]));
    }

    for (i=0; i<PARAMETER_N_BANDS; i++)
    {
        TRACE_PUT(("Antenna  Gain[%d]         : %d\r\n", i, pars.AntennaGain[i]));
    }

    TRACE_PUT(("Antenna  Max Tilt        : %d\r\n", pars.AntennaMaxTilt));
    TRACE_PUT(("Antenna  Min Tilt        : %d\r\n", pars.AntennaMinTilt));

    for (i=0; i<PARAMETER_OPERATOR_INSTALL_DATE_LENGTH; i++)
    {
        if ((pars.OperatorInstallDate[i] == 0x00) || (pars.OperatorInstallDate[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.OperatorInstallDate[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Operator Install Date    : %s\r\n", tmp_str));

    for (i=0; i<PARAMETER_OPERATOR_INSTALL_ID_LENGTH; i++)
    {
        if ((pars.OperatorInstallId[i] == 0x00) || (pars.OperatorInstallId[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.OperatorInstallId[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Operator Install Id      : %s\r\n", tmp_str));

    for (i=0; i<PARAMETER_OPERATOR_BASESTATION_ID_LENGTH; i++)
    {
        if ((pars.OperatorBasestationId[i] == 0x00) || (pars.OperatorBasestationId[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.OperatorBasestationId[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Operator Basestation Id  : %s\r\n", tmp_str));

    for (i=0; i<PARAMETER_OPERATOR_SECTOR_ID_LENGTH; i++)
    {
        if ((pars.OperatorSectorId[i] == 0x00) || (pars.OperatorSectorId[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.OperatorSectorId[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Operator Sector Id       : %s\r\n", tmp_str));

    TRACE_PUT(("Operator Antenna Bearing : %d\r\n", pars.OperatorAntennaBearing));
    TRACE_PUT(("Operator Mechanical Tilt : %d\r\n", pars.OperatorMechanicalTilt));

    for (i=0; i<PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH; i++)
    {
        if ((pars.ProductNumber[i] == 0x00) || (pars.ProductNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.ProductNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Board    Product Number  : %s\r\n", tmp_str));

    for (i=0; i<PARAMETER_ANTENNA_SERIAL_NUMBER_LENGTH; i++)
    {
        if ((pars.SerialNumber[i] == 0x00) || (pars.SerialNumber[i] == 0xFF))
        {
            tmp_str[i] = ' ';
        }
        else
        {
            tmp_str[i] = pars.SerialNumber[i];
        }
    }
    tmp_str[i++] = '\0';
    TRACE_PUT(("Board    Serial Number   : %s\r\n", tmp_str));

    TRACE_PUT(("Not Calibrated           : %d\r\n", pars.NotCalibrated));
    TRACE_PUT(("Tilt                     : %d\r\n", pars.Tilt));
    TRACE_PUT(("Last Motor Position      : %d\r\n", pars.MotorPosition));
    TRACE_PUT(("Last Direction           : %d\r\n", pars.LastDirection));
    TRACE_PUT(("Tilt Conversion          : %d\r\n", pars.TurnRatio));
    TRACE_PUT(("Backlash                 : %d\r\n", pars.Backlash));
    TRACE_PUT(("Unwind                   : %d\r\n", pars.Unwind));

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Eeprom_Update (uint8_t *pData, uint8_t Length)
{
    uint8_t i;
	volatile uint32_t eeprom_address = (uint32_t)pData - (uint32_t)&pars;
	volatile uint8_t data = 0;

    for (i=0; i<Length; i++)
    {
        data = *pData;
        eeprom_write(eeprom_address, data);
        TRACE_DBG(("eeprom write data: 0x%.2x @ address: 0x%.2x\r\n",
                   data, eeprom_address));
        pData++;
        eeprom_address++;
    }

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
uint8_t Task_Parameters (uint8_t state)
{
    return_t result;

    switch (state)
    {
        case TASK_PARAMETERS_IDLE :
            if (pars.flags.value != 0)
            {
                state = TASK_PARAMETERS_RUNNING;
            }
        break;

        case TASK_PARAMETERS_RUNNING :
            Parameter_Process(AntennaModelNumber);
            Parameter_Process(SerialNumber);
            Parameter_Process(AntennaFrequencyBand);
            Parameter_Process(AntennaBeamwidth);
            Parameter_Process(AntennaGain);
            Parameter_Process(AntennaMaxTilt);
            Parameter_Process(AntennaMinTilt);
            Parameter_Process(ProductNumber);
            Parameter_Process(AntennaSerialNumber);
            Parameter_Process(OperatorInstallDate);
            Parameter_Process(OperatorInstallId);
            Parameter_Process(OperatorBasestationId);
            Parameter_Process(OperatorSectorId);
            Parameter_Process(OperatorAntennaBearing);
            Parameter_Process(OperatorMechanicalTilt);
            Parameter_Process(NotCalibrated);
            Parameter_Process(TurnRatio);
            Parameter_Process(Tilt);
            Parameter_Process(MotorPosition);
            Parameter_Process(Backlash);
            Parameter_Process(LastDirection);
            Parameter_Process(Unwind);
            state = TASK_PARAMETERS_IDLE;
        break;

        case TASK_PARAMETERS_WAIT_DELAY:
            state = TASK_PARAMETERS_CONFIG_DATA;
            break;

        case TASK_PARAMETERS_CONFIG_DATA:
            result = Parameters_Process_Config_Data();
            Parameters_Config_Data_Completed(result);
            state = TASK_PARAMETERS_IDLE;
        break;

        default:
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Send_Config_Data (uint8_t *pData, uint8_t length)
{
    memcpy(config_data, pData, length);
    config_data_length = length;
    config_data[length] = '\0';

    Task_Set_State(TASK_PARS_ID, TASK_PARAMETERS_WAIT_DELAY);
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Config_Data_Completed (return_t result)
{
    last_tcp.completed = true;
    last_tcp.return_code = result;

    TRACE_DBG(("Task_Parameters   (SEND CONFIG DATA DONE)\r\n"));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Parameters_Process_Config_Data (void)
{
	uint16_t length;
    uint16_t *pdata;
    config_data_file_t *config;
    uint16_t checksum;
    uint16_t i;
    uint8_t Type[CONFIG_DATA_TYPE_LEN+1] = CONFIG_DATA_TYPE;

    length = config_data_length;
    config = (config_data_file_t *)&config_data;

	// Checksum Verification
    checksum = 0;
    pdata = (uint16_t *)config;
    for (i=0; i<length; i+= 2)
    {
    	checksum += *pdata;
    	pdata++;
    }

    if (checksum != MAGIC_CRC)
    {
        TRACE_PUT(("wrong checksum\r\n"));
        return (RESULT_CHECKSUM_ERROR);
    }

    if (memcmp(config->Type, Type, CONFIG_DATA_TYPE_LEN) != 0)
    {
        TRACE_PUT(("wrong type\r\n"));
        return (RESULT_INVALID_FILE);
    }

    if (config->Version != CONFIG_DATA_VERSION)
    {
        TRACE_PUT(("wrong version\r\n"));
        return (RESULT_INVALID_FILE);
    }

    if (config->Flags & 0x01)
    {
        if (memcmp(config->ProductNumber, pars.ProductNumber, PARAMETER_ANTENNA_MODEL_NUMBER_LENGTH) != 0)
        {
            TRACE_PUT(("wrong product number\r\n"));
            return (RESULT_INVALID_FILE);
        }
    }

    TRACE_PUT(("flag                : %d\r\n", config->Flags));
    TRACE_PUT(("length              : %d\r\n", config->Length));
    TRACE_PUT(("checksum            : 0x%.4x\r\n", checksum));
    TRACE_PUT(("ProductNumber       : %s\r\n", config->ProductNumber));
    TRACE_PUT(("AntennaModelNumber  : %s\r\n", config->AntennaModelNumber));
    TRACE_PUT(("AntennaFrequencyBand: 0x%.4x\r\n", config->AntennaFrequencyBand));
    TRACE_PUT(("AntennaBeamwidth[0] : %d\r\n", config->AntennaBeamwidth[0]));
    TRACE_PUT(("AntennaBeamwidth[1] : %d\r\n", config->AntennaBeamwidth[1]));
    TRACE_PUT(("AntennaBeamwidth[2] : %d\r\n", config->AntennaBeamwidth[2]));
    TRACE_PUT(("AntennaBeamwidth[3] : %d\r\n", config->AntennaBeamwidth[3]));
    TRACE_PUT(("AntennaGain[0]      : %d\r\n", config->AntennaGain[0]));
    TRACE_PUT(("AntennaGain[1]      : %d\r\n", config->AntennaGain[1]));
    TRACE_PUT(("AntennaGain[2]      : %d\r\n", config->AntennaGain[2]));
    TRACE_PUT(("AntennaGain[3]      : %d\r\n", config->AntennaGain[3]));
    TRACE_PUT(("AntennaMaxTilt      : %d\r\n", config->AntennaMaxTilt));
    TRACE_PUT(("AntennaMinTilt      : %d\r\n", config->AntennaMinTilt));
    TRACE_PUT(("TurnRatio           : %d\r\n", config->TurnRatio));
    TRACE_PUT(("Backlash            : %d\r\n", config->Backlash));
    TRACE_PUT(("Unwind              : %d\r\n", config->Unwind));

    memcpy(pars.AntennaModelNumber, config->AntennaModelNumber, Parameter_Length(AntennaModelNumber));
    pars.TurnRatio = config->TurnRatio;
    pars.Unwind = config->Unwind;
    pars.Backlash = config->Backlash;
    pars.AntennaMinTilt = config->AntennaMinTilt;
    pars.AntennaMaxTilt = config->AntennaMaxTilt;
    pars.AntennaFrequencyBand = config->AntennaFrequencyBand;
    for (i=0; i< PARAMETER_N_BANDS; i++)
    {
        pars.AntennaBeamwidth[i] = config->AntennaBeamwidth[i];
        pars.AntennaGain[i] = config->AntennaGain[i];
    }

    Parameter_Write(AntennaModelNumber);
    Parameter_Process(AntennaModelNumber);
    if (strcmp(pars.AntennaModelNumber, "Not Configured") == 0)
    {
        pars.NotCalibrated = 0xFF;
        Parameter_Write(NotCalibrated);
        Parameter_Process(NotCalibrated);
        Alarm_Set(Not_Configured);
    }
    else
    {
        pars.NotCalibrated = 0x0F;
        Parameter_Write(TurnRatio);
        Parameter_Process(TurnRatio);
        Parameter_Write(Unwind);
        Parameter_Process(Unwind);
        Parameter_Write(Backlash);
        Parameter_Process(Backlash);
        Parameter_Write(AntennaMinTilt);
        Parameter_Process(AntennaMinTilt);
        Parameter_Write(AntennaMaxTilt);
        Parameter_Process(AntennaMaxTilt);
        Parameter_Write(AntennaFrequencyBand);
        Parameter_Process(AntennaFrequencyBand);
        Parameter_Write(AntennaBeamwidth);
        Parameter_Process(AntennaBeamwidth);
        Parameter_Write(AntennaGain);
        Parameter_Process(AntennaGain);
        Parameter_Write(NotCalibrated);
        Parameter_Process(NotCalibrated);
        Alarm_Clr(Not_Configured);
        Alarm_Set(Not_Calibrated);
    }

    return (RESULT_SUCCESS);
}
