//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#include "system.h"

#define round_float_to_uint16(x) ((x)>=0?(int16_t)((x)+0.5):(int16_t)((x)-0.5))

bool pos_backslash = false;
bool neg_backslash = false;
int16_t ret_tilt = 0;
int16_t motor_position = 0;
int16_t reset_position = 0;
int16_t offset_position = 0;
int16_t actual_position = 0;
int16_t target_position = 0;
int16_t backlash_position = 0;

//############################################################################//
//
//############################################################################//
return_t Motor_Init (void)
{
    TRACE_DBG(("Task_Motor initialized\r\n"));

    AMIS_30624_Reset();

    ret_tilt = max(pars.AntennaMinTilt, pars.Tilt);

    if (pars.MotorPosition != 0xFFFF)
    {
        reset_position = pars.MotorPosition;
    }

    backlash_position = round_float_to_uint16(
                       (float)pars.Backlash  * (float)pars.TurnRatio /
                       (float)(pars.AntennaMaxTilt - pars.AntennaMinTilt));
    
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
uint8_t Task_Motor (uint8_t state)
{
    static uint8_t timeout = 0;

    if (state == TASK_MOTOR_IDLE)
    {
        return (state);
    }

//  AMIS_30624_Get_Positions(&actual_position, &target_position);
    AMIS_30624_Get_HW_Status(false);

    TRACE_DBG(("m:%d o:%d a:%d t:%d d:%d\r\n",
        motor_position, offset_position, actual_position, target_position,
        pars.LastDirection));
    
    switch (state)
    {
        default:
        break;

        case TASK_MOTOR_SET_TILT_START :
            timeout = 0;
            motor_position = round_float_to_uint16(
                            (float)(ret_tilt - pars.AntennaMinTilt) * (float)pars.TurnRatio /
                            (float)(pars.AntennaMaxTilt - pars.AntennaMinTilt));

            offset_position = motor_position - reset_position;
            
            if (motor_position > pars.MotorPosition)
            {
                if ((pars.LastDirection == 0) && (neg_backslash == false))
                {
                    offset_position = offset_position + backlash_position;
                    pos_backslash = true;
                }
                pars.LastDirection = 1;
                Parameter_Write(LastDirection);
            }
            else if (motor_position < pars.MotorPosition)
            {
                // actual new position here will always have the slop factor
                // moving towards max tilt had the slop removed via the unwind number
                if ((pars.LastDirection == 1) && (pos_backslash == false) ||
                    (pars.LastDirection == 0) && (neg_backslash == true))
                {
                    offset_position = offset_position - backlash_position;
                    neg_backslash = true;
                }
                pars.LastDirection = 0;
                Parameter_Write(LastDirection);
            }
            else // no change
            {
                state = TASK_MOTOR_SET_TILT_COMPLETED;
                return state;
            }

            AMIS_30624_Set_Position(offset_position);
            state = TASK_MOTOR_SET_TILT_RUNNING;
            timeout = 0;
        break;

        case TASK_MOTOR_SET_TILT_RUNNING :
            if ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))
            {
                Alarm_Set(Actuator_Jam_Temporary);
                state = TASK_MOTOR_SET_TILT_FAILED;
                break;
            }
            if (Motor_Is_Stopped() == true)
            {
                state = TASK_MOTOR_SET_TILT_COMPLETED;
            }
            else
            {
                if (timeout++ >= MOTOR_CALIBRATION_TIMEOUT_SEC)
                {
                    state = TASK_MOTOR_SET_TILT_FAILED;
                }
            }
        break;

        case TASK_MOTOR_SET_TILT_FAILED :
            timeout = 0;
            Motor_Set_Tilt_Completed();
            state = TASK_MOTOR_IDLE;
        break;

        case TASK_MOTOR_SET_TILT_COMPLETED :
            timeout = 0;
            Motor_Set_Tilt_Completed();
            state = TASK_MOTOR_IDLE;
        break;

        case TASK_MOTOR_CALIBRATION_START :
            timeout = 0;
            if (Motor_Move_To_End_Stop() == RESULT_SUCCESS)
            {
                state = TASK_MOTOR_CALIBRATION_GO_END_STOP;
            }
            else
            {
                state = TASK_MOTOR_CALIBRATION_FAILED;
            }
        break;

        case TASK_MOTOR_CALIBRATION_GO_END_STOP :
            if (Motor_Is_Stopped() == true)
            {
                if (Motor_Move_To_Unwind() == RESULT_SUCCESS)
                {
                    state = TASK_MOTOR_CALIBRATION_GO_UNWIND;
                }
                else
                {
                    state = TASK_MOTOR_CALIBRATION_FAILED;
                }
            }
            else
            {
                if (timeout++ >= MOTOR_CALIBRATION_TIMEOUT_SEC)
                {
                    state = TASK_MOTOR_CALIBRATION_FAILED;
                }
            }
        break;

        case TASK_MOTOR_CALIBRATION_GO_UNWIND :
            if ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))
            {
                Alarm_Set(Actuator_Jam_Temporary);
                state = TASK_MOTOR_SET_TILT_FAILED;
                break;
            }
            if (Motor_Is_Stopped() == true)
            {
                if (Motor_Move_To_Max_Tilt() == RESULT_SUCCESS)
                {
                    state = TASK_MOTOR_CALIBRATION_GO_MAX;
                }
                else
                {
                    state = TASK_MOTOR_CALIBRATION_FAILED;
                }
            }
            else
            {
                if (timeout++ >= MOTOR_CALIBRATION_TIMEOUT_SEC)
                {
                    state = TASK_MOTOR_CALIBRATION_FAILED;
                }
            }
        break;

        case TASK_MOTOR_CALIBRATION_GO_MAX :
            if ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))
            {
                Alarm_Set(Actuator_Jam_Temporary);
                state = TASK_MOTOR_SET_TILT_FAILED;
                break;
            }
            if (Motor_Is_Stopped() == true)
            {
                if (Motor_Move_To_Last_Tilt() == RESULT_SUCCESS)
                {
                    state = TASK_MOTOR_CALIBRATION_GO_LAST;
                }
                else
                {
                    state = TASK_MOTOR_CALIBRATION_FAILED;
                }
            }
            else
            {
                if (timeout++ >= MOTOR_CALIBRATION_TIMEOUT_SEC)
                {
                    state = TASK_MOTOR_CALIBRATION_FAILED;
                }
            }
        break;

        case TASK_MOTOR_CALIBRATION_GO_LAST :
            if ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))
            {
                Alarm_Set(Actuator_Jam_Temporary);
                state = TASK_MOTOR_CALIBRATION_FAILED;
                break;
            }
            if (Motor_Is_Stopped() == true)
            {
                state = TASK_MOTOR_CALIBRATION_COMPLETED;
            }
            else
            {
                if (timeout++ >= MOTOR_CALIBRATION_TIMEOUT_SEC)
                {
                    state = TASK_MOTOR_CALIBRATION_FAILED;
                }
            }
        break;

        case TASK_MOTOR_CALIBRATION_FAILED :
            last_tcp.completed = true;
            last_tcp.return_code = Alarm_Get();
            state = TASK_MOTOR_IDLE;
        break;

        case TASK_MOTOR_CALIBRATION_COMPLETED :
            timeout = 0;
            Motor_Calibration_Completed();
            state = TASK_MOTOR_IDLE;
        break;
    }

    return state;
}

//############################################################################//
//
//############################################################################//
return_t Motor_Get_Tilt (int16_t *tilt)
{
    return_t result;

    result = Alarm_Get();
    if (result == RESULT_SUCCESS)
    {
        *tilt = ret_tilt;
    }
    return (result);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Set_Tilt (int16_t tilt)
{
    return_t result;

    if ((tilt > pars.AntennaMaxTilt) || (tilt < pars.AntennaMinTilt))
    {
        return (RESULT_OUT_OF_RANGE);
    }

    result = Alarm_Get();
    if (result == RESULT_SUCCESS)
    {
        ret_tilt = tilt;

        pars.NotCalibrated = 0x0F;
        Parameter_Write(NotCalibrated);

        Task_Set_State(TASK_MOTO_ID, TASK_MOTOR_SET_TILT_START);
    }

    return (result);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Set_Tilt_Completed (void)
{
    return_t Result = Alarm_Get();

    last_tcp.completed = true;
    last_tcp.return_code = Result;

    if (Result == RESULT_SUCCESS)
    {
        pars.MotorPosition = motor_position;
        pars.Tilt = ret_tilt;
        pars.NotCalibrated = 0;
        Parameter_Write(MotorPosition);
        Parameter_Write(Tilt);
        Parameter_Write(NotCalibrated);
    }
    
    TRACE_DBG(("Task_Motor   (SET TILT DONE)\r\n"));
    return (Result);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Calibrate (void)
{
    if (Alarm_Is_Active(Not_Configured))
    {
        return (RESULT_NOT_CONFIGURED);
    }

    if (Alarm_Is_Active(Not_Calibrated))
	{
		pars.Tilt = pars.AntennaMinTilt;
		pars.MotorPosition = 0;
        Parameter_Write(Tilt);
        Parameter_Write(MotorPosition);
	}

    pars.NotCalibrated = 0x0F;
    Parameter_Write(NotCalibrated);

    Task_Set_State(TASK_MOTO_ID, TASK_MOTOR_CALIBRATION_START);
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Calibration_Completed (void)
{
    pars.NotCalibrated = 0x00;
    Parameter_Write(NotCalibrated);

    reset_position = 0;
    pos_backslash = false;

    Alarm_Clr(Not_Calibrated);
    Alarm_Clr(Actuator_Jam_Temporary);
    Alarm_Clr(Actuator_Jam_Permanent);

    last_tcp.completed = true;
    last_tcp.return_code = RESULT_SUCCESS;

	TRACE_DBG(("Motor_Calibration_Done  :\r\n"));
    TRACE_DBG(("Task_Motor   (CALIBRATION DONE)\r\n"));
    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
bool Motor_Is_Stopped (void)
{
    if (hw_status.bits.Motion & 0x03)
    {
        return (false);
    }
    else
    {
        return (true);
    }
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_End_Stop (void)
{
	// Redefine the current tilt position as position zero.
	AMIS_30624_Reset_Position();

	// Now we can turn in a large negative direction until we hit the end stop
	// Move to some very large negative value.
	// This will ensure we hit the end stop.  The motor controller will stop turning
	// on its own when it hits the end stop, so this line is just sure to get it there
	TRACE_DBG(("Motor_Move_To_End_Stop  : INF, -INF\r\n"));

    AMIS_30624_Set_Position(INT16_MIN); // 10.x turns of motor shaft

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_Unwind (void)
{
    int16_t unwind_pos;

    // Motor stall OK here
	if ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))
	{
        AMIS_30624_Get_Positions(&actual_position, &target_position);
	}
	else
	{
        // motion stopped due to something other than a stall, likely not able to reach stall
        Alarm_Set(Actuator_Jam_Temporary);
        return (RESULT_ACTUATOR_JAM_TEMPORARY);
	}

	AMIS_30624_Reset_Position();

    unwind_pos = round_float_to_uint16(
                    (float)pars.Unwind  * (float)pars.TurnRatio /
                    (float)(pars.AntennaMaxTilt - pars.AntennaMinTilt));

	TRACE_DBG(("Motor_Move_To_Unwind    : %3.3d, %5.5d\r\n", pars.Unwind, unwind_pos));

    AMIS_30624_Set_Position(unwind_pos);

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_Max_Tilt (void)
{
    int16_t max_pos;

	if ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))
	{
        // should not be a stop due to a stall during this move
        Alarm_Set(Actuator_Jam_Temporary);
        return (RESULT_ACTUATOR_JAM_TEMPORARY);
	}

    // 0 degree point now
	AMIS_30624_Reset_Position();

    max_pos = pars.TurnRatio;
    
	TRACE_DBG(("Motor_Move_To_Max_Tilt  : %3.3d, %5.5d\r\n", pars.AntennaMaxTilt, max_pos));

    AMIS_30624_Set_Position(max_pos);

    return (RESULT_SUCCESS);
}

//############################################################################//
//
//############################################################################//
return_t Motor_Move_To_Last_Tilt (void)
{
    int16_t last_pos;

    if ((hw_status.bits.StepLoss == true) || (hw_status.bits.Stall == true))
	{
        // should not be a stop due to a stall during this move
        Alarm_Set(Actuator_Jam_Temporary);
        return (RESULT_ACTUATOR_JAM_TEMPORARY);
	}

    // get former tilt position, restore after calibration
    ret_tilt = max(pars.AntennaMinTilt, pars.Tilt);

    // downwards direction
	neg_backslash = true;
    pars.LastDirection = 0;
    Parameter_Write(LastDirection);

    last_pos = round_float_to_uint16(
                    (float)(ret_tilt - pars.AntennaMinTilt) * (float)pars.TurnRatio /
                    (float)(pars.AntennaMaxTilt - pars.AntennaMinTilt));

    offset_position = last_pos - backlash_position;

	TRACE_DBG(("Motor_Move_To_Last_Tilt : %3.3d, %d, %5.5d\r\n",
               pars.Tilt, pars.MotorPosition, offset_position));

    AMIS_30624_Set_Position(offset_position);

    return (RESULT_SUCCESS);
}
