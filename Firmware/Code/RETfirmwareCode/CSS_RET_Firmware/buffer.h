//############################################################################//
//  NAME        :                                                             //
//  PRODUCT		:                                            		          //
//  VERSION		:                                                             //
//  DATE   		:   	                                                      //
//  SCOPE		:                    			                              //
//############################################################################//
//                                                                            //
//                       Copyright 2015, JMA WIRELESS                         //
//                           ALL RIGHTS RESERVED                              //
//                                                                            //
//############################################################################//

#ifndef _BUFFER_H_
#define _BUFFER_H_

#include "system.h"

typedef struct
{
    uint16_t head;
    uint16_t tail;
    uint16_t size;
    uint8_t *data;
} buffer_t;

#define Buffer_Get_Count(BUFFER) ((BUFFER)->tail - (BUFFER)->head)

void Buffer_Init (buffer_t *buffer, uint8_t *data, uint16_t size);

bool Buffer_Write (buffer_t *buffer, uint8_t data);

bool Buffer_WriteData (buffer_t *buffer, uint8_t *data, uint8_t length);

bool Buffer_Read (buffer_t *buffer, uint8_t *data);

bool Buffer_ReadData (buffer_t *buffer, uint8_t *data, uint8_t length);

#endif //_BUFFER_H_
