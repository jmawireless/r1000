<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC-JMA">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
<text x="162.56" y="15.24" size="2.54" layer="94" ratio="15">Proprietory Information
JMA WIRELESS </text>
<text x="217.17" y="20.32" size="2.54" layer="94" font="fixed">RGANDHI</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC-JMA" prefix="FRAME">
<gates>
<gate name="G$1" symbol="A4L-LOC-JMA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="JMA_LBR">
<packages>
<package name="RESC1005X40N">
<wire x1="-0.48" y1="-0.94" x2="-0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.94" x2="0.48" y2="0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.94" x2="0.48" y2="-0.94" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.94" x2="-0.48" y2="-0.94" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.48" dx="0.57" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="RES2013X38N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1" dx="0.95" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2" y="-2" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="RESC1608X50N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.8" dx="0.95" dy="1" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="CAPC1005X55N">
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="21"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="21" style="shortdash"/>
<smd name="1" x="0" y="0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.45" dx="0.62" dy="0.62" layer="1" roundness="25" rot="R270"/>
<text x="0.635" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.524" y="1.397" size="0.635" layer="27" font="vector" ratio="10" rot="R270">&gt;VALUE</text>
<wire x1="-0.48" y1="-0.93" x2="-0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="-0.48" y1="0.93" x2="0.48" y2="0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="0.93" x2="0.48" y2="-0.93" width="0.127" layer="51"/>
<wire x1="0.48" y1="-0.93" x2="-0.48" y2="-0.93" width="0.127" layer="51" style="shortdash"/>
</package>
<package name="CAPC1608X55N">
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="21"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.762" dx="0.9" dy="0.95" layer="1" roundness="25" rot="R270"/>
<text x="0.889" y="1.27" size="0.889" layer="25" ratio="11" rot="R270">&gt;NAME</text>
<text x="-1.5875" y="1.27" size="0.635" layer="27" ratio="10" rot="R270">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="AISG_C091-C006-804-2">
<pad name="1" x="3.5" y="0" drill="1.1"/>
<pad name="3" x="-3.5" y="0" drill="1.1"/>
<pad name="4" x="2.475" y="2.475" drill="1.1"/>
<pad name="5" x="-2.475" y="2.475" drill="1.1"/>
<pad name="6" x="2.475" y="-2.475" drill="1.1"/>
<pad name="7" x="-2.475" y="-2.475" drill="1.1"/>
<circle x="0" y="0" radius="9.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.5405" width="0.127" layer="51"/>
<pad name="PAD" x="-3.615" y="6.7262" drill="1.2"/>
<wire x1="-1" y1="-5" x2="1" y2="-5" width="0.127" layer="21" curve="-180"/>
<wire x1="-1" y1="-5" x2="-1" y2="-6" width="0.127" layer="21"/>
<wire x1="1" y1="-5" x2="1" y2="-6" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-6" x2="1.5" y2="-6" width="0.127" layer="21"/>
<text x="5.08" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="6.1" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="7.62" y="-7.62" size="1.4224" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.4224" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-1.27" y="-8.255" size="1.016" layer="25" font="vector" ratio="15">MALE</text>
<pad name="2" x="0" y="3.5" drill="1.1"/>
<pad name="8" x="0" y="-0.7" drill="1.1"/>
<text x="0.6" y="5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.42" y="5" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="-0.42" y="0.5" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.6" y="0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-5.08" y="5.08" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
<text x="-6.35" y="5.08" size="1.4224" layer="21" font="vector" ratio="15">P</text>
</package>
<package name="AISG_C091-G006-804-2">
<pad name="3" x="3.5" y="0" drill="1.1"/>
<pad name="1" x="-3.5" y="0" drill="1.1"/>
<pad name="5" x="2.475" y="2.475" drill="1.1"/>
<pad name="4" x="-2.475" y="2.475" drill="1.1"/>
<pad name="7" x="2.475" y="-2.475" drill="1.1"/>
<pad name="6" x="-2.475" y="-2.475" drill="1.1"/>
<circle x="0" y="0" radius="9.6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.5405" width="0.127" layer="51"/>
<pad name="PAD" x="-4.123" y="7.2342" drill="1.2"/>
<wire x1="1" y1="-5.16" x2="-1" y2="-5.16" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="-5.16" x2="1" y2="-4.16" width="0.127" layer="21"/>
<wire x1="-1" y1="-5.16" x2="-1" y2="-4.16" width="0.127" layer="21"/>
<text x="-6" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">1</text>
<text x="5" y="-0.5" size="1.4224" layer="21" font="vector" ratio="15">3</text>
<text x="-5" y="2" size="1.4224" layer="21" font="vector" ratio="15">4</text>
<text x="-5" y="-3" size="1.4224" layer="21" font="vector" ratio="15">6</text>
<text x="4" y="-3" size="1.4224" layer="21" font="vector" ratio="15">7</text>
<text x="4" y="2" size="1.4224" layer="21" font="vector" ratio="15">5</text>
<text x="-5" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">1</text>
<text x="6" y="-0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">3</text>
<text x="-4" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">4</text>
<text x="5" y="2" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">5</text>
<text x="5" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">7</text>
<text x="-4" y="-3" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">6</text>
<text x="7.62" y="-7.62" size="1.4224" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.4224" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<text x="-2.54" y="-8.255" size="1.016" layer="25" font="vector" ratio="15">FEMALE</text>
<pad name="2" x="0" y="3.5" drill="1.1"/>
<pad name="8" x="0" y="-0.7" drill="1.1"/>
<text x="-0.5" y="5" size="1.4224" layer="21" font="vector" ratio="15">2</text>
<text x="0.5" y="5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">2</text>
<text x="-0.5" y="0.5" size="1.4224" layer="21" font="vector" ratio="15">8</text>
<text x="0.5" y="0.5" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">8</text>
<text x="-6.35" y="5.08" size="1.4224" layer="21" font="vector" ratio="15">P</text>
<text x="-5.08" y="5.08" size="1.4224" layer="22" font="vector" ratio="15" rot="MR0">P</text>
</package>
<package name="ONSEMI_1SMC5.0AT3G">
<smd name="1" x="-3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<smd name="2" x="3.5685" y="0" dx="2.794" dy="3.81" layer="1"/>
<wire x1="-4" y1="2.5" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="21"/>
<wire x1="4" y1="3" x2="4" y2="2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="-2.5" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="4" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="4" y2="3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="3" x2="-4" y2="2" width="0.127" layer="51"/>
<wire x1="-4" y1="2" x2="-5" y2="2" width="0.127" layer="51"/>
<wire x1="-5" y1="2" x2="-5" y2="-2" width="0.127" layer="51"/>
<wire x1="-5" y1="-2" x2="-4" y2="-2" width="0.127" layer="51"/>
<wire x1="-4" y1="-2" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="4" y1="3" x2="4" y2="2" width="0.127" layer="51"/>
<wire x1="4" y1="2" x2="5" y2="2" width="0.127" layer="51"/>
<wire x1="5" y1="2" x2="5" y2="-2" width="0.127" layer="51"/>
<wire x1="5" y1="-2" x2="4" y2="-2" width="0.127" layer="51"/>
<wire x1="4" y1="-2" x2="4" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="21"/>
<text x="-5.08" y="3.81" size="0.8128" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="0" y="3.81" size="0.8128" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<wire x1="-5.5" y1="3" x2="-5.5" y2="-3" width="0.127" layer="51"/>
</package>
<package name="2035-XX-SM">
<smd name="1" x="-2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="2" y="0" dx="5.6" dy="1.3" layer="1" rot="R90"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="51"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="51"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="51"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="51"/>
<text x="-3" y="4" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="6" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-4" y1="3" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3.5" y2="3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3" x2="-3.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3" x2="-4" y2="-3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3.5" y2="3" width="0.127" layer="51"/>
<wire x1="-3.5" y1="3" x2="-3.5" y2="-3" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3" x2="-4" y2="-3" width="0.127" layer="51"/>
<wire x1="-4" y1="-3" x2="-4" y2="3" width="0.127" layer="51"/>
</package>
<package name="RES5750X230N">
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="21"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.6" dx="1.4" dy="5.25" layer="1" roundness="25" rot="R270"/>
<text x="4" y="-2" size="0.635" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-3" y="-2" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="2.925" y1="3.72" x2="2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="2.925" y1="-3.72" x2="-2.925" y2="-3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="-3.72" x2="-2.925" y2="3.72" width="0.127" layer="51"/>
<wire x1="-2.925" y1="3.72" x2="2.925" y2="3.72" width="0.127" layer="51"/>
</package>
<package name="CAPC2013X145N">
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="21"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.9" dx="1.15" dy="1.45" layer="1" roundness="25" rot="R270"/>
<text x="2.111" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.1625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="0.5MM_FIDUCIAL">
<wire x1="0" y1="1" x2="0" y2="-1" width="0.0508" layer="51"/>
<wire x1="-1" y1="0" x2="1" y2="0" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.5" width="0" layer="1"/>
<circle x="0" y="0" radius="0.889" width="0" layer="29"/>
<circle x="0" y="0" radius="1" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="1.0307" width="0" layer="41"/>
<circle x="0" y="0" radius="0.5" width="0.0508" layer="51"/>
</package>
<package name="MOLEX_5031591200_SD">
<pad name="P$1" x="0" y="0" drill="0.7"/>
<pad name="P$2" x="1.5" y="2" drill="0.7"/>
<pad name="P$3" x="3" y="0" drill="0.7"/>
<pad name="P$4" x="4.5" y="2" drill="0.7"/>
<wire x1="-2.75" y1="-1.4" x2="-2.75" y2="5.6" width="0.127" layer="21"/>
<wire x1="-2.75" y1="5.6" x2="19.25" y2="5.6" width="0.127" layer="21"/>
<wire x1="19.25" y1="5.6" x2="19.25" y2="-1.4" width="0.127" layer="21"/>
<wire x1="19.25" y1="-1.4" x2="-2.75" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-1.4" x2="19.25" y2="-1.4" width="0.127" layer="51"/>
<wire x1="19.25" y1="-1.4" x2="19.25" y2="5.6" width="0.127" layer="51"/>
<wire x1="19.25" y1="5.6" x2="-2.75" y2="5.6" width="0.127" layer="51"/>
<wire x1="-2.75" y1="5.6" x2="-2.75" y2="-1.4" width="0.127" layer="51"/>
<text x="26.67" y="1.27" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="26.67" y="0" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<hole x="-1.65" y="3.8" drill="0.8"/>
<pad name="P$5" x="6" y="0" drill="0.7"/>
<pad name="P$6" x="7.2" y="2" drill="0.7"/>
<pad name="P$7" x="9" y="0" drill="0.7"/>
<pad name="P$8" x="10.5" y="2" drill="0.7"/>
<pad name="P$9" x="12" y="0" drill="0.7"/>
<pad name="P$10" x="13.5" y="2" drill="0.7"/>
<pad name="P$11" x="15" y="0" drill="0.7"/>
<pad name="P$12" x="16.5" y="2" drill="0.7"/>
</package>
<package name="RESC2013X70N">
<wire x1="1" y1="1.8" x2="1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="1" y1="-1.8" x2="-1" y2="-1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.8" x2="-1" y2="1.8" width="0.127" layer="21"/>
<wire x1="-1" y1="1.8" x2="1" y2="1.8" width="0.127" layer="21"/>
<smd name="1" x="0" y="0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-0.95" dx="1.05" dy="1.4" layer="1" roundness="25" rot="R270"/>
<text x="1.511" y="-1.47" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.2125" y="-1.47" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.508" x2="1.27" y2="0.508" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC3216X84N">
<wire x1="1.275" y1="2.27" x2="1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="1.275" y1="-2.27" x2="-1.275" y2="-2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="-2.27" x2="-1.275" y2="2.27" width="0.127" layer="21"/>
<wire x1="-1.275" y1="2.27" x2="1.275" y2="2.27" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.05" dy="1.75" layer="1" roundness="25" rot="R270"/>
<text x="1.95" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.5" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.675" y1="1.47" x2="0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="0.675" y1="-1.47" x2="-0.675" y2="-1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="-1.47" x2="-0.675" y2="1.47" width="0.127" layer="51"/>
<wire x1="-0.675" y1="1.47" x2="0.675" y2="1.47" width="0.127" layer="51"/>
</package>
<package name="RESC5325X84N">
<wire x1="1.775" y1="3.52" x2="1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="1.775" y1="-3.52" x2="-1.775" y2="-3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="-3.52" x2="-1.775" y2="3.52" width="0.127" layer="21"/>
<wire x1="-1.775" y1="3.52" x2="1.775" y2="3.52" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.55" dx="1.1" dy="2.65" layer="1" roundness="25" rot="R270"/>
<text x="2.45" y="-2" size="0.4064" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2" y="-2" size="0.4064" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3" y1="-1.27" x2="3" y2="1.27" layer="39" rot="R270"/>
</package>
<package name="CAPC3216X190N">
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-1.5" dx="1.15" dy="1.8" layer="1" roundness="25" rot="R270"/>
<text x="2.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-1.4125" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.27" y1="-0.75" x2="1.27" y2="0.75" layer="39" rot="R270"/>
<wire x1="0.925" y1="1.72" x2="0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="0.925" y1="-1.72" x2="-0.925" y2="-1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="-1.72" x2="-0.925" y2="1.72" width="0.127" layer="51"/>
<wire x1="-0.925" y1="1.72" x2="0.925" y2="1.72" width="0.127" layer="51"/>
</package>
<package name="CAPC4532X279N">
<wire x1="2.175" y1="3.22" x2="2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="2.175" y1="-3.22" x2="-2.175" y2="-3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="-3.22" x2="-2.175" y2="3.22" width="0.127" layer="21"/>
<wire x1="-2.175" y1="3.22" x2="2.175" y2="3.22" width="0.127" layer="21"/>
<smd name="1" x="0" y="2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<smd name="2" x="0" y="-2.05" dx="1.4" dy="3.4" layer="1" roundness="25" rot="R270"/>
<text x="3.361" y="-2.02" size="0.889" layer="25" ratio="11" rot="R90">&gt;NAME</text>
<text x="-2.6625" y="-1.52" size="0.635" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.264" y1="-1.494" x2="2.256" y2="1.514" layer="39" rot="R270"/>
<wire x1="1.56" y1="2.331" x2="1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="1.56" y1="-2.355" x2="-1.56" y2="-2.355" width="0.127" layer="51"/>
<wire x1="-1.56" y1="-2.355" x2="-1.56" y2="2.331" width="0.127" layer="51"/>
<wire x1="-1.56" y1="2.331" x2="1.56" y2="2.331" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="RESISTER">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-1.27" y="1.4986" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.27" y="-3.302" size="1.27" layer="96" distance="49">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CAPACITOR">
<wire x1="-0.635" y1="-1.016" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-1.27" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="PIN">
<pin name="1" x="-5.08" y="0" visible="off"/>
<wire x1="-3.302" y1="0" x2="-1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="94"/>
<text x="-3.302" y="0.508" size="1.27" layer="95" ratio="15">&gt;NAME</text>
</symbol>
<symbol name="NC">
<wire x1="-0.635" y1="0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
</symbol>
<symbol name="VALUE_LABEL">
<text x="0" y="0" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ZENER">
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="+" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="-" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.15875" layer="94"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="GDT">
<polygon width="0.254" layer="94">
<vertex x="-0.635" y="1.27"/>
<vertex x="0.635" y="1.27"/>
<vertex x="0.635" y="0.635"/>
<vertex x="-0.635" y="0.635"/>
</polygon>
<wire x1="-0.635" y1="-0.635" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" rot="R90"/>
<text x="5.08" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="0" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="0.254" layer="94" curve="90"/>
</symbol>
<symbol name="VARISTOR_ESD">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="0" width="0.254" layer="94"/>
<text x="-7.62" y="1.27" size="1.27" layer="95" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.15875" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.16001875" layer="94"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="3.81" width="0" layer="94"/>
<circle x="0" y="0" radius="6.35" width="0.254" layer="94"/>
<text x="-4.826" y="13.97" size="1.778" layer="125">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES_SMD_" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTER" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="RESC1005X40N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="RESC1608X50N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="RESC2013X70N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3216X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805_SH" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="RESC5325X84N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_SMD_" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="CAPC1005X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="CAPC1608X55N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2013X145N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="CAPC3216X190N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="CAPC4532X279N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NC" prefix="NC">
<gates>
<gate name="G$1" symbol="NC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AISG_" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-PAD" symbol="PIN" x="5.08" y="-20.32"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-5" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-6" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="-7" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="G$1" symbol="VALUE_LABEL" x="2.54" y="2.54"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-8" symbol="PIN" x="5.08" y="-17.78"/>
</gates>
<devices>
<device name="MALE" package="AISG_C091-C006-804-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-PAD" pin="1" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FEMALE" package="AISG_C091-G006-804-2">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
<connect gate="-3" pin="1" pad="3"/>
<connect gate="-4" pin="1" pad="4"/>
<connect gate="-5" pin="1" pad="5"/>
<connect gate="-6" pin="1" pad="6"/>
<connect gate="-7" pin="1" pad="7"/>
<connect gate="-8" pin="1" pad="8"/>
<connect gate="-PAD" pin="1" pad="PAD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER_ONSEMI_1SMC5.0AT3G" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="ONSEMI_1SMC5.0AT3G">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GDT_2035-XX-SM" prefix="F" uservalue="yes">
<gates>
<gate name="G$1" symbol="GDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2035-XX-SM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PANASONIC_EZJ-S2VB223" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="VARISTOR_ESD" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="RES2013X38N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVX_VC15MA0340KBA" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="VARISTOR_ESD" x="0" y="0"/>
</gates>
<devices>
<device name="2220" package="RES5750X230N">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.5MM_FIDUCIAL" prefix="F">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0.5MM_FIDUCIAL">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOLEX_503159-1200" prefix="J" uservalue="yes">
<gates>
<gate name="-1" symbol="PIN" x="5.08" y="0"/>
<gate name="-2" symbol="PIN" x="5.08" y="-2.54"/>
<gate name="-3" symbol="PIN" x="5.08" y="-5.08"/>
<gate name="-4" symbol="PIN" x="5.08" y="-7.62"/>
<gate name="-5" symbol="PIN" x="5.08" y="-10.16"/>
<gate name="-6" symbol="PIN" x="5.08" y="-12.7"/>
<gate name="-7" symbol="PIN" x="5.08" y="-15.24"/>
<gate name="-8" symbol="PIN" x="5.08" y="-17.78"/>
<gate name="-9" symbol="PIN" x="5.08" y="-20.32"/>
<gate name="-10" symbol="PIN" x="5.08" y="-22.86"/>
<gate name="-11" symbol="PIN" x="5.08" y="-25.4"/>
<gate name="-12" symbol="PIN" x="5.08" y="-27.94"/>
</gates>
<devices>
<device name="" package="MOLEX_5031591200_SD">
<connects>
<connect gate="-1" pin="1" pad="P$1"/>
<connect gate="-10" pin="1" pad="P$10"/>
<connect gate="-11" pin="1" pad="P$11"/>
<connect gate="-12" pin="1" pad="P$12"/>
<connect gate="-2" pin="1" pad="P$2"/>
<connect gate="-3" pin="1" pad="P$3"/>
<connect gate="-4" pin="1" pad="P$4"/>
<connect gate="-5" pin="1" pad="P$5"/>
<connect gate="-6" pin="1" pad="P$6"/>
<connect gate="-7" pin="1" pad="P$7"/>
<connect gate="-8" pin="1" pad="P$8"/>
<connect gate="-9" pin="1" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="AGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="AGND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AGND" prefix="AGND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VR1" symbol="AGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="A4L-LOC-JMA" device=""/>
<part name="J100" library="JMA_LBR" deviceset="AISG_" device="FEMALE" value="AISG_C091-G006-804-2(F)"/>
<part name="J101" library="JMA_LBR" deviceset="AISG_" device="MALE" value="AISG G091-G006-804-2(M)"/>
<part name="D8" library="JMA_LBR" deviceset="ZENER_ONSEMI_1SMC5.0AT3G" device="" value="1SMC33AT3G"/>
<part name="F3" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="F1" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="F2" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="F4" library="JMA_LBR" deviceset="GDT_2035-XX-SM" device="" value="2035-09-SM"/>
<part name="AGND2" library="supply1" deviceset="AGND" device=""/>
<part name="AGND3" library="supply1" deviceset="AGND" device=""/>
<part name="AGND4" library="supply1" deviceset="AGND" device=""/>
<part name="AGND5" library="supply1" deviceset="AGND" device=""/>
<part name="AGND6" library="supply1" deviceset="AGND" device=""/>
<part name="R31" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="R29" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="0"/>
<part name="C27" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="C28" library="JMA_LBR" deviceset="CAP_SMD_" device="0402" value="15pF"/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="R30" library="JMA_LBR" deviceset="RES_SMD_" device="0402" value="(DNP)"/>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="D10" library="JMA_LBR" deviceset="PANASONIC_EZJ-S2VB223" device="0805" value="EZJ-S2VB223"/>
<part name="D11" library="JMA_LBR" deviceset="PANASONIC_EZJ-S2VB223" device="0805" value="EZJ-S2VB223"/>
<part name="SUPPLY50" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY51" library="supply2" deviceset="GND" device=""/>
<part name="D3" library="JMA_LBR" deviceset="AVX_VC15MA0340KBA" device="2220" value="VC15MA0340KBA"/>
<part name="SUPPLY52" library="supply2" deviceset="GND" device=""/>
<part name="AGND7" library="supply1" deviceset="AGND" device=""/>
<part name="NC1" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC2" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC3" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC7" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC8" library="JMA_LBR" deviceset="NC" device=""/>
<part name="NC9" library="JMA_LBR" deviceset="NC" device=""/>
<part name="FD1" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="FD2" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="FD3" library="JMA_LBR" deviceset="0.5MM_FIDUCIAL" device=""/>
<part name="J2" library="JMA_LBR" deviceset="MOLEX_503159-1200" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="66.04" y1="78.74" x2="48.26" y2="78.74" width="0.1524" layer="93"/>
<text x="25.4" y="139.7" size="1.27" layer="97" align="bottom-right">A</text>
<text x="25.4" y="149.86" size="1.27" layer="97" align="bottom-right">B</text>
<text x="25.4" y="129.54" size="1.27" layer="97" align="bottom-right">DC RETURN</text>
<text x="25.4" y="134.62" size="1.27" layer="97" align="bottom-right">10-30V</text>
<text x="25.4" y="160.02" size="1.27" layer="97" align="bottom-right">+12V</text>
<text x="25.4" y="144.78" size="1.27" layer="97" align="bottom-right">GND</text>
<text x="25.4" y="119.38" size="1.27" layer="97" align="bottom-right">PAD</text>
<text x="25.4" y="88.9" size="1.27" layer="97" align="bottom-right">A</text>
<text x="25.4" y="99.06" size="1.27" layer="97" align="bottom-right">B</text>
<text x="25.4" y="78.74" size="1.27" layer="97" align="bottom-right">DC RETURN</text>
<text x="25.4" y="83.82" size="1.27" layer="97" align="bottom-right">10-30V</text>
<text x="25.4" y="109.22" size="1.27" layer="97" align="bottom-right">+12V</text>
<text x="25.4" y="93.98" size="1.27" layer="97" align="bottom-right">GND</text>
<text x="25.4" y="68.58" size="1.27" layer="97" align="bottom-right">PAD</text>
<wire x1="7.62" y1="165.1" x2="7.62" y2="60.96" width="0.1524" layer="97" style="shortdash"/>
<wire x1="7.62" y1="60.96" x2="10.16" y2="58.42" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="58.42" x2="137.16" y2="58.42" width="0.1524" layer="97" style="shortdash"/>
<wire x1="137.16" y1="58.42" x2="139.7" y2="60.96" width="0.1524" layer="97" style="shortdash"/>
<wire x1="139.7" y1="60.96" x2="139.7" y2="165.1" width="0.1524" layer="97" style="shortdash"/>
<wire x1="139.7" y1="165.1" x2="137.16" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="137.16" y1="167.64" x2="10.16" y2="167.64" width="0.1524" layer="97" style="shortdash"/>
<wire x1="10.16" y1="167.64" x2="7.62" y2="165.1" width="0.1524" layer="97" style="shortdash"/>
<text x="10.16" y="165.1" size="1.27" layer="97">AISG CONNECTOR INTERFACE + SURGE PROTECTION</text>
<text x="25.4" y="154.94" size="1.27" layer="97" align="bottom-right">-48V</text>
<text x="25.4" y="124.46" size="1.27" layer="97" align="bottom-right">NC</text>
<text x="25.4" y="73.66" size="1.27" layer="97" align="bottom-right">NC</text>
<text x="25.4" y="104.14" size="1.27" layer="97" align="bottom-right">-48V</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="1.27" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="1.27" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="1.27" layer="94"/>
</instance>
<instance part="FD1" gate="G$1" x="200.66" y="53.34" smashed="yes">
<attribute name="NAME" x="198.374" y="62.23" size="1.778" layer="125"/>
</instance>
<instance part="FD2" gate="G$1" x="220.98" y="53.34" smashed="yes">
<attribute name="NAME" x="218.694" y="62.23" size="1.778" layer="125"/>
</instance>
<instance part="FD3" gate="G$1" x="241.3" y="53.34" smashed="yes">
<attribute name="NAME" x="239.014" y="62.23" size="1.778" layer="125"/>
</instance>
<instance part="J100" gate="-1" x="30.48" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.782" y="109.728" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-PAD" x="30.48" y="68.58" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.782" y="69.088" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-3" x="30.48" y="99.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.782" y="99.568" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-4" x="30.48" y="93.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.782" y="94.488" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-5" x="30.48" y="88.9" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.782" y="89.408" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-6" x="30.48" y="83.82" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.782" y="84.328" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J100" gate="-7" x="30.48" y="78.74" smashed="yes" rot="MR0">
<attribute name="NAME" x="33.782" y="79.248" size="1.27" layer="95" ratio="15" rot="MR0"/>
</instance>
<instance part="J101" gate="-1" x="30.48" y="160.02" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="159.512" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-PAD" x="30.48" y="119.38" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="118.872" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-3" x="30.48" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="149.352" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-4" x="30.48" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="144.272" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-5" x="30.48" y="139.7" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="139.192" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-6" x="30.48" y="134.62" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="134.112" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="J101" gate="-7" x="30.48" y="129.54" smashed="yes" rot="R180">
<attribute name="NAME" x="33.782" y="129.032" size="1.27" layer="95" ratio="15" rot="R180"/>
</instance>
<instance part="D8" gate="G$1" x="60.96" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="63.5" y="107.315" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="58.42" y="106.68" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="F3" gate="G$1" x="68.58" y="111.76" smashed="yes">
<attribute name="NAME" x="70.485" y="106.045" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="66.04" y="106.68" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F1" gate="G$1" x="76.2" y="111.76" smashed="yes">
<attribute name="NAME" x="78.74" y="106.68" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="73.66" y="106.68" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F2" gate="G$1" x="83.82" y="111.76" smashed="yes">
<attribute name="NAME" x="85.725" y="106.045" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="81.28" y="106.68" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="F4" gate="G$1" x="91.44" y="111.76" smashed="yes">
<attribute name="NAME" x="93.345" y="106.68" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.9" y="106.68" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="J101" gate="G$1" x="15.24" y="129.54" smashed="yes" rot="R90">
<attribute name="VALUE" x="15.24" y="134.62" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="J100" gate="G$1" x="15.24" y="83.82" smashed="yes" rot="R90">
<attribute name="VALUE" x="15.24" y="91.44" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND2" gate="VR1" x="60.96" y="99.06" smashed="yes">
<attribute name="VALUE" x="58.42" y="93.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND3" gate="VR1" x="68.58" y="99.06" smashed="yes">
<attribute name="VALUE" x="66.04" y="93.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND4" gate="VR1" x="76.2" y="99.06" smashed="yes">
<attribute name="VALUE" x="73.66" y="93.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND5" gate="VR1" x="83.82" y="99.06" smashed="yes">
<attribute name="VALUE" x="81.28" y="93.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND6" gate="VR1" x="91.44" y="99.06" smashed="yes">
<attribute name="VALUE" x="88.9" y="93.98" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R31" gate="G$1" x="104.14" y="149.86" smashed="yes">
<attribute name="NAME" x="98.933" y="150.7236" size="1.27" layer="95"/>
<attribute name="VALUE" x="107.061" y="150.368" size="1.27" layer="96"/>
</instance>
<instance part="R29" gate="G$1" x="104.14" y="139.7" smashed="yes">
<attribute name="NAME" x="98.933" y="140.5636" size="1.27" layer="95"/>
<attribute name="VALUE" x="107.061" y="140.208" size="1.27" layer="96"/>
</instance>
<instance part="C27" gate="G$1" x="111.76" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="112.395" y="128.905" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="112.395" y="122.555" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="C28" gate="G$1" x="116.84" y="124.46" smashed="yes" rot="R90">
<attribute name="NAME" x="117.475" y="128.905" size="1.27" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="117.475" y="122.555" size="1.27" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="111.76" y="111.76" smashed="yes">
<attribute name="VALUE" x="109.855" y="108.585" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY20" gate="GND" x="116.84" y="111.76" smashed="yes">
<attribute name="VALUE" x="114.935" y="108.585" size="1.27" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="104.14" y="144.78" smashed="yes">
<attribute name="NAME" x="98.933" y="145.6436" size="1.27" layer="95"/>
<attribute name="VALUE" x="107.061" y="145.288" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY49" gate="GND" x="45.72" y="63.5" smashed="yes">
<attribute name="VALUE" x="43.815" y="60.325" size="1.27" layer="96"/>
</instance>
<instance part="D10" gate="G$1" x="99.06" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="100.838" y="105.156" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="96.52" y="106.68" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="D11" gate="G$1" x="106.68" y="111.76" smashed="yes" rot="R90">
<attribute name="NAME" x="108.712" y="104.902" size="1.27" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="104.14" y="106.68" size="1.27" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="SUPPLY50" gate="GND" x="99.06" y="99.06" smashed="yes">
<attribute name="VALUE" x="97.155" y="95.885" size="1.27" layer="96"/>
</instance>
<instance part="SUPPLY51" gate="GND" x="106.68" y="99.06" smashed="yes">
<attribute name="VALUE" x="104.775" y="95.885" size="1.27" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="66.04" y="144.78" smashed="yes" rot="R180">
<attribute name="NAME" x="69.342" y="142.24" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="58.42" y="147.32" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="SUPPLY52" gate="GND" x="78.74" y="142.24" smashed="yes">
<attribute name="VALUE" x="81.915" y="142.875" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="AGND7" gate="VR1" x="40.64" y="63.5" smashed="yes">
<attribute name="VALUE" x="35.56" y="63.5" size="1.27" layer="96"/>
</instance>
<instance part="J101" gate="-2" x="30.48" y="154.94" rot="R180"/>
<instance part="J101" gate="-8" x="30.48" y="124.46" rot="R180"/>
<instance part="J100" gate="-2" x="30.48" y="104.14" rot="R180"/>
<instance part="J100" gate="-8" x="30.48" y="73.66" rot="R180"/>
<instance part="NC1" gate="G$1" x="38.1" y="154.94"/>
<instance part="NC2" gate="G$1" x="38.1" y="144.78"/>
<instance part="NC3" gate="G$1" x="38.1" y="124.46"/>
<instance part="NC7" gate="G$1" x="38.1" y="104.14"/>
<instance part="NC8" gate="G$1" x="38.1" y="93.98"/>
<instance part="NC9" gate="G$1" x="38.1" y="73.66"/>
<instance part="J2" gate="-1" x="129.54" y="91.44"/>
<instance part="J2" gate="-2" x="129.54" y="88.9"/>
<instance part="J2" gate="-3" x="129.54" y="86.36"/>
<instance part="J2" gate="-4" x="129.54" y="83.82"/>
<instance part="J2" gate="-5" x="129.54" y="81.28"/>
<instance part="J2" gate="-6" x="129.54" y="78.74"/>
<instance part="J2" gate="-7" x="129.54" y="76.2"/>
<instance part="J2" gate="-8" x="129.54" y="73.66"/>
<instance part="J2" gate="-9" x="129.54" y="71.12"/>
<instance part="J2" gate="-10" x="129.54" y="68.58"/>
<instance part="J2" gate="-11" x="129.54" y="66.04"/>
<instance part="J2" gate="-12" x="129.54" y="63.5"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="111.76" y1="119.38" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="1"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<wire x1="116.84" y1="119.38" x2="116.84" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J101" gate="-7" pin="1"/>
<wire x1="35.56" y1="129.54" x2="45.72" y2="129.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="129.54" x2="45.72" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J100" gate="-7" pin="1"/>
<wire x1="45.72" y1="78.74" x2="35.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="45.72" y1="78.74" x2="45.72" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<junction x="45.72" y="78.74"/>
</segment>
<segment>
<pinref part="D10" gate="G$1" pin="1"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
<wire x1="99.06" y1="104.14" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="1"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
<wire x1="106.68" y1="104.14" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="1"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
<wire x1="73.66" y1="144.78" x2="78.74" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="-6" pin="1"/>
<wire x1="124.46" y1="78.74" x2="121.92" y2="78.74" width="0.1524" layer="91"/>
<wire x1="121.92" y1="78.74" x2="121.92" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="-5" pin="1"/>
<wire x1="121.92" y1="81.28" x2="121.92" y2="83.82" width="0.1524" layer="91"/>
<wire x1="124.46" y1="81.28" x2="121.92" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J2" gate="-4" pin="1"/>
<wire x1="124.46" y1="83.82" x2="121.92" y2="83.82" width="0.1524" layer="91"/>
<label x="114.3" y="83.82" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="121.92" y="83.82"/>
<wire x1="121.92" y1="83.82" x2="114.3" y2="83.82" width="0.1524" layer="91"/>
<junction x="121.92" y="81.28"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="J101" gate="-1" pin="1"/>
<wire x1="35.56" y1="160.02" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
<pinref part="J100" gate="-1" pin="1"/>
<wire x1="35.56" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<wire x1="48.26" y1="109.22" x2="48.26" y2="160.02" width="0.1524" layer="91"/>
<junction x="48.26" y="160.02"/>
<pinref part="F4" gate="G$1" pin="1"/>
<wire x1="48.26" y1="160.02" x2="91.44" y2="160.02" width="0.1524" layer="91"/>
<wire x1="91.44" y1="160.02" x2="91.44" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="+"/>
<pinref part="AGND2" gate="VR1" pin="AGND"/>
<wire x1="60.96" y1="106.68" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F3" gate="G$1" pin="2"/>
<pinref part="AGND3" gate="VR1" pin="AGND"/>
<wire x1="68.58" y1="106.68" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F1" gate="G$1" pin="2"/>
<pinref part="AGND4" gate="VR1" pin="AGND"/>
<wire x1="76.2" y1="106.68" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F2" gate="G$1" pin="2"/>
<pinref part="AGND5" gate="VR1" pin="AGND"/>
<wire x1="83.82" y1="106.68" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="F4" gate="G$1" pin="2"/>
<pinref part="AGND6" gate="VR1" pin="AGND"/>
<wire x1="91.44" y1="106.68" x2="91.44" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J101" gate="-PAD" pin="1"/>
<wire x1="35.56" y1="119.38" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J100" gate="-PAD" pin="1"/>
<wire x1="35.56" y1="68.58" x2="40.64" y2="68.58" width="0.1524" layer="91"/>
<wire x1="40.64" y1="119.38" x2="40.64" y2="68.58" width="0.1524" layer="91"/>
<wire x1="40.64" y1="68.58" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<pinref part="AGND7" gate="VR1" pin="AGND"/>
<junction x="40.64" y="68.58"/>
</segment>
</net>
<net name="AISG_P" class="0">
<segment>
<pinref part="J101" gate="-3" pin="1"/>
<wire x1="35.56" y1="149.86" x2="50.8" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J100" gate="-3" pin="1"/>
<wire x1="35.56" y1="99.06" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
<wire x1="50.8" y1="99.06" x2="50.8" y2="149.86" width="0.1524" layer="91"/>
<junction x="50.8" y="149.86"/>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="149.86" x2="83.82" y2="149.86" width="0.1524" layer="91"/>
<wire x1="83.82" y1="149.86" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="129.54" x2="83.82" y2="116.84" width="0.1524" layer="91"/>
<wire x1="83.82" y1="149.86" x2="99.06" y2="149.86" width="0.1524" layer="91"/>
<junction x="83.82" y="149.86"/>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="D11" gate="G$1" pin="2"/>
<wire x1="106.68" y1="119.38" x2="106.68" y2="129.54" width="0.1524" layer="91"/>
<wire x1="106.68" y1="129.54" x2="83.82" y2="129.54" width="0.1524" layer="91"/>
<junction x="83.82" y="129.54"/>
</segment>
</net>
<net name="AISG_N" class="0">
<segment>
<pinref part="J101" gate="-5" pin="1"/>
<wire x1="35.56" y1="139.7" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
<pinref part="J100" gate="-5" pin="1"/>
<wire x1="35.56" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
<junction x="53.34" y="139.7"/>
<pinref part="F1" gate="G$1" pin="1"/>
<wire x1="53.34" y1="139.7" x2="76.2" y2="139.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="139.7" x2="76.2" y2="124.46" width="0.1524" layer="91"/>
<wire x1="76.2" y1="124.46" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<wire x1="76.2" y1="139.7" x2="96.52" y2="139.7" width="0.1524" layer="91"/>
<junction x="76.2" y="139.7"/>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="96.52" y1="139.7" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<wire x1="99.06" y1="144.78" x2="96.52" y2="144.78" width="0.1524" layer="91"/>
<wire x1="96.52" y1="144.78" x2="96.52" y2="139.7" width="0.1524" layer="91"/>
<junction x="96.52" y="139.7"/>
<pinref part="D10" gate="G$1" pin="2"/>
<wire x1="99.06" y1="119.38" x2="99.06" y2="124.46" width="0.1524" layer="91"/>
<wire x1="99.06" y1="124.46" x2="76.2" y2="124.46" width="0.1524" layer="91"/>
<junction x="76.2" y="124.46"/>
</segment>
</net>
<net name="10-30VIN" class="0">
<segment>
<pinref part="J100" gate="-6" pin="1"/>
<wire x1="35.56" y1="83.82" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
<wire x1="55.88" y1="83.82" x2="55.88" y2="134.62" width="0.1524" layer="91"/>
<pinref part="J101" gate="-6" pin="1"/>
<wire x1="35.56" y1="134.62" x2="55.88" y2="134.62" width="0.1524" layer="91"/>
<wire x1="55.88" y1="134.62" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<junction x="55.88" y="134.62"/>
<pinref part="D8" gate="G$1" pin="-"/>
<wire x1="58.42" y1="134.62" x2="60.96" y2="134.62" width="0.1524" layer="91"/>
<wire x1="60.96" y1="134.62" x2="60.96" y2="116.84" width="0.1524" layer="91"/>
<pinref part="F3" gate="G$1" pin="1"/>
<wire x1="60.96" y1="134.62" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
<wire x1="68.58" y1="134.62" x2="68.58" y2="116.84" width="0.1524" layer="91"/>
<wire x1="68.58" y1="134.62" x2="119.38" y2="134.62" width="0.1524" layer="91"/>
<junction x="60.96" y="134.62"/>
<junction x="68.58" y="134.62"/>
<label x="119.38" y="134.62" size="1.27" layer="95" xref="yes"/>
<pinref part="D3" gate="G$1" pin="2"/>
<wire x1="58.42" y1="144.78" x2="58.42" y2="134.62" width="0.1524" layer="91"/>
<junction x="58.42" y="134.62"/>
</segment>
<segment>
<pinref part="J2" gate="-3" pin="1"/>
<wire x1="124.46" y1="86.36" x2="121.92" y2="86.36" width="0.1524" layer="91"/>
<wire x1="121.92" y1="86.36" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="121.92" y1="88.9" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<wire x1="121.92" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<pinref part="J2" gate="-1" pin="1"/>
<wire x1="124.46" y1="91.44" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<pinref part="J2" gate="-2" pin="1"/>
<wire x1="124.46" y1="88.9" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<label x="114.3" y="91.44" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="121.92" y="91.44"/>
<junction x="121.92" y="88.9"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="J100" gate="-8" pin="1"/>
<wire x1="35.56" y1="73.66" x2="38.1" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="J100" gate="-4" pin="1"/>
<wire x1="35.56" y1="93.98" x2="38.1" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="J100" gate="-2" pin="1"/>
<wire x1="35.56" y1="104.14" x2="38.1" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="J101" gate="-8" pin="1"/>
<wire x1="35.56" y1="124.46" x2="38.1" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="J101" gate="-4" pin="1"/>
<wire x1="35.56" y1="144.78" x2="38.1" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="J101" gate="-2" pin="1"/>
<wire x1="35.56" y1="154.94" x2="38.1" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IN_N" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="109.22" y1="149.86" x2="111.76" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="111.76" y1="149.86" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<wire x1="116.84" y1="149.86" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
<wire x1="116.84" y1="129.54" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<label x="119.38" y="149.86" size="1.27" layer="95" xref="yes"/>
<junction x="116.84" y="149.86"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="109.22" y1="144.78" x2="111.76" y2="144.78" width="0.1524" layer="91"/>
<wire x1="111.76" y1="144.78" x2="111.76" y2="149.86" width="0.1524" layer="91"/>
<junction x="111.76" y="149.86"/>
</segment>
<segment>
<pinref part="J2" gate="-9" pin="1"/>
<wire x1="124.46" y1="71.12" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<wire x1="121.92" y1="71.12" x2="121.92" y2="73.66" width="0.1524" layer="91"/>
<pinref part="J2" gate="-7" pin="1"/>
<wire x1="121.92" y1="73.66" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<wire x1="124.46" y1="76.2" x2="121.92" y2="76.2" width="0.1524" layer="91"/>
<label x="114.3" y="76.2" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="121.92" y="76.2"/>
<wire x1="121.92" y1="76.2" x2="114.3" y2="76.2" width="0.1524" layer="91"/>
<pinref part="J2" gate="-8" pin="1"/>
<wire x1="124.46" y1="73.66" x2="121.92" y2="73.66" width="0.1524" layer="91"/>
<junction x="121.92" y="73.66"/>
</segment>
</net>
<net name="IN_P" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="109.22" y1="139.7" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="111.76" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="111.76" y1="129.54" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<label x="119.38" y="139.7" size="1.27" layer="95" xref="yes"/>
<junction x="111.76" y="139.7"/>
</segment>
<segment>
<pinref part="J2" gate="-12" pin="1"/>
<wire x1="124.46" y1="63.5" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="63.5" x2="121.92" y2="66.04" width="0.1524" layer="91"/>
<pinref part="J2" gate="-11" pin="1"/>
<wire x1="121.92" y1="66.04" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<wire x1="124.46" y1="66.04" x2="121.92" y2="66.04" width="0.1524" layer="91"/>
<pinref part="J2" gate="-10" pin="1"/>
<wire x1="124.46" y1="68.58" x2="121.92" y2="68.58" width="0.1524" layer="91"/>
<label x="114.3" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<junction x="121.92" y="68.58"/>
<wire x1="121.92" y1="68.58" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<junction x="121.92" y="66.04"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
