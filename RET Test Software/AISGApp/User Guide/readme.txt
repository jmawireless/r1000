THE UNIVERSITY OF SHEFFIELD
AISG APPLICATION GUI, operator delivery
Contains:
applicationUserGuide.doc	User Guide
3rdPartyInstalls\
- j2re-1_4_2-windows-i586.exe	Java Runtime Environment, version used 
				during development and testing
- javacomm20-win32.zip		Java Communications API version 2.0, win32.
AISGApp\
- aisgApp.jar			Java Executable Jar File.
- launcher.bat			Batch file to verify required directory 
				hierarchy and launch the application
- aisg.ico			Icon file for the application shortcut
- AISG Application GUI.lnk	Windows 32 shortcut to the application 
				launcher.
AISGApp\devDescriptions\
- templatefile_v01.device	AISG Application Device Description File 
				template

The files in AISGApp are preconfigured to be installed in C:\AISGApp, if 
the path is modified, the shortcut must be modified accordingly.  Refer 
to the user guide.  The files aisgApp.jar and launcher.bat must be placed 
in the same directory.  It is possible to modify the location by modifying 
the batch file, but not recommended.